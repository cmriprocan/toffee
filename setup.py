"""
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

NOTE: Modified from:
https://github.com/pybind/cmake_example/blob/master/conda.recipe/meta.yaml
"""
import glob
import logging
import multiprocessing as mp
import os
import platform
import re
import subprocess
import sys
from distutils.command.install_headers import install_headers
from distutils.version import LooseVersion

from setuptools import Extension, find_packages, setup
from setuptools.command.build_ext import build_ext
try:
    from sphinx.setup_command import BuildDoc as BaseBuildDoc
    sphinx_enabled = True
except ModuleNotFoundError:
    sphinx_enabled = False


version_path = 'version'


def _get_version(include_patch=True):
    _version = None
    # check if we are on CircleCI to extract version from the environment
    # configured in .circleci/config.yml
    if os.environ.get('CI', False):
        _version = os.environ.get('NEW_GIT_TAG', None)
    if _version is None:
        with open(version_path, 'r') as f:
            _version = f.readlines()[-1].split(' ')[-1].strip()
    if include_patch:
        return _version
    else:
        return '.'.join(_version.split('.')[:2])


name = 'toffee'
logger = logging.getLogger(name)
logger.handlers = list()
_ch = logging.StreamHandler(sys.stdout)
_fmt = '%(asctime)s [%(levelname)s] {%(filename)s:%(funcName)s:%(lineno)d} | %(message)s'
_ch.setFormatter(logging.Formatter(_fmt))
logger.addHandler(_ch)
logger.info('Running on CircleCI = %s', os.environ.get('CI', False))

pysrc_dir = 'pysrc'
version = _get_version(include_patch=False)
release = _get_version(include_patch=True)
packages = [p for p in find_packages(pysrc_dir) if not p.startswith('tests')]
package_dir = {'': pysrc_dir}

# Often, additional files need to be installed into a package. These files are often data that’s closely related to
# the package’s implementation, or text files containing documentation that might be of interest to programmers using
# the package. These files are called package data.
# Source: https://docs.python.org/3/distutils/setupscript.html
package_data = {
    name: [],
}

# The data_files option can be used to specify additional files needed by the module distribution: configuration files,
# message catalogs, data files, anything which doesn’t fit in the previous categories.
# Source: https://docs.python.org/3/distutils/setupscript.html
data_files = [
    'LICENSE',
]

entry_points = {
    'console_scripts': [
        'mzml_to_toffee = toffee.conversion:mzml_to_toffee',
        'toffee_to_mzml = toffee.conversion:toffee_to_mzml',
        'raw_sciex_data_to_toffee = toffee.conversion:raw_sciex_data_to_toffee',
        'requantify_pyprophet_tsv = toffee.requant:requantify_pyprophet_tsv',
        'requantify_pyprophet_sqlite = toffee.requant:requantify_pyprophet_sqlite',
    ],
}


class CMakeExtension(Extension):
    def __init__(self, name, sourcedir='', builddir='build'):
        Extension.__init__(self, name, sources=[])
        self.sourcedir = os.path.abspath(sourcedir)
        self.builddir = os.path.abspath(builddir)


class CMakeBuild(build_ext):
    def run(self):
        try:
            out = subprocess.check_output(['cmake', '--version'])
        except OSError:
            msg = 'CMake must be installed to build the following extensions: '
            msg += ', '.join(e.name for e in self.extensions)
            raise RuntimeError(msg)

        if platform.system() == 'Windows':
            cmake_version = LooseVersion(re.search(r'version\s*([\d.]+)', out.decode()).group(1))
            if cmake_version < '3.1.0':
                raise RuntimeError('CMake >= 3.1.0 is required on Windows')

        for ext in self.extensions:
            self.build_extension(ext)

    def build_extension(self, ext):

        extdir = os.path.abspath(os.path.dirname(self.get_ext_fullpath(ext.name)))
        self.build_temp = ext.builddir

        cmake_args = [
            '-DCMAKE_LIBRARY_OUTPUT_DIRECTORY=' + extdir,
            '-DPYTHON_EXECUTABLE=' + sys.executable,
        ]

        cfg = 'Debug' if self.debug else 'RelWithDebInfo'
        build_args = ['--config', cfg]

        env = os.environ.copy()
        env['CXXFLAGS'] = '{} -DVERSION_INFO=\\"{}\\"'.format(
            env.get('CXXFLAGS', ''),
            self.distribution.get_version(),
        )

        if platform.system() == 'Windows':
            cmake_args += ['-DCMAKE_LIBRARY_OUTPUT_DIRECTORY_{}={}'.format(cfg.upper(), extdir)]
            cmake_args += ['-DCMAKE_RUNTIME_OUTPUT_DIRECTORY={}'.format(extdir)]
            cmake_args += ['-DCMAKE_RUNTIME_OUTPUT_DIRECTORY_{}={}'.format(cfg.upper(), extdir)]
            cmake_args += ['-DCMAKE_ARCHIVE_OUTPUT_DIRECTORY={}'.format(extdir)]
            cmake_args += ['-DCMAKE_ARCHIVE_OUTPUT_DIRECTORY_{}={}'.format(cfg.upper(), extdir)]
            if sys.maxsize > 2 ** 32:
                cmake_args += ['-A', 'x64']
            build_args += ['--', '/m']
            env['CXXFLAGS'] = '{} -GL-'.format(
                env.get('CXXFLAGS', ''),
            )
        else:
            cmake_args += ['-DCMAKE_BUILD_TYPE=' + cfg]
            # Parallel build causes CircleCI to run out of memory, so set to only use one thread
            nprocs = 1  # if os.environ.get('CI', False) else max(1, mp.cpu_count() // 2)
            build_args += ['--', f'-j{nprocs}']
            if platform.system() == 'Linux':
                env['CXXFLAGS'] = '{} -Wl,-rpath=\'$ORIGIN\''.format(
                    env.get('CXXFLAGS', ''),
                )
            elif platform.system() == 'Darwin':
                env['CXXFLAGS'] = '{} -Wl,-rpath,\'@loader_path\''.format(
                    env.get('CXXFLAGS', ''),
                )
                sysroot = env.get('CONDA_BUILD_SYSROOT', '/opt/MacOSX13.3.sdk')
                logger.info('*' * 40)
                logger.info(sysroot)
                logger.info('*' * 40)
                if not os.path.isdir(sysroot):
                    url = 'https://docs.conda.io/projects/conda-build/en/latest/resources/compiler-tools.html#macos-sdk'
                    raise RuntimeError(f'{sysroot} does not exist. You need to follow the instructions here: {url}')
                env['CONDA_BUILD_SYSROOT'] = sysroot

        if not os.path.exists(self.build_temp):
            os.makedirs(self.build_temp)

        cmake_args = ['cmake', ext.sourcedir] + cmake_args
        logger.info('\n\ncmake args: %s\n\n', ' '.join(cmake_args))
        subprocess.check_call(cmake_args, cwd=self.build_temp, env=env)

        build_args = ['cmake', '--build', '.'] + build_args
        logger.info('\n\nBuild args: %s\n\n', ' '.join(build_args))
        subprocess.check_call(build_args, cwd=self.build_temp, env=env)


class InstallHeaders(install_headers):
    """
    Wrapper to ensure that the structure of the headers is respected
    See: https://stackoverflow.com/a/50114715/758811
    """
    SRC_DIR = os.path.join('src', 'toffee', '')

    @classmethod
    def header_files(cls):
        headers = sorted(glob.glob(cls.SRC_DIR + '**/*.h', recursive=True))
        headers.extend(sorted(glob.glob(cls.SRC_DIR + '**/*.hpp', recursive=True)))
        return headers

    def run(self):
        headers = self.distribution.headers or list()
        self.mkpath(self.install_dir)
        for header in headers:
            header_path = os.path.dirname(header[len(self.SRC_DIR):])
            dst = os.path.join(self.install_dir, header_path)
            self.mkpath(dst)
            (out, _) = self.copy_file(header, dst)
            self.outfiles.append(out)


cmdclass = {
    'build_ext': CMakeBuild,
    'install_headers': InstallHeaders,
}
command_options = dict()


if sphinx_enabled:
    class BuildDoc(BaseBuildDoc):
        def run(self):
            try:
                subprocess.check_call(['doxygen', 'Doxyfile'], cwd='docs')
            except OSError:
                raise RuntimeError('Doxygen must be installed to created documentation')
            super(BuildDoc, self).run()

    cmdclass['build_sphinx'] = BuildDoc
    command_options['build_sphinx'] = {
        'project': ('setup.py', name),
        'version': ('setup.py', version),
        'release': ('setup.py', release),
        'source_dir': ('setup.py', 'docs'),
        'build_dir': ('setup.py', 'docs/_build/sphinx'),
    }

logger.info('Building: %s v%s', name, version)

setup(
    name=name,
    version=release,
    author='ProCan Software Engineering',
    author_email='ProCanSofEngRobot@cmri.org.au',
    description='Efficient mass spectrometry file format',
    long_description='A file format and library for efficient access of Time of Flight mass spectrometry data',
    python_requires='>=3.10',
    ext_modules=[
        CMakeExtension('toffee.toffee_python'),
    ],
    headers=InstallHeaders.header_files(),
    cmdclass=cmdclass,
    command_options=command_options,
    packages=packages,
    package_dir=package_dir,
    package_data=package_data,
    data_files=data_files,
    include_package_data=True,
    zip_safe=False,
    entry_points=entry_points,
)
