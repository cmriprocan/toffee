"""
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import logging

import numpy as np

import toffee

from tqdm import tqdm

from . import util
from .log import set_stream_logger


class _ToffeeManiuplator():
    """
    A private base class for sharing common code for maniuplators
    """
    def __init__(
        self,
        show_progress_bar=True,
        debug=False,
    ):
        self.show_progress_bar = show_progress_bar
        self.debug = debug
        self.logger = set_stream_logger(
            name=__name__,
            level=logging.DEBUG if debug else logging.INFO,
        )

    @classmethod
    def _load_swath_run_and_ms1(cls, tof_fname):
        """
        Load the swath run, check that it has an appropriate version, and if so, load the MS1 window
        SwathMap and SwathMapSpectrumAccess
        """
        min_allowable_version = toffee.Version.combineFileFormatVersions(1, 1)
        swath_run = toffee.SwathRun(tof_fname)
        file_version = toffee.Version.combineFileFormatVersions(
            swath_run.formatMajorVersion(),
            swath_run.formatMinorVersion(),
        )
        if file_version < min_allowable_version:
            raise ValueError(
                f'File format is invalid: {file_version} < {min_allowable_version}',
            )
        ms1 = swath_run.loadSwathMap(toffee.ToffeeWriter.MS1_NAME)
        sa1 = swath_run.loadSwathMapInMemorySpectrumAccess(toffee.ToffeeWriter.MS1_NAME)
        return swath_run, ms1, sa1

    @classmethod
    def _load_isotopes(cls, df, n_isotopes, swath_run, lower_offset, upper_offset, filter_ms2_windows):
        """
        Convert the precursor and product dataframe into one that includes isotope offsets
        """
        if df is None:
            return None, None

        expected_cols = [
            'ModifiedPeptideSequence',
            'PrecursorCharge',
            'PrecursorMz',
            'ProductCharge',
            'ProductMz',
        ]
        err = []
        for c in expected_cols:
            if c not in df.columns:
                err.append(f'\t-- {c} missing from dataframe')
        if len(err) > 0:
            err = '\n'.join(err)
            raise ValueError(f'Error with input dataframe:\n{err}')

        if 'TransitionGroupId' not in df.columns:
            df['TransitionGroupId'] = df.ModifiedPeptideSequence + '_' + df.PrecursorCharge.map(str)
        if 'TransitionId' not in df.columns:
            df['TransitionId'] = df.TransitionGroupId.groupby(df.TransitionGroupId).cumcount().map(str)
            df['TransitionId'] = df.TransitionGroupId + '_' + df.TransitionId

        assert isinstance(swath_run, toffee.SwathRun)

        precursor_ms2_name_map = swath_run.mapPrecursorsToMS2Names(
            df.PrecursorMz.unique(),
            lower_offset,
            upper_offset,
        )
        df['ms2Name'] = df.PrecursorMz.map(precursor_ms2_name_map)
        df = df[~df.ms2Name.isna()]
        if len(filter_ms2_windows) > 0:
            df = df[df.ms2Name.isin(filter_ms2_windows)]

        precursors, products = util.calculate_isotope_mz(
            precursor_and_product_df=df,
            n_isotopes=n_isotopes,
        )
        return precursors, products


class Subsampler(_ToffeeManiuplator):
    """
    A class to subsample a toffee file to only include data at mass over charge values of specific peptides
    """

    def __init__(
        self,
        tof_fname,
        precursor_and_product_df,
        ppm_width=200,
        lower_offset=0.0,
        upper_offset=1.0,
        filter_ms2_windows=None,
        n_isotopes=4,
        show_progress_bar=True,
        debug=False,
    ):
        super().__init__(
            show_progress_bar=show_progress_bar,
            debug=debug,
        )
        self.swath_run, self.ms1_swath_map, self.ms1_spectrum_access = self._load_swath_run_and_ms1(tof_fname)

        # load peptides
        if filter_ms2_windows is None:
            filter_ms2_windows = []
        assert isinstance(filter_ms2_windows, (list, tuple))
        self.filter_ms2_windows = filter_ms2_windows
        self.precursors, self.products = self._load_isotopes(
            df=precursor_and_product_df,
            n_isotopes=n_isotopes,
            swath_run=self.swath_run,
            lower_offset=lower_offset,
            upper_offset=upper_offset,
            filter_ms2_windows=self.filter_ms2_windows,
        )

        # set up ancillarly objects
        self.ppm_width = ppm_width
        self.n_isotopes = n_isotopes

    def run(self, new_tof_fname):
        writer = toffee.ToffeeWriter(new_tof_fname, self.swath_run.imsType())
        writer.addHeaderMetadata(self.swath_run.header())

        # prepare the MS1 sparse matrix
        ms1_sparse_matrix = util.convert_swath_map_to_dok_matrix(self.ms1_swath_map, copy_intensities=False)

        for ms2_name in tqdm(sorted(self.precursors.ms2Name.unique().tolist()), disable=not self.show_progress_bar):
            # filter the library
            try:
                precursors = self.precursors[self.precursors.ms2Name == ms2_name]
                products = self.products[self.products.ms2Name == ms2_name]
            except KeyError:
                # this will happen if the window is not in the library
                continue
            assert precursors.shape[0] == precursors.IsotopeMz.unique().size
            assert products.shape[0] == products.IsotopeMz.unique().size

            # load the MS2 swath map
            ms2_swath_map = self.swath_run.loadSwathMap(ms2_name)
            ms2_spectrum_access = self.swath_run.loadSwathMapSpectrumAccess(ms2_name)
            ms2_sparse_matrix = util.convert_swath_map_to_dok_matrix(ms2_swath_map, copy_intensities=False)

            # collect the MS1 data
            for _, row in precursors.iterrows():
                self._add_to_global_matrix(
                    swath_map=self.ms1_swath_map,
                    mz_value=row.IsotopeMz,
                    ppm_width=self.ppm_width,
                    dok_matrix=ms1_sparse_matrix,
                )

            # collect the MS2 data
            for _, row in products.iterrows():
                self._add_to_global_matrix(
                    swath_map=ms2_swath_map,
                    mz_value=row.IsotopeMz,
                    ppm_width=self.ppm_width,
                    dok_matrix=ms2_sparse_matrix,
                )

            # save MS2 data
            ms2_pcl = util.to_point_cloud(
                swath_run=self.swath_run,
                swath_map=ms2_swath_map,
                swath_map_spectrum_access=ms2_spectrum_access,
                dok_matrix=ms2_sparse_matrix,
            )
            writer.addPointCloud(ms2_pcl)

        # save MS1 data
        ms1_pcl = util.to_point_cloud(
            swath_run=self.swath_run,
            swath_map=self.ms1_swath_map,
            swath_map_spectrum_access=self.ms1_spectrum_access,
            dok_matrix=ms1_sparse_matrix,
        )
        writer.addPointCloud(ms1_pcl)

    @classmethod
    def _add_to_global_matrix(cls, swath_map, mz_value, ppm_width, dok_matrix):
        """
        Take the swath map, extract data from it, and add to the global sparse matrix
        """
        mz_range = toffee.MassOverChargeRangeWithPPMFullWidth(mz_value, ppm_width)
        util.add_ion_data_to_dok_matrix(
            swath_map=swath_map,
            mz_transformer=swath_map.getMzTransformer(),
            dok_matrix=dok_matrix,
            mz_range=mz_range,
        )


class InSilicoDilutionConstructor(_ToffeeManiuplator):
    """
    A class to combine two toffee files, one is considered the 'background' and the other is
    a 'foreground' from which data of individual PSMs is extracted and added to the background.
    In taking this approach, we can artificially scale the data from the foreground by a dilution
    factor -- thus, giving the ability to create in-silico dilution series for testing
    quantification algorithm performance.
    """

    def __init__(
        self,
        background_tof_fname,
        foreground_tof_fname,
        precursor_and_product_df,
        irt_precursor_and_product_df=None,
        px_half_width=10,
        lower_offset=0.0,
        upper_offset=1.0,
        filter_ms2_windows=None,
        n_isotopes=4,
        show_progress_bar=True,
        debug=False,
    ):
        """
        Set up the class ready for constructing a dilution series. This is a slightly expensive operation,
        but it only needs to be done once per background and foreground file.

        :param background_tof_fname: path to the background toffee file
        :param foreground_tof_fname: path to the foreground toffee file
        :param precursor_and_product_df: a pandas DataFrame that describes the peptide queries that we wish
            to copy from the foreground toffee file. It should have the following columns:
                - 'ModifiedPeptideSequence'
                - 'PrecursorCharge'
                - 'PrecursorMz'
                - 'ProductCharge'
                - 'ProductMz'
                - 'ForegroundRetentionTime': The retention time in the foreground toffee file for this precursor
        :param irt_precursor_and_product_df: similar to precursor_and_product_df, however, describing the
            retention time peptide queries. If this is None, then the iRT peptides will not be added to the
            combined file
        :param px_half_width: the radius (in pixels) from which data should be extracted from the
            foreground toffee file. We assume that data should be collected from a circle centered
            at the ``m/z`` and ``InSilicoRT`` of the ion of interest.
        :param lower_offset: specifies the lower overlap of the MS2 windows of the toffee files (in Da)
        :param upper_offset: specifies the upper overlap of the MS2 windows of the toffee files (in Da)
        :param filter_ms2_windows: a list that defines a subset of windows to use
        :param n_isotopes: the number of isotopes that will be included
        """
        super().__init__(
            show_progress_bar=show_progress_bar,
            debug=debug,
        )

        # set up the swath runs
        self.background_tof_fname = background_tof_fname
        self.background, self.background_ms1, self.background_sa1 = self._load_swath_run_and_ms1(background_tof_fname)
        self.foreground_tof_fname = foreground_tof_fname
        if foreground_tof_fname == background_tof_fname:
            self.foreground = self.background
            self.foreground_ms1, self.foreground_sa1 = self.background_ms1, self.background_sa1
        else:
            self.foreground, self.foreground_ms1, self.foreground_sa1 = self._load_swath_run_and_ms1(
                foreground_tof_fname,
            )
            # ensure that the retention times are the same, otherwise we cannot add the data
            # using the assumptions in this class
            np.testing.assert_almost_equal(
                self.background_ms1.scanCycleTime(),
                self.foreground_ms1.scanCycleTime(),
            )
            background_n_scans = self.background_ms1.retentionTime().size
            foreground_n_scans = self.foreground_ms1.retentionTime().size
            if background_n_scans != foreground_n_scans:
                raise ValueError(
                    f'Retention time vectors do not match: {background_n_scans} != {foreground_n_scans}',
                )
            # ensure that we are adding the same IMS type files
            if self.background.imsType() != self.foreground.imsType():
                raise ValueError(
                    f'IMS type incompatibility: {self.background.imsType()} != {self.foreground.imsType()}',
                )
            # check MS2 settings are the same for both
            for w1, w2 in zip(self.background.ms2Windows(), self.foreground.ms2Windows()):
                if w1.name != w2.name:
                    raise ValueError(f'Window name mistmatch: {w1.name} != {w2.name}')
                if w1.lower != w2.lower:
                    raise ValueError(f'Window lower m/z mistmatch: {w1.lower} != {w2.lower}')
                if w1.upper != w2.upper:
                    raise ValueError(f'Window upper m/z mistmatch: {w1.upper} != {w2.upper}')

        # calculate the minimum allowable intensity. This is a property of the
        # mass analyser, so can be based off the MS1 scan
        intensity_freq_count = self.background_sa1.intensityFrequencyCount()
        self.min_intensity = min([k for k, _ in intensity_freq_count.items() if k != 0])

        # load peptides
        if filter_ms2_windows is None:
            filter_ms2_windows = []
        assert isinstance(filter_ms2_windows, (list, tuple))
        self.filter_ms2_windows = filter_ms2_windows
        isotope_kwargs = dict(
            n_isotopes=n_isotopes,
            swath_run=self.background,
            lower_offset=lower_offset,
            upper_offset=upper_offset,
            filter_ms2_windows=self.filter_ms2_windows,
        )
        if 'ForegroundRetentionTime' not in precursor_and_product_df.columns:
            raise ValueError('ForegroundRetentionTime not in dataframe')
        self.precursors, self.products = self._load_isotopes(
            df=precursor_and_product_df,
            **isotope_kwargs,
        )
        if (
            irt_precursor_and_product_df is not None
            and 'ForegroundRetentionTime' not in irt_precursor_and_product_df.columns  # noqa
        ):
            raise ValueError('ForegroundRetentionTime not in dataframe')
        self.irt_precursors, self.irt_products = self._load_isotopes(
            df=irt_precursor_and_product_df,
            **isotope_kwargs,
        )

        # set up ancillarly objects
        self.px_half_width = px_half_width
        self.n_isotopes = n_isotopes

    def run(self, new_tof_fname, dilution):
        """
        Generate a new toffee file that first of all copies the background toffee file
        and then adds data from the foreground for each PSM that was specified in the
        constructor.

        :param new_tof_fname: path to the new toffee file. If a file already exists, it is overwritten
        :param dilution: The dilution to use, such that foreground data is scaled by ``1 / dilution``
        """
        if dilution <= 0:
            raise ValueError('Dilution must be greater than zero, you asked for {}'.format(dilution))

        new_ms1_dok = util.convert_swath_map_to_dok_matrix(self.background_ms1)
        background_ms1_mz_transformer = self.background_ms1.getMzTransformer()

        # create the toffee writer so we can write as we go
        tof_writer = toffee.ToffeeWriter(new_tof_fname, self.background.imsType())
        tof_writer.addHeaderMetadata(self.background.header())

        # create masks that apply the dilution
        unit_intensity_mask = self._build_circular_psm_mask(self.px_half_width, dilution=1)
        dilution_intensity_mask = self._build_circular_psm_mask(self.px_half_width, dilution=dilution)

        # loop through each of the MS2 windows
        for window_desc in tqdm(self.background.ms2Windows(), disable=not self.show_progress_bar):
            window = window_desc.name
            if len(self.filter_ms2_windows) > 0 and window not in self.filter_ms2_windows:
                continue

            background_ms2 = self.background.loadSwathMap(window)
            background_sa2 = self.background.loadSwathMapSpectrumAccess(window)
            new_ms2_dok = util.convert_swath_map_to_dok_matrix(background_ms2)
            background_ms2_mz_transformer = background_ms2.getMzTransformer()

            foreground_ms2 = self.foreground.loadSwathMap(window)

            # most of the arguments are shared
            kwargs = dict(
                window=window,
                background_ms1_mz_transformer=background_ms1_mz_transformer,
                background_ms2_mz_transformer=background_ms2_mz_transformer,
                new_ms1_dok=new_ms1_dok,
                new_ms2_dok=new_ms2_dok,
                foreground_ms1=self.foreground_ms1,
                foreground_ms2=foreground_ms2,
                px_half_width=self.px_half_width,
                min_intensity=self.min_intensity,
                n_isotopes=self.n_isotopes,
            )

            # add iRT PSMs
            self._add_fragments(
                precursors=self.irt_precursors,
                products=self.irt_products,
                intensity_mask=unit_intensity_mask,  # don't dilute iRT peptides
                **kwargs,
            )

            # add real PSMs
            self._add_fragments(
                precursors=self.precursors,
                products=self.products,
                intensity_mask=dilution_intensity_mask,
                **kwargs,
            )

            # save MS2 to toffee file
            pcl = util.to_point_cloud(
                swath_run=self.background,
                swath_map=background_ms2,
                swath_map_spectrum_access=background_sa2,
                dok_matrix=new_ms2_dok,
            )
            tof_writer.addPointCloud(pcl)

        # save MS1 to toffee file
        pcl = util.to_point_cloud(
            swath_run=self.background,
            swath_map=self.background_ms1,
            swath_map_spectrum_access=self.background_sa1,
            dok_matrix=new_ms1_dok,
        )
        tof_writer.addPointCloud(pcl)

    @classmethod
    def _build_circular_psm_mask(cls, px_half_width, dilution=1):
        """
        Build a mask with ``1 / dilution`` inside the circle of radius ``px_half_width``,
        and zero outside
        """
        assert px_half_width > 0
        px_width = 2 * px_half_width + 1
        dists = np.arange(-px_half_width, px_half_width + 1) ** 2
        dists = np.sqrt(dists[:, None] + dists)
        psm_mask = (dists <= px_half_width).astype(float)
        assert psm_mask.shape == (px_width, px_width)
        if dilution != 1:
            psm_mask /= dilution
        return psm_mask

    @classmethod
    def _add_fragments(
        cls,
        precursors,
        products,
        window,
        background_ms1_mz_transformer,
        background_ms2_mz_transformer,
        new_ms1_dok,
        new_ms2_dok,
        foreground_ms1,
        foreground_ms2,
        px_half_width,
        intensity_mask,
        min_intensity,
        n_isotopes,
    ):
        """
        Add the ions from the specified spectral library to the DOK sparse matrix. This adds
        both MS1 and MS2 data to the appropriate parameters
        """
        if precursors is None or products is None:
            # for example if there were no iRT peptides
            return

        try:
            precursors = precursors[precursors.ms2Name == window]
            products = products[products.ms2Name == window]
        except KeyError:
            # this will happen if the window is not in the library
            return

        # add MS1 data
        for _, row in precursors.iterrows():
            cls._add_ion_data(
                row.IsotopeMz,
                row.ForegroundRetentionTime,
                background_ms1_mz_transformer,
                foreground_ms1,
                new_ms1_dok,
                px_half_width,
                intensity_mask,
                min_intensity,
            )

        # add MS2 data
        for _, row in products.iterrows():
            cls._add_ion_data(
                row.IsotopeMz,
                row.ForegroundRetentionTime,
                background_ms2_mz_transformer,
                foreground_ms2,
                new_ms2_dok,
                px_half_width,
                intensity_mask,
                min_intensity,
            )

    @classmethod
    def _add_ion_data(
        cls,
        mass_over_charge,
        retention_time,
        background_mz_transformer,
        swath_map,
        new_dok,
        px_half_width,
        intensity_mask,
        min_intensity,
    ):
        """
        Add the ion specified by ``mass_over_charge`` and ``retention_time`` from the ``swath_map``
        to the DOK sparse matrix. Data is scaled by ``1 / dilution`` and only added if it is greater
        or equal to the minimum allowable intensity (a property of the mass analyser, and extracted
        from the background toffee file).
        """
        mz_range = toffee.MassOverChargeRangeWithPixelHalfWidth(mass_over_charge, px_half_width)
        rt_range = toffee.RetentionTimeRangeWithPixelHalfWidth(retention_time, px_half_width)
        util.add_ion_data_to_dok_matrix(
            swath_map=swath_map,
            mz_transformer=background_mz_transformer,
            dok_matrix=new_dok,
            mz_range=mz_range,
            rt_range=rt_range,
            intensity_mask=intensity_mask,
            min_intensity=min_intensity,
        )
