"""
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import logging
import os

import numpy as np

import pandas as pd

import scipy
import scipy.sparse

import toffee

from .log import set_stream_logger


def calculate_isotope_mz(
    precursor_and_product_df,
    n_isotopes=4,
    drop_other_cols=False,
    sort_by_intensity=False,
    transition_group_id=None,
    isotope_mass_offset=None,
):
    """
    Calculates the isotope masses for both precursors and products in the spectral library
    by assuming that the mass offset is derived from the mass difference between Carbon12 and
    Carbon13

    :param pd.DataFrame precursor_and_product_df: a pandas DataFrame that describes the peptide queries
        that we wish to calculate isotope masses for. It should have (at least) the following columns:
            - 'TransitionGroupId'
            - 'PrecursorCharge'
            - 'PrecursorMz'
            - 'TransitionId'
            - 'ProductCharge'
            - 'ProductMz'
    :param int n_isotopes: a positive integer that determines how many isotope masses will be
        included in the output. Specifically, the returned data frames will include masses of
        `[ion_a, ion_a + 1 * offset_a, ..., ion_a + n_isotopes * offset_a,
        ion_b, ion_b + 1 * offse_b, ..., ion_b + n_isotopes * offset_b, ...]` where `offset_i` is
        related to the difference between the masses of Carbon12 and Carbon13 and the charge of
        `ion_i`.
    :param bool drop_other_cols: if True, other columns of the SpectralLibrary will be excluded
        from the returned data frames
    :param bool sort_by_intensity: if True, the results will be ordered by the LibraryIntensity
    :param str transition_group_id: the specific transition group that shall be used for calculating.
        If None, the full library will be returned.
    :param float isotope_mass_offset: The delta mass that should be used for calculating each isotope.
        If None is pass, we will use the default for the C12/C13 difference

    :return: a pair of :class:`pandas.DataFrame` representing the precusor and product isotope
        offsets. The dataframes have Id and Charge coloumns that match the data in the spectral
        library, as well as IsotopeNr and IsotopeMz that represent the number of the isotope (i.e.
        its distance from the monoisotopic m/z) and the isotope m/z, respectively.
    """
    assert n_isotopes >= 0
    intensity_col = 'LibraryIntensity'
    sort_by_intensity = sort_by_intensity and intensity_col in precursor_and_product_df.columns

    if isotope_mass_offset is None:
        # This is the mass difference between the two isotopes of Carbon (C12 and C13)
        isotope_mass_offset = 1.0033548
    if not isinstance(isotope_mass_offset, float) or isotope_mass_offset <= 0:
        raise ValueError(f'Invalid isotop mass offset: {isotope_mass_offset}')

    # assume that any ProductCharge that is NaN should be 1
    precursor_and_product_df = precursor_and_product_df.copy()
    precursor_and_product_df.loc[
        precursor_and_product_df.ProductCharge.isna(),
        'ProductCharge',
    ] = 1
    precursor_and_product_df['PrecursorCharge'] = precursor_and_product_df.PrecursorCharge.astype(int)
    precursor_and_product_df['ProductCharge'] = precursor_and_product_df.ProductCharge.astype(int)

    def _impl(id_col, mz_col, charge_col, drop_duplicates, grouping_col=None):
        # set up the data to build the isotopes
        cols = [id_col, mz_col, charge_col]
        if grouping_col is not None:
            cols.append(grouping_col)
        if sort_by_intensity:
            cols.append(intensity_col)
        if drop_other_cols:
            df = precursor_and_product_df.loc[:, cols].copy()
        else:
            df = precursor_and_product_df.copy()

        if transition_group_id is not None:
            df = df.loc[df.TransitionGroupId == transition_group_id].copy()

        if drop_duplicates:
            df = df.drop_duplicates(subset=[id_col, mz_col, charge_col])
        df['IsotopeNr'] = 0
        col_names = {id_col: 'Id', charge_col: 'Charge', mz_col: 'IsotopeMz'}
        if grouping_col is not None:
            col_names[grouping_col] = 'GroupId'
        df.rename(columns=col_names, inplace=True)

        # fill in the isotope number
        isotopes = []
        for i in range(n_isotopes + 1):
            df['IsotopeNr'] = i
            isotopes.append(df.copy())
        isotopes = pd.concat(isotopes)

        # calculate the m/z offsets for each isotope but assuming that it is offset
        # by a multiple of the mass difference
        isotopes['IsotopeMz'] += isotopes.IsotopeNr * isotope_mass_offset / isotopes.Charge

        # sort the result
        sort_cols = []
        ascending = []
        if grouping_col is None:
            sort_cols.append('Id')
            ascending.append(True)
        else:
            sort_cols.append('GroupId')
            ascending.append(True)
        if sort_by_intensity:
            sort_cols.append(intensity_col)
            ascending.append(False)
        if grouping_col is not None:
            sort_cols.append('Id')
            ascending.append(True)
        sort_cols.append('IsotopeNr')
        ascending.append(True)
        isotopes.sort_values(sort_cols, ascending=ascending, inplace=True)

        return isotopes

    # for the precursors, we need to drop duplicates, as we only want one
    # set per transition group
    precursor_rows = _impl(
        id_col='TransitionGroupId',
        mz_col='PrecursorMz',
        charge_col='PrecursorCharge',
        drop_duplicates=True,
    )

    # for the product ions, we assume that these are unique in the SpectralLibrary
    product_rows = _impl(
        id_col='TransitionId',
        mz_col='ProductMz',
        charge_col='ProductCharge',
        drop_duplicates=True,
        grouping_col='TransitionGroupId',
    )

    return precursor_rows, product_rows


def convert_swath_map_to_dok_matrix(swath_map, copy_intensities=True):
    """
    Generate a Dictionary of Keys type sparse matrix that spans the full range of the swath map
    """
    assert isinstance(swath_map, toffee.SwathMap)
    if copy_intensities:
        mz_transformer = swath_map.getMzTransformer()
        mz_range = toffee.MassOverChargeRangeIMSCoords(
            mz_transformer.lowerMzInIMSCoords(),
            mz_transformer.upperMzInIMSCoords(),
        )
        xic = swath_map.extractedIonChromatogramSparse(mz_range)
        np.testing.assert_allclose(xic.retentionTime, swath_map.retentionTime())
        np.testing.assert_allclose(xic.massOverCharge, swath_map.massOverCharge())
        dok_matrix = xic.intensities.todok()
    else:
        dok_matrix = scipy.sparse.dok_matrix(
            (swath_map.retentionTime().size, swath_map.massOverCharge().size),
            dtype=np.uint32,
        )
    return dok_matrix


def add_ion_data_to_dok_matrix(
    swath_map,
    mz_transformer,
    dok_matrix,
    mz_range=None,
    rt_range=None,
    intensity_mask=None,
    min_intensity=0,
):
    """
    Add the ion specified by ``mass_over_charge`` and ``retention_time`` from the ``swath_map``
    to the DOK sparse matrix. Data is scaled by ``1 / dilution`` and only added if it is greater
    or equal to the minimum allowable intensity (a property of the mass analyser, and extracted
    from the background toffee file).
    """
    assert isinstance(swath_map, toffee.SwathMap)
    assert isinstance(mz_transformer, toffee.MassOverChargeTransformer)
    assert isinstance(dok_matrix, scipy.sparse.dok_matrix)

    if mz_range is None:
        mz_range = toffee.MassOverChargeRangeIMSCoords(
            mz_transformer.lowerMzInIMSCoords(),
            mz_transformer.upperMzInIMSCoords(),
        )
    assert isinstance(mz_range, toffee.IMassOverChargeRange)

    extraction_args = [mz_range]
    if rt_range is not None:
        assert isinstance(rt_range, toffee.IRetentionTimeRange)
        lower_rt_idx = rt_range.lowerRTIdx(swath_map.retentionTime())
        extraction_args.append(rt_range)
        if intensity_mask is not None:
            extraction_func = swath_map.filteredExtractedIonChromatogram
        else:
            extraction_func = swath_map.filteredExtractedIonChromatogramSparse
    else:
        lower_rt_idx = 0
        if intensity_mask is not None:
            extraction_func = swath_map.extractedIonChromatogram
        else:
            extraction_func = swath_map.extractedIonChromatogramSparse

    xic = extraction_func(*extraction_args)

    lower_mz_idx = mz_transformer.toIMSCoords(xic.massOverCharge[0])
    lower_mz_idx -= mz_transformer.lowerMzInIMSCoords()

    # mask out the intensities
    if intensity_mask is not None:
        intensities = np.round(
            np.multiply(xic.intensities, intensity_mask),
        ).astype(np.uint32)
    else:
        intensities = xic.intensities

    # drop low intensities
    if min_intensity > 0:
        intensities[intensities < min_intensity] = 0

    # convert to DOK
    sparse_intensities = scipy.sparse.dok_matrix(intensities)

    # collect the sparse data from this chromatogram into the global one
    for k, v in sparse_intensities.items():
        rt_idx = k[0] + lower_rt_idx
        mz_idx = k[1] + lower_mz_idx
        dok_matrix[rt_idx, mz_idx] = max(v, dok_matrix[rt_idx, mz_idx])


def to_point_cloud(swath_run, swath_map, swath_map_spectrum_access, dok_matrix):
    """
    Convert the data into a form that can be saved to toffee
    """
    assert isinstance(swath_run, toffee.SwathRun)
    assert isinstance(swath_map, toffee.SwathMap)
    assert isinstance(swath_map_spectrum_access, toffee.SwathMapSpectrumAccessBase)
    assert isinstance(dok_matrix, scipy.sparse.dok_matrix)

    min_allowable_version = toffee.Version.combineFileFormatVersions(1, 1)
    file_version = toffee.Version.combineFileFormatVersions(
        swath_run.formatMajorVersion(),
        swath_run.formatMinorVersion(),
    )
    if file_version < min_allowable_version:
        raise ValueError(
            f'File format is invalid: {file_version} < {min_allowable_version}',
        )

    pcl = toffee.ScanDataPointCloud()
    pcl.name = swath_map.name()
    pcl.windowLower = swath_map.precursorLowerMz()
    pcl.windowCenter = swath_map.precursorCenterMz()
    pcl.windowUpper = swath_map.precursorUpperMz()
    pcl.scanCycleTime = swath_map.scanCycleTime()
    pcl.firstScanRetentionTimeOffset = swath_map.firstScanRetentionTimeOffset()

    sparse_matrix = dok_matrix.tocsr()
    sparse_matrix.sort_indices()
    pcl.setIntensityFromNumpy(sparse_matrix.data)
    pcl.imsProperties.sliceIndex = sparse_matrix.indptr[1:]

    rt = swath_map.retentionTime()
    num_scans = rt.shape[0]

    mz_transformer = swath_map.getMzTransformer()
    pcl.imsProperties.medianAlpha = mz_transformer.imsAlpha()
    pcl.imsProperties.medianBeta = mz_transformer.imsBeta()
    pcl.imsProperties.gamma = mz_transformer.imsGamma()

    # add the lower IMS coord offset back in
    sparse_matrix.indices += mz_transformer.lowerMzInIMSCoords()

    alpha = [0.0] * num_scans
    beta = [0.0] * num_scans
    for i in range(num_scans):
        scan_mz_transformer = swath_map_spectrum_access.getMzTransformer(i)
        alpha[i] = scan_mz_transformer.imsAlpha()
        beta[i] = scan_mz_transformer.imsBeta()

    pcl.imsProperties.alpha = alpha
    pcl.imsProperties.beta = beta

    pcl.imsProperties.setCoordinateFromNumpy(sparse_matrix.indices)

    return pcl


class ToffeeFragmentsDataExtractor():
    def __init__(
        self,
        tof_fname,
        use_ppm=False,
        extraction_width=15,
        use_ms1=True,
        debug=False,
    ):
        """
        A class the extracts XIC data from a toffee file and collates it in a way that is
        amenable to plotting.

        :param tof_fname str: path to the toffee file of interest
        :param use_ppm bool: If true extraction_width is given in ppm full width otherwise
            it's given in pixel half width
        :param extraction_width int: the width of the extracted XIC data that is centred around
            the relevent mass over charge value. If `use_ppm` is True, then this a full width,
            otherwise, it is a half width of pixels.
        :param use_ms1 bool: if true, include the MS1 data in the 2D XIC
        :param debug bool: if True, add addition debugging logs
        """
        self.use_ppm = use_ppm
        self.extraction_width = extraction_width

        assert os.path.isfile(tof_fname)
        self.tof_fname = tof_fname
        self.swath_run = toffee.SwathRun(tof_fname)
        self.use_ms1 = use_ms1
        if self.use_ms1:
            self.ms1_swath_map = self.swath_run.loadSwathMap(toffee.ToffeeWriter.MS1_NAME)
        else:
            self.ms1_swath_map = None

        self.debug = debug
        self.logger = set_stream_logger(
            name=__name__,
            level=logging.DEBUG if debug else logging.INFO,
        )

    def load_raw_data(
        self,
        ms2_swath_map,
        precursor_mz_list,
        product_mz_list,
        number_of_isotopes,
        log_heatmap,
        normalise_per_fragment,
        retention_time_zoom,
    ):
        """
        Load the raw data from the toffee file that corresponds to the specified
        precursor m/z and its corresponding fragment m/z values.

        :param ms2_swath_map: Allow the MS2 swath map to be pre-loaded for performance reasons
        :param precursor_mz_list list[float]: the m/z value of the precursor ion and its isotopes. The length
            of this list is enforced to be the `number_of_isotopes + 1` where the first entry is the
            monoisotopic ion
        :param product_mz_list list[float]: a list of the corresponding product (fragment) ion m/z values. The
            length of this list is enforced to be a multiple of the `number_of_isotopes + 1`.
        :param int number_of_isotopes: a positive integer that defines how many isotope masses are
            included in the mass lists. Specifically, the product m/z list will include masses of
            `[ion_a, ion_a + 1 * offset_a, ..., ion_a + n_isotopes * offset_a,
            ion_b, ion_b + 1 * offse_b, ..., ion_b + n_isotopes * offset_b, ...]` where `offset_i` is
            related to the difference between the masses of Carbon12 and Carbon13 and the charge of
            `ion_i`.
        :param log_heatmap: Apply log10 to the intensities
        :param normalise_per_fragment: Normalize heat map of each fragment so heat maps of the fragments
            are roughly on the same scale
        :param retention_time_zoom: a tuple that defines the retention time boundary for plotting. If None,
            the full experiment will be returned.

        :return: None if no data could be found in the toffee file, or a tuple of:
          - :class:`numpy.ndarray` a vertical stack of the 2D XICs for each fragment. The top
            entry in the stack corresponds to the MS1 data if this was enabled in the constructor. These
            are log10-scaled and normalised to have a maximum value of 1
          - list[int] of the height of each XIC so that the first entry can be sliced into constituent
            elements if required
          - list[str] of the labels of each XIC so that slices for each XIC can have associated text
          - :class:`numpy.ndarray` the summed 1D XIC corresponding the un-normalised 2D XIC for MS1
          - list[:class:`numpy.ndarray`] each entry is the summed 1D XIC corresponding the un-normalised
            2D XIC for each entry in the product_mz_list
          - :class:`numpy.ndarray` the retention time for the XICs
        """
        n_isotopes_including_mono = number_of_isotopes + 1
        if len(precursor_mz_list) != n_isotopes_including_mono:
            raise ValueError(
                'The precusor m/z list must have a length equal to the number of isotopes '
                'plus one (i.e. the monoisotopic m/z): '
                f'{len(precursor_mz_list)} != {n_isotopes_including_mono}'
            )
        if len(product_mz_list) % n_isotopes_including_mono != 0:
            raise ValueError(
                'The product m/z list must have a length that is a multiple of the number of '
                'isotopes plus one (i.e. the monoisotopic m/z): '
                f'{len(product_mz_list) % n_isotopes_including_mono} != 0'
            )

        rt = []
        xic_2d = []
        xic_2d_hline = []
        xic_2d_hline_label = []
        ms1_chrom = []
        chroms = []

        def _add_raw_data(swath_map, mz, log_heatmap, normalise_per_fragment, isotope_number):

            if self.use_ppm:
                mz_range = toffee.MassOverChargeRangeWithPPMFullWidth(mz, self.extraction_width)
            else:
                mz_range = toffee.MassOverChargeRangeWithPixelHalfWidth(mz, self.extraction_width)

            if retention_time_zoom is not None:
                rt_range = toffee.RetentionTimeRange(*retention_time_zoom)
                raw_data = swath_map.filteredExtractedIonChromatogram(mz_range, rt_range)
            else:
                raw_data = swath_map.extractedIonChromatogram(mz_range)

            intensities = raw_data.intensities.T.astype(np.float)
            if log_heatmap:
                intensities = np.log10(intensities + 1)
            if normalise_per_fragment:
                intensities /= max(1e-15, intensities.max())

            if len(rt) == 0:
                rt.append(raw_data.retentionTime)
            else:
                intensities = intensities[:, :rt[0].shape[0]]

            xic_2d.append(intensities)
            xic_2d_hline.append(intensities.shape[0])

            if isotope_number != 0:
                xic_2d_hline_label.append(f'+{isotope_number} iso')
            else:
                if swath_map.isMS1():
                    xic_2d_hline_label.append(f'MS1: {mz:.2F}')
                    ms1_chrom.append(raw_data.intensities.sum(axis=1))
                else:
                    xic_2d_hline_label.append(f'MS2: {mz:.2F}')
                    chroms.append(raw_data.intensities.sum(axis=1))

        if self.ms1_swath_map is not None:
            for idx, precursor_mz in enumerate(precursor_mz_list):
                _add_raw_data(
                    self.ms1_swath_map,
                    precursor_mz,
                    log_heatmap,
                    normalise_per_fragment,
                    idx % n_isotopes_including_mono,
                )
            ms1_chrom = ms1_chrom[0]
        else:
            ms1_chrom = None

        for idx, product_mz in enumerate(product_mz_list):
            _add_raw_data(
                ms2_swath_map,
                product_mz,
                log_heatmap,
                normalise_per_fragment,
                idx % n_isotopes_including_mono,
            )

        rt = rt[0]
        px_x = min(x.shape[1] for x in xic_2d)
        rt = rt[:px_x]
        if ms1_chrom is not None:
            ms1_chrom = ms1_chrom[:px_x]
        try:
            xic_2d = np.vstack([x[:, :px_x] for x in xic_2d])
        except ValueError:
            for x in xic_2d:
                self.logger.warn(x.shape)
            raise
        xic_2d_hline = np.cumsum([h for h in xic_2d_hline])
        assert xic_2d.shape[0] == xic_2d_hline[-1]

        return xic_2d, xic_2d_hline, xic_2d_hline_label, ms1_chrom, chroms, rt
