"""
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import gc
import logging
import os

import matplotlib

# This is new way to handle E402 noqa according to flake8
# https://gitlab.com/pycqa/flake8/-/issues/638
matplotlib.use('QT5Agg')  # noqa
import matplotlib.cm  # noqa
import matplotlib.pyplot as plt  # noqa
from matplotlib import gridspec  # noqa
from matplotlib.widgets import Button  # noqa

import numpy as np  # noqa

import pandas as pd  # noqa

import plotly.graph_objs as go  # noqa
import plotly.io as pio  # noqa

import toffee  # noqa

from tqdm import tqdm  # noqa

from . import util  # noqa
from .log import set_stream_logger  # noqa


def matplotlib_colourmap_to_plotly(name='plasma', ncolours=256):
    """
    See https://matplotlib.org/examples/color/colormaps_reference.html
    for a list of the available matplotlib colour maps, and the following
    https://plot.ly/python/matplotlib-colorscales/ plotly resource for
    examples of when you should use the ``rgb`` vs ``colorscale`` returned
    lists
    """
    max_range = max(1, (ncolours - 1))
    cmap = matplotlib.cm.get_cmap(name)
    norm = matplotlib.colors.Normalize(vmin=0, vmax=max_range)
    to_rgb = matplotlib.colors.colorConverter.to_rgb
    rgb = [to_rgb(cmap(norm(i))) for i in range(ncolours)]

    h = 1.0 / max_range
    colorscale = []
    for k in range(ncolours):
        idx = k * h
        c = map(np.uint8, np.array(cmap(idx)[:3]) * np.iinfo(np.uint8).max)
        colorscale.append([idx, 'rgb({}, {}, {})'.format(*c)])

    return rgb, colorscale


class ToffeeFragmentsPlotter():
    def __init__(
        self,
        tof_fname,
        use_ppm=False,
        extraction_width=15,
        lower_mz_overlap=0.0,
        upper_mz_overlap=1.0,
        use_ms1=True,
        matplotlib_colourmap='viridis',
        debug=False,
    ):
        """
        A class the extracts XIC data from a toffee file and collates it in a way that is
        amenable to plotting.

        :param tof_fname str: path to the toffee file of interest
        :param use_ppm bool: If true extraction_width is given in ppm full width otherwise
            it's given in pixel half width
        :param extraction_width int: the width of the extracted XIC data that is centred around
            the relevent mass over charge value. If `use_ppm` is True, then this a full width,
            otherwise, it is a half width of pixels.
        :param lower_mz_overlap float: the amount (in Da) that we should exclude from the
            lower bound of the ms2 windows when determining the correct window for a precursor
        :param upper_mz_overlap float: the amount (in Da) that we should exclude from the
            upper bound of the ms2 windows when determining the correct window for a precursor
        :param use_ms1 bool: if true, include the MS1 data in the 2D XIC
        :param matplotlib_colourmap str: the name of the matplotlib colour map to be used
        """
        self.extractor = util.ToffeeFragmentsDataExtractor(
            tof_fname=tof_fname,
            use_ppm=use_ppm,
            extraction_width=extraction_width,
            use_ms1=use_ms1,
            debug=debug,
        )
        self.lower_mz_overlap = lower_mz_overlap
        self.upper_mz_overlap = upper_mz_overlap

        assert os.path.isfile(tof_fname)
        self.tof_fname = tof_fname
        self.swath_run = toffee.SwathRun(tof_fname)
        self.use_ms1 = use_ms1
        if self.use_ms1:
            self.ms1_swath_map = self.swath_run.loadSwathMap(toffee.ToffeeWriter.MS1_NAME)
        else:
            self.ms1_swath_map = None

        self.matplotlib_colourmap = matplotlib_colourmap
        self.rgb_colourscale, self.colourscale = matplotlib_colourmap_to_plotly(
            name=matplotlib_colourmap,
        )
        self.debug = debug
        self.logger = set_stream_logger(
            name=__name__,
            level=logging.DEBUG if debug else logging.INFO,
        )

    def create_plotly_figure(
        self,
        precursor_mz_list,
        product_mz_list,
        psm_name,
        number_of_isotopes=0,
        log_heatmap=True,
        normalise_per_fragment=False,
        retention_time_zoom=None,
        retention_time_line=None,
        px_width=900,
        aspect_scale=1.0,
        fontsize=14,
        output_fname=None,
        rt_axis_in_minutes=False
    ):
        """
        Load the raw data from the toffee file that corresponds to the specified
        precursor m/z and its corresponding fragment m/z values and place it into a dict
        of traces that can be used by `plotly`.

        :param precursor_mz_list list[float]: the m/z value of the precursor ion and its isotopes. The length
            of this list is enforced to be the `number_of_isotopes + 1` where the first entry is the
            monoisotopic ion
        :param product_mz_list list[float]: a list of the corresponding product (fragment) ion m/z values. The
            length of this list is enforced to be a multiple of the `number_of_isotopes + 1`.
        :param psm_name str: name of the precursor ion that is used as a header in the final image
        :param int number_of_isotopes: a positive integer that defines how many isotope masses are
            included in the mass lists. Specifically, the product m/z list will include masses of
            `[ion_a, ion_a + 1 * offset_a, ..., ion_a + n_isotopes * offset_a,
            ion_b, ion_b + 1 * offse_b, ..., ion_b + n_isotopes * offset_b, ...]` where `offset_i` is
            related to the difference between the masses of Carbon12 and Carbon13 and the charge of
            `ion_i`.
        :param log_heatmap: Apply log10 to the intensities
        :param normalise_per_fragment: Normalize heat map of each fragment so heat maps of the fragments
            are roughly on the same scale
        :param retention_time_zoom: a tuple that defines the retention time boundary for plotting. If None,
            the full experiment will be returned.
        :param retention_time_line: if not None, a vertical line will be shown at this retention time. This can be
            useful for indicating the retention time identified by a DIA pipeline.
        :param pw_width: set the width of the image, the height will be set automatically such
            that each subplot is square
        :param aspect_scale: values great than 1 stretch the plot in the vertical direction, less than 1
            (but greater than zero) stretch the plot in the horizontal direction.
        :param fontsize: Size of the font on the axis labels
        :param output_fname: if not None, the plotly figure will be saved to this location. Note,
            if the directory of this path does not exist, this will fail.
        :param rt_axis_in_minutes: True x values transformed from seconds to minutes after all data is loaded.

        :return: None if no data could be found in the toffee file, or dict suitable to be used
        by `plotly` plotting functions. The dictionary will have two primary traces, the first is the
        heatmap of the raw toffee data, and the second is the plot of the 1D XIC chromatograms. These
        are linked through the x-axis (retention time) so that zooming will be consistent.
        """
        assert number_of_isotopes >= 0
        if not isinstance(precursor_mz_list, list):
            assert number_of_isotopes == 0
            precursor_mz_list = [precursor_mz_list]

        # find the correct MS2 map
        xic_2d, xic_2d_hline, xic_2d_hline_label, _, chroms, rt = self.load_raw_data(
            precursor_mz_list,
            product_mz_list,
            number_of_isotopes,
            log_heatmap,
            normalise_per_fragment,
            retention_time_zoom,
        )

        if rt_axis_in_minutes:
            rt = rt / 60

        # create plotly figure
        y_size, x_size = xic_2d.shape
        y_axis_index = list(range(1, y_size + 1))
        chrom_equivalent_size = 5  # size of the chrom plot is equal to this number of XICs
        y_axis_split = chrom_equivalent_size / (chrom_equivalent_size + len(xic_2d_hline))

        data = self._plotly_figure_data(xic_2d, xic_2d_hline, number_of_isotopes, chroms, rt, y_axis_index)
        layout = self._plotly_layout(rt, xic_2d_hline, xic_2d_hline_label, psm_name, fontsize, y_axis_split)

        if retention_time_line is not None:
            assert isinstance(retention_time_line, float)
            if rt_axis_in_minutes:
                retention_time_line = retention_time_line / 60
                layout['xaxis']['title'] = 'Retention time (m)'

            shapes = layout.get('shapes', [])
            shapes.append(
                {
                    'type': 'line',
                    'xref': 'x',
                    'yref': 'paper',
                    'x0': retention_time_line,
                    'x1': retention_time_line,
                    'y0': 0,
                    'y1': 1,
                    'line': {
                        'color': '#aaaaaa',
                        'width': 1,
                        'dash': 'dashdot',
                    },
                },
            )
            layout['shapes'] = shapes

        scale = aspect_scale * px_width / x_size
        px_height = int(scale * y_size) + px_width // 3
        layout.update(
            height=px_height,
            width=px_width,
        )

        figure = {
            'data': data,
            'layout': layout,
        }

        if output_fname is not None:
            self.logger.debug('Saving to %s', output_fname)
            pio.write_image(figure, output_fname)

        return figure

    def load_raw_data(
        self,
        precursor_mz_list,
        product_mz_list,
        number_of_isotopes,
        log_heatmap,
        normalise_per_fragment,
        retention_time_zoom,
        ms2_swath_map=None,
    ):
        """
        Load the raw data from the toffee file that corresponds to the specified
        precursor m/z and its corresponding fragment m/z values.

        :param precursor_mz_list list[float]: the m/z value of the precursor ion and its isotopes. The length
            of this list is enforced to be the `number_of_isotopes + 1` where the first entry is the
            monoisotopic ion
        :param product_mz_list list[float]: a list of the corresponding product (fragment) ion m/z values. The
            length of this list is enforced to be a multiple of the `number_of_isotopes + 1`.
        :param int number_of_isotopes: a positive integer that defines how many isotope masses are
            included in the mass lists. Specifically, the product m/z list will include masses of
            `[ion_a, ion_a + 1 * offset_a, ..., ion_a + n_isotopes * offset_a,
            ion_b, ion_b + 1 * offse_b, ..., ion_b + n_isotopes * offset_b, ...]` where `offset_i` is
            related to the difference between the masses of Carbon12 and Carbon13 and the charge of
            `ion_i`.
        :param log_heatmap: Apply log10 to the intensities
        :param normalise_per_fragment: Normalize heat map of each fragment so heat maps of the fragments
            are roughly on the same scale
        :param retention_time_zoom: a tuple that defines the retention time boundary for plotting. If None,
            the full experiment will be returned.
        :param ms2_swath_map: Allow the MS2 sath map to be pre-loaded for performance reasons. If this is
            None, it will be automatically loaded from the swath run

        :return: None if no data could be found in the toffee file, or a tuple of:
          - :class:`numpy.ndarray` a vertical stack of the 2D XICs for each fragment. The top
            entry in the stack corresponds to the MS1 data if this was enabled in the constructor. These
            are log10-scaled and normalised to have a maximum value of 1
          - list[int] of the height of each XIC so that the first entry can be sliced into constituent
            elements if required
          - list[str] of the labels of each XIC so that slices for each XIC can have associated text
          - :class:`numpy.ndarray` the summed 1D XIC corresponding the un-normalised 2D XIC for MS1
          - list[:class:`numpy.ndarray`] each entry is the summed 1D XIC corresponding the un-normalised
            2D XIC for each entry in the product_mz_list
          - :class:`numpy.ndarray` the retention time for the XICs
        """
        if ms2_swath_map is None:
            primary_precursor_mz = precursor_mz_list[0]
            ms2_swath_map = self.load_ms2_swath_map(primary_precursor_mz)
        return self.extractor.load_raw_data(
            ms2_swath_map,
            precursor_mz_list,
            product_mz_list,
            number_of_isotopes,
            log_heatmap,
            normalise_per_fragment,
            retention_time_zoom,
        )

    def load_ms2_swath_map(self, precursor_mz):
        """
        Load the correct MS2 raw data based on the supplied precursor m/z
        """
        ms2_name = self.swath_run.mapPrecursorsToMS2Names(
            [precursor_mz],
            self.lower_mz_overlap,
            self.upper_mz_overlap,
        ).get(precursor_mz, None)
        if ms2_name is None:
            raise RuntimeError(f'No MS2 data exists with m/z value of {precursor_mz}')
        return self.swath_run.loadSwathMap(ms2_name)

    @classmethod
    def _plotly_layout(cls, rt, xic_2d_hline, xic_2d_hline_label, psm_name, fontsize, y_axis_split):
        """
        Create the Axes layout such that the chromatograms and heatmap share the retention
        time x-axis and are linked for zooming with the following layout

            ************
            *          *
            * heatmap  * y
            *          *
            ************
            |  chrom   | y2
            ************
                x
        """

        common = dict(
            showgrid=False,
            showline=False,
            titlefont=dict(size=fontsize),
            tickfont=dict(size=fontsize),
        )

        xaxis = dict(
            title='Retention Time (s)',
            tickvals=rt,
            tickmode='auto',
            showticklabels=True,
            **common,
        )

        yaxis = dict(
            title='XIC',
            side='left',
            domain=[0.0, y_axis_split],
            showticklabels=True,
            **common,
        )

        yaxis2 = dict(
            title='Mass over charge',
            domain=[y_axis_split, 1.0],
            showticklabels=False,
            **common,
        )

        annotations = []
        for i, label in enumerate(xic_2d_hline_label):
            y_bot = 0 if i == 0 else xic_2d_hline[i - 1]
            y_top = xic_2d_hline[i]
            y = xic_2d_hline[-1] - 0.5 * (y_bot + y_top)
            annotations.append(
                dict(
                    x=rt[0] + (rt[-1] - rt[0]) / 25,
                    y=y,
                    xref='x',
                    yref='y2',
                    text=label,
                    showarrow=False,
                    align='left',
                    font=dict(
                        size=8,
                        color='rgb(255, 255, 255)',
                    ),
                ),
            )

        # create the basic layout
        layout = dict(
            title=psm_name,
            xaxis=xaxis,
            yaxis=yaxis,
            yaxis2=yaxis2,
            annotations=annotations,
            showlegend=False,
        )

        return layout

    def _plotly_figure_data(self, xic_2d, xic_2d_hline, n_isotopes, chroms, rt, y_axis_index):
        """
        Plot the traces such that the chromatograms and heatmap share the retention
        time x-axis and are linked for zooming with the layout above
        """
        n_chroms = len(chroms)
        _, line_colours = matplotlib_colourmap_to_plotly(
            name=self.matplotlib_colourmap,
            ncolours=n_chroms,
        )
        line_colours = [c for _, c in line_colours]
        n_isotopes_including_mono = n_isotopes + 1

        # plotly indexes the heatmap from 0,0 in the bottom left corner, so we need to
        # reverse everything so that MS1 is at the top
        xic_2d = np.flipud(xic_2d)
        max_y_offset = xic_2d.shape[0] + 0.5

        # add base heatmap
        data = [
            go.Heatmap(
                xaxis='x',
                yaxis='y2',
                x=rt,
                y=y_axis_index,
                z=xic_2d,
                colorscale=self.colourscale,
                showscale=False,
            ),
        ]

        # add horizontal lines to separate fragments
        for i, y in enumerate(xic_2d_hline[:-1]):
            is_bounding_line = i % n_isotopes_including_mono == n_isotopes
            data.append(
                go.Scatter(
                    xaxis='x',
                    yaxis='y2',
                    showlegend=False,
                    x=[rt[0], rt[-1]],
                    y=[max_y_offset - y, max_y_offset - y],
                    mode='lines',
                    line=dict(
                        color='rgb(255, 255, 255)',
                        width=1,
                        dash=None if is_bounding_line else 'dash',
                    ),
                    opacity=1.0 if is_bounding_line else 0.5,
                ),
            )

        # add chromatograms
        if not self.use_ms1:
            xic_2d_hline = np.insert(xic_2d_hline, 0, [0] * n_isotopes_including_mono)
        assert len(xic_2d_hline) // n_isotopes_including_mono == n_chroms + 1
        for i, (chrom_y, c) in enumerate(zip(chroms, line_colours)):
            y_bot = xic_2d_hline[i * n_isotopes_including_mono + n_isotopes]
            y_top = xic_2d_hline[(i + 1) * n_isotopes_including_mono + n_isotopes]
            shared_kwargs = dict(
                xaxis='x',
                yaxis='y2',
                showlegend=False,
                x=[rt[0], rt[0]],
                y=[max_y_offset - y_bot, max_y_offset - y_top],
                mode='lines',
            )
            data.append(
                go.Scatter(
                    line=dict(color='rgb(255, 255, 255)', width=10),
                    **shared_kwargs,
                ),
            )
            data.append(
                go.Scatter(
                    line=dict(color=c, width=8),
                    **shared_kwargs,
                ),
            )
            # add chrom
            data.append(
                go.Scatter(
                    xaxis='x',
                    yaxis='y',
                    showlegend=False,
                    x=rt,
                    y=chrom_y,
                    mode='lines',
                    line=dict(
                        color=c,
                        width=2,
                    ),
                ),
            )

        return data


class ManualValidator():

    HELP_TEXT = """
    Right click selects and zooms for refinement opportunity.
    Reset: clear selection and zoom out || Zoom: toggle zoom || Copy: copy annotation from previous validation
    Not Here: if you don't think this PQP exists || Previous: go back one PQP || Next: move to next PQP
    """.strip()

    DETAILED_HELP_TEXT = """
    Explanation
    * The 'truth' toffee file is shown above
    * The 'validating' toffee file is shown below
    * MS1 at top.
    * Each fragment strip is seperated into further isotope strips. Default is 1 isotopes per fragment.
    * Summed chromatograms are below.

    Instructions
    * Right click to make a selection. This will automatically be saved to the specified output file.
    * Reset: Clear RT selection and zoom out
    * Zoom: toggle zoom level of selection
    * Copy: Copy annotation from previous validation if it exists
    * Not Here: If you don't think this pqp exists
    * Next: Go to next pqp
    * Prev: Go to previous pqp
    """.strip()

    TRUTH = 'truth'
    TEST = 'test'

    ZOOM_STATE_OUT = 'ZOOM_STATE_OUT'
    ZOOM_STATE_PARTIAL = 'ZOOM_STATE_PARTIAL'
    ZOOM_STATE_FULL = 'ZOOM_STATE_FULL'

    SIDE_OFFSET = 0.05
    BUTTON_HEIGHT = 0.03
    BUTTON_WIDTH = 0.12
    BUTTON_BOTTOM = 0.01

    RESET_BUTTON_POSITION = [
        SIDE_OFFSET,
        BUTTON_BOTTOM,
        BUTTON_WIDTH / 2,
        BUTTON_HEIGHT,
    ]
    INCREMENT_ISOTOPES_POSITION = [
        RESET_BUTTON_POSITION[0] + BUTTON_BOTTOM + RESET_BUTTON_POSITION[2],
        BUTTON_BOTTOM,
        BUTTON_WIDTH / 2,
        BUTTON_HEIGHT,
    ]
    DECREMENT_ISOTOPES_POSITION = [
        INCREMENT_ISOTOPES_POSITION[0] + BUTTON_BOTTOM + INCREMENT_ISOTOPES_POSITION[2],
        BUTTON_BOTTOM,
        BUTTON_WIDTH / 2,
        BUTTON_HEIGHT,
    ]
    ZOOM_TOGGLE_BUTTON_POSITION = [
        DECREMENT_ISOTOPES_POSITION[0] + BUTTON_BOTTOM + DECREMENT_ISOTOPES_POSITION[2],
        BUTTON_BOTTOM,
        BUTTON_WIDTH / 2,
        BUTTON_HEIGHT,
    ]
    COPY_BUTTON_POSITION = [
        ZOOM_TOGGLE_BUTTON_POSITION[0] + BUTTON_BOTTOM + ZOOM_TOGGLE_BUTTON_POSITION[2],
        BUTTON_BOTTOM,
        BUTTON_WIDTH / 2,
        BUTTON_HEIGHT,
    ]

    NEXT_BUTTON_POSITION = [
        1.0 - SIDE_OFFSET - BUTTON_WIDTH,
        BUTTON_BOTTOM,
        BUTTON_WIDTH,
        BUTTON_HEIGHT,
    ]
    PREV_BUTTON_POSITION = [
        NEXT_BUTTON_POSITION[0] - BUTTON_BOTTOM - NEXT_BUTTON_POSITION[2],
        BUTTON_BOTTOM,
        BUTTON_WIDTH,
        BUTTON_HEIGHT,
    ]
    NOT_HERE_BUTTON_POSITION = [
        PREV_BUTTON_POSITION[0] - BUTTON_BOTTOM - PREV_BUTTON_POSITION[2],
        BUTTON_BOTTOM,
        BUTTON_WIDTH,
        BUTTON_HEIGHT,
    ]

    ZOOM_WIDTH = 400
    DEFAULT_ISOTOPES = 0
    MAX_ISOTOPES = 1
    MIN_ISOTOPES = 0

    LOOKING_LINE_COLOR = '#ff6f00'  # orange
    SELECTED_LINE_COLOR = '#ff0d00'  # red
    VALID_LINE_COLOR = '#ededed'  # near white

    def __init__(
        self,
        truth_tof,
        test_tof,
        pqps_to_annotate,
        output_fname,
        rt_offset=0.0,
        truth_validations=None,
        test_validations=None,
        selected_pqps=None,
        extraction_width=20,
        debug=False,
    ):
        """
        :param truth_tof str:
        :param test_tof str:
        :param pqps_to_annotate str|pd.DataFrame:
        :param output_fname str:
        :param rt_offset float:
        :param truth_validations str|pd.DataFrame:
        :param test_validations str|pd.DataFrame:
        :param selected_pqps tuple(str):
        """
        self.logger = set_stream_logger(level=logging.DEBUG if debug else logging.INFO)

        # ---
        self.logger.info('Loading PQP data')
        pqps_to_annotate = self._init_df(pqps_to_annotate, required=True)
        self._validate_pqp_df(pqps_to_annotate)

        if selected_pqps is not None:
            assert isinstance(selected_pqps, (list, tuple))
            mask = pqps_to_annotate.PQP.isin(selected_pqps)
            pqps_to_annotate = pqps_to_annotate.loc[mask]
        self.pqps = sorted(list(pqps_to_annotate.PQP.unique()))

        # ---
        self.logger.info('Loading manual validation files')
        self.truth_validations = self._init_df(truth_validations, required=False)
        if self.truth_validations is not None:
            self._validate_manual_validation_df(self.truth_validations)

        self.test_validations = self._init_df(test_validations, required=False)
        if self.test_validations is not None:
            self._validate_manual_validation_df(self.test_validations)

        # ---
        self.logger.info('Loading cache of all raw data')
        self.data = self._cache_all_data(truth_tof, test_tof, pqps_to_annotate, extraction_width)

        # ---
        self.rt_offset = rt_offset
        self.output_fname = output_fname
        self.validating = self._init_output_file(self.pqps, output_fname)

        self.pqp_idx = 0
        self.n_isotopes = self.DEFAULT_ISOTOPES
        self.mouse_marker = 1
        self.zoom_state = self._zoom_to_start()
        self._init_plotting()

    @classmethod
    def _init_df(cls, df, required=True):
        if isinstance(df, pd.DataFrame):
            pass
        elif isinstance(df, str):
            df = pd.read_csv(df, sep='\t')
        elif not required:
            df = None
        else:
            raise ValueError(f'{df} must be of type pd.DataFrame or a str file path')
        return df

    @classmethod
    def _validate_pqp_df(cls, df):
        if 'ModifiedPeptide' not in df.columns and 'ModifiedPeptideSequence' in df.columns:
            df['ModifiedPeptide'] = df['ModifiedPeptideSequence']
        expected_cols = [
            'ModifiedPeptide',
            'PrecursorMz',
            'PrecursorCharge',
            'ProductMz',
            'ProductCharge',
        ]
        err = []
        for c in expected_cols:
            if c not in df.columns:
                err.append(f'Required column not in dataframe: {c}')
        if 'PrecursorCharge' in df.columns and df.PrecursorCharge.isna().any():
            err.append('NaNs in PrecursorCharge column')
        if len(err) > 0:
            err = '\n'.join(err)
            raise ValueError(f'Incorrect columns in dataframe:\n{err}')
        df['PrecursorCharge'] = df.PrecursorCharge.astype(int)
        df['PQP'] = df.ModifiedPeptide + '_' + df.PrecursorCharge.map(str)
        if 'TransitionGroupId' in df.columns:
            df['TransitionGroupId.bkp'] = df['TransitionGroupId']
        df['TransitionGroupId'] = df['PQP']
        if 'TransitionId' not in df.columns:
            df['TransitionId'] = df['PQP'] + '_' + df.PQP.groupby(df.PQP).cumcount().map(str)

    @classmethod
    def _validate_manual_validation_df(cls, df):
        if 'ModifiedPeptide' not in df.columns and 'ModifiedPeptideSequence' in df.columns:
            df['ModifiedPeptide'] = df['ModifiedPeptideSequence']
        if 'PrecursorCharge' not in df.columns and 'Charge' in df.columns:
            df['PrecursorCharge'] = df['Charge']
        expected_cols = [
            'ModifiedPeptide',
            'PrecursorCharge',
            'RetentionTime',
        ]
        err = []
        for c in expected_cols:
            if c not in df.columns:
                err.append(f'Required column not in dataframe: {c}')
        if 'PrecursorCharge' in df.columns and df.PrecursorCharge.isna().any():
            err.append('NaNs in PrecursorCharge column')
        if len(err) > 0:
            err = '\n'.join(err)
            raise ValueError(f'Incorrect columns in dataframe:\n{err}')
        df['PrecursorCharge'] = df.PrecursorCharge.astype(int)
        df['PQP'] = df.ModifiedPeptide + '_' + df.PrecursorCharge.map(str)
        if 'TransitionGroupId' in df.columns:
            df['TransitionGroupId.bkp'] = df['TransitionGroupId']
        df['TransitionGroupId'] = df['PQP']
        df.set_index('PQP', inplace=True)

    @classmethod
    def _init_output_file(cls, pqps, output_fname):
        if os.path.isfile(output_fname):
            df = pd.read_csv(output_fname, sep='\t')
            if 'PQP' not in df.columns:
                df['PQP'] = df.ModifiedPeptide + '_' + df.PrecursorCharge.map(str)
            df.set_index('PQP', inplace=True)
        else:
            df = pd.DataFrame({'PQP': pqps})
            df['RetentionTime'] = None
            df['Curated'] = False
            df['ModifiedPeptide'] = df.PQP.apply(lambda x: '_'.join(x.split('_')[:-1]))
            df['PrecursorCharge'] = df.PQP.apply(lambda x: int(x.split('_')[-1]))
            df.set_index('PQP', inplace=True)

        df.sort_index(inplace=True)
        if pqps != df.index.tolist():
            raise ValueError('Output file exists already and ids do not match input data')

        return df

    @classmethod
    def _cache_all_data(cls, truth_tof, test_tof, pqps_to_annotate, extraction_width):
        """
        This is potentially very memory hungry, but will significantly increase the performance of the app

        :return: dict of dicts with structure -- data[src][n_isotopes][pqp] -- and values are
            the results of `_calculate_isotopes`
        """
        # add in missing columns
        pqps_to_annotate = pqps_to_annotate.copy()
        pqps_to_annotate.loc[pqps_to_annotate.ProductCharge.isna(), 'ProductCharge'] = 1

        fnames = {
            cls.TRUTH: truth_tof,
        }
        input_are_the_same = truth_tof == test_tof
        if not input_are_the_same:
            fnames[cls.TEST] = test_tof

        data = {}
        for src, tof_fname in tqdm(fnames.items()):
            # we're using matplotlib here instead of plotly (which ToffeeFragmentsPlotter uses), but the
            # class still comes with handy helper functions for us to use here
            toffee_plotter = ToffeeFragmentsPlotter(
                tof_fname,
                extraction_width=extraction_width,
                use_ms1=True,
            )
            data[src] = {}

            pqps = pqps_to_annotate.copy()
            lower_mz_overlap = 0.0
            upper_mz_overlap = 1.0
            ms2_name_map = toffee_plotter.swath_run.mapPrecursorsToMS2Names(
                pqps.PrecursorMz.unique(),
                lower_mz_overlap,
                upper_mz_overlap,
            )
            pqps['ms2Name'] = pqps_to_annotate.PrecursorMz.map(ms2_name_map)
            pqps = pqps[~pqps.ms2Name.isna()]
            ms2_names = pqps.ms2Name.unique()

            for ms2_name in tqdm(ms2_names):
                ms2_swath_map = toffee_plotter.swath_run.loadSwathMap(ms2_name)
                window_pqps = pqps[pqps.ms2Name == ms2_name].copy()

                for n_isotopes in tqdm(range(cls.MAX_ISOTOPES + 1)):
                    if n_isotopes not in data[src]:
                        data[src][n_isotopes] = {}

                    precursors, products = cls._calculate_isotopes(window_pqps, n_isotopes)
                    for pqp in window_pqps.PQP.unique():
                        data[src][n_isotopes][pqp] = cls._get_data(
                            toffee_plotter=toffee_plotter,
                            pqp=pqp,
                            ms2_swath_map=ms2_swath_map,
                            precursors=precursors,
                            products=products,
                            n_isotopes=n_isotopes,
                        )
        gc.collect()

        if input_are_the_same:
            data[cls.TEST] = data[cls.TRUTH]
        return data

    @classmethod
    def _calculate_isotopes(cls, pqps_to_annotate, n_isotopes):
        precursors, products = util.calculate_isotope_mz(
            precursor_and_product_df=pqps_to_annotate,
            n_isotopes=n_isotopes,
            sort_by_intensity=True,
            drop_other_cols=False,
        )
        precursors.set_index('Id', inplace=True)
        products.set_index('GroupId', inplace=True)
        intersection = precursors.index.intersection(products.index)
        union = precursors.index.union(products.index)
        diff = union.difference(intersection)
        if len(diff) > 0:
            raise ValueError(f'The Ids in precursor and product should be the same! {diff}')
        return precursors, products

    @classmethod
    def _get_data(
        cls,
        toffee_plotter,
        pqp,
        ms2_swath_map,
        precursors,
        products,
        n_isotopes,
        max_num_fragments=6,
    ):
        """
        Retrieves the data required to draw our toffee plot
        """
        assert isinstance(toffee_plotter, ToffeeFragmentsPlotter)
        pqp_precursors = precursors.loc[pqp].IsotopeMz.tolist()
        pqp_products = products.loc[pqp].IsotopeMz.tolist()[:max_num_fragments * (n_isotopes + 1)]
        if not isinstance(pqp_precursors, list):
            pqp_precursors = [pqp_precursors]
        if not isinstance(pqp_products, list):
            pqp_products = [pqp_products]

        xic_2d, xic_2d_hline, xic_2d_hline_label, ms1_chrom, chroms, rt = toffee_plotter.load_raw_data(
            pqp_precursors,
            pqp_products,
            n_isotopes,
            log_heatmap=True,
            normalise_per_fragment=True,
            retention_time_zoom=None,
            ms2_swath_map=ms2_swath_map,
        )

        mz_band_count = len(pqp_precursors) + len(pqp_products)
        mz_band_height = len(xic_2d) / mz_band_count

        return xic_2d, xic_2d_hline, xic_2d_hline_label, ms1_chrom, chroms, rt, mz_band_count, mz_band_height

    def _init_plotting(self):
        # init subplots
        gs = gridspec.GridSpec(4, 1, height_ratios=[3, 1, 3, 1])

        self.truth_im_ax = plt.subplot(gs[0, 0])
        self.truth_chrom_ax = plt.subplot(gs[1, 0], autoscale_on=True)
        self.test_im_ax = plt.subplot(gs[2, 0])
        self.test_chrom_ax = plt.subplot(gs[3, 0], autoscale_on=True)

        # init buttons & events
        self._load_button('reset_button', 'Reset', self.RESET_BUTTON_POSITION, self._reset)
        self._load_button('not_here_button', 'Not Here', self.NOT_HERE_BUTTON_POSITION, self._not_here)
        self._load_button('inc_isos_button', 'Isotopes (+)', self.INCREMENT_ISOTOPES_POSITION, self._increment_isotopes)
        self._load_button('dec_isos_button', 'Isotopes (-)', self.DECREMENT_ISOTOPES_POSITION, self._decrement_isotopes)
        self._load_button('next_button', 'Next', self.NEXT_BUTTON_POSITION, self._next)
        self._load_button('prev_button', 'Previous', self.PREV_BUTTON_POSITION, self._prev)
        self._load_button('zoom_button', 'Zoom +/-', self.ZOOM_TOGGLE_BUTTON_POSITION, self._toggle_zoom)
        self._load_button('copy_button', 'Copy', self.COPY_BUTTON_POSITION, self._copy_annotation)

        self.test_im_ax.figure.canvas.mpl_connect('motion_notify_event', self._canvas_mouse_event)
        self.test_im_ax.figure.canvas.mpl_connect('button_press_event', self._canvas_click_event)
        self.test_im_ax.figure.canvas.mpl_connect('key_press_event', self._enter_key)

    # Plotting

    def draw(self):
        """
        Draws the manual validator to screen
        """
        self._redraw()

        fm = plt.get_current_fig_manager()
        fm.window.showMaximized()

        plt.ion()
        plt.show(block=True)

    def _redraw(self, full_draw=True):
        """
        Called everytime the UI requires updating

        :param full_draw: if True, plot all elements, otherwise, only draw the line markers
        """
        if full_draw:
            self._draw_title()

            self._toffee_plot(self.truth_im_ax, self.truth_chrom_ax, self.TRUTH, -self.rt_offset)
            self.truth_im_ax.set_ylabel('Truth')
            self._toffee_plot(self.test_im_ax, self.test_chrom_ax, self.TEST, 0)
            self.test_im_ax.set_ylabel('Validating')

        self._draw_selection_lines()
        plt.draw()

    def _draw_title(self):
        title = '%s (%d of %d):\n%s' % (
            self.pqps[self.pqp_idx],
            self.pqp_idx + 1,
            len(self.pqps),
            self.HELP_TEXT
        )
        plt.suptitle(t=title, fontsize=8)

    def _toffee_plot(self, im_ax, chrom_ax, data_src, rt_offset):
        """
        Draws a toffee image and chromatogram pair
        """
        pqp = self.pqps[self.pqp_idx]
        data = self.data[data_src][self.n_isotopes][pqp]
        xic_2d, xic_2d_hline, xic_2d_hline_label, ms1_chrom, chroms, rt, mz_band_count, mz_band_height = data

        self._draw_image(im_ax, xic_2d, xic_2d_hline, rt, mz_band_count, mz_band_height)
        ms1_chrom, chroms = self._scale_chromatograms(ms1_chrom, chroms)
        self._draw_chromatograms(chrom_ax, rt, ms1_chrom, chroms)
        self._toffee_plot_zoom(im_ax, chrom_ax, rt, ms1_chrom, chroms, rt_offset)

    def _draw_image(self, ax, xic_2d, xic_2d_hline, rt, mz_band_count, mz_band_height):
        """
        Draws the toffee image
        """
        if ax is not None:
            ax.clear()

        ax.imshow(
            xic_2d,
            interpolation='none',
            extent=[min(rt), max(rt), 0, len(xic_2d)],
            aspect='auto',
        )

        for i, hline in enumerate(reversed(xic_2d_hline[:-1])):
            is_bounding_line = i % (self.n_isotopes + 1) == self.n_isotopes
            alpha = 1.0 if is_bounding_line else 0.2
            ls = '-' if is_bounding_line else '--'
            ax.axhline(hline - 0.5, ls=ls, c='c' if i <= self.n_isotopes else 'w', alpha=alpha)

        ax.yaxis.set_ticklabels([])

        plt.setp(ax.get_xticklabels(), visible=False)

        if self.n_isotopes > 0:
            isotope_labels = list(reversed(range(1, self.n_isotopes + 2)))
            ms2_band_count = 4
            ms2_labels = list(reversed(range(1, ms2_band_count + 1)))
            tick_labels = [
                f'MS2({ms2_label}), ISO({isotope_label})'
                for ms2_label in ms2_labels
                for isotope_label in isotope_labels
            ]
            tick_labels += ['MS1, ISO({})'.format(i + 1) for i in reversed(range(self.n_isotopes + 1))]
            self.logger.debug(tick_labels)
        else:
            tick_labels = [f'MS2({i})' for i in reversed(range(1, mz_band_count))] + ['MS1']

        ax.set_yticks(np.arange(mz_band_height / 2, mz_band_height * mz_band_count, mz_band_height))
        ax.set_yticklabels(tick_labels)

    def _scale_chromatograms(self, ms1_chrom, chroms):
        """
        Normalise the MS2 chromatograms to plot on the same scale as MS1
        """
        scale = np.max(ms1_chrom) / max(1, max(np.max(c) for c in chroms))
        new_chroms = [scale * c for c in chroms]
        return ms1_chrom, new_chroms

    def _draw_chromatograms(self, ax, rt, ms1_chrom, chroms):
        """
        Draws the toffee summed chromatogram
        """
        if ax is not None:
            ax.clear()
        ax.plot(rt, ms1_chrom, c=plt.cm.Reds_r(0.2), label='MS1', zorder=100)
        n_chroms = len(chroms)
        for i, ms2 in enumerate(chroms):
            ax.plot(rt, ms2, c=plt.cm.Blues_r(i / n_chroms), zorder=i, label=f'MS2-{i}')
        ax.legend_ = None
        # ax.legend(loc='upper right')
        ax.tick_params(axis='y')

    def _toffee_plot_zoom(self, im_ax, chrom_ax, rt, ms1_chrom, chroms, rt_offset):
        validating_pqp = self.pqps[self.pqp_idx]
        rt_selection = self.validating.loc[validating_pqp].RetentionTime
        if rt_selection is not None and self.zoom_state in [self.ZOOM_STATE_PARTIAL, self.ZOOM_STATE_FULL]:
            # left/right bounds
            offset = self.ZOOM_WIDTH
            if self.zoom_state == self.ZOOM_STATE_FULL:
                offset *= 0.5
            left, right = rt_selection - offset + rt_offset, rt_selection + offset + rt_offset

            # chrom ax bounds (zoom close to the selection point)
            top = []
            left_rt_idx = np.searchsorted(rt, rt_selection - 0.2 * offset + rt_offset)
            right_rt_idx = np.searchsorted(rt, rt_selection + 0.2 * offset + rt_offset)
            for chrom in [ms1_chrom] + chroms:
                top.append(np.max(chrom[left_rt_idx:right_rt_idx]))
            top = max(top)
        else:
            left, right = np.min(rt) + rt_offset, np.max(rt) + rt_offset
            top = np.max(ms1_chrom)
        # set x limits
        for ax in (im_ax, chrom_ax):
            ax.set_xlim(left=left, right=right)
        # set y limits on chrom axis
        bottom, top = 0, max(1, 1.01 * top)
        chrom_ax.set_ylim(bottom=bottom, top=top)

    def _draw_selection_lines(self):
        """
        Draws the user selection lines
        """
        for ax in (self.test_im_ax, self.test_chrom_ax):
            self._draw_selection_lines_impl(
                ax,
                offset=0,
                draw_selection=True,
                prev_valid=self.test_validations,
            )
        for ax in (self.truth_im_ax, self.truth_chrom_ax):
            self._draw_selection_lines_impl(
                ax,
                offset=-self.rt_offset,
                draw_selection=False,
                prev_valid=self.truth_validations,
            )

    def _draw_selection_lines_impl(self, ax, offset=0, draw_selection=False, prev_valid=None):
        """
        Draws the user selection lines helper
        """
        pqp = self.pqps[self.pqp_idx]

        # clear old lines
        # horrible hack to clear our selection lines without clearing any other lines :|
        ax.lines = [
            li for li in ax.lines
            if li._color not in [self.LOOKING_LINE_COLOR, self.SELECTED_LINE_COLOR, self.VALID_LINE_COLOR]
        ]

        # mouse cursor line
        ax.axvline(x=self.mouse_marker + offset, color=self.LOOKING_LINE_COLOR, alpha=0.5)

        # selection line
        rt = self.validating.loc[pqp].RetentionTime
        if draw_selection and rt is not None and not np.isnan(rt):
            ax.axvline(x=rt, ls='--', color=self.SELECTED_LINE_COLOR, alpha=0.9, zorder=20)

        # previous validation line
        rt = self._prev_validation_rt(pqp, prev_valid)
        if rt is not None and not np.isnan(rt):
            ax.axvline(x=rt, color=self.VALID_LINE_COLOR, alpha=0.9, zorder=10)

    def _prev_validation_rt(self, pqp, prev_valid):
        try:
            rt = None if prev_valid is None else prev_valid.loc[pqp].RetentionTime
        except KeyError:
            rt = None
        if rt is not None and np.isnan(rt):
            rt = None
        return rt

    # Event Handling

    def _load_button(self, instance_key, title, position, callback):
        ax = plt.axes(position)

        # we need to keep the button in self to avoid it getting gc'd
        setattr(self, instance_key, Button(ax, title))
        getattr(self, instance_key).on_clicked(callback)

    def _next(self, event):
        if self.pqp_idx + 1 == len(self.pqps):
            return
        self.pqp_idx += 1
        self.zoom_state = self._zoom_to_start()
        self._redraw()

    def _prev(self, event):
        if self.pqp_idx == 0:
            return
        self.pqp_idx -= 1
        self.zoom_state = self._zoom_to_start()
        self._redraw()

    def _toggle_zoom(self, event):
        if self.zoom_state not in [self.ZOOM_STATE_PARTIAL, self.ZOOM_STATE_FULL]:
            return
        if self.zoom_state == self.ZOOM_STATE_FULL:
            self.zoom_state = self.ZOOM_STATE_PARTIAL
        else:
            self.zoom_state = self.ZOOM_STATE_FULL
        self._redraw()

    def _copy_annotation(self, event):
        pqp = self.pqps[self.pqp_idx]
        rt = self._prev_validation_rt(pqp, self.test_validations)
        if rt is None:
            return
        self._selection_event(rt, True)
        if self.zoom_state not in [self.ZOOM_STATE_PARTIAL, self.ZOOM_STATE_FULL]:
            self.zoom_state = self.ZOOM_STATE_PARTIAL
        self._redraw()

    def _zoom_to_start(self):
        pqp = self.pqps[self.pqp_idx]
        rt = self.validating.loc[pqp].RetentionTime
        if rt is not None and not np.isnan(rt):
            return self.ZOOM_STATE_PARTIAL
        else:
            return self.ZOOM_STATE_OUT

    def _reset(self, event):
        self._selection_event(np.nan, False)
        self.zoom_state = self.ZOOM_STATE_OUT
        self._redraw()

    def _not_here(self, event):
        self._selection_event(np.nan, True)
        self._next(event)

    def _increment_isotopes(self, event):
        if self.n_isotopes == self.MAX_ISOTOPES:
            return
        self.n_isotopes = self.n_isotopes + 1
        self._redraw()

    def _decrement_isotopes(self, event):
        if self.n_isotopes == self.MIN_ISOTOPES:
            return
        self.n_isotopes = self.n_isotopes - 1
        self._redraw()

    def _canvas_mouse_event(self, event):
        self._update_mouse_marker(event)
        self._draw_selection_lines()
        self._redraw(full_draw=False)

    def _canvas_click_event(self, event):
        # right click's only (to make my life easy)
        # otherwise off canvas clicks cause weird effects (e.g. on the buttons)
        if event.button != 3:
            return

        self._update_mouse_marker(event)
        self._selection_event(self.mouse_marker, True)
        if self.zoom_state not in [self.ZOOM_STATE_PARTIAL, self.ZOOM_STATE_FULL]:
            self.zoom_state = self.ZOOM_STATE_PARTIAL
        self._redraw()

    def _enter_key(self, event):
        if event.key == 'enter':
            self._next(event)

    def _update_mouse_marker(self, event):
        if event.xdata is not None:
            if event.inaxes in [self.test_im_ax, self.test_chrom_ax]:
                self.mouse_marker = event.xdata
            elif event.inaxes in [self.truth_im_ax, self.truth_chrom_ax]:
                self.mouse_marker = event.xdata + self.rt_offset

    def _selection_event(self, x, curated):
        validating_pqp = self.pqps[self.pqp_idx]
        self.validating.at[validating_pqp, 'RetentionTime'] = x
        self.validating.at[validating_pqp, 'Curated'] = curated
        self.validating.to_csv(self.output_fname, sep='\t')
