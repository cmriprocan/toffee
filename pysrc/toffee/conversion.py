"""
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import argparse
import gc
import io
import json
import logging
import os
import zipfile
from collections import namedtuple

import numpy as np

import pandas as pd

try:
    from psims.mzml.writer import IndexedMzMLWriter
    from psims.transform.mzml import MzMLTransformer
except ImportError:
    logging.critical('Failed to import psims, you might need to install with pip')
    raise

try:
    from pyteomics import mzml
except ImportError:
    logging.critical('Failed to import pyteomics, you might need to install with pip')
    raise

import toffee

from tqdm import tqdm

from .log import set_stream_logger


def deprecation_warning():
    logging.warning(
        'Testing for Toffee <-> MzML conversions is limited in this version. '
        'Consider using Toffee version 0.14.7 or earlier if conversions fail.'
    )


class _NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return str(obj)
        return json.JSONEncoder.default(self, obj)


class _ModifiedMzMLTransformer(MzMLTransformer):
    """
    A wrapper class to enable us to read in an mzML file and copy out just the metadata
    """
    def __init__(self, input_stream, writer=None):
        self.output_stream = io.BytesIO()
        super().__init__(input_stream, self.output_stream)
        if writer is not None:
            self.writer = writer

    def copy_metadata(self):
        """
        Overwrite the parent implementation to prevent adding the additional software and
        processing elements
        """
        self.reader.reset()
        file_description = next(self.reader.iterfind('fileDescription'))
        if file_description is None:
            file_description = {}
        source_files = file_description.get('sourceFileList', {}).get('sourceFile')
        self.writer.file_description(file_description.get('fileContent', {}).items(), source_files)

        param_groups = self.format_referenceable_param_groups()
        if param_groups:
            self.writer.reference_param_group_list(param_groups)

        self.reader.reset()
        software_list = next(self.reader.iterfind('softwareList'))
        if software_list is None:
            software_list = {}
        software_list = software_list.get('software', [])
        self.writer.software_list(software_list)

        configurations = self.format_instrument_configuration()
        self.writer.instrument_configuration_list(configurations)

        data_processing = self.format_data_processing()
        self.writer.data_processing_list(data_processing)


class MzMLToToffee():
    Window = namedtuple(
        'Window',
        ['name', 'center', 'lower', 'upper', 'ids'],
    )

    def __init__(
        self,
        mzml_fname,
        tof_fname,
        ims_type_str,
        filter_ms2_window=0,
        mass_accuracy_path='',
        show_progress_bar=True,
        debug=False,
    ):
        """
        Builds mzml-to-toffee converter.

        This requires that you have installed the optional dependency `pyteomics` using `pip`

        :param mzml_fname: The output mzml filename (*.mzML)
        :param tof_fname: The input toffee filename (*.tof)
        :param ims_type_str: A string that defines the Intrinsic Mass Spacing type for this file
        :param filter_ms2_window: if an integer great than zero is passed in, then this is the only
            MS2 window that will be added to the output file
        :param mass_accuracy_path: if set, then the mass accuracy data will be written to this HDF5
            file
        :param show_progress_bar: Switch on the progress bar as we iterate through the windows
        :param debug: Switch on more detailed logging
        """
        toffee_ext = os.path.splitext(tof_fname)[1].lower()
        mzml_ext = os.path.splitext(mzml_fname)[1].lower()
        if toffee_ext != '.tof':
            raise ValueError(f'Invalid input file extension (should be .tof): {toffee_ext}')
        if mzml_ext != '.mzml':
            raise ValueError(f'Invalid input file extension (should be .mzml): {mzml_ext}')
        if not os.path.isfile(mzml_fname):
            raise ValueError(f'Input file does not exist: {mzml_fname}')

        self.mzml_fname = mzml_fname
        self.tof_fname = tof_fname
        self.ims_type = toffee.ToffeeWriter.imsTypeFromStr(ims_type_str)
        self.filter_ms2_window = filter_ms2_window
        if filter_ms2_window <= 0:
            self.filter_ms2_window_name = None
        else:
            self.filter_ms2_window_name = toffee.ToffeeWriter.ms2Name(filter_ms2_window)
        if len(mass_accuracy_path) > 0:
            self.mass_accuracy_writer = toffee.MassAccuracyWriter(mass_accuracy_path)
        else:
            self.mass_accuracy_writer = None

        self._writer = toffee.ToffeeWriter(self.tof_fname, self.ims_type)
        self._ms1_window = self.Window('ms1', -1, -1, -1, [])
        self._ms2_windows = []

        logger_kwargs = {}
        if debug:
            logger_kwargs['level'] = logging.DEBUG
        self.logger = set_stream_logger(**logger_kwargs)
        self.debug = debug
        self.show_progress_bar = show_progress_bar

    def convert(self):
        """
        Converts the mzML to a toffee file.
        """
        deprecation_warning()

        if self.show_progress_bar:
            log_func = self.logger.info
        else:
            log_func = self.logger.debug

        log_func('Getting the header')
        self._writer.addHeaderMetadata(self._header())

        log_func('Collecting the window locations in the mzML file')
        with mzml.PreIndexedMzML(self.mzml_fname) as reader:
            for i, ele in enumerate(tqdm(reader, disable=not self.show_progress_bar)):
                precursor_list = ele.get('precursorList', {})
                ms_level = 1 if not precursor_list else 2
                ms_level = ele.get('ms level', ms_level)
                if ms_level is None:
                    msg = json.dumps(ele, indent=2, cls=_NumpyEncoder)
                    raise ValueError(f'No MS level found!\n{msg}')

                scan_id = ele.get('id', None)
                if scan_id is None:
                    msg = json.dumps(ele, indent=2, cls=_NumpyEncoder)
                    raise ValueError(f'No id found!\n{msg}')

                if ms_level == 1:
                    window = self._ms1_window
                else:
                    window = self._get_ms2_window(ele)
                window.ids.append(scan_id)

        # force the garbage collector to manage memory more explicitly
        gc.collect()

        # filter the MS2 windows if requrested
        windows = [self._ms1_window]
        windows.extend(
            w for w in self._ms2_windows  # noqa: S101
            if self.filter_ms2_window_name is None or w.name == self.filter_ms2_window_name
        )

        log_func('Processing each window')
        for window in tqdm(windows):
            self._process_window(window)

    def _header(self):
        """
        Extract all but the raw data from the mzML file and convert to a string
        """
        trans = _ModifiedMzMLTransformer(self.mzml_fname)
        with trans.writer:
            trans.writer.controlled_vocabularies()
            trans.copy_metadata()
        return trans.output_stream.getvalue()

    def _get_ms2_window(self, ele):
        """
        MS2 windows are centered around a common m/z value. Use this to make sure that new id's are added
        to the same window
        """
        precursor_list = ele.get('precursorList', {})
        num_precursors = precursor_list.get('count', 0)
        if num_precursors != 1:
            msg = json.dumps(ele, indent=2, cls=_NumpyEncoder)
            raise ValueError(f'There should only be one precursor!\n{msg}')
        precursor = precursor_list.get('precursor')[0]
        isolation = precursor.get('isolationWindow', {})
        center = isolation.get('isolation window target m/z', None)
        if center is None:
            msg = json.dumps(ele, indent=2, cls=_NumpyEncoder)
            raise ValueError(f'No window center found!\n{msg}')
        window = None
        for w in self._ms2_windows:
            if toffee.MathUtils.almostEqual(w.center, center):
                window = w
                break
        if window is None:
            lower_offet = isolation.get('isolation window lower offset', None)
            upper_offet = isolation.get('isolation window upper offset', None)
            window_id = len(self._ms2_windows) + 1
            window_name = toffee.ToffeeWriter.ms2Name(window_id)
            window = self.Window(
                window_name,
                center,
                center - lower_offet,
                center + upper_offet,
                [],
            )
            self._ms2_windows.append(window)
        return window

    def _process_window(self, window):
        """
        Convert the XML into toffee data
        """
        assert isinstance(window, self.Window)
        # create the raw data container
        scan_data = toffee.RawSwathScanData(window.name, self.ims_type)

        if self.mass_accuracy_writer is not None:
            scan_data.addMassAccuracyWriter(self.mass_accuracy_writer)

        scan_data.windowLower = window.lower
        scan_data.windowCenter = window.center
        scan_data.windowUpper = window.upper

        # add raw data to the scan
        with mzml.PreIndexedMzML(self.mzml_fname) as reader:
            for scan_id in tqdm(window.ids, disable=not self.show_progress_bar):
                ele = reader.get_by_id(scan_id)
                scan_list = ele.get('scanList', {})
                assert scan_list.get('count', None) == 1
                scan = scan_list.get('scan', [])
                assert len(scan) == 1
                scan = scan[0]
                rt = scan.get('scan start time', -1) * 60
                assert rt >= 0
                mz = ele.get('m/z array', np.array([]))
                intensity = ele.get('intensity array', np.array([])).astype(np.uint32)
                mask = intensity != 0
                scan_data.addScan(rt, mz[mask], intensity[mask])

        # create our point cloud
        pcl = scan_data.toPointCloud()
        # add to the file
        self._writer.addPointCloud(pcl)

        # force the garbage collector to manage memory more explicitly
        gc.collect()


def mzml_to_toffee():
    parser = argparse.ArgumentParser(
        description='Convert mzML file to toffee',
    )
    parser.add_argument(
        'mzml_filename',
        type=str,
        help='The output filename (*.mzML or *.mzXML).',
    )
    parser.add_argument(
        'toffee_filename',
        type=str,
        help='The input filename (*.tof).',
    )
    parser.add_argument(
        '--ims_type',
        type=str,
        default='TOF',
        help='The intrinsic mass spacing type (e.g. "TOF", "Orbitrap").',
    )
    parser.add_argument(
        '--filter_ms2_window',
        type=int,
        default=0,
        help='If positive integer, only this MS2 window will be included.',
    )
    parser.add_argument(
        '--mass_accuracy_path',
        type=str,
        default='',
        help='Path for the debugging mass accuracy HDF file (*.h5). If not specified, no       debugging occurs.',
    )
    parser.add_argument(
        '--hide_progress_bar',
        action='store_true',
        help='If set, then progress bar will not be shown',
    )
    parser.add_argument(
        '--debug',
        action='store_true',
        help='If set, then debugging logs will be printed',
    )

    args = parser.parse_args()
    converter = MzMLToToffee(
        mzml_fname=args.mzml_filename,
        tof_fname=args.toffee_filename,
        ims_type_str=args.ims_type,
        filter_ms2_window=args.filter_ms2_window,
        mass_accuracy_path=args.mass_accuracy_path,
        show_progress_bar=not args.hide_progress_bar,
        debug=args.debug,
    )
    converter.convert()


class ToffeeToMzML():
    encodings = {
        'm/z array': np.float64,
        'intensity array': np.float64,
        'charge array': np.float64,
    }

    def __init__(self, tof_fname, mzml_fname, show_progress_bar=True, debug=False):
        """
        Builds toffee-to-mzml converter.

        This requires that you have installed the optional dependency `psims` using `pip`

        :param tof_fname: The input toffee filename (*.tof)
        :param mzml_fname: The output mzml filename (*.mzML)
        :param show_progress_bar: Switch on the progress bar as we iterate through the windows
        :param debug: Switch on more detailed logging
        """
        toffee_ext = os.path.splitext(tof_fname)[1].lower()
        mzml_ext = os.path.splitext(mzml_fname)[1].lower()
        if toffee_ext != '.tof':
            raise ValueError(f'Invalid input file extension (should be .tof): {toffee_ext}')
        if mzml_ext != '.mzml':
            raise ValueError(f'Invalid input file extension (should be .mzml): {mzml_ext}')
        if not os.path.isfile(tof_fname):
            raise ValueError(f'Input file does not exist: {tof_fname}')

        self.tof_fname = tof_fname
        self.mzml_fname = mzml_fname
        self.debug = debug
        self.show_progress_bar = show_progress_bar

        logger_kwargs = {}
        if debug:
            logger_kwargs['level'] = logging.DEBUG
        self.logger = set_stream_logger(**logger_kwargs)

        self.swath_run = toffee.SwathRun(tof_fname)

    def convert(self):
        """
        Converts the toffee to an mzml file.
        """
        deprecation_warning()

        with IndexedMzMLWriter(open(self.mzml_fname, 'wb'), encoding='ascii', close=True) as writer:
            self._load_meta_data(writer)
            self._load_raw_data(writer)

    def _load_meta_data(self, writer):
        """
        Loads header meta data -- this requires that the header information in the toffee file is
        valid mzML XML. This will be true if it is created by the MzMLToToffee class

        :param writer: a psims IndexedMzMLWriter object
        """
        self.logger.debug('Loading meta data')

        # load in the header string and try to parse it as mzML (the format it will be if written)
        # by the MzMLToToffee class
        in_stream = io.BytesIO()
        in_stream.write(self.swath_run.header().encode('utf-8'))

        trans = _ModifiedMzMLTransformer(in_stream, writer=writer)
        trans.writer.controlled_vocabularies()
        trans.copy_metadata()

    def _load_raw_data(self, writer):
        """
        Writes raw spectra from the toffee file to the mzml file.

        :param writer: a psims IndexedMzMLWriter object
        """
        self.logger.debug('Loading raw data')

        tof_fname_no_ext, _ = os.path.splitext(self.tof_fname)

        with writer.run(id=tof_fname_no_ext, instrument_configuration='IC1'):
            self.logger.debug('Loading toffee swath maps into memory')
            ms1_swath_map, ms2_swath_maps = self._load_swath_maps()

            num_spectrums = ms1_swath_map.numberOfSpectra() * (1 + len(ms2_swath_maps))
            self.logger.debug(f'Loading {num_spectrums} spectrums')

            with writer.spectrum_list(num_spectrums):
                self._write_spectrums(writer, ms1_swath_map, ms2_swath_maps)

    def _write_spectrums(self, writer, ms1_swath_map, ms2_swath_maps):
        """
        Writes spectra from the toffee swath maps to the mzml file.

        :param writer: a psims IndexedMzMLWriter object
        :param ms1_swath_map: a toffee SwathMap object of ms1
        :param ms2_swath_maps: a list of toffee SwathMap objects for each MS2 window
        """
        for cycle in tqdm(range(ms1_swath_map.numberOfSpectra()), disable=not self.show_progress_bar):
            experiment = cycle * (len(ms2_swath_maps) + 1)

            ms1_spectrum_id = self._write_ms1_spectrum(writer, cycle, experiment, ms1_swath_map)

            for window, ms2_swath_map in enumerate(ms2_swath_maps):
                self._write_ms2_spectrum(writer, cycle, window, experiment + window + 1, ms1_spectrum_id, ms2_swath_map)

    def _write_ms1_spectrum(self, writer, cycle, experiment, ms1_swath_map):
        """
        Writes a MS1 spectrum defined by the cycle, to the mzml file.

        :param writer: a psims IndexedMzMLWriter object
        :param cycle: the instrument cycle for this MS1 spectrum
        :param experiment: unique id for each spectra, used for labelling purposes only
        :param ms1_swath_map: a toffee SwathMap object of ms1
        """
        scan_time, spectrum_id, mz, intensities, tic = self._get_spectrum_properties(
            ms1_swath_map,
            cycle,
            experiment,
        )
        writer.write_spectrum(
            mz,
            intensities,
            id=spectrum_id,
            scan_start_time=scan_time,
            encoding=self.encodings,
            compression='none',
            params=[
                {'ms level': 1},
                {'total ion current': tic},
            ],
        )
        return spectrum_id

    def _write_ms2_spectrum(self, writer, cycle, window, experiment, ms1_spectrum_id, ms2_swath_map):
        """
        Writes a MS2 spectrum defined by the cycle and window, to the mzml file.

        :param writer: a psims IndexedMzMLWriter object
        :param cycle: the instrument cycle for this MS2 spectrum
        :param window: the MS2 window index
        :param experiment: unique id for each spectra, used for labelling purposes only
        :param ms1_spectrum_id: the precursor MS1 spectrum for this MS2 spectrum
        :param ms2_swath_map: a toffee SwathMap object of this MS2 window
        """
        scan_time, spectrum_id, mz, intensities, tic = self._get_spectrum_properties(
            ms2_swath_map,
            cycle,
            experiment,
        )
        precursor_info = {
            'mz': ms2_swath_map.precursorCenterMz(),
            'intensity': None,
            'charge': None,
            'spectrum_reference': ms1_spectrum_id,
            'isolation_window_args': {
                'lower': ms2_swath_map.precursorCenterMz() - ms2_swath_map.precursorLowerMz(),
                'target': ms2_swath_map.precursorCenterMz(),
                'upper': ms2_swath_map.precursorUpperMz() - ms2_swath_map.precursorCenterMz(),
            },
            'activation': ['collision induce dissassociation'],
        }
        writer.write_spectrum(
            mz,
            intensities,
            id=spectrum_id,
            scan_start_time=scan_time,
            precursor_information=precursor_info,
            encoding=self.encodings,
            compression='none',
            params=[
                {'ms level': 2},
                {'total ion current': tic},
            ],
        )
        return spectrum_id

    @classmethod
    def _get_spectrum_properties(cls, swath_map, cycle, experiment):
        assert isinstance(swath_map, toffee.SwathMapSpectrumAccessBase)
        if cycle < swath_map.numberOfSpectra():
            scan_time = swath_map.retentionTime()[cycle]
            spectrum = swath_map.spectrumByIndex(cycle)
            mz = spectrum.massOverCharge
            intensities = spectrum.intensities
            tic = intensities.sum()
        else:
            scan_time = swath_map.firstScanRetentionTimeOffset() + cycle * swath_map.scanCycleTime()
            mz, intensities = None, None
            tic = 0.0

        scan_time /= 60.0  # in minutes, toffee stores in seconds
        spectrum_id = f'sample=0 period=0 cycle={cycle} experiment={experiment}'
        return scan_time, spectrum_id, mz, intensities, tic

    def _load_swath_maps(self):
        """
        Loads all toffee swath maps into memory
        """
        ms1_swath_map = self.swath_run.loadSwathMapSpectrumAccess(toffee.ToffeeWriter.MS1_NAME)
        ms2_swath_maps = [
            self.swath_run.loadSwathMapSpectrumAccess(window.name)
            for window in self.swath_run.ms2Windows()
        ]

        ms1_nspectra = ms1_swath_map.numberOfSpectra()
        ms2_nspectra = list(set(sm.numberOfSpectra() for sm in ms2_swath_maps))
        if len(ms2_nspectra) != 1 or ms2_nspectra[0] != ms1_nspectra:
            self.logger.debug(
                'The number of spectra is not constant across all swath maps!'
                f' ms1 = {ms1_nspectra}, ms2 = {ms2_nspectra}',
            )

        return ms1_swath_map, ms2_swath_maps


def toffee_to_mzml():
    parser = argparse.ArgumentParser(
        description='Convert toffee file to mzML',
    )
    parser.add_argument(
        'toffee_filename',
        type=str,
        help='The input filename (*.tof).',
    )
    parser.add_argument(
        'mzml_filename',
        type=str,
        help='The output filename (*.mzML or *.mzXML).',
    )
    parser.add_argument(
        '--hide_progress_bar',
        action='store_true',
        help='If set, then progress bar will not be shown',
    )
    parser.add_argument(
        '--debug',
        action='store_true',
        help='If set, then debugging logs will be printed',
    )

    args = parser.parse_args()
    converter = ToffeeToMzML(
        tof_fname=args.toffee_filename,
        mzml_fname=args.mzml_filename,
        show_progress_bar=not args.hide_progress_bar,
        debug=args.debug,
    )
    converter.convert()


class RawSciexToToffee():
    SUMMARY_REQUIRED_COLS = [
        'Experiment',
        'WindowLower',
        'WindowCenter',
        'WindowUpper',
        'ScanCycleTimeInSeconds',
        'FirstScanRetentionTimeOffsetInSeconds',
    ]
    SUMMARY_FNAME = 'IMSSummary.tsv'

    IMS_PARAMS_REQUIRED_COLS = [
        'IMSAlpha',
        'IMSBeta',
        'RetentionTimeIndex',
    ]
    IMS_PARAMS_FNAME_FMT = 'IMSParams_{}.tsv'

    RAW_DATA_REQUIRED_COLS = [
        'IMSCoord',
        'Intensity',
    ]
    RAW_DATA_FNAME_FMT = 'IMSRawData_{}.tsv'

    def __init__(
        self,
        zip_fname,
        tof_fname,
        filter_ms2_window=0,
        show_progress_bar=True,
        debug=False,
    ):
        """
        Builds mzml-to-toffee converter.

        This requires that you have installed the optional dependency `pyteomics` using `pip`

        :param zip_fname: The input raw zip file filename (*.zip). This file must contain at least
            the following files:
                - `IMSSummary.tsv`
                - `IMSParams_{experiment}.tsv` for each `experiment` in the `IMSSummary.tsv`
                - `IMSRawData_{experiment}.tsv` for each `experiment` in the `IMSSummary.tsv`
        :param tof_fname: The input toffee filename (*.tof)
        :param ims_type_str: A string that defines the Intrinsic Mass Spacing type for this file
        :param filter_ms2_window: if an integer great than zero is passed in, then this is the only
            MS2 window that will be added to the output file
        :param show_progress_bar: Switch on the progress bar as we iterate through the windows
        :param debug: Switch on more detailed logging
        """
        toffee_ext = os.path.splitext(tof_fname)[1].lower()
        zip_ext = os.path.splitext(zip_fname)[1].lower()
        if toffee_ext != '.tof':
            raise ValueError(f'Invalid input file extension (should be .tof): {toffee_ext}')
        if zip_ext != '.zip':
            raise ValueError(f'Invalid input file extension (should be .zip): {zip_ext}')
        if not os.path.isfile(zip_fname):
            raise ValueError(f'Input file does not exist: {zip_fname}')

        self.zip_fname = zip_fname
        self.tof_fname = tof_fname
        self.ims_type = toffee.IntrinsicMassSpacingType.TOF
        self.filter_ms2_window = filter_ms2_window
        if filter_ms2_window <= 0:
            self.filter_ms2_window_name = None
        else:
            self.filter_ms2_window_name = toffee.ToffeeWriter.ms2Name(filter_ms2_window)

        self._writer = toffee.ToffeeWriter(self.tof_fname, self.ims_type)

        logger_kwargs = {}
        if debug:
            logger_kwargs['level'] = logging.DEBUG
        self.logger = set_stream_logger(**logger_kwargs)
        self.debug = debug
        self.show_progress_bar = show_progress_bar

    def convert(self):
        """
        Converts the mzML to a toffee file.
        """
        deprecation_warning()

        if self.show_progress_bar:
            log_func = self.logger.info
        else:
            log_func = self.logger.debug

        log_func('Getting the header')
        self._writer.addHeaderMetadata(self.header)

        log_func('Collecting the summary data')
        summary = self._get_df(self.SUMMARY_FNAME, self.SUMMARY_REQUIRED_COLS)

        log_func('Converting to toffee')
        for i in tqdm(range(summary.shape[0]), disable=not self.show_progress_bar):
            window_summary = summary.iloc[i]
            window_name = window_summary.Experiment
            get_data = (
                self.filter_ms2_window_name is None
                or window_name == toffee.ToffeeWriter.MS1_NAME  # noqa
                or window_name == self.filter_ms2_window_name  # noqa
            )
            if not get_data:
                continue
            self._write_raw_data_to_file(window_summary)
            gc.collect()

    @property
    def header(self):
        header = """\
        <?xml version='1.0' encoding='utf-8'?>
        <mzML xmlns="http://psi.hupo.org/ms/mzml" version="1.1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://psi.hupo.org/ms/mzml http://psidev.info/files/ms/mzML/xsd/mzML1.1.0.xsd">
            <cvList count="2">
            <cv URI="https://raw.githubusercontent.com/HUPO-PSI/psi-ms-CV/master/psi-ms.obo" fullName="PSI-MS" id="PSI-MS" version="4.1.25"/>
            <cv URI="http://ontologies.berkeleybop.org/uo.obo" fullName="UNIT-ONTOLOGY" id="UO" version="releases/2019-04-01"/>
            </cvList>
            <fileDescription>
            <fileContent>
                <cvParam accession="MS:1000579" cvRef="PSI-MS" name="MS1 spectrum" value=""/>
                <cvParam accession="MS:1000580" cvRef="PSI-MS" name="MSn spectrum" value=""/>
            </fileContent>
            <sourceFileList count="1">
                <sourceFile location="file:///wiff" name="raw.wiff" id="WF1">
                <cvParam accession="MS:1000770" cvRef="PSI-MS" name="WIFF nativeID format" value=""/>
                <userParam name="ABI WIFF file" value=""/>
                </sourceFile>
            </sourceFileList>
            </fileDescription>
            <softwareList count="2">
            <software version="{version}" id="toffee">
            </software>
            </softwareList>
            <instrumentConfigurationList count="1">
            <instrumentConfiguration id="instrumentConfiguration1">
                <userParam name="AB SCIEX instrument model" value=""/>
                <componentList count="3">
                <source order="1">
                    <cvParam accession="MS:1000008" cvRef="PSI-MS" name="ionization type" value=""/>
                </source>
                <analyzer order="2">
                    <cvParam accession="MS:1000443" cvRef="PSI-MS" name="mass analyzer type" value=""/>
                </analyzer>
                <detector order="3">
                    <cvParam accession="MS:1000026" cvRef="PSI-MS" name="detector type" value=""/>
                </detector>
                </componentList>
            </instrumentConfiguration>
            </instrumentConfigurationList>
            <dataProcessingList count="1">
            <dataProcessing id="dataProcessing1">
                <processingMethod order="1" softwareRef="toffee">
                <cvParam accession="MS:1000544" cvRef="PSI-MS" name="Conversion from raw to toffee" value=""/>
                </processingMethod>
            </dataProcessing>
            </dataProcessingList>
        </mzML>
        """.strip()  # noqa
        return header.format(version=toffee.__version__)

    def _write_raw_data_to_file(self, window_summary):
        """
        Convert the raw data into toffee data and save to file
        """
        # Note: this function is dotted with gc.collect() calls to manage memory usage more explicitly
        # we found that without doing this, python would hold on to much more memory than it needed to,
        # making this conversion very inefficient on large files

        window_name = window_summary.Experiment
        if not self.show_progress_bar:
            self.logger.debug(f'Converting {window_name}')

        # ---
        # create the raw data container
        pcl = toffee.ScanDataPointCloud()
        pcl.name = window_name
        pcl.imsType = self.ims_type
        pcl.windowLower = window_summary.WindowLower
        pcl.windowCenter = window_summary.WindowCenter
        pcl.windowUpper = window_summary.WindowUpper
        pcl.scanCycleTime = window_summary.ScanCycleTimeInSeconds
        pcl.firstScanRetentionTimeOffset = window_summary.FirstScanRetentionTimeOffsetInSeconds
        if not self.show_progress_bar:
            self.logger.debug('\tCreated point cloud object')

        self._add_ims_params_to_point_cloud(pcl, window_name)
        gc.collect()
        self._add_raw_data_to_point_cloud(pcl, window_name)
        gc.collect()

        self._writer.addPointCloud(pcl)
        if not self.show_progress_bar:
            self.logger.debug('\tWrote to toffee')

    def _add_ims_params_to_point_cloud(self, pcl, window_name):
        params = self._get_df(
            self.IMS_PARAMS_FNAME_FMT.format(window_name),
            self.IMS_PARAMS_REQUIRED_COLS,
            dtype={'IMSAlpha': np.float, 'IMSBeta': np.float, 'RetentionTimeIndex': np.uint32},
        )

        # copy RT slice indices
        num_scans = params.shape[0]
        slice_index = params.RetentionTimeIndex.values
        pcl.imsProperties.sliceIndex = slice_index
        if not self.show_progress_bar:
            self.logger.debug('\tAdded slice_index')

        # copy alpha and beta
        mean_alpha = 0.0
        mean_beta = 0.0
        num_points = 0
        for i in range(num_scans):
            num_scan_points = slice_index[i] - slice_index[max(0, i - 1)]
            mean_alpha += params.IMSAlpha[i] * num_scan_points
            mean_beta += params.IMSBeta[i] * num_scan_points
            num_points += num_scan_points
        mean_alpha /= num_points
        mean_beta /= num_points
        pcl.imsProperties.medianAlpha = mean_alpha
        pcl.imsProperties.medianBeta = mean_beta
        pcl.imsProperties.alpha = params.IMSAlpha.values
        pcl.imsProperties.beta = params.IMSBeta.values
        if not self.show_progress_bar:
            self.logger.debug('\tAdded IMS_properties')

    def _add_raw_data_to_point_cloud(self, pcl, window_name):
        raw_data = self._get_df(
            self.RAW_DATA_FNAME_FMT.format(window_name),
            self.RAW_DATA_REQUIRED_COLS,
            dtype={'Intensity': np.uint32, 'IMSCoord': np.int32},
        )
        if not self.show_progress_bar:
            self.logger.debug('\tRead raw_data')

        # copy intensity
        pcl.setIntensityFromNumpy(raw_data.Intensity.values)
        raw_data = raw_data.drop('Intensity', axis=1)
        if not self.show_progress_bar:
            self.logger.debug('\tAdded intensity')

        # copy gamma and IMS coords
        gamma_offset = 10  # ensure that the the min IMS coord is >0 so that we can convert to uint32
        pcl.imsProperties.gamma = raw_data.IMSCoord.min() - gamma_offset
        raw_data.IMSCoord -= pcl.imsProperties.gamma
        pcl.imsProperties.setCoordinateFromNumpy(raw_data.IMSCoord.values)  # implicit conversion from int32 to uint32
        if not self.show_progress_bar:
            self.logger.debug('\tAdded IMS_coords')

    def _get_df(self, dataset_name, required_cols, dtype=None):
        """
        Load a dataframe from the zip file
        """
        with zipfile.ZipFile(self.zip_fname, 'r') as z:
            with z.open(dataset_name) as stream:
                df = pd.read_csv(stream, sep='\t', engine='c', dtype=dtype)

        err = []
        for c in required_cols:
            if c not in df.columns:
                err.append(f'Required column not in dataframe: {c}')
        if len(err) > 0:
            err = '\n'.join(err)
            raise ValueError(f'Incorrect columns in dataframe for {dataset_name} of {self.zip_fname}:\n{err}')
        return df


def raw_sciex_data_to_toffee():
    parser = argparse.ArgumentParser(
        description='Convert raw Sciex zip data file to toffee',
    )
    parser.add_argument(
        'zip_filename',
        type=str,
        help='The input filename (*.zip).',
    )
    parser.add_argument(
        'toffee_filename',
        type=str,
        help='The output filename (*.tof).',
    )
    parser.add_argument(
        '--filter_ms2_window',
        type=int,
        default=0,
        help='If positive integer, only this MS2 window will be included.',
    )
    parser.add_argument(
        '--hide_progress_bar',
        action='store_true',
        help='If set, then progress bar will not be shown',
    )
    parser.add_argument(
        '--debug',
        action='store_true',
        help='If set, then debugging logs will be printed',
    )

    args = parser.parse_args()
    converter = RawSciexToToffee(
        zip_fname=args.zip_filename,
        tof_fname=args.toffee_filename,
        filter_ms2_window=args.filter_ms2_window,
        show_progress_bar=not args.hide_progress_bar,
        debug=args.debug,
    )
    converter.convert()
