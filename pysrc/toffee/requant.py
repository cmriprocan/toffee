"""
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import argparse
import os
import sqlite3

import pandas as pd

import toffee

try:
    from .toffee_requant_python import *  # noqa: F401,F403
except ImportError:
    # this is some hackery so that we can autodoc the python code
    # without having to build all of our c++ on readthedocs.org
    if os.environ.get('READTHEDOCS', None) != 'True':
        raise


class _PyProphetRequantifier():
    PROTEIN_NAME = 'ProteinName'
    PEPTIDE_NAME = 'Sequence'
    PRECURSOR_NAME = 'FullPeptideName'
    PRECURSOR_CHARGE = 'Charge'

    PRECURSOR_MZ = 'PrecursorMz'
    PRODUCT_MZ = 'ProductMz'

    PEAK_GROUP_RANK = 'peak_group_rank'
    PEAK_GROUP_RT = 'RT'

    INTENSITY = 'Intensity'
    MS1_NEW_COL = 'MS1Intensity'
    MS2_NEW_COL = 'MS2Intensity'
    MODEL_SIGMA_RT_COL = 'ModelParamSigmaRT'
    MODEL_SIGMA_MZ_COL = 'ModelParamSigmaMz'
    MODEL_RT0_COL = 'ModelParamRT0'
    MODEL_MZ0_MS1_COL = 'ModelParamMz0MS1'
    MODEL_MZ0_MS2_COL = 'ModelParamMz0MS2'
    MODEL_AMPLITUDES_COL = 'ModelParamAmplitudes'

    MS2_WINDOW_NAME = 'MS2Name'

    def __init__(
        self,
        output_filename,
        toffee_filename,
        lower_window_overlap,
        upper_window_overlap,
    ):
        if not os.path.exists(toffee_filename):
            raise RuntimeError(f'Invalid path: {toffee_filename}')
        if not output_filename.endswith('.csv.gz'):
            raise RuntimeError(f'Output filename must be a gzipped csv path (*.csv.gz), not {output_filename}')
        self.output_filename = output_filename
        self.toffee_filename = toffee_filename
        self.lower_window_overlap = lower_window_overlap
        self.upper_window_overlap = upper_window_overlap

    def _run(self, srl_dataframe, scores_dataframe):
        swath_run = toffee.SwathRun(self.toffee_filename)
        srl_dataframe = self._add_ms2_windows(swath_run, srl_dataframe)

        # make sure we requant all peak groups that were ranked, by giving them unique names
        new_tg_name = 'PeakGroupId'
        scores_dataframe[new_tg_name] = scores_dataframe[self.PRECURSOR_NAME] \
            + '_' + scores_dataframe[self.PRECURSOR_CHARGE].map(str) \
            + '_' + scores_dataframe[self.PEAK_GROUP_RANK].map(str)

        # --- set up the peak groups for annotation
        df = pd.merge(
            scores_dataframe,
            srl_dataframe,
            on=[self.PRECURSOR_NAME, self.PRECURSOR_CHARGE],
            how='inner',
        )
        df = df.set_index([self.MS2_WINDOW_NAME, new_tg_name])
        peak_groups = self._build_requant_peak_groups(df)

        # --- run the requantification
        quantifier = Requantifier()  # noqa: F405
        result = quantifier.eval(swath_run, peak_groups)
        result = pd.DataFrame(
            result,
            columns=[
                self.PRECURSOR_NAME,
                self.PRECURSOR_CHARGE,
                self.PEAK_GROUP_RANK,
                self.MS1_NEW_COL,
                self.MS2_NEW_COL,
                self.MODEL_SIGMA_RT_COL,
                self.MODEL_SIGMA_MZ_COL,
                self.MODEL_RT0_COL,
                self.MODEL_MZ0_MS1_COL,
                self.MODEL_MZ0_MS2_COL,
                self.MODEL_AMPLITUDES_COL,
            ],
        )
        pyprophet = pd.merge(
            scores_dataframe.drop(new_tg_name, axis=1),
            result,
            on=[self.PRECURSOR_NAME, self.PRECURSOR_CHARGE, self.PEAK_GROUP_RANK],
            how='inner',
        )
        pyprophet.to_csv(self.output_filename, index=False, compression='gzip')

    def _add_ms2_windows(self, swath_run, srl_dataframe):
        srl = srl_dataframe.copy()
        ms2_map = swath_run.mapPrecursorsToMS2Names(
            srl[self.PRECURSOR_MZ].unique(),
            self.lower_window_overlap,
            self.upper_window_overlap,
        )
        srl[self.MS2_WINDOW_NAME] = srl[self.PRECURSOR_MZ].map(ms2_map)
        return srl

    def _build_requant_peak_groups(self, df):
        peak_groups = {}
        for ms2_name in sorted(df.index.get_level_values(0).unique()):
            if pd.isna(ms2_name):
                continue

            peak_groups[ms2_name] = []
            ms2_df = df.loc[pd.IndexSlice[ms2_name, :]]
            for tg in ms2_df.index.get_level_values(0).unique():
                rows = ms2_df.loc[tg]

                mod_peptide = rows[self.PRECURSOR_NAME].iloc[0]
                charge = rows[self.PRECURSOR_CHARGE].iloc[0]
                rank = rows[self.PEAK_GROUP_RANK].iloc[0]
                rt = rows[self.PEAK_GROUP_RT].iloc[0]
                precursor_mz = rows[self.PRECURSOR_MZ].iloc[0]
                product_mz = rows[self.PRODUCT_MZ].tolist()

                peak_groups[ms2_name].append(
                    RequantPeakGroup(mod_peptide, charge, rank, rt, precursor_mz, product_mz),  # noqa: 501, F405
                )
        return peak_groups


class PyProphetTSVRequantifier(_PyProphetRequantifier):
    def __init__(
        self,
        output_filename,
        toffee_filename,
        pyprophet_filename,
        library_filename,
        lower_window_overlap=0.0,
        upper_window_overlap=1.0,
    ):
        super().__init__(
            output_filename=output_filename,
            toffee_filename=toffee_filename,
            lower_window_overlap=lower_window_overlap,
            upper_window_overlap=upper_window_overlap,
        )

        if not os.path.exists(pyprophet_filename):
            raise RuntimeError(f'Invalid path: {pyprophet_filename}')
        if not os.path.exists(library_filename):
            raise RuntimeError(f'Invalid path: {library_filename}')
        self.pyprophet_filename = pyprophet_filename
        self.library_filename = library_filename

    def run(self):
        def check_column(df, cols):
            for c in cols:
                if c in df.columns:
                    return c
            raise RuntimeError(f'None of {cols} found in {df.columns}')

        # --- load the pyprophet results
        pyprophet = pd.read_csv(self.pyprophet_filename, sep='\t', low_memory=False, engine='c')
        protein_name = check_column(pyprophet, [self.PROTEIN_NAME])
        peptide_name = check_column(pyprophet, [self.PEPTIDE_NAME])
        mod_peptide_name = check_column(pyprophet, [self.PRECURSOR_NAME, 'ModifiedPeptide'])
        charge_name = check_column(pyprophet, [self.PRECURSOR_CHARGE])
        pg_rank_name = check_column(pyprophet, [self.PEAK_GROUP_RANK])
        intensity_name = check_column(pyprophet, [self.INTENSITY])
        rt_name = check_column(pyprophet, [self.PEAK_GROUP_RT])
        cols = [protein_name, peptide_name, mod_peptide_name, charge_name, pg_rank_name, rt_name, intensity_name]
        pyprophet = pyprophet[cols].rename(
            columns={
                protein_name: self.PROTEIN_NAME,
                peptide_name: self.PEPTIDE_NAME,
                mod_peptide_name: self.PRECURSOR_NAME,
                charge_name: self.PRECURSOR_CHARGE,
                pg_rank_name: self.PEAK_GROUP_RANK,
                rt_name: self.PEAK_GROUP_RT,
                intensity_name: self.INTENSITY,
            },
        ).drop_duplicates()

        # --- load the SRL
        srl = pd.read_csv(self.library_filename, sep='\t', low_memory=False, engine='c')
        precursor_z_name = check_column(srl, ['PrecursorCharge', 'prec_z'])
        precursor_mz_name = check_column(srl, ['PrecursorMz', 'Q1'])
        product_mz_name = check_column(srl, ['ProductMz', 'Q3'])
        srl_mod_peptide_name = check_column(srl, ['ModifiedPeptideSequence', 'modification_sequence'])
        srl = srl[[srl_mod_peptide_name, precursor_z_name, precursor_mz_name, product_mz_name]].rename(
            columns={
                srl_mod_peptide_name: self.PRECURSOR_NAME,
                precursor_z_name: self.PRECURSOR_CHARGE,
                precursor_mz_name: self.PRECURSOR_MZ,
                product_mz_name: self.PRODUCT_MZ,
            },
        ).drop_duplicates()
        self._run(
            srl_dataframe=srl,
            scores_dataframe=pyprophet,
        )


def requantify_pyprophet_tsv():
    parser = argparse.ArgumentParser(
        description=(
            'Take the output (in legacy format) from PyProphet and re-quantifies the intensities. The new file will '
            'contain the following columns'
            f' || "{PyProphetTSVRequantifier.PROTEIN_NAME}": The identifier of the protein'
            f' || "{PyProphetTSVRequantifier.PEPTIDE_NAME}": The identifier of the peptide'
            f' || "{PyProphetTSVRequantifier.PRECURSOR_NAME}": The identifier of the precursor'
            f' || "{PyProphetTSVRequantifier.PRECURSOR_CHARGE}": The charge of the precursor'
            f' || "{PyProphetTSVRequantifier.PEAK_GROUP_RANK}": The rank of the precursor peak group (lower is better)'
            f' || "{PyProphetTSVRequantifier.MS1_NEW_COL}": The newly quantified MS1 intensity'
            f' || "{PyProphetTSVRequantifier.MS2_NEW_COL}": The newly quantified MS2 intensity'
            f' || "{PyProphetTSVRequantifier.MODEL_SIGMA_RT_COL}": The Sigma RT parameter of the analytic model'
            f' || "{PyProphetTSVRequantifier.MODEL_SIGMA_MZ_COL}": The Sigma m/z parameter of the analytic model'
            f' || "{PyProphetTSVRequantifier.MODEL_RT0_COL}": The RT0 parameter of the analytic model'
            f' || "{PyProphetTSVRequantifier.MODEL_MZ0_MS1_COL}": The m/z_0 parameter of the analytic model for MS1'
            f' || "{PyProphetTSVRequantifier.MODEL_MZ0_MS2_COL}": The m/z_0 parameter of the analytic model for MS2'
            f' || "{PyProphetTSVRequantifier.MODEL_AMPLITUDES_COL}": The amplitude parameters of the analytic model '
            'with ";" separating MS1 and MS2, and "," separating each fragment.'
        ),
    )
    parser.add_argument(
        'output_filename',
        type=str,
        help='Filename for the output results (*.csv.gz).',
    )
    parser.add_argument(
        'toffee_filename',
        type=str,
        help='The raw data toffee filename (*.tof).',
    )
    parser.add_argument(
        'pyprophet_filename',
        type=str,
        help=(
            'Filename for the PyProphet results that matches the toffee file (*.tsv). '
            'This should be in the legacy format'
        ),
    )
    parser.add_argument(
        'library_filename',
        type=str,
        help='Filename for the TSV spectral library that matches the pyprophet results file (*.tsv).',
    )
    parser.add_argument(
        '--lower_window_overlap',
        type=float,
        default=0.0,
        help=(
            'Positive value to indicate the MS2 window lower overlap (in Da).'
            'This should match the settings used in OpenMSToffee/OpenSwath.'
        ),
    )
    parser.add_argument(
        '--upper_window_overlap',
        type=float,
        default=1.0,
        help=(
            'Positive value to indicate the MS2 window upper overlap (in Da).'
            'This should match the settings used in OpenMSToffee/OpenSwath.'
        ),
    )

    args = parser.parse_args()
    quantifier = PyProphetTSVRequantifier(
        output_filename=args.output_filename,
        toffee_filename=args.toffee_filename,
        pyprophet_filename=args.pyprophet_filename,
        library_filename=args.library_filename,
        lower_window_overlap=args.lower_window_overlap,
        upper_window_overlap=args.upper_window_overlap,
    )
    quantifier.run()


class PyProphetSQLiteRequantifier(_PyProphetRequantifier):
    def __init__(
        self,
        output_filename,
        toffee_filename,
        pyprophet_filename,
        max_q_value_rs=0.05,
        max_peptide_q_value_rs=1.0,
        max_protein_q_value_rs=1.0,
        max_peptide_q_value_experiment_wide=1.0,
        max_protein_q_value_experiment_wide=1.0,
        max_peptide_q_value_global=0.01,
        max_protein_q_value_global=0.01,
        max_peak_group_rank=1,
        lower_window_overlap=0.0,
        upper_window_overlap=1.0,
    ):
        super().__init__(
            output_filename=output_filename,
            toffee_filename=toffee_filename,
            lower_window_overlap=lower_window_overlap,
            upper_window_overlap=upper_window_overlap,
        )
        if not os.path.exists(pyprophet_filename):
            raise RuntimeError(f'Invalid path: {pyprophet_filename}')
        self.pyprophet_filename = pyprophet_filename

        self.max_q_value_rs = max_q_value_rs
        self.max_peptide_q_value_rs = max_peptide_q_value_rs
        self.max_protein_q_value_rs = max_protein_q_value_rs
        self.max_peptide_q_value_experiment_wide = max_peptide_q_value_experiment_wide
        self.max_protein_q_value_experiment_wide = max_protein_q_value_experiment_wide
        self.max_peptide_q_value_global = max_peptide_q_value_global
        self.max_protein_q_value_global = max_protein_q_value_global
        self.max_peak_group_rank = max_peak_group_rank

        self.con = self._get_connection()
        self.scores = self._results_as_dataframe()
        self.srl = self._srl_as_dataframe()

    def run(self):
        self._run(
            srl_dataframe=self.srl,
            scores_dataframe=self.scores,
        )

    def _get_connection(self):
        con = sqlite3.connect(self.pyprophet_filename)
        tables = pd.read_sql_query("SELECT name as Name FROM sqlite_master WHERE type='table';", con).Name.unique()
        required_tables = [
            'PROTEIN',
            'PEPTIDE_PROTEIN_MAPPING',
            'PEPTIDE',
            'PRECURSOR_PEPTIDE_MAPPING',
            'COMPOUND',
            'PRECURSOR_COMPOUND_MAPPING',
            'PRECURSOR',
            'TRANSITION_PRECURSOR_MAPPING',
            'TRANSITION_PEPTIDE_MAPPING',
            'TRANSITION',
            'RUN',
            'FEATURE',
            'FEATURE_MS1',
            'FEATURE_MS2',
            'SCORE_MS2',
            'PYPROPHET_WEIGHTS',
            'SCORE_PEPTIDE',
            'SCORE_PROTEIN',
        ]
        err = []
        for t in required_tables:
            if t not in tables:
                err.append(f'Required table {t} not found')
        if len(err) != 0:
            msg = '\n  - '.join(err)
            raise RuntimeError(f'Error loading results, the following tables are missing:\n  - {msg}')

        idx = []
        idx.append(('idx_run_run_id', 'RUN', 'ID'))
        idx.append(('idx_precursor_precursor_id', 'PRECURSOR', 'ID'))
        idx.append(('idx_peptide_peptide_id', 'PEPTIDE', 'ID'))
        idx.append(('idx_protein_protein_id', 'PROTEIN', 'ID'))
        idx.append(('idx_feature_run_id', 'FEATURE', 'RUN_ID'))
        idx.append(('idx_feature_feature_id', 'FEATURE', 'ID'))
        idx.append(('idx_feature_precursor_id', 'FEATURE', 'PRECURSOR_ID'))
        idx.append(('idx_feature_ms1_feature_id', 'FEATURE_MS1', 'FEATURE_ID'))
        idx.append(('idx_feature_ms2_feature_id', 'FEATURE_MS2', 'FEATURE_ID'))
        idx.append(('idx_score_ms2_feature_id', 'SCORE_MS2', 'FEATURE_ID'))
        idx.append(('idx_score_peptide_peptide_id', 'SCORE_PEPTIDE', 'PEPTIDE_ID'))
        idx.append(('idx_score_peptide_run_id', 'SCORE_PEPTIDE', 'RUN_ID'))
        idx.append(('idx_score_protein_protein_id', 'SCORE_PROTEIN', 'PROTEIN_ID'))
        idx.append(('idx_score_protein_run_id', 'SCORE_PROTEIN', 'RUN_ID'))
        idx.append(('idx_precursor_peptide_mapping_precursor_id', 'PRECURSOR_PEPTIDE_MAPPING', 'PRECURSOR_ID'))
        idx.append(('idx_precursor_peptide_mapping_peptide_id', 'PRECURSOR_PEPTIDE_MAPPING', 'PEPTIDE_ID'))
        idx.append(('idx_peptide_protein_mapping_protein_id', 'PEPTIDE_PROTEIN_MAPPING', 'PROTEIN_ID'))
        idx.append(('idx_transition_precursor_mapping_precursor_id', 'TRANSITION_PRECURSOR_MAPPING', 'PRECURSOR_ID'))
        idx.append(('idx_transition_precursor_mapping_transition_id', 'TRANSITION_PRECURSOR_MAPPING', 'TRANSITION_ID'))
        idx_query = '\n'.join(
            [
                f'CREATE INDEX IF NOT EXISTS {name} ON {table} ({colname});'
                for name, table, colname in idx
            ],
        )
        con.executescript(idx_query)
        return con

    def _results_as_dataframe(self):
        results = pd.merge(
            self._extract_precursor(),
            self._extract_peptide(),
            on=['run_id', 'peptide_id'],
            how='inner',
        )
        results = pd.merge(
            results,
            self._extract_protein(),
            on=['run_id', 'peptide_id'],
            how='inner',
        )
        return results

    def _extract_precursor(self):
        return pd.read_sql_query(
            f"""\
            SELECT
                RUN.ID AS run_id,
                PEPTIDE.ID AS peptide_id,
                PEPTIDE.UNMODIFIED_SEQUENCE AS {self.PEPTIDE_NAME},
                PEPTIDE.MODIFIED_SEQUENCE AS {self.PRECURSOR_NAME},
                PRECURSOR.CHARGE AS {self.PRECURSOR_CHARGE},
                SCORE_MS2.RANK AS {self.PEAK_GROUP_RANK},
                FEATURE.EXP_RT AS {self.PEAK_GROUP_RT},
                FEATURE_MS2.AREA_INTENSITY AS {self.INTENSITY},
                SCORE_MS2.QVALUE AS m_score
            FROM PRECURSOR
            INNER JOIN PRECURSOR_PEPTIDE_MAPPING ON PRECURSOR.ID = PRECURSOR_PEPTIDE_MAPPING.PRECURSOR_ID
            INNER JOIN PEPTIDE ON PRECURSOR_PEPTIDE_MAPPING.PEPTIDE_ID = PEPTIDE.ID
            INNER JOIN FEATURE ON FEATURE.PRECURSOR_ID = PRECURSOR.ID
            INNER JOIN RUN ON RUN.ID = FEATURE.RUN_ID
            LEFT JOIN FEATURE_MS1 ON FEATURE_MS1.FEATURE_ID = FEATURE.ID
            LEFT JOIN FEATURE_MS2 ON FEATURE_MS2.FEATURE_ID = FEATURE.ID
            LEFT JOIN SCORE_MS2 ON SCORE_MS2.FEATURE_ID = FEATURE.ID
            WHERE
                SCORE_MS2.QVALUE < {self.max_q_value_rs}
                AND SCORE_MS2.RANK <= {self.max_peak_group_rank}
                AND PRECURSOR.DECOY = 0
            ORDER BY
                {self.PEPTIDE_NAME},
                {self.PRECURSOR_NAME},
                {self.PRECURSOR_CHARGE},
                {self.PEAK_GROUP_RANK}
            ;
            """,
            self.con,
        )

    def _extract_peptide(self):
        peptide = pd.read_sql_query(
            f"""
            SELECT
                RUN_ID AS run_id,
                PEPTIDE_ID AS peptide_id,
                QVALUE AS m_score_peptide_run_specific
            FROM SCORE_PEPTIDE
            WHERE
                CONTEXT == 'run-specific'
                AND QVALUE < {self.max_peptide_q_value_rs}
            ;
            """,
            self.con,
        )

        def _check_and_add(df, name, threshold):
            cur = self.con.cursor()
            cur.execute(
                f"""
                SELECT
                    PEPTIDE_ID AS peptide_id
                FROM SCORE_PEPTIDE
                WHERE
                    CONTEXT == '{name}'
                ;
                """,
            )
            if cur.fetchone():
                if name == 'global':
                    run_id = ''
                    on = 'peptide_id'
                else:
                    run_id = 'RUN_ID AS run_id,'
                    on = ['run_id', 'peptide_id']

                tmp_df = pd.read_sql_query(
                    f"""
                    SELECT
                        {run_id}
                        PEPTIDE_ID AS peptide_id,
                        QVALUE AS m_score_peptide_{name.replace('-', '_')}
                    FROM SCORE_PEPTIDE
                    WHERE
                        CONTEXT == '{name}'
                        AND QVALUE < {threshold}
                    ;
                    """,
                    self.con,
                )
                df = pd.merge(df, tmp_df, on=on, how='inner')
            return df

        peptide = _check_and_add(peptide, 'experiment-wide', self.max_peptide_q_value_experiment_wide)
        peptide = _check_and_add(peptide, 'global', self.max_peptide_q_value_global)
        return peptide

    def _extract_protein(self):
        protein = pd.read_sql_query(
            f"""
            SELECT
                RUN_ID AS run_id,
                PEPTIDE_ID AS peptide_id,
                MIN(QVALUE) AS m_score_protein_run_specific
            FROM PEPTIDE_PROTEIN_MAPPING
            INNER JOIN SCORE_PROTEIN ON PEPTIDE_PROTEIN_MAPPING.PROTEIN_ID = SCORE_PROTEIN.PROTEIN_ID
            WHERE
                CONTEXT == 'run-specific'
                AND QVALUE < {self.max_protein_q_value_rs}
            GROUP BY
                RUN_ID,
                PEPTIDE_ID
            ;
            """,
            self.con,
        )

        def _check_and_add(df, name, threshold):
            cur = self.con.cursor()
            cur.execute(
                f"""
                SELECT
                    QVALUE
                FROM SCORE_PROTEIN
                WHERE
                    CONTEXT == '{name}'
                ;
                """,
            )
            if cur.fetchone():
                if name == 'global':
                    run_id = ''
                    on = 'peptide_id'
                else:
                    run_id = 'RUN_ID AS run_id,'
                    on = ['run_id', 'peptide_id']

                tmp_df = pd.read_sql_query(
                    f"""
                    SELECT
                        {run_id}
                        PEPTIDE_ID AS peptide_id,
                        MIN(QVALUE) AS m_score_protein_{name.replace('-', '_')}
                    FROM PEPTIDE_PROTEIN_MAPPING
                    INNER JOIN SCORE_PROTEIN ON PEPTIDE_PROTEIN_MAPPING.PROTEIN_ID = SCORE_PROTEIN.PROTEIN_ID
                    WHERE
                        CONTEXT == '{name}'
                        AND QVALUE < {threshold}
                    GROUP BY
                        PEPTIDE_ID
                    ;
                    """,
                    self.con,
                )
                df = pd.merge(df, tmp_df, on=on, how='inner')
            return df

        protein = _check_and_add(protein, 'experiment-wide', self.max_protein_q_value_experiment_wide)
        protein = _check_and_add(protein, 'global', self.max_protein_q_value_global)

        protein_meta = pd.read_sql_query(
            f"""
            SELECT
                PEPTIDE_ID AS peptide_id,
                GROUP_CONCAT(PROTEIN.PROTEIN_ACCESSION,';') AS {self.PROTEIN_NAME}
            FROM PEPTIDE_PROTEIN_MAPPING
            INNER JOIN PROTEIN ON PEPTIDE_PROTEIN_MAPPING.PROTEIN_ID = PROTEIN.ID
            WHERE
                PEPTIDE_ID IN ({', '.join(map(str, protein.peptide_id.unique()))})
            GROUP BY
                PEPTIDE_ID
            ;
            """,
            self.con,
        )
        return pd.merge(
            protein,
            protein_meta,
            on='peptide_id',
            how='inner',
        )

    def _srl_as_dataframe(self):
        return pd.read_sql_query(
            f"""
            SELECT
                PEPTIDE.ID AS peptide_id,
                PEPTIDE.MODIFIED_SEQUENCE AS {self.PRECURSOR_NAME},
                PRECURSOR.CHARGE AS {self.PRECURSOR_CHARGE},
                PRECURSOR.PRECURSOR_MZ AS {self.PRECURSOR_MZ},
                TRANSITION.PRODUCT_MZ AS {self.PRODUCT_MZ}
            FROM PRECURSOR
            INNER JOIN PRECURSOR_PEPTIDE_MAPPING ON PRECURSOR.ID = PRECURSOR_PEPTIDE_MAPPING.PRECURSOR_ID
            INNER JOIN PEPTIDE ON PRECURSOR_PEPTIDE_MAPPING.PEPTIDE_ID = PEPTIDE.ID
            INNER JOIN TRANSITION_PRECURSOR_MAPPING ON PRECURSOR.ID = TRANSITION_PRECURSOR_MAPPING.PRECURSOR_ID
            INNER JOIN TRANSITION ON TRANSITION_PRECURSOR_MAPPING.TRANSITION_ID = TRANSITION.ID
            WHERE
                PEPTIDE.ID IN ({', '.join(map(str, self.scores.peptide_id.unique()))})
            ORDER BY
                {self.PRECURSOR_NAME},
                {self.PRECURSOR_CHARGE},
                {self.PRECURSOR_MZ},
                {self.PRODUCT_MZ}
            ;
            """,
            self.con,
        )


def requantify_pyprophet_sqlite():
    parser = argparse.ArgumentParser(
        description=(
            'Take the SQLite output from PyProphet and re-quantifies the intensities. The new file will '
            'contain the following columns'
            f' || "{PyProphetSQLiteRequantifier.PROTEIN_NAME}": The identifier of the protein'
            f' || "{PyProphetSQLiteRequantifier.PEPTIDE_NAME}": The identifier of the peptide'
            f' || "{PyProphetSQLiteRequantifier.PRECURSOR_NAME}": The identifier of the precursor'
            f' || "{PyProphetSQLiteRequantifier.PRECURSOR_CHARGE}": The charge of the precursor'
            f' || "{PyProphetSQLiteRequantifier.PEAK_GROUP_RANK}": The rank of the precursor peak group'
            f' || "{PyProphetSQLiteRequantifier.MS1_NEW_COL}": The newly quantified MS1 intensity'
            f' || "{PyProphetSQLiteRequantifier.MS2_NEW_COL}": The newly quantified MS2 intensity'
            f' || "{PyProphetTSVRequantifier.MODEL_SIGMA_RT_COL}": The Sigma RT parameter of the analytic model'
            f' || "{PyProphetTSVRequantifier.MODEL_SIGMA_MZ_COL}": The Sigma m/z parameter of the analytic model'
            f' || "{PyProphetTSVRequantifier.MODEL_RT0_COL}": The RT0 parameter of the analytic model'
            f' || "{PyProphetTSVRequantifier.MODEL_MZ0_MS1_COL}": The m/z_0 parameter of the analytic model for MS1'
            f' || "{PyProphetTSVRequantifier.MODEL_MZ0_MS2_COL}": The m/z_0 parameter of the analytic model for MS2'
            f' || "{PyProphetTSVRequantifier.MODEL_AMPLITUDES_COL}": The amplitude parameters of the analytic model '
            'with ";" separating MS1 and MS2, and "," separating each fragment.'
        ),
    )
    parser.add_argument(
        'output_filename',
        type=str,
        help='Filename for the output results (*.csv.gz).',
    )
    parser.add_argument(
        'toffee_filename',
        type=str,
        help='The raw data toffee filename (*.tof).',
    )
    parser.add_argument(
        'pyprophet_filename',
        type=str,
        help=(
            'Filename for the PyProphet SQLite results that matches the toffee file (*.osw).'
        ),
    )

    parser.add_argument(
        '--max_q_value_rs',
        type=float,
        default=0.05,
        help='Run specific peak group FDR threshold.',
    )
    parser.add_argument(
        '--max_peptide_q_value_rs',
        type=float,
        default=1.0,
        help='Run specific peptide FDR threshold.',
    )
    parser.add_argument(
        '--max_protein_q_value_rs',
        type=float,
        default=1.0,
        help='Run specific protein FDR threshold.',
    )
    parser.add_argument(
        '--max_peptide_q_value_experiment_wide',
        type=float,
        default=1.0,
        help='Experiment wide peptide FDR threshold.',
    )
    parser.add_argument(
        '--max_protein_q_value_experiment_wide',
        type=float,
        default=1.0,
        help='Experiment wide protein FDR threshold.',
    )
    parser.add_argument(
        '--max_peptide_q_value_global',
        type=float,
        default=0.01,
        help='Global peptide FDR threshold.',
    )
    parser.add_argument(
        '--max_protein_q_value_global',
        type=float,
        default=0.01,
        help='Global protein FDR threshold.',
    )
    parser.add_argument(
        '--max_peak_group_rank',
        type=int,
        default=1,
        help='Number of peak groups to consider.',
    )

    parser.add_argument(
        '--lower_window_overlap',
        type=float,
        default=0.0,
        help=(
            'Positive value to indicate the MS2 window lower overlap (in Da).'
            'This should match the settings used in OpenMSToffee/OpenSwath.'
        ),
    )
    parser.add_argument(
        '--upper_window_overlap',
        type=float,
        default=1.0,
        help=(
            'Positive value to indicate the MS2 window upper overlap (in Da).'
            'This should match the settings used in OpenMSToffee/OpenSwath.'
        ),
    )

    args = parser.parse_args()
    quantifier = PyProphetSQLiteRequantifier(
        output_filename=args.output_filename,
        toffee_filename=args.toffee_filename,
        pyprophet_filename=args.pyprophet_filename,
        max_q_value_rs=args.max_q_value_rs,
        max_peptide_q_value_rs=args.max_peptide_q_value_rs,
        max_protein_q_value_rs=args.max_protein_q_value_rs,
        max_peptide_q_value_experiment_wide=args.max_peptide_q_value_experiment_wide,
        max_protein_q_value_experiment_wide=args.max_protein_q_value_experiment_wide,
        max_peptide_q_value_global=args.max_peptide_q_value_global,
        max_protein_q_value_global=args.max_protein_q_value_global,
        max_peak_group_rank=args.max_peak_group_rank,
        lower_window_overlap=args.lower_window_overlap,
        upper_window_overlap=args.upper_window_overlap,
    )
    quantifier.run()
