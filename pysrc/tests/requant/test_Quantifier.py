"""
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import logging
import os
import unittest

import pandas as pd

import toffee
from toffee import requant


class TestRequantifier(unittest.TestCase):
    @classmethod
    def setUp(cls):
        base_dir = os.environ.get('DIA_TEST_DATA_REPO', None)
        assert base_dir is not None
        cls.spectral_library_fname = base_dir + '/ProCan90/srl/hek_srl.OpenSwath.iRT.tsv'
        cls.srl = pd.read_csv(cls.spectral_library_fname, sep='\t')

        cls.tof_fname = base_dir + '/ProCan90/tof/ProCan90-M03-01.iRT.tof'
        cls.swath_run = toffee.SwathRun(cls.tof_fname)
        cls.ms1_swath_map = cls.swath_run.loadSwathMapPtr(toffee.ToffeeWriter.MS1_NAME)

        cls.peak_groups = {}

        # ---
        ms2_name = 'ms2-025'

        peptide, charge, rank, rt, pre = 'ELHINLIPNKQDR', 3, 1, 1842.77, 530.630126953125
        pro = [757.395141601563, 607.31982421875, 720.403930664063, 660.342346191406, 546.2994384765631, 1097.60620117188]  # noqa: 501
        cls.peak_groups[peptide] = (ms2_name, requant.RequantPeakGroup(peptide, charge, rank, rt, pre, pro))

        peptide, charge, rank, rt, pre = 'KSDIDEIVLVGGSTR', 3, 1, 2231.21, 530.28955078125
        pro = [576.309997558594, 801.3988647460941, 689.39404296875, 688.314819335938, 900.46728515625, 559.272216796875]  # noqa: 501
        cls.peak_groups[peptide] = (ms2_name, requant.RequantPeakGroup(peptide, charge, rank, rt, pre, pro))

        # ---
        ms2_name = 'ms2-046'

        peptide, charge, rank, rt, pre = 'EATFGVDESANK', 2, 1, 830.17, 634.293701171875
        pro = [819.38427734375, 663.29443359375, 419.22488403320295, 966.4526977539059, 548.2674560546881, 762.3628540039059]  # noqa: 501
        cls.peak_groups[peptide] = (ms2_name, requant.RequantPeakGroup(peptide, charge, rank, rt, pre, pro))

        # ---
        ms2_name = 'ms2-052'

        peptide, charge, rank, rt, pre = 'LAAEESVGTMGNR', 2, 1, 826.85, 667.822082519531
        pro = [635.29296875, 821.393432617188, 950.43603515625, 1150.51574707031, 734.3613891601559, 1079.47863769531]  # noqa: 501
        cls.peak_groups[peptide] = (ms2_name, requant.RequantPeakGroup(peptide, charge, rank, rt, pre, pro))

        peptide, charge, rank, rt, pre = 'TPVISGGPYYER', 2, 1, 1467.61, 669.838073730469
        pro = [928.415893554688, 1041.5, 1140.568359375, 841.383911132813, 784.362426757813, 727.340942382813]  # noqa: 501
        cls.peak_groups[peptide] = (ms2_name, requant.RequantPeakGroup(peptide, charge, rank, rt, pre, pro))

        # ---
        ms2_name = 'ms2-056'

        peptide, charge, rank, rt, pre = 'TLEEDEEELFK', 2, 1, 2659.49, 691.3220825195309
        pro = [1167.50524902344, 1038.46264648438, 909.4199829101559, 794.39306640625, 717.2937622070309, 846.3363647460941]  # noqa: 501
        cls.peak_groups[peptide] = (ms2_name, requant.RequantPeakGroup(peptide, charge, rank, rt, pre, pro))

        peptide, charge, rank, rt, pre = 'VQVALEELQDLK', 2, 1, 3044.61, 692.8877563476559
        pro = [1058.57287597656, 1157.64123535156, 987.5357055664059, 616.366455078125, 874.45166015625, 745.409057617188]  # noqa: 501
        cls.peak_groups[peptide] = (ms2_name, requant.RequantPeakGroup(peptide, charge, rank, rt, pre, pro))

        # ---
        ms2_name = 'ms2-057'

        peptide, charge, rank, rt, pre = 'GDLDAASYYAPVR', 2, 1, 1882.61, 699.3384399414059
        pro = [855.435913085938, 926.473022460938, 997.510131835938, 768.403930664063, 605.340576171875, 1112.537109375]  # noqa: 501
        cls.peak_groups[peptide] = (ms2_name, requant.RequantPeakGroup(peptide, charge, rank, rt, pre, pro))

        # ---
        ms2_name = 'ms2-062'

        peptide, charge, rank, rt, pre = 'DAVTPADFSEWSK', 2, 1, 2609.69, 726.835693359375
        pro = [1066.48400878906, 969.4312133789059, 1167.53173828125, 420.22415161132795, 898.3941040039059, 783.3671875]  # noqa: 501
        cls.peak_groups[peptide] = (ms2_name, requant.RequantPeakGroup(peptide, charge, rank, rt, pre, pro))

        # ---
        ms2_name = 'ms2-066'

        peptide, charge, rank, rt, pre = 'GILAAEESVGTMGNR', 2, 1, 1962.29, 752.8748168945309
        pro = [1150.51574707031, 1221.55285644531, 635.29296875, 821.393432617188, 1079.47863769531, 950.43603515625]  # noqa: 501
        cls.peak_groups[peptide] = (ms2_name, requant.RequantPeakGroup(peptide, charge, rank, rt, pre, pro))

        peptide, charge, rank, rt, pre = 'SPFTVGVAAPLDLSK', 2, 1, 3263.73, 751.4166870117191
        pro = [970.556762695313, 814.466918945313, 1069.62524414063, 1170.6728515625, 913.5353393554691, 1317.74133300781]  # noqa: 501
        cls.peak_groups[peptide] = (ms2_name, requant.RequantPeakGroup(peptide, charge, rank, rt, pre, pro))

        cls.expected_intensities = {
            'ELHINLIPNKQDR': (
                5936.093918843983,
                1105.2723672759626,
                0.23143129991652484,
                2.438000848243307,
                14.924245679784471,
                21.104070419257646,
                20.988504119546832,
                '49946.1,40634.640634.6;8032.58032.5,1727.631727.63,3298.413298.41,1229.891229.89,144.628144.628',  # noqa: 501
            ),
            'KSDIDEIVLVGGSTR': (
                1565.3407841251933,
                467.98228784510826,
                0.24211634917615787,
                2.9101193915924366,
                16.36954033244036,
                21.38774652665772,
                21.08244665601231,
                '9883.91,10200.710200.7;1542.811542.81,1063.961063.96,870.771870.771,561.308561.308,1002.181002.18,146.987146.987',  # noqa: 501
            ),
            'EATFGVDESANK': (
                1408.4575782992852,
                249.5239806225419,
                0.2040521922843587,
                2.6315964587561163,
                16.401371332298684,
                20.921098847582904,
                21.21558604800942,
                '11118.2,7094.927094.92;1194.611194.61,753.409753.409,402.302402.302,315.828315.828,281.613281.613',  # noqa: 501
            ),
            'LAAEESVGTMGNR': (
                640.527566555166,
                193.01385582922805,
                0.18161965447526804,
                3.0392587178097727,
                14.878875697138138,
                19.379535026136153,
                21.314838783590975,
                '5528.18,1455.781455.78;645.542645.542,393.809393.809,247.588247.588,207.769207.769,255.275255.275,159.312159.312',  # noqa: 501
            ),
            'TPVISGGPYYER': (
                13335.645154056563,
                3232.360347672976,
                0.1994586611481954,
                2.672529497680725,
                16.13633492880202,
                21.369920294016026,
                21.123719062667412,
                '95438.8,69738.769738.7;13465.613465.6,8649.098649.09,3215.993215.99,5117.595117.59,1846.141846.14,1415.581415.58',  # noqa: 501
            ),
            'TLEEDEEELFK': (
                11542.835493639752,
                1334.076737060385,
                0.2278137942084868,
                2.6985761175307577,
                16.25026469037657,
                20.412533986087883,
                20.983848552855463,
                '82869.6,56749.856749.8;5753.815753.81,3258.283258.28,2385.332385.33,1446.231446.23,200.723200.723,160.68160.68',  # noqa: 501
            ),
            'VQVALEELQDLK': (
                1448.1158573958178,
                242.93041179302435,
                0.2482168150624772,
                8.69164710695146,
                15.817200283026608,
                15.726542273578746,
                21.13877718604927,
                '3263.27,2383.232383.23;184.372184.372,167.647167.647,174.645174.645,160.872160.872,92.437492.4374',  # noqa: 501
            ),
            'GDLDAASYYAPVR': (
                7664.521052669274,
                1927.778535112678,
                0.20280008728517657,
                2.647603309848523,
                16.173346580021054,
                21.04062767840509,
                21.475871449609702,
                '52598.4,41199.141199.1;6268.296268.29,4489.614489.61,2476.332476.33,2850.532850.53,3093.053093.05,2118.762118.76',  # noqa: 501
            ),
            'DAVTPADFSEWSK': (
                8230.961097592291,
                991.0828086775997,
                0.21764158716379642,
                3.0235250613746745,
                17.383056565184397,
                20.94793932962864,
                20.612161860716355,
                '47204.5,3959839598;4109.454109.45,1102.541102.54,1513.561513.56,1178.491178.49,937.997937.997',  # noqa: 501
            ),
            'GILAAEESVGTMGNR': (
                4804.0134195311275,
                1101.525153840663,
                0.20594011876069473,
                2.8993482944355784,
                16.34373362130983,
                20.810376667868454,
                21.109460219926103,
                '27926.6,23829.823829.8;2050.522050.52,1233.741233.74,2580.542580.54,1775.461775.46,1679.851679.85,1358.541358.54',  # noqa: 501
            ),
            'SPFTVGVAAPLDLSK': (
                580.4259312698803,
                126.41925839454709,
                0.1978857468460366,
                6.665121857148582,
                15.983529625155317,
                16.54777972570456,
                20.468511549736203,
                '1590.26,1149.241149.24;240.602240.602,120.031120.031,70.73570.735,48.248948.2489,33.15633.156,10.928710.9287',  # noqa: 501
            ),
        }

    def test_model_eval_1(self):
        peptide = 'ELHINLIPNKQDR'
        model = requant.RequantModel()
        ms2_name, pg = self.peak_groups[peptide]

        ms2_swath_map = self.swath_run.loadSwathMapPtr(ms2_name)
        r = model.eval(self.ms1_swath_map, ms2_swath_map, pg)
        logging.info(r)

        e = self.expected_intensities[peptide]
        self.assertEqual(pg.modifiedSequence(), peptide)
        self.assertEqual(pg.modifiedSequence(), r[0])
        self.assertEqual(pg.charge(), r[1])
        self.assertEqual(pg.rank(), r[2])
        self.assertAlmostEqual(e[0], r[3], places=3)
        self.assertAlmostEqual(e[1], r[4], places=3)
        self.assertAlmostEqual(e[2], r[5], places=3)
        self.assertAlmostEqual(e[3], r[6], places=3)
        self.assertAlmostEqual(e[4], r[7], places=3)
        self.assertAlmostEqual(e[5], r[8], places=3)
        self.assertAlmostEqual(e[6], r[9], places=3)
        self.assertEqual(e[7], r[10])

    def test_quantifier_eval(self):
        quantifier = requant.Requantifier()
        peak_groups = {}
        for _, (ms2_name, pg) in self.peak_groups.items():
            if ms2_name not in peak_groups:
                peak_groups[ms2_name] = []
            peak_groups[ms2_name].append(pg)

        result = quantifier.eval(self.swath_run, peak_groups)
        logging.info(result)
        # for r in result:
        #     print(f"'{r[0]}': (")
        #     print(f"    {r[3]},")
        #     print(f"    {r[4]},")
        #     print(f"    {r[5]},")
        #     print(f"    {r[6]},")
        #     print(f"    {r[7]},")
        #     print(f"    {r[8]},")
        #     print(f"    {r[9]},")
        #     print(f"    '{r[10]}',  # noqa: 501")
        #     print(f"),")
        self.assertEqual(len(self.peak_groups), len(result))
        for r in result:
            peptide = r[0]
            _, pg = self.peak_groups[peptide]
            e = self.expected_intensities[peptide]
            self.assertEqual(pg.modifiedSequence(), peptide)
            self.assertEqual(pg.charge(), r[1])
            self.assertEqual(pg.rank(), r[2])
            self.assertAlmostEqual(e[0], r[3], places=3)
            self.assertAlmostEqual(e[1], r[4], places=3)
            self.assertAlmostEqual(e[2], r[5], places=3)
            self.assertAlmostEqual(e[3], r[6], places=3)
            self.assertAlmostEqual(e[4], r[7], places=3)
            self.assertAlmostEqual(e[5], r[8], places=3)
            self.assertAlmostEqual(e[6], r[9], places=3)
            self.assertEqual(e[7], r[10])


if __name__ == '__main__':
    import unittest
    unittest.main()
