"""
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import unittest

from toffee import requant


class TestRequantPeakGroup(unittest.TestCase):
    def test_ctor(self):
        seq = 'AAGYELGK'
        charge = 2
        rank = 1
        rt = 1452.98
        mz = 615.2
        fragment_mz = [681.0, 836.63, 926.269]

        pg = requant.RequantPeakGroup(seq, charge, rank, rt, mz, fragment_mz)
        self.assertEqual(seq, pg.modifiedSequence())
        self.assertEqual(charge, pg.charge())
        self.assertEqual(rank, pg.rank())
        self.assertEqual(rt, pg.retentionTime())
        self.assertEqual(mz, pg.massOverCharge())
        self.assertListEqual(fragment_mz, pg.fragmentMassOverCharges())


if __name__ == '__main__':
    import unittest
    unittest.main()
