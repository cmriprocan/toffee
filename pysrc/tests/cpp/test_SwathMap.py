"""
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import numpy as np

import scipy.sparse

import toffee

from ..helper import HelperCreateTestToffeeFile


class _BaseTestSwathMap(HelperCreateTestToffeeFile):
    IMS_TYPE = None
    __test__ = False

    @classmethod
    def setUpClass(cls):
        cls.create_simple_tof_file(name=cls.__name__, ims_type=cls.IMS_TYPE)
        cls.swath_run = toffee.SwathRun(cls.tof_fname)
        cls.swath_maps = {name: cls.swath_run.loadSwathMap(name) for name in [cls.ms1_name, cls.ms2_name]}

    def test_map_properties(self):
        ms = self.swath_maps[self.ms1_name]
        rt_vector = self.ms1_rt_vector
        self.assertEqual(self.ms1_name, ms.name())
        self.assertTrue(ms.isMS1())
        self.assertEqual(-1, ms.precursorLowerMz())
        self.assertEqual(-1, ms.precursorCenterMz())
        self.assertEqual(-1, ms.precursorUpperMz())
        self.assertAlmostEqual(self.scan_cycle_time, ms.scanCycleTime())
        self.assertAlmostEqual(self.ms1_rt_offset, ms.firstScanRetentionTimeOffset())
        np.testing.assert_allclose(rt_vector, ms.retentionTime())
        self.assertEqual(rt_vector.size, ms.numberOfSpectra())
        np.testing.assert_allclose(self.mz_vector, ms.massOverCharge())

        ms = self.swath_maps[self.ms2_name]
        rt_vector = self.ms2_rt_vector
        self.assertEqual(self.ms2_name, ms.name())
        self.assertFalse(ms.isMS1())
        self.assertEqual(self.window_lower, ms.precursorLowerMz())
        self.assertEqual(self.window_center, ms.precursorCenterMz())
        self.assertEqual(self.window_upper, ms.precursorUpperMz())
        self.assertAlmostEqual(self.scan_cycle_time, ms.scanCycleTime())
        self.assertAlmostEqual(self.ms2_rt_offset, ms.firstScanRetentionTimeOffset())
        np.testing.assert_allclose(rt_vector, ms.retentionTime())
        self.assertEqual(rt_vector.size, ms.numberOfSpectra())
        np.testing.assert_allclose(self.mz_vector, ms.massOverCharge())

    def test_lossyAdjustIMSCoords(self):  # noqa: N802
        mz_range = toffee.MassOverChargeRange(self.mz_vector[0], self.mz_vector[-1])
        for name in [self.ms1_name, self.ms2_name]:
            swath_map = self.swath_maps[name]
            lossy_swath_map = self.swath_run.loadSwathMap(name, lossyAdjustIMSCoords=True)
            self._chromatogram_assert_allclose(
                swath_map.extractedIonChromatogramSparse(mz_range),
                lossy_swath_map.extractedIonChromatogramSparse(mz_range),
            )

    # ================================
    # extractedIonChromatogram
    # ================================

    def test_extractedIonChromatogram_ms1(self):  # noqa: N802
        ms = self.swath_maps[self.ms1_name]

        self._base_test_extractedIonChromatogram(
            ms,
            self.ms1_rt_vector,
            self.expected_intensities_ms1,
            self.mz_vector.size // 4,
        )
        self._base_test_extractedIonChromatogram(
            ms,
            self.ms1_rt_vector,
            self.expected_intensities_ms1,
            self.mz_vector.size // 2,
        )
        self._base_test_extractedIonChromatogram(
            ms,
            self.ms1_rt_vector,
            self.expected_intensities_ms1,
            3 * self.mz_vector.size // 4,
        )
        np.random.seed(1)
        half_width = 25
        for mz_idx in np.random.uniform(half_width, self.mz_vector.size - half_width, size=100):
            self._base_test_extractedIonChromatogram(
                ms,
                self.ms1_rt_vector,
                self.expected_intensities_ms1,
                int(mz_idx),
                half_width=half_width,
            )

        for mz_outside_range in [0.5 * self.mz_vector[0], 1.5 * self.mz_vector[-1]]:
            self._base_test_extractedIonChromatogram(
                ms,
                self.ms1_rt_vector,
                self.expected_intensities_ms1,
                mz_idx=None,
                mz=mz_outside_range,
                half_width=3,
            )

    def test_extractedIonChromatogram_ms2(self):  # noqa: N802
        ms = self.swath_maps[self.ms2_name]
        self._base_test_extractedIonChromatogram(
            ms,
            self.ms2_rt_vector,
            self.expected_intensities_ms2,
            self.mz_vector.size // 4,
        )
        self._base_test_extractedIonChromatogram(
            ms,
            self.ms2_rt_vector,
            self.expected_intensities_ms2,
            self.mz_vector.size // 2,
        )
        self._base_test_extractedIonChromatogram(
            ms,
            self.ms2_rt_vector,
            self.expected_intensities_ms2,
            3 * self.mz_vector.size // 4,
        )
        np.random.seed(2)
        half_width = 25
        for mz_idx in np.random.uniform(half_width, self.mz_vector.size - half_width, size=100):
            self._base_test_extractedIonChromatogram(
                ms,
                self.ms2_rt_vector,
                self.expected_intensities_ms2,
                int(mz_idx),
                half_width=half_width,
            )

        for mz_outside_range in [0.5 * self.mz_vector[0], 1.5 * self.mz_vector[-1]]:
            self._base_test_extractedIonChromatogram(
                ms,
                self.ms2_rt_vector,
                self.expected_intensities_ms2,
                mz_idx=None,
                mz=mz_outside_range,
                half_width=3,
            )

    def _base_test_extractedIonChromatogram(  # noqa: N802
        self,
        ms,
        rt_vector,
        expected_intensities,
        mz_idx,
        mz=None,
        half_width=25,
    ):
        if mz is None:
            self.assertIsNotNone(mz_idx)
            mz = self.mz_vector[mz_idx]
            outside = False
        else:
            self.assertIsNone(mz_idx)
            outside = mz < self.mz_vector[0] or mz > self.mz_vector[-1]

        mz_range = toffee.MassOverChargeRangeWithPixelHalfWidth(mz, half_width)
        chrom = ms.extractedIonChromatogram(mz_range)

        full_width = mz_range.pixelFullWidth()
        self.assertTrue(full_width % 2 == 0)
        self.assertEqual(2 * half_width, full_width)

        if outside:
            self.assertEqual(1, chrom.massOverCharge.size)
        else:
            self.assertEqual(full_width + 1, chrom.massOverCharge.size)
        self.assertEqual(chrom.intensities.shape[0], chrom.retentionTime.size)
        self.assertEqual(chrom.intensities.shape[1], chrom.massOverCharge.size)

        np.testing.assert_allclose(chrom.retentionTime, rt_vector)

        if outside:
            if mz < self.mz_vector[0]:
                expected_slice = slice(None, 1)
            else:
                expected_slice = slice(-1, None)
        else:
            mz_idx = np.searchsorted(self.mz_vector, mz) if mz_idx is None else mz_idx
            expected_slice = slice(mz_idx - half_width, mz_idx + half_width + 1)
        np.testing.assert_allclose(chrom.massOverCharge, self.mz_vector[expected_slice])
        np.testing.assert_allclose(chrom.intensities, expected_intensities[:, expected_slice])

    # ================================
    # filteredExtractedIonChromatogram
    # ================================

    def test_filteredExtractedIonChromatogram_ms1(self):  # noqa: N802
        ms = self.swath_maps[self.ms1_name]
        self._base_test_filteredExtractedIonChromatogram(
            ms,
            self.ms1_rt_vector,
            self.expected_intensities_ms1,
            self.ms1_rt_vector.size // 4,
            self.mz_vector.size // 4,
        )
        self._base_test_filteredExtractedIonChromatogram(
            ms,
            self.ms1_rt_vector,
            self.expected_intensities_ms1,
            self.ms1_rt_vector.size // 4,
            self.mz_vector.size // 2,
        )
        self._base_test_filteredExtractedIonChromatogram(
            ms,
            self.ms1_rt_vector,
            self.expected_intensities_ms1,
            self.ms1_rt_vector.size // 4,
            3 * self.mz_vector.size // 4,
        )
        np.random.seed(1)
        half_width = 25
        for rt_idx in np.random.uniform(half_width, self.ms1_rt_vector.size - half_width, size=20):
            for mz_idx in np.random.uniform(half_width, self.mz_vector.size - half_width, size=20):
                self._base_test_filteredExtractedIonChromatogram(
                    ms,
                    self.ms1_rt_vector,
                    self.expected_intensities_ms1,
                    int(rt_idx),
                    int(mz_idx),
                    half_width=half_width,
                )

    def test_filteredExtractedIonChromatogram_ms2(self):  # noqa: N802
        ms = self.swath_maps[self.ms2_name]
        self._base_test_filteredExtractedIonChromatogram(
            ms,
            self.ms2_rt_vector,
            self.expected_intensities_ms2,
            self.ms2_rt_vector.size // 4,
            self.mz_vector.size // 4,
        )
        self._base_test_filteredExtractedIonChromatogram(
            ms,
            self.ms2_rt_vector,
            self.expected_intensities_ms2,
            self.ms2_rt_vector.size // 4,
            self.mz_vector.size // 2,
        )
        self._base_test_filteredExtractedIonChromatogram(
            ms,
            self.ms2_rt_vector,
            self.expected_intensities_ms2,
            self.ms2_rt_vector.size // 4,
            3 * self.mz_vector.size // 4,
        )
        np.random.seed(2)
        half_width = 25
        for rt_idx in np.random.uniform(half_width, self.ms2_rt_vector.size - half_width, size=20):
            for mz_idx in np.random.uniform(half_width, self.mz_vector.size - half_width, size=20):
                self._base_test_filteredExtractedIonChromatogram(
                    ms,
                    self.ms2_rt_vector,
                    self.expected_intensities_ms2,
                    int(rt_idx),
                    int(mz_idx),
                    half_width=half_width,
                )

    def _base_test_filteredExtractedIonChromatogram(  # noqa: N802
        self,
        ms,
        rt_vector,
        expected_intensities,
        rt_idx,
        mz_idx,
        half_width=25,
    ):
        mz_range = toffee.MassOverChargeRangeWithPixelHalfWidth(self.mz_vector[mz_idx], half_width)
        rt_range = toffee.RetentionTimeRangeWithPixelHalfWidth(rt_vector[rt_idx], half_width)
        chrom = ms.filteredExtractedIonChromatogram(mz_range, rt_range)

        full_width = mz_range.pixelFullWidth()
        self.assertEqual(full_width, rt_range.pixelFullWidth())
        self.assertTrue(full_width % 2 == 0)

        self.assertEqual(full_width + 1, chrom.retentionTime.size)
        self.assertEqual(full_width + 1, chrom.massOverCharge.size)
        self.assertEqual(chrom.intensities.shape[0], chrom.retentionTime.size)
        self.assertEqual(chrom.intensities.shape[1], chrom.massOverCharge.size)

        expected_rt_slice = slice(rt_idx - half_width, rt_idx + half_width + 1)
        expected_mz_slice = slice(mz_idx - half_width, mz_idx + half_width + 1)
        np.testing.assert_allclose(chrom.massOverCharge, self.mz_vector[expected_mz_slice])
        np.testing.assert_allclose(chrom.retentionTime, rt_vector[expected_rt_slice])
        np.testing.assert_allclose(chrom.intensities, expected_intensities[expected_rt_slice, expected_mz_slice])

    # ================================
    # extractedIonChromatogramSparse
    # ================================

    def test_extractedIonChromatogramSparse_ms1(self):  # noqa: N802
        ms = self.swath_maps[self.ms1_name]
        self._base_test_extractedIonChromatogramSparse(
            ms,
            self.ms1_rt_vector,
            self.expected_intensities_ms1,
            self.mz_vector.size // 4,
        )
        self._base_test_extractedIonChromatogramSparse(
            ms,
            self.ms1_rt_vector,
            self.expected_intensities_ms1,
            self.mz_vector.size // 2,
        )
        self._base_test_extractedIonChromatogramSparse(
            ms,
            self.ms1_rt_vector,
            self.expected_intensities_ms1,
            3 * self.mz_vector.size // 4,
        )

    def test_extractedIonChromatogramSparse_ms2(self):  # noqa: N802
        ms = self.swath_maps[self.ms2_name]
        self._base_test_extractedIonChromatogramSparse(
            ms,
            self.ms2_rt_vector,
            self.expected_intensities_ms2,
            self.mz_vector.size // 4,
        )
        self._base_test_extractedIonChromatogramSparse(
            ms,
            self.ms2_rt_vector,
            self.expected_intensities_ms2,
            self.mz_vector.size // 2,
        )
        self._base_test_extractedIonChromatogramSparse(
            ms,
            self.ms2_rt_vector,
            self.expected_intensities_ms2,
            3 * self.mz_vector.size // 4,
        )

    def _base_test_extractedIonChromatogramSparse(self, ms, rt_vector, expected_intensities, mz_idx):  # noqa: N802
        half_width = 25
        mz_range = toffee.MassOverChargeRangeWithPixelHalfWidth(self.mz_vector[mz_idx], half_width)
        chrom = ms.extractedIonChromatogramSparse(mz_range)

        full_width = mz_range.pixelFullWidth()
        self.assertTrue(full_width % 2 == 0)
        self.assertEqual(2 * half_width, full_width)

        self.assertEqual(rt_vector.size, chrom.retentionTime.size)
        self.assertEqual(full_width + 1, chrom.massOverCharge.size)
        self.assertEqual(chrom.intensities.shape[0], chrom.retentionTime.size)
        self.assertEqual(chrom.intensities.shape[1], chrom.massOverCharge.size)

        expected_slice = slice(mz_idx - half_width, mz_idx + half_width + 1)
        np.testing.assert_allclose(chrom.massOverCharge, self.mz_vector[expected_slice])
        np.testing.assert_allclose(chrom.retentionTime, rt_vector)

        self._sparse_assert_allclose(
            chrom.intensities,
            scipy.sparse.csc_matrix(expected_intensities[:, expected_slice]),
        )


class TestSwathMapTOF(_BaseTestSwathMap):
    IMS_TYPE = toffee.IntrinsicMassSpacingType.TOF
    __test__ = True


class TestSwathMapOrbitrap(_BaseTestSwathMap):
    IMS_TYPE = toffee.IntrinsicMassSpacingType.ORBITRAP
    __test__ = True


if __name__ == '__main__':
    import unittest
    unittest.main()
