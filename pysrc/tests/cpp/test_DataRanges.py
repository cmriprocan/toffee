"""
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import numpy as np

import toffee

from ..helper import HelperCreateTestToffeeFile


class _BaseMassOverChargeRange(HelperCreateTestToffeeFile):
    IMS_TYPE = None
    __test__ = False

    @classmethod
    def setUpClass(cls):
        cls.create_simple_tof_file(name=cls.__name__, ims_type=cls.IMS_TYPE)
        cls.swath_run = toffee.SwathRun(cls.tof_fname)

    def test_MassOverChargeRangeWithPixelWidth(self):  # noqa: N802
        ms = self.swath_run.loadSwathMap(self.ms1_name)
        mz_transformer = ms.getMzTransformer()
        mz_vector = ms.massOverCharge()
        half_width = 10
        full_width = 2 * half_width

        # middle of range
        for i, mz in enumerate(mz_vector[half_width:-half_width - 1]):
            mz_range = toffee.MassOverChargeRangeWithPixelHalfWidth(mz, half_width)
            self.assertEqual(full_width, mz_range.pixelFullWidth())
            lower = mz_range.lowerMzInIMSCoords(mz_transformer)
            upper = mz_range.upperMzInIMSCoords(mz_transformer)
            self.assertEqual(full_width, upper - lower)
            self.assertEqual(self.ims_coord_vector[i], lower)
            self.assertEqual(
                self.ims_coord_vector[i + full_width],
                upper,
            )

    def test_MassOverChargeRangeWithPPMWidth(self):  # noqa: N802
        alpha, beta, gamma = self.ims_alpha, self.ims_beta, self.ims_gamma
        mz_transformer = self.mass_over_charge_transformer(self.IMS_TYPE, alpha, beta, gamma)

        ppm = 1e4

        mz = 1400.0
        mz_range = toffee.MassOverChargeRangeWithPPMFullWidth(mz, ppm)

        self.assertAlmostEqual(
            mz * (1 - 0.5 * 1e-6 * ppm),
            mz_range.massOverChargeLower(),
        )
        self.assertAlmostEqual(
            mz * (1 + 0.5 * 1e-6 * ppm),
            mz_range.massOverChargeUpper(),
        )

        lower = mz_range.lowerMzInIMSCoords(mz_transformer)
        upper = mz_range.upperMzInIMSCoords(mz_transformer)
        self.assertEqual(
            self.test_MassOverChargeRangeWithPPMWidth_delta,
            upper - lower,
        )
        self.assertEqual(
            self.test_MassOverChargeRangeWithPPMWidth_lower,
            lower,
        )
        self.assertEqual(
            self.test_MassOverChargeRangeWithPPMWidth_upper,
            upper,
        )

    def test_MassOverChargeRange(self):  # noqa: N802
        alpha, beta, gamma = self.ims_alpha, self.ims_beta, self.ims_gamma
        mz_transformer = self.mass_over_charge_transformer(self.IMS_TYPE, alpha, beta, gamma)

        mz_range = toffee.MassOverChargeRange(
            self.mz_vector[0],
            self.mz_vector[-1],
        )

        self.assertEqual(
            self.mz_vector[0],
            mz_range.massOverChargeLower(),
        )
        self.assertEqual(
            self.mz_vector[-1],
            mz_range.massOverChargeUpper(),
        )

        lower = mz_range.lowerMzInIMSCoords(mz_transformer)
        upper = mz_range.upperMzInIMSCoords(mz_transformer)
        self.assertEqual(
            self.mz_vector.size - 1,
            upper - lower,
        )
        self.assertEqual(
            self.ims_coord_vector[0],
            lower,
        )
        self.assertEqual(
            self.ims_coord_vector[-1],
            upper,
        )

    def test_MassOverChargeRangeIMSCoords(self):  # noqa: N802
        alpha, beta, gamma = self.ims_alpha, self.ims_beta, self.ims_gamma
        mz_transformer = self.mass_over_charge_transformer(self.IMS_TYPE, alpha, beta, gamma)

        lower, upper = 0, self.mz_vector.shape[0] - 1
        mz_range = toffee.MassOverChargeRangeIMSCoords(
            lower,
            upper,
        )
        self.assertEqual(
            lower,
            mz_range.lowerMzInIMSCoords(mz_transformer),
        )
        self.assertEqual(
            upper,
            mz_range.upperMzInIMSCoords(mz_transformer),
        )


class TestMassOverChargeRangeTOF(_BaseMassOverChargeRange):
    IMS_TYPE = toffee.IntrinsicMassSpacingType.TOF
    __test__ = True

    # regression test values
    test_MassOverChargeRangeWithPPMWidth_delta = 5  # noqa: N815
    test_MassOverChargeRangeWithPPMWidth_lower = 776  # noqa: N815
    test_MassOverChargeRangeWithPPMWidth_upper = 781  # noqa: N815


class TestMassOverChargeRangeOrbitrap(_BaseMassOverChargeRange):
    IMS_TYPE = toffee.IntrinsicMassSpacingType.ORBITRAP
    __test__ = True

    # regression test values
    test_MassOverChargeRangeWithPPMWidth_delta = 1  # noqa: N815
    test_MassOverChargeRangeWithPPMWidth_lower = 951  # noqa: N815
    test_MassOverChargeRangeWithPPMWidth_upper = 952  # noqa: N815


class TestRetentionTimeRange(HelperCreateTestToffeeFile):
    @classmethod
    def setUpClass(cls):
        cls.create_simple_tof_file(name=cls.__name__)
        cls.swath_run = toffee.SwathRun(cls.tof_fname)

    def test_toRTIdx1(self):  # noqa: N802
        rt_vector = [1.0, 2.0]
        self.assertEqual(
            0,
            toffee.IRetentionTimeRange.toRTIdx(0.9, rt_vector),
        )
        self.assertEqual(
            0,
            toffee.IRetentionTimeRange.toRTIdx(1.0, rt_vector),
        )
        self.assertEqual(
            0,
            toffee.IRetentionTimeRange.toRTIdx(1.1, rt_vector),
        )
        self.assertEqual(
            0,
            toffee.IRetentionTimeRange.toRTIdx(np.nextafter(1.5, 0), rt_vector),
        )
        self.assertEqual(
            1,
            toffee.IRetentionTimeRange.toRTIdx(1.5, rt_vector),
        )
        self.assertEqual(
            1,
            toffee.IRetentionTimeRange.toRTIdx(1.75, rt_vector),
        )
        self.assertEqual(
            1,
            toffee.IRetentionTimeRange.toRTIdx(2.0, rt_vector),
        )
        self.assertEqual(
            1,
            toffee.IRetentionTimeRange.toRTIdx(2.1, rt_vector),
        )

    def test_toRTIdx2(self):  # noqa: N802
        ms = self.swath_run.loadSwathMap(self.ms1_name)
        rt_vector = ms.retentionTime()
        for expected_idx, rt in enumerate(rt_vector):
            self.assertEqual(
                expected_idx,
                toffee.IRetentionTimeRange.toRTIdx(rt, rt_vector),
            )
            self.assertEqual(
                expected_idx,
                toffee.IRetentionTimeRange.toRTIdx(
                    np.nextafter(rt, -np.finfo(float).max),
                    rt_vector,
                ),
            )
            self.assertEqual(
                expected_idx,
                toffee.IRetentionTimeRange.toRTIdx(
                    np.nextafter(rt, np.finfo(float).max),
                    rt_vector,
                ),
            )

    def test_RetentionTimeRange(self):  # noqa: N802
        ms = self.swath_run.loadSwathMap(self.ms1_name)
        rt_vector = ms.retentionTime()
        last_idx = len(rt_vector) - 1

        # full range
        rt_range = toffee.RetentionTimeRange(rt_vector[0], rt_vector[-1])
        self.assertEqual(0, rt_range.lowerRTIdx(rt_vector))
        self.assertEqual(last_idx, rt_range.upperRTIdx(rt_vector))

        # mid range
        for i in range(10):
            top = i * last_idx // 10
            bot = top // 2
            rt_range = toffee.RetentionTimeRange(rt_vector[bot], rt_vector[top])
            self.assertEqual(bot, rt_range.lowerRTIdx(rt_vector))
            self.assertEqual(top, rt_range.upperRTIdx(rt_vector))

    def test_RetentionTimeRangeWithPixelWidth(self):  # noqa: N802
        ms = self.swath_run.loadSwathMap(self.ms1_name)
        rt_vector = ms.retentionTime()
        last_idx = len(rt_vector) - 1

        half_width = 10
        full_width = 2 * half_width

        # start of range
        rt_range = toffee.RetentionTimeRangeWithPixelHalfWidth(rt_vector[0], half_width)
        self.assertEqual(full_width, rt_range.pixelFullWidth())
        self.assertEqual(0, rt_range.lowerRTIdx(rt_vector))
        self.assertEqual(full_width, rt_range.upperRTIdx(rt_vector))

        rt_range = toffee.RetentionTimeRangeWithPixelHalfWidth(rt_vector[half_width], half_width)
        self.assertEqual(full_width, rt_range.pixelFullWidth())
        self.assertEqual(0, rt_range.lowerRTIdx(rt_vector))
        self.assertEqual(full_width, rt_range.upperRTIdx(rt_vector))

        # end of range
        rt_range = toffee.RetentionTimeRangeWithPixelHalfWidth(rt_vector[-1], half_width)
        self.assertEqual(full_width, rt_range.pixelFullWidth())
        self.assertEqual(last_idx - full_width, rt_range.lowerRTIdx(rt_vector))
        self.assertEqual(last_idx, rt_range.upperRTIdx(rt_vector))

        rt_range = toffee.RetentionTimeRangeWithPixelHalfWidth(rt_vector[-half_width], half_width)
        self.assertEqual(full_width, rt_range.pixelFullWidth())
        self.assertEqual(last_idx - full_width, rt_range.lowerRTIdx(rt_vector))
        self.assertEqual(last_idx, rt_range.upperRTIdx(rt_vector))

        # middle of range
        for rt in rt_vector[half_width:-half_width]:
            rt_range = toffee.RetentionTimeRangeWithPixelHalfWidth(rt, half_width)
            self.assertEqual(full_width, rt_range.pixelFullWidth())
            self.assertEqual(
                full_width,
                rt_range.upperRTIdx(rt_vector) - rt_range.lowerRTIdx(rt_vector),
            )


if __name__ == '__main__':
    import unittest
    unittest.main()
