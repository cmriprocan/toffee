"""
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import os
import unittest

import h5py

import numpy as np

import toffee


class TestMassAccuracyWriter(unittest.TestCase):
    def test_statics(self):
        self.assertEqual(
            'original',
            toffee.MassAccuracyWriter.DATASET_ORIGINAL,
        )
        self.assertEqual(
            'convertedLossless',
            toffee.MassAccuracyWriter.DATASET_CONVERTED_LOSSLESS,
        )
        self.assertEqual(
            'convertedLossy',
            toffee.MassAccuracyWriter.DATASET_CONVERTED_LOSSY,
        )
        self.assertEqual(
            'retentionTime',
            toffee.MassAccuracyWriter.DATASET_RETENTION_TIME,
        )

    def test_dummy_file(self):
        fname = self.__class__.__name__ + '-test_dummy_file.h5'
        writer = toffee.MassAccuracyWriter(fname)

        np.random.seed(0)
        num_pts = 10
        original = np.random.uniform(size=num_pts)
        converted_lossless = np.random.uniform(size=num_pts)
        converted_lossy = np.random.uniform(size=num_pts)
        rt = np.random.uniform(size=num_pts)
        window_name = 'test-name'

        writer.addWindowData(
            window_name,
            original,
            converted_lossless,
            converted_lossy,
            rt,
        )

        with h5py.File(fname, 'r') as f:
            grp = f[window_name]
            np.testing.assert_array_equal(
                original,
                grp[toffee.MassAccuracyWriter.DATASET_ORIGINAL],
            )
            np.testing.assert_array_equal(
                converted_lossless,
                grp[toffee.MassAccuracyWriter.DATASET_CONVERTED_LOSSLESS],
            )
            np.testing.assert_array_equal(
                converted_lossy,
                grp[toffee.MassAccuracyWriter.DATASET_CONVERTED_LOSSY],
            )
            np.testing.assert_array_equal(
                rt,
                grp[toffee.MassAccuracyWriter.DATASET_RETENTION_TIME],
            )

        if os.path.isfile(fname):
            os.remove(fname)


if __name__ == '__main__':
    import unittest
    unittest.main()
