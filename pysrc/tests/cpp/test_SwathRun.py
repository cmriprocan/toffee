"""
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import toffee

from ..helper import HelperCreateTestToffeeFile


class TestSwathRun(HelperCreateTestToffeeFile):
    @classmethod
    def setUpClass(cls):
        cls.create_simple_tof_file(name=cls.__name__)

    def test_constructor(self):
        swath_run = toffee.SwathRun(self.tof_fname)

        self.assertEqual(self.header, swath_run.header())
        self.assertEqual(1, swath_run.formatMajorVersion())
        self.assertEqual(2, swath_run.formatMinorVersion())
        self.assertEqual(self.ims_type, swath_run.imsType())

        self.assertTrue(swath_run.hasMS1Data())
        windows = swath_run.ms2Windows()
        self.assertEqual(1, len(windows))
        w = windows[0]
        self.assertEqual(self.ms2_name, w.name)
        self.assertEqual(self.window_lower, w.lower)
        self.assertEqual(self.window_upper, w.upper)

        ms2_map = swath_run.mapPrecursorsToMS2Names(
            [self.window_lower, self.window_center, self.window_upper],
            0.5 * (self.window_center - self.window_lower),
            0.5 * (self.window_upper - self.window_center),
        )
        self.assertDictEqual(
            {self.window_center: self.ms2_name},
            ms2_map,
        )


if __name__ == '__main__':
    import unittest
    unittest.main()
