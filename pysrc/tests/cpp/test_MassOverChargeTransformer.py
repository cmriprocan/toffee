"""
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import numpy as np

import toffee

from ..helper import HelperCreateTestToffeeFile


class _BaseMassOverChargeTransformer(HelperCreateTestToffeeFile):
    IMS_TYPE = None
    __test__ = False

    @classmethod
    def setUpClass(cls):
        cls.create_simple_tof_file(name=cls.__name__, ims_type=cls.IMS_TYPE)
        cls.swath_run = toffee.SwathRun(cls.tof_fname)
        cls.swath_maps = {name: cls.swath_run.loadSwathMap(name) for name in [cls.ms1_name, cls.ms2_name]}

    def test_transformer_creation_ms1(self):
        self._base_test_transformer_creation(self.ms1_name)

    def test_transformer_creation_ms2(self):
        self._base_test_transformer_creation(self.ms2_name)

    def _base_test_transformer_creation(self, name):
        ms = self.swath_maps[name]
        mz_transformer = ms.getMzTransformer()
        self.assertAlmostEqual(self.ims_alpha, mz_transformer.imsAlpha())
        self.assertAlmostEqual(self.ims_beta, mz_transformer.imsBeta())
        self.assertAlmostEqual(self.ims_gamma, mz_transformer.imsGamma())
        self.assertAlmostEqual(self.mz_vector[0], mz_transformer.lowerMzInWorldCoords())
        self.assertAlmostEqual(self.mz_vector[-1], mz_transformer.upperMzInWorldCoords())
        self.assertEqual(self.ims_coord_vector[0], mz_transformer.lowerMzInIMSCoords())
        self.assertEqual(self.ims_coord_vector[-1], mz_transformer.upperMzInIMSCoords())
        for expected_mz, expected_root_mz_idx in zip(
            self.mz_vector,
            self.ims_coord_vector,
        ):
            self.assertAlmostEqual(expected_mz, mz_transformer.toWorldCoords(expected_root_mz_idx))
            self.assertEqual(expected_root_mz_idx, mz_transformer.toIMSCoords(expected_mz))

    def test_idx_conversions_ms1(self):
        self._base_test_idx_conversions(self.ms1_name)

    def test_idx_conversions_ms2(self):
        self._base_test_idx_conversions(self.ms2_name)

    def _base_test_idx_conversions(self, name):
        ms = self.swath_maps[name]
        mz_transformer = ms.getMzTransformer()
        mz_vector = ms.massOverCharge()
        for i, mz in enumerate(mz_vector):
            expected_idx = int(self.ims_coord_vector[i])
            self.assertEqual(
                expected_idx,
                mz_transformer.toIMSCoords(mz),
            )
            self.assertEqual(
                expected_idx,
                mz_transformer.toIMSCoords(np.nextafter(mz, -np.finfo(np.float).max)),
            )
            self.assertEqual(
                expected_idx,
                mz_transformer.toIMSCoords(np.nextafter(mz, np.finfo(np.float).max)),
            )
            self.assertAlmostEqual(
                mz,
                mz_transformer.toWorldCoords(expected_idx),
            )


class TestMassOverChargeTransformerTof(_BaseMassOverChargeTransformer):
    IMS_TYPE = toffee.IntrinsicMassSpacingType.TOF
    __test__ = True


class TestMassOverChargeTransformerOrbitrap(_BaseMassOverChargeTransformer):
    IMS_TYPE = toffee.IntrinsicMassSpacingType.ORBITRAP
    __test__ = True


if __name__ == '__main__':
    import unittest
    unittest.main()
