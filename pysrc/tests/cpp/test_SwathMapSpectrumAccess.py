"""
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import os
import unittest

import numpy as np

import toffee

from ..helper import HelperCreateTestToffeeFile


class _BaseTestSwathMapSpectrumAccess(HelperCreateTestToffeeFile):
    IMS_TYPE = None
    __test__ = False

    @classmethod
    def setUpClass(cls):
        cls.create_simple_tof_file(name=cls.__name__, ims_type=cls.IMS_TYPE)
        cls.swath_run = toffee.SwathRun(cls.tof_fname)
        cls.swath_maps = {name: cls.swath_run.loadSwathMapSpectrumAccess(name) for name in [cls.ms1_name, cls.ms2_name]}

    def test_map_properties(self):
        ms = self.swath_maps[self.ms1_name]
        rt_vector = self.ms1_rt_vector
        self.assertEqual(self.ms1_name, ms.name())
        self.assertTrue(ms.isMS1())
        self.assertEqual(-1, ms.precursorLowerMz())
        self.assertEqual(-1, ms.precursorCenterMz())
        self.assertEqual(-1, ms.precursorUpperMz())
        self.assertAlmostEqual(self.scan_cycle_time, ms.scanCycleTime())
        self.assertAlmostEqual(self.ms1_rt_offset, ms.firstScanRetentionTimeOffset())
        np.testing.assert_allclose(rt_vector, ms.retentionTime())
        self.assertEqual(rt_vector.size, ms.numberOfSpectra())

        ms = self.swath_maps[self.ms2_name]
        rt_vector = self.ms2_rt_vector
        self.assertEqual(self.ms2_name, ms.name())
        self.assertFalse(ms.isMS1())
        self.assertEqual(self.window_lower, ms.precursorLowerMz())
        self.assertEqual(self.window_center, ms.precursorCenterMz())
        self.assertEqual(self.window_upper, ms.precursorUpperMz())
        self.assertAlmostEqual(self.scan_cycle_time, ms.scanCycleTime())
        self.assertAlmostEqual(self.ms2_rt_offset, ms.firstScanRetentionTimeOffset())
        np.testing.assert_allclose(rt_vector, ms.retentionTime())
        self.assertEqual(rt_vector.size, ms.numberOfSpectra())

    # ================================
    # spectrumByIndex
    # ================================

    def test_spectrumByIndex_ms1(self):  # noqa: N802
        ms = self.swath_maps[self.ms1_name]
        self._base_test_spectrumByIndex(ms, self.expected_intensities_ms1, self.ms1_rt_vector.size // 4)
        self._base_test_spectrumByIndex(ms, self.expected_intensities_ms1, self.ms1_rt_vector.size // 2)
        self._base_test_spectrumByIndex(ms, self.expected_intensities_ms1, 3 * self.ms1_rt_vector.size // 4)

        # check an empty scan still works
        empty_scans = self.expected_intensities_ms1.sum(axis=1) == 0
        self.assertEqual(empty_scans.size, self.ms1_rt_vector.size)
        empty_idx = np.argmax(empty_scans)
        self.assertTrue(empty_scans[empty_idx])
        self._base_test_spectrumByIndex(ms, self.expected_intensities_ms1, empty_idx)

        for i in range(0, self.ms1_rt_vector.size, 5):
            self._base_test_spectrumByIndex(ms, self.expected_intensities_ms1, i)

    def test_spectrumByIndex_ms2(self):  # noqa: N802
        ms = self.swath_maps[self.ms2_name]
        self._base_test_spectrumByIndex(ms, self.expected_intensities_ms2, self.ms2_rt_vector.size // 4)
        self._base_test_spectrumByIndex(ms, self.expected_intensities_ms2, self.ms2_rt_vector.size // 2)
        self._base_test_spectrumByIndex(ms, self.expected_intensities_ms2, 3 * self.ms2_rt_vector.size // 4)

        # check an empty scan still works
        empty_scans = self.expected_intensities_ms2.sum(axis=1) == 0
        self.assertEqual(empty_scans.size, self.ms2_rt_vector.size)
        empty_idx = np.argmax(empty_scans)
        self.assertTrue(empty_scans[empty_idx])
        self._base_test_spectrumByIndex(ms, self.expected_intensities_ms2, empty_idx)

        for i in range(0, self.ms2_rt_vector.size, 5):
            self._base_test_spectrumByIndex(ms, self.expected_intensities_ms2, i)

    def _base_test_spectrumByIndex(self, ms, expected_intensities, rt_idx):  # noqa: N802
        spectrum = ms.spectrumByIndex(rt_idx)
        expected = expected_intensities[rt_idx, :]
        mask = expected != 0
        expected_num_points = mask.sum()
        self.assertEqual(expected_num_points, spectrum.intensities.shape[0])
        np.testing.assert_allclose(spectrum.intensities, expected[mask])
        self.assertEqual(expected_num_points, spectrum.massOverCharge.shape[0])
        np.testing.assert_allclose(spectrum.massOverCharge, self.mz_vector[mask])


class TestSwathMapSpectrumAccessTOF(_BaseTestSwathMapSpectrumAccess):
    IMS_TYPE = toffee.IntrinsicMassSpacingType.TOF
    __test__ = True

    # ================================
    # spectrumByIndex
    # ================================

    def test_spectrumByIndex_procan90(self):  # noqa: N802
        specimen = 'ProCan90-M03-01.iRT'
        data_dir = 'ProCan90'
        self._base_test_spectrumByIndex_real_data_no_throw(specimen, data_dir)

    def test_spectrumByIndex_sgs2014(self):  # noqa: N802
        specimen = 'napedro_l120224_002_sw.iRT'
        data_dir = 'SwathGoldStandard'
        self._base_test_spectrumByIndex_real_data_no_throw(specimen, data_dir)

    def _base_test_spectrumByIndex_real_data_no_throw(self, specimen, data_dir):  # noqa: N802
        base_dir = os.environ.get('DIA_TEST_DATA_REPO', None)
        assert base_dir is not None
        tof_fname = base_dir + '/{}/tof/{}.tof'.format(data_dir, specimen)
        swath_run = toffee.SwathRun(tof_fname)

        def _run_test(ms):
            num_spectra = ms.numberOfSpectra()
            self.assertGreater(num_spectra, 0)
            for i in range(5):
                ms.spectrumByIndex(int(i * num_spectra / 5))

        ms = swath_run.loadSwathMapSpectrumAccess(toffee.ToffeeWriter.MS1_NAME)
        _run_test(ms)

        windows = swath_run.ms2Windows()
        for w in windows:
            ms = swath_run.loadSwathMapSpectrumAccess(w.name)
            _run_test(ms)


class TestSwathMapSpectrumAccessOrbitrap(_BaseTestSwathMapSpectrumAccess):
    IMS_TYPE = toffee.IntrinsicMassSpacingType.ORBITRAP
    __test__ = True


if __name__ == '__main__':
    unittest.main()
