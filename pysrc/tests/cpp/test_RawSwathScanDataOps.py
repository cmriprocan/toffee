"""
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import os

import h5py

import numpy as np

import toffee

from ..helper import DummyToffeeFile, HelperCreateTestToffeeFile


class _BaseRawSwathScanDataOps(HelperCreateTestToffeeFile):
    IMS_TYPE = None
    TRANSFORM_FUNC = None
    __test__ = False

    @classmethod
    def setUpClass(cls):
        assert cls.IMS_TYPE is not None
        if cls.IMS_TYPE == toffee.IntrinsicMassSpacingType.TOF:
            cls.ops = toffee.RawSwathScanDataOpsTOF()
        elif cls.IMS_TYPE == toffee.IntrinsicMassSpacingType.ORBITRAP:
            cls.ops = toffee.RawSwathScanDataOpsOrbitrap()
        else:
            raise ValueError('Upsupported IMS type: {}'.format(cls.IMS_TYPE))

    def test_applyTransform(self):  # noqa: N802
        self.assertIsNotNone(self.TRANSFORM_FUNC)
        np.random.seed(len(self.__class__.__name__))
        mz = np.random.uniform(100, 2000, size=1000)
        transformed = self.TRANSFORM_FUNC(mz)
        ops_transformed = [self.ops.applyIMSTransform(m) for m in mz]
        np.testing.assert_array_almost_equal(transformed, ops_transformed)

    def test_calculateIMSProperties_simple(self):  # noqa: N802
        lower = 90
        upper = 2100
        num_mz_points = 1000
        (
            ims_alpha,
            ims_beta,
            ims_gamma,
            ims_coord_vector,
            mz_vector,
        ) = DummyToffeeFile.create_mz_vector(self.IMS_TYPE, lower, upper, num_mz_points)
        result = self.ops.calculateIMSProperties([list(mz_vector)])

        self.assertEqual(1, len(result.alpha))
        self.assertAlmostEqual(ims_alpha, result.alpha[0])
        self.assertAlmostEqual(ims_alpha, result.medianAlpha)

        self.assertEqual(1, len(result.beta))
        self.assertAlmostEqual(ims_beta, result.beta[0])
        self.assertAlmostEqual(ims_beta, result.medianBeta)

        self.assertEqual(ims_gamma, result.gamma)

        self.assertEqual(1, len(result.sliceIndex))
        self.assertEqual(mz_vector.shape[0], result.sliceIndex[0])

        np.testing.assert_array_equal(ims_coord_vector, result.coordinate)

        # add another scan
        result = self.ops.calculateIMSProperties([list(mz_vector), list(mz_vector)])

        self.assertEqual(2, len(result.alpha))
        self.assertAlmostEqual(ims_alpha, result.alpha[0])
        self.assertAlmostEqual(ims_alpha, result.alpha[1])
        self.assertAlmostEqual(ims_alpha, result.medianAlpha)

        self.assertEqual(2, len(result.beta))
        self.assertAlmostEqual(ims_beta, result.beta[0])
        self.assertAlmostEqual(ims_beta, result.beta[1])
        self.assertAlmostEqual(ims_beta, result.medianBeta)

        self.assertEqual(ims_gamma, result.gamma)

        self.assertEqual(2, len(result.sliceIndex))
        self.assertEqual(mz_vector.shape[0], result.sliceIndex[0])
        self.assertEqual(mz_vector.shape[0] * 2, result.sliceIndex[1])

        np.testing.assert_array_equal(np.hstack([ims_coord_vector, ims_coord_vector]), result.coordinate)

        # add an empty scan in the middle
        result = self.ops.calculateIMSProperties([list(mz_vector), [], list(mz_vector)])

        self.assertEqual(3, len(result.alpha))
        self.assertAlmostEqual(ims_alpha, result.alpha[0])
        self.assertAlmostEqual(ims_alpha, result.alpha[1])
        self.assertAlmostEqual(ims_alpha, result.alpha[2])
        self.assertAlmostEqual(ims_alpha, result.medianAlpha)

        self.assertEqual(3, len(result.beta))
        self.assertAlmostEqual(ims_beta, result.beta[0])
        self.assertAlmostEqual(ims_beta, result.beta[1])
        self.assertAlmostEqual(ims_beta, result.beta[2])
        self.assertAlmostEqual(ims_beta, result.medianBeta)

        self.assertEqual(ims_gamma, result.gamma)

        self.assertEqual(3, len(result.sliceIndex))
        self.assertEqual(mz_vector.shape[0], result.sliceIndex[0])
        self.assertEqual(mz_vector.shape[0], result.sliceIndex[1])
        self.assertEqual(mz_vector.shape[0] * 2, result.sliceIndex[2])

        np.testing.assert_array_equal(np.hstack([ims_coord_vector, ims_coord_vector]), result.coordinate)

        # add a single entry in the middle
        mid_point = mz_vector.shape[0] // 2
        result = self.ops.calculateIMSProperties([list(mz_vector), [mz_vector[mid_point]], list(mz_vector)])

        self.assertEqual(3, len(result.alpha))
        self.assertAlmostEqual(ims_alpha, result.alpha[0])
        self.assertAlmostEqual(ims_alpha, result.alpha[1])
        self.assertAlmostEqual(ims_alpha, result.alpha[2])
        self.assertAlmostEqual(ims_alpha, result.medianAlpha)

        self.assertEqual(3, len(result.beta))
        self.assertAlmostEqual(ims_beta, result.beta[0])
        self.assertAlmostEqual(ims_beta, result.beta[1])
        self.assertAlmostEqual(ims_beta, result.beta[2])
        self.assertAlmostEqual(ims_beta, result.medianBeta)

        self.assertEqual(ims_gamma, result.gamma)

        self.assertEqual(3, len(result.sliceIndex))
        self.assertEqual(mz_vector.shape[0], result.sliceIndex[0])
        self.assertEqual(mz_vector.shape[0] + 1, result.sliceIndex[1])
        self.assertEqual(mz_vector.shape[0] * 2 + 1, result.sliceIndex[2])

        np.testing.assert_array_equal(
            np.hstack([ims_coord_vector, [ims_coord_vector[mid_point]], ims_coord_vector]),
            result.coordinate,
        )


class TestRawSwathScanDataOpsTOF(_BaseRawSwathScanDataOps):
    __test__ = True
    IMS_TYPE = toffee.IntrinsicMassSpacingType.TOF
    TRANSFORM_FUNC = np.sqrt

    def test_calculateIMSProperties_complex_1(self):  # noqa: N802
        self._test_calculateIMSProperties_complex_impl('180508_blank_S_M02_1.e.IMSProps.ms2-050.h5')

    def test_calculateIMSProperties_complex_2(self):  # noqa: N802
        self._test_calculateIMSProperties_complex_impl('180508_blank_S_M02_2.e.IMSProps.ms2-050.h5')

    def _test_calculateIMSProperties_complex_impl(self, basename):  # noqa: N802
        """
        The data input here comes from the prototyping python code that has been visually
        inspected as a correct representation of the transformation of m/z. This is
        effectively a regression test to ensure that the C++ implementation is a true reflection
        of that python code
        """
        curdir = os.path.dirname(os.path.abspath(__file__))
        tof_ims_fname = os.path.join(curdir, basename)
        with h5py.File(tof_ims_fname, 'r') as f:
            median_alpha = f.attrs['median_alpha']
            median_beta = f.attrs['median_beta']
            gamma = f.attrs['gamma']
            flat_mz = f['mz'][()]
            alpha = f['alpha'][()]
            beta = f['beta'][()]
            slice_index = f['slice_index'][()]
            coord = f['coord'][()]
        mz = []
        for i, end in enumerate(slice_index):
            start = 0 if i == 0 else slice_index[i - 1]
            mz.append(list(flat_mz[start:end]))

        result = self.ops.calculateIMSProperties(mz)
        self.assertAlmostEqual(median_alpha, result.medianAlpha)
        np.testing.assert_almost_equal(alpha, result.alpha)
        self.assertAlmostEqual(median_beta, result.medianBeta)
        np.testing.assert_almost_equal(beta, result.beta)
        self.assertEqual(gamma, result.gamma)
        np.testing.assert_almost_equal(slice_index, result.sliceIndex)
        np.testing.assert_almost_equal(coord, result.coordinate)


class TestRawSwathScanDataOpsOrbitrap(_BaseRawSwathScanDataOps):
    __test__ = True
    IMS_TYPE = toffee.IntrinsicMassSpacingType.ORBITRAP

    @staticmethod
    def transform_func(x):
        return 1 / np.sqrt(x)

    TRANSFORM_FUNC = transform_func


if __name__ == '__main__':
    import unittest
    unittest.main()
