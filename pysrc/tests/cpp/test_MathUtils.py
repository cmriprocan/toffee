"""
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import unittest

import numpy as np

import toffee


class TestMathUtils(unittest.TestCase):
    def test_almostEqual(self):  # noqa: N802
        self.assertFalse(toffee.MathUtils.almostEqual(-1.0, 1.0))
        self.assertTrue(toffee.MathUtils.almostEqual(0.0, 0.0))
        self.assertTrue(
            toffee.MathUtils.almostEqual(
                0.0,
                np.nextafter(0.0, 1.0),
            ),
        )
        self.assertTrue(
            toffee.MathUtils.almostEqual(
                np.nextafter(0.0, -1.0),
                np.nextafter(0.0, 1.0),
            ),
        )
        self.assertTrue(
            toffee.MathUtils.almostEqual(
                np.nextafter(1e3, 0.0),
                np.nextafter(1e3, 1e4),
            ),
        )
        self.assertTrue(
            toffee.MathUtils.almostEqual(
                np.nextafter(1e8, 0.0),
                np.nextafter(1e8, 1e9),
            ),
        )
        self.assertFalse(
            toffee.MathUtils.almostEqual(
                0.0,
                15.0 * np.finfo(np.float).eps,
            ),
        )

    def test_lessThanOrAlmostEqual(self):  # noqa: N802
        self.assertTrue(toffee.MathUtils.lessThanOrAlmostEqual(0.0, 1.0))
        self.assertTrue(
            toffee.MathUtils.lessThanOrAlmostEqual(
                0.0,
                np.nextafter(0.0, 1.0),
            ),
        )
        a = 7.0571796324969682e-05
        b = -3.5285898164261198e-05
        self.assertTrue(toffee.MathUtils.lessThanOrAlmostEqual(abs(b), 0.5 * a))

    def test_median(self):
        with self.assertRaises(RuntimeError):
            toffee.MathUtils.median([])
        self.assertEqual(1.0, toffee.MathUtils.median([1.0]))
        self.assertEqual(1.5, toffee.MathUtils.median([1.0, 2.0]))
        self.assertEqual(2.0, toffee.MathUtils.median([1.0, 2.0, 2.0]))

    def test_nanmedian(self):
        with self.assertRaises(RuntimeError):
            toffee.MathUtils.nanmedian([np.NaN])
        self.assertEqual(1.0, toffee.MathUtils.nanmedian([1.0, np.NaN]))
        self.assertEqual(1.5, toffee.MathUtils.nanmedian([1.0, 2.0, np.NaN]))
        self.assertEqual(2.0, toffee.MathUtils.nanmedian([1.0, 2.0, 2.0, np.NaN, np.NaN]))

    def test_mean(self):
        with self.assertRaises(RuntimeError):
            toffee.MathUtils.mean([])
        self.assertEqual(1.0, toffee.MathUtils.mean([1.0]))
        self.assertEqual(1.5, toffee.MathUtils.mean([1.0, 2.0]))
        self.assertEqual(5.0 / 3.0, toffee.MathUtils.mean([1.0, 2.0, 2.0]))

    def test_nanmean(self):
        with self.assertRaises(RuntimeError):
            toffee.MathUtils.nanmean([np.NaN])
        self.assertEqual(1.0, toffee.MathUtils.nanmean([1.0, np.NaN]))
        self.assertEqual(1.5, toffee.MathUtils.nanmean([1.0, 2.0, np.NaN]))
        self.assertEqual(5.0 / 3.0, toffee.MathUtils.nanmean([1.0, 2.0, 2.0, np.NaN, np.NaN]))


if __name__ == '__main__':
    import unittest
    unittest.main()
