"""
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import unittest

import toffee


class TestToffeeWriter(unittest.TestCase):
    def test_statics(self):
        self.assertEqual(
            'FILE_FORMAT_MAJOR_VERSION',
            toffee.ToffeeWriter.ATTR_MAJOR_VERSION,
        )
        self.assertEqual(
            'FILE_FORMAT_MINOR_VERSION',
            toffee.ToffeeWriter.ATTR_MINOR_VERSION,
        )
        self.assertEqual(
            'CREATED_BY_LIBRARY_VERSION',
            toffee.ToffeeWriter.ATTR_LIBRARY_VERSION,
        )

        self.assertEqual(
            'IMSType',
            toffee.ToffeeWriter.ATTR_IMS_TYPE,
        )
        self.assertEqual(
            'IMSAlpha',
            toffee.ToffeeWriter.ATTR_IMS_ALPHA,
        )
        self.assertEqual(
            'IMSBeta',
            toffee.ToffeeWriter.ATTR_IMS_BETA,
        )
        self.assertEqual(
            'IMSGamma',
            toffee.ToffeeWriter.ATTR_IMS_GAMMA,
        )
        self.assertEqual(
            'precursorLower',
            toffee.ToffeeWriter.ATTR_WINDOW_BOUNDS_LOWER,
        )
        self.assertEqual(
            'precursorUpper',
            toffee.ToffeeWriter.ATTR_WINDOW_BOUNDS_UPPER,
        )
        self.assertEqual(
            'precursorCenter',
            toffee.ToffeeWriter.ATTR_WINDOW_CENTER,
        )

        self.assertEqual(
            'retentionTimeIdx',
            toffee.ToffeeWriter.DATASET_RETENTION_TIME_IDX,
        )
        self.assertEqual(
            'IMSAlphaPerScan',
            toffee.ToffeeWriter.DATASET_IMS_ALPHA_PER_SCAN,
        )
        self.assertEqual(
            'IMSBetaPerScan',
            toffee.ToffeeWriter.DATASET_IMS_BETA_PER_SCAN,
        )
        self.assertEqual(
            'imsCoord',
            toffee.ToffeeWriter.DATASET_MZ_IMS_IDX,
        )
        self.assertEqual(
            'intensity',
            toffee.ToffeeWriter.DATASET_INTENSITY,
        )

        self.assertEqual(
            'ms1',
            toffee.ToffeeWriter.MS1_NAME,
        )
        self.assertEqual(
            'ms2-',
            toffee.ToffeeWriter.MS2_PREFIX,
        )
        self.assertEqual(
            'ms2-001',
            toffee.ToffeeWriter.ms2Name(1),
        )
        self.assertEqual(
            'ms2-010',
            toffee.ToffeeWriter.ms2Name(10),
        )
        self.assertEqual(
            'ms2-100',
            toffee.ToffeeWriter.ms2Name(100),
        )


if __name__ == '__main__':
    import unittest
    unittest.main()
