"""
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import os
import sys
import unittest

import pandas as pd

import toffee
from toffee.manipulation import Subsampler

from .helper import DummyToffeeFile, HelperCreateTestToffeeFile


class TestSubsampler(HelperCreateTestToffeeFile):
    @classmethod
    def setUp(cls):
        base_dir = os.environ.get('DIA_TEST_DATA_REPO', None)
        assert base_dir is not None
        cls.spectral_library_fname = base_dir + '/ProCan90/srl/hek_srl.OpenSwath.iRT.tsv'
        cls.srl = pd.read_csv(cls.spectral_library_fname, sep='\t')

    def test_ctor_missing_file(self):
        class _DummyFile():
            tof_fname = 'path-to-a-file-that-doesnt-exist.tof'
            ms2_name = 'ms2-050'

        with self.assertRaises(RuntimeError):
            constructor, _ = self._create_subsampler(_DummyFile())

    def test_run(self):
        base_name = '{}.{}'.format(self.__class__.__name__, sys._getframe().f_code.co_name)
        test_file = DummyToffeeFile(base_name)
        subsampler = self._create_subsampler(test_file)
        new_tof_file = 'new.tof'
        subsampler.run(new_tof_file)
        self.assertTrue(os.path.isfile(new_tof_file))

        self._test_expect_identical_data(subsampler, new_tof_file)

        # clean up
        if os.path.isfile(test_file.tof_fname):
            os.remove(test_file.tof_fname)
        if os.path.isfile(new_tof_file):
            os.remove(new_tof_file)

    @classmethod
    def _create_subsampler(cls, tof_file):
        # get a dummy set of PSMs
        psm_a = cls.srl.TransitionGroupId.iloc[0]

        def to_psm_list(psm):
            return cls.srl[cls.srl.TransitionGroupId == psm].copy()

        ms2_window_of_interest = tof_file.ms2_name
        constructor = Subsampler(
            tof_fname=tof_file.tof_fname,
            precursor_and_product_df=to_psm_list(psm_a),
            filter_ms2_windows=[ms2_window_of_interest],
        )
        assert constructor.precursors.shape[0] > 0
        assert constructor.products.shape[0] > 0
        return constructor

    def _test_expect_identical_data(self, subsampler, new_tof_file):
        self.assertTrue(isinstance(subsampler, Subsampler))
        old_ms1 = subsampler.ms1_swath_map
        new_swath_run = toffee.SwathRun(new_tof_file)
        new_ms1 = new_swath_run.loadSwathMap(toffee.ToffeeWriter.MS1_NAME)

        ppm_width = 0.5 * subsampler.ppm_width
        for ms2_name in sorted(subsampler.precursors.ms2Name.unique().tolist()):
            # filter the library
            try:
                precursors = subsampler.precursors[subsampler.precursors.ms2Name == ms2_name]
                products = subsampler.products[subsampler.products.ms2Name == ms2_name]
            except KeyError:
                # this will happen if the window is not in the library
                continue

            # load the MS2 swath map
            old_ms2 = subsampler.swath_run.loadSwathMap(ms2_name)
            new_ms2 = new_swath_run.loadSwathMap(ms2_name)

            # check MS1
            for _, row in precursors.iterrows():
                mz_range = toffee.MassOverChargeRangeWithPPMFullWidth(row.IsotopeMz, ppm_width)
                old_xic = old_ms1.extractedIonChromatogramSparse(mz_range)
                new_xic = new_ms1.extractedIonChromatogramSparse(mz_range)
                self._chromatogram_assert_allclose(old_xic, new_xic)

            # check MS2
            for _, row in products.iterrows():
                mz_range = toffee.MassOverChargeRangeWithPPMFullWidth(row.IsotopeMz, ppm_width)
                old_xic = old_ms2.extractedIonChromatogramSparse(mz_range)
                new_xic = new_ms2.extractedIonChromatogramSparse(mz_range)
                self._chromatogram_assert_allclose(old_xic, new_xic)

        # clean up
        if os.path.isfile(new_tof_file):
            os.remove(new_tof_file)


if __name__ == '__main__':
    unittest.main()
