"""
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import os
import unittest

import numpy as np

import pandas as pd

from toffee import util


class TestUtil(unittest.TestCase):
    @classmethod
    def setUp(cls):
        cls.base_dir = os.environ.get('DIA_TEST_DATA_REPO', None)
        assert cls.base_dir is not None

    def test_calculate_isotope_mz(self):
        srl_fname = self.base_dir + '/ProCan90/srl/hek_srl.OpenSwath.iRT.tsv'
        srl = pd.read_csv(srl_fname, sep='\t')

        n_isotopes = 4
        n_isotopes_plus_mono = n_isotopes + 1
        isotope_mass_offset = 1.0033548
        precursor_isotopes, product_isotopes = util.calculate_isotope_mz(
            precursor_and_product_df=srl,
            n_isotopes=n_isotopes,
            isotope_mass_offset=isotope_mass_offset,
            drop_other_cols=True,
        )

        header = ['Id', 'Charge', 'IsotopeMz', 'IsotopeNr']
        self.assertCountEqual(
            precursor_isotopes.columns.tolist(),
            header,
        )
        header.append('GroupId')
        self.assertCountEqual(
            product_isotopes.columns.tolist(),
            header,
        )

        n_precursors = srl.TransitionGroupId.unique().size
        self.assertEqual(n_precursors * n_isotopes_plus_mono, precursor_isotopes.shape[0])
        precursor_isotopes = precursor_isotopes.iloc[:n_isotopes_plus_mono]
        expected_id = precursor_isotopes.iloc[0].Id
        expected_charge = precursor_isotopes.iloc[0].Charge
        self.assertTrue((precursor_isotopes.Id == expected_id).all())
        self.assertTrue((precursor_isotopes.Charge == expected_charge).all())
        delta_mass = np.diff(precursor_isotopes.IsotopeMz) * expected_charge
        self.assertAlmostEqual(isotope_mass_offset, delta_mass.mean())

        n_products = srl.TransitionId.unique().size
        self.assertEqual(n_products * n_isotopes_plus_mono, product_isotopes.shape[0])
        product_isotopes = product_isotopes.iloc[:n_isotopes_plus_mono]
        expected_id = product_isotopes.iloc[0].Id
        expected_charge = product_isotopes.iloc[0].Charge
        self.assertTrue((product_isotopes.Id == expected_id).all())
        self.assertTrue((product_isotopes.Charge == expected_charge).all())
        delta_mass = np.diff(product_isotopes.IsotopeMz) * expected_charge
        self.assertAlmostEqual(isotope_mass_offset, delta_mass.mean())
