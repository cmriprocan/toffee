"""
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import logging
import os
import unittest

import numpy as np

import scipy
import scipy.sparse

import toffee


# need to add some debugging
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)


class DummyToffeeFile():
    def __init__(
        self,
        name,
        num_rt_points=1626,
        scan_cycle_time=3.32,
        ms1_rt_offset=0.17,
        ms2_rt_offset=0.506,
        num_mz_points=1000,
        ms2_name=None,
        ims_type=toffee.IntrinsicMassSpacingType.TOF,
        fraction_not_zero=0.01,
    ):
        self.name = name
        self.hash = sum(ord(ch) for ch in name)
        self.seed = self.hash % ((np.iinfo(np.int32).max + 1) * 2)
        logger.info('%s: hash=%s, seed=%s', self.name, self.hash, self.seed)
        np.random.seed(self.seed)

        self.header = self._header(ims_type)

        # create retention time vectors for each window
        self.scan_cycle_time = scan_cycle_time
        self.ms1_rt_offset = ms1_rt_offset
        self.ms2_rt_offset = ms2_rt_offset
        self.ms1_rt_vector = np.arange(num_rt_points) * self.scan_cycle_time + self.ms1_rt_offset
        self.ms2_rt_vector = np.arange(num_rt_points) * self.scan_cycle_time + self.ms2_rt_offset

        # set the mass over charge bounds of the data
        lower = 90
        upper = 2100
        (
            self.ims_alpha,
            self.ims_beta,
            self.ims_gamma,
            self.ims_coord_vector,
            self.mz_vector,
        ) = self.create_mz_vector(ims_type, lower, upper, num_mz_points)

        # we're going to create 1x MS1 window and 1x MS2 window
        self.ms1_name = toffee.ToffeeWriter.MS1_NAME
        if ms2_name is None:
            ms2_name = toffee.ToffeeWriter.ms2Name(41)
        self.ms2_name = ms2_name
        # window boundaries match ProCan90-M06-07.tof/ms2-041
        self.window_lower, self.window_center, self.window_upper = (608.5, 611.5, 614.5)

        # create a dense matrix of fake data
        ms1_mz, ms1_rt = np.meshgrid(self.mz_vector, self.ms1_rt_vector)
        self.expected_intensities_ms1 = (ms1_rt * ms1_mz).astype(np.uint32)
        ms2_mz, ms2_rt = np.meshgrid(self.mz_vector, self.ms2_rt_vector)
        self.expected_intensities_ms2 = (ms2_rt * ms2_mz).astype(np.uint32)

        # generate our toffee writer so we can add windows to it
        self.tof_fname = name + '.tof'
        self.ims_type = ims_type
        writer = toffee.ToffeeWriter(self.tof_fname, self.ims_type)
        writer.addHeaderMetadata(self.header)

        # loop through the two windows, modify their intensities by setting
        # a random seclectin to zero, convert this into a point cloud and
        # then add it to the toffee writer
        self.scan_data = {}
        for map_name, rt_vector, expected_intensities in zip(
            [self.ms1_name, self.ms2_name],
            [self.ms1_rt_vector, self.ms2_rt_vector],
            [self.expected_intensities_ms1, self.expected_intensities_ms2],
        ):
            # create the raw data container
            scan_data = toffee.RawSwathScanData(map_name, ims_type)
            if map_name == self.ms2_name:
                scan_data.windowLower = self.window_lower
                scan_data.windowCenter = self.window_center
                scan_data.windowUpper = self.window_upper
            else:
                scan_data.windowLower = -1
                scan_data.windowCenter = -1
                scan_data.windowUpper = -1

            # slice the data as if we were a mass spectrometer creating 'scans'
            for r, rt_value in enumerate(rt_vector):
                intensity_vector = expected_intensities[r, :]

                # add some zeros by generating a mask and assigning a random
                # fraction to be True - then set anything in the intensity
                # vector, masked by True, to be zero. In one case, set all
                # intensities to be zero (mimicking a scan with no data)
                if r % 10 == 0:
                    tmp_fraction_not_zero = 0.0
                else:
                    tmp_fraction_not_zero = fraction_not_zero
                mask = np.random.uniform(0, 1, size=intensity_vector.shape)
                mask[mask < tmp_fraction_not_zero] = 0
                mask[mask != 0] = 1
                mask = mask.astype(bool)
                intensity_vector[mask] = 0
                # double check the masking worked as expected
                if (~mask).any():
                    assert (intensity_vector > 0).any()
                mz = self.mz_vector[~mask]
                intens = intensity_vector[~mask]

                # add to the scan
                scan_data.addScan(rt_value, mz, intens)

            # create our point cloud
            pcl = scan_data.toPointCloud()

            # save scan data for later
            self.scan_data[map_name] = scan_data

            # assert that the point cloud properties were set correctly
            assert pcl.name == map_name
            assert pcl.imsType == ims_type
            assert pcl.windowLower == scan_data.windowLower
            assert pcl.windowCenter == scan_data.windowCenter
            assert pcl.windowUpper == scan_data.windowUpper

            # assert that the retention time was correctly calculated
            np.testing.assert_allclose(pcl.scanCycleTime, self.scan_cycle_time)
            if map_name == self.ms2_name:
                np.testing.assert_allclose(pcl.firstScanRetentionTimeOffset, self.ms2_rt_offset)
            else:
                np.testing.assert_allclose(pcl.firstScanRetentionTimeOffset, self.ms1_rt_offset)

            ims_props = pcl.imsProperties
            assert len(ims_props.sliceIndex) == num_rt_points, \
                '{} {}\n{}'.format(len(ims_props.sliceIndex), num_rt_points, ims_props.sliceIndex)

            # assert that the Intrinsic Mass Spacing parameters were calculated
            # correctly
            np.testing.assert_allclose(ims_props.medianAlpha, self.ims_alpha)
            np.testing.assert_allclose(ims_props.medianBeta, self.ims_beta)
            np.testing.assert_allclose(ims_props.gamma, self.ims_gamma)

            # add to the file
            writer.addPointCloud(pcl)

    @classmethod
    def create_mz_vector(cls, ims_type, lower, upper, num_mz_points):
        """
        Create a m/z vector that respects the IMS spacings for a Sciex (TOF)
        type instrument
        """
        if ims_type == toffee.IntrinsicMassSpacingType.TOF:
            return cls._create_mz_vector_tof(lower, upper, num_mz_points)
        elif ims_type == toffee.IntrinsicMassSpacingType.ORBITRAP:
            return cls._create_mz_vector_orbitrap(lower, upper, num_mz_points)
        raise ValueError('Upsupported IMS type: {}'.format(ims_type))

    @classmethod
    def _create_mz_vector_tof(cls, lower, upper, num_mz_points):
        """
        Create a m/z vector that respects the IMS spacings for a Sciex (TOF)
        type instrument
        """
        sqrt_lower = np.sqrt(lower)
        sqrt_upper = np.sqrt(upper)

        # and use these to create dummy Intrinsic Mass Spacing properties
        ims_alpha = (sqrt_upper - sqrt_lower) / num_mz_points
        ims_gamma = int(np.round(sqrt_lower / ims_alpha, decimals=0))
        ims_beta = sqrt_lower - ims_gamma * ims_alpha
        if ims_beta > 0:
            ims_gamma += 1
            ims_beta -= ims_alpha

        # from these create an mass over charge vector with known properties
        ims_coord = np.arange(num_mz_points + 1).astype(int)
        mz = ((ims_coord + ims_gamma) * ims_alpha + ims_beta) ** 2

        # add some padding to ims
        ims_gamma -= 10
        ims_coord += 10

        return ims_alpha, ims_beta, ims_gamma, ims_coord, mz

    @classmethod
    def _create_mz_vector_orbitrap(cls, lower, upper, num_mz_points):
        """
        Create a m/z vector that respects the IMS spacings for a Thermo (Orbitrap)
        type instrument
        """
        inverse_sqrt_lower = 1 / np.sqrt(lower)
        inverse_sqrt_upper = 1 / np.sqrt(upper)

        # and use these to create dummy Intrinsic Mass Spacing properties
        ims_alpha = (inverse_sqrt_upper - inverse_sqrt_lower) / num_mz_points
        ims_gamma = int(np.round(inverse_sqrt_lower / ims_alpha, decimals=0))
        ims_beta = inverse_sqrt_lower - ims_gamma * ims_alpha
        if ims_beta > 0:
            ims_gamma -= 1
            ims_beta += ims_alpha

        # from these create an mass over charge vector with known properties
        ims_coord = np.arange(num_mz_points + 1).astype(int)
        mz = ((ims_coord + ims_gamma) * ims_alpha + ims_beta) ** -2

        # add some padding to ims
        ims_gamma -= 10
        ims_coord += 10

        return ims_alpha, ims_beta, ims_gamma, ims_coord, mz

    @classmethod
    def _header(cls, ims_type):
        if ims_type == toffee.IntrinsicMassSpacingType.TOF:
            header_fname = 'Header-TOF.mzML'
        elif ims_type == toffee.IntrinsicMassSpacingType.ORBITRAP:
            header_fname = 'Header-Orbitrap.mzML'
        else:
            raise ValueError('Upsupported IMS type: {}'.format(ims_type))

        curdir = os.path.dirname(os.path.abspath(__file__))
        header_fname = os.path.join(curdir, header_fname)
        with open(header_fname, 'r') as f:
            return f.read()


class HelperCreateTestToffeeFile(unittest.TestCase):
    @classmethod
    def create_simple_tof_file(
        cls,
        name,
        ms2_name=None,
        ims_type=toffee.IntrinsicMassSpacingType.TOF,
    ):
        dummy_file = DummyToffeeFile(
            name,
            num_rt_points=1626,
            scan_cycle_time=3.32,
            ms1_rt_offset=0.17,
            ms2_rt_offset=0.506,
            num_mz_points=1000,
            ms2_name=ms2_name,
            ims_type=ims_type,
        )
        cls.header = dummy_file.header

        cls.scan_cycle_time = dummy_file.scan_cycle_time
        cls.ms1_rt_offset = dummy_file.ms1_rt_offset
        cls.ms2_rt_offset = dummy_file.ms2_rt_offset
        cls.ms1_rt_vector = dummy_file.ms1_rt_vector
        cls.ms2_rt_vector = dummy_file.ms2_rt_vector

        cls.ims_alpha = dummy_file.ims_alpha
        cls.ims_beta = dummy_file.ims_beta
        cls.ims_gamma = dummy_file.ims_gamma
        cls.ims_coord_vector = dummy_file.ims_coord_vector
        cls.mz_vector = dummy_file.mz_vector

        cls.ms1_name = dummy_file.ms1_name
        cls.ms2_name = dummy_file.ms2_name
        cls.window_lower = dummy_file.window_lower
        cls.window_center = dummy_file.window_center
        cls.window_upper = dummy_file.window_upper

        cls.expected_intensities_ms1 = dummy_file.expected_intensities_ms1
        cls.expected_intensities_ms2 = dummy_file.expected_intensities_ms2

        cls.tof_fname = dummy_file.tof_fname
        cls.ims_type = dummy_file.ims_type

        cls.scan_data = dummy_file.scan_data

    @classmethod
    def mass_over_charge_transformer(cls, ims_type, alpha, beta, gamma):
        """
        Construct a ``toffee.MassOverChargeTransformer`` object relevant to the specified
        IMS type
        """
        if ims_type == toffee.IntrinsicMassSpacingType.TOF:
            return toffee.MassOverChargeTransformerTOF(alpha, beta, gamma)
        elif ims_type == toffee.IntrinsicMassSpacingType.ORBITRAP:
            return toffee.MassOverChargeTransformerOrbitrap(alpha, beta, gamma)
        raise ValueError('Upsupported IMS type: {}'.format(ims_type))

    @classmethod
    def tearDownClass(cls):
        if hasattr(cls, 'tof_fname') and os.path.isfile(cls.tof_fname):
            os.remove(cls.tof_fname)

    @classmethod
    def _sparse_assert_allclose(cls, a, b, atol=1e-8):
        """
        Test to see if two sparse matrices compare equal.
        Modified from: https://stackoverflow.com/a/47771340/758811
        """
        np.testing.assert_array_equal(a.shape, b.shape)
        r1, c1, v1 = scipy.sparse.find(a)
        r2, c2, v2 = scipy.sparse.find(b)
        np.testing.assert_array_equal(r1, r2)
        np.testing.assert_array_equal(c1, c2)
        np.testing.assert_allclose(v1, v2, atol=atol)

    @classmethod
    def _chromatogram_assert_allclose(cls, a, b):
        np.testing.assert_allclose(
            a.retentionTime,
            b.retentionTime,
        )
        np.testing.assert_allclose(
            a.massOverCharge,
            b.massOverCharge,
        )
        cls._sparse_assert_allclose(
            a.intensities,
            b.intensities,
        )
