"""
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import os
import sys
import unittest

import numpy as np

import pandas as pd

import toffee
from toffee.manipulation import InSilicoDilutionConstructor

from .helper import DummyToffeeFile, HelperCreateTestToffeeFile


class TestInSilicoDilutionConstructor(HelperCreateTestToffeeFile):
    @classmethod
    def setUp(cls):
        base_dir = os.environ.get('DIA_TEST_DATA_REPO', None)
        assert base_dir is not None
        cls.spectral_library_fname = base_dir + '/ProCan90/srl/hek_srl.OpenSwath.iRT.tsv'
        cls.srl = pd.read_csv(cls.spectral_library_fname, sep='\t')

    def test_ctor_missing_file(self):
        class _DummyFile():
            tof_fname = 'path-to-a-file-that-doesnt-exist.tof'
            ms2_name = 'ms2-050'

        with self.assertRaises(RuntimeError):
            constructor, _ = self._create_dilution_constructor(_DummyFile(), _DummyFile())

    def test_ctor_mismatched_windows(self):
        base_name = '{}.{}'.format(self.__class__.__name__, sys._getframe().f_code.co_name)
        test_file_a = DummyToffeeFile(base_name + '.a', ms2_name='ms2-001')
        test_file_b = DummyToffeeFile(base_name + '.b', ms2_name='ms2-002')
        with self.assertRaises(ValueError):
            self._create_dilution_constructor(test_file_a, test_file_b)
        # clean up
        if os.path.isfile(test_file_a.tof_fname):
            os.remove(test_file_a.tof_fname)
        if os.path.isfile(test_file_b.tof_fname):
            os.remove(test_file_b.tof_fname)

    def test_ctor_mismatched_retention_time(self):
        base_name = '{}.{}'.format(self.__class__.__name__, sys._getframe().f_code.co_name)
        test_file_a = DummyToffeeFile(base_name + '.a', num_rt_points=1000)
        test_file_b = DummyToffeeFile(base_name + '.b', num_rt_points=1002)
        with self.assertRaises(ValueError):
            self._create_dilution_constructor(test_file_a, test_file_b)
        # clean up
        if os.path.isfile(test_file_a.tof_fname):
            os.remove(test_file_a.tof_fname)
        if os.path.isfile(test_file_b.tof_fname):
            os.remove(test_file_b.tof_fname)

    def test_ctor_mismatched_ims_type(self):
        base_name = '{}.{}'.format(self.__class__.__name__, sys._getframe().f_code.co_name)
        test_file_a = DummyToffeeFile(base_name + '.a', ims_type=toffee.IntrinsicMassSpacingType.TOF)
        test_file_b = DummyToffeeFile(base_name + '.b', ims_type=toffee.IntrinsicMassSpacingType.ORBITRAP)
        with self.assertRaises(ValueError):
            self._create_dilution_constructor(test_file_a, test_file_b)
        # clean up
        if os.path.isfile(test_file_a.tof_fname):
            os.remove(test_file_a.tof_fname)
        if os.path.isfile(test_file_b.tof_fname):
            os.remove(test_file_b.tof_fname)

    def test_run_dilution_0(self):
        base_name = '{}.{}'.format(self.__class__.__name__, sys._getframe().f_code.co_name)
        test_file = DummyToffeeFile(base_name)
        dilution = 0
        constructor, _ = self._create_dilution_constructor(test_file, test_file)
        new_tof_file = 'new.tof'
        with self.assertRaises(ValueError):
            constructor.run(new_tof_file, dilution)
        # clean up
        if os.path.isfile(test_file.tof_fname):
            os.remove(test_file.tof_fname)
        if os.path.isfile(new_tof_file):
            os.remove(new_tof_file)

    def test_run_same_file_dilution_1(self):
        # if we set 1x of the same data into itself as a background, then we should expect the
        # created file to be identical to the background
        base_name = '{}.{}'.format(self.__class__.__name__, sys._getframe().f_code.co_name)
        test_file = DummyToffeeFile(base_name)
        dilution = 1
        results_from_run = self._test_run(test_file, test_file, dilution)
        self._test_expect_identical_data(*results_from_run)
        # clean up
        if os.path.isfile(test_file.tof_fname):
            os.remove(test_file.tof_fname)

    def test_run_same_file_dilution_2(self):
        # if we set 0.5x of the same data into itself as a background, then we should expect the
        # created file to be identical to the background because the dilution series will only
        # include the foreground if it is greater than the existing background
        base_name = '{}.{}'.format(self.__class__.__name__, sys._getframe().f_code.co_name)
        test_file = DummyToffeeFile(base_name, fraction_not_zero=0.02)
        dilution = 2
        results_from_run = self._test_run(test_file, test_file, dilution)
        self._test_expect_identical_data(*results_from_run)
        # clean up
        if os.path.isfile(test_file.tof_fname):
            os.remove(test_file.tof_fname)

    def test_run_same_file_concentration_execution_only(self):
        # if we set 100x of the same data into itself as a background, then we should expect the
        # created file to be different to the background. This test does not currently attempt
        # to show that the dilution has increased the requisite amount, just that they are
        # different
        base_name = '{}.{}'.format(self.__class__.__name__, sys._getframe().f_code.co_name)
        test_file = DummyToffeeFile(base_name)
        dilution = 0.01
        results_from_run = self._test_run(test_file, test_file, dilution)
        with self.assertRaises(AssertionError):
            self._test_expect_identical_data(*results_from_run)
        # clean up
        if os.path.isfile(test_file.tof_fname):
            os.remove(test_file.tof_fname)
        new_tof_file = results_from_run[-1]
        if os.path.isfile(new_tof_file):
            os.remove(new_tof_file)

    def test_different_files_execution_only(self):
        # Test that adding different files results in a different output
        base_name = '{}.{}'.format(self.__class__.__name__, sys._getframe().f_code.co_name)
        test_file_a = DummyToffeeFile(base_name + '-a')
        test_file_b = DummyToffeeFile(base_name + '-b', fraction_not_zero=0.1)
        dilution = 1
        results_from_run = self._test_run(test_file_a, test_file_b, dilution)
        with self.assertRaises(AssertionError):
            self._test_expect_identical_data(*results_from_run)
        # clean up
        if os.path.isfile(test_file_a.tof_fname):
            os.remove(test_file_a.tof_fname)
        if os.path.isfile(test_file_b.tof_fname):
            os.remove(test_file_b.tof_fname)
        new_tof_file = results_from_run[-1]
        if os.path.isfile(new_tof_file):
            os.remove(new_tof_file)

    @classmethod
    def _create_dilution_constructor(cls, background_tof_file, foreground_tof_file):
        np.random.seed(0)

        # just generate dummy RT values here, they aren't important
        df = pd.DataFrame({'TransitionGroupId': cls.srl.TransitionGroupId.unique()[:2]})
        df['ForegroundRetentionTime'] = np.random.uniform(10, 70, size=df.shape[0]) * 60.0
        precursor_and_product_df = pd.merge(cls.srl, df, on='TransitionGroupId', how='inner')
        assert precursor_and_product_df.shape[0] > 0

        ms2_window_of_interest = background_tof_file.ms2_name
        constructor = InSilicoDilutionConstructor(
            background_tof_fname=background_tof_file.tof_fname,
            foreground_tof_fname=foreground_tof_file.tof_fname,
            precursor_and_product_df=precursor_and_product_df,
            irt_precursor_and_product_df=None,
            filter_ms2_windows=[ms2_window_of_interest],
        )
        assert constructor.precursors.shape[0] > 0
        assert constructor.products.shape[0] > 0
        assert constructor.irt_precursors is None
        assert constructor.irt_products is None
        return constructor, ms2_window_of_interest

    def _test_run(self, background_tof_file, foreground_tof_file, dilution):
        """
        Create the in-silico dilution constructor and then call the run method
        """
        constructor, ms2_window_of_interest = self._create_dilution_constructor(
            background_tof_file,
            foreground_tof_file,
        )

        self.assertGreater(constructor.precursors.shape[0], 0)
        self.assertGreater(constructor.products.shape[0], 0)
        self.assertIsNone(constructor.irt_precursors)
        self.assertIsNone(constructor.irt_products)

        new_tof_file = 'new.tof'
        constructor.run(new_tof_file, dilution)
        self.assertTrue(os.path.isfile(new_tof_file))
        return constructor, ms2_window_of_interest, new_tof_file

    def _test_expect_identical_data(self, constructor, ms2_window_of_interest, new_tof_file):
        # compare results
        old_swath_run = constructor.background
        old_ms1 = constructor.background_ms1

        new_swath_run = toffee.SwathRun(new_tof_file)
        new_ms1 = new_swath_run.loadSwathMap(toffee.ToffeeWriter.MS1_NAME)

        # check MS1
        for _, row in constructor.products.iterrows():
            mz = row.IsotopeMz
            rt = row.ForegroundRetentionTime
            mz_range = toffee.MassOverChargeRangeWithPixelHalfWidth(mz, constructor.px_half_width)
            rt_range = toffee.RetentionTimeRangeWithPixelHalfWidth(rt, constructor.px_half_width)
            old_xic = old_ms1.filteredExtractedIonChromatogramSparse(mz_range, rt_range)
            new_xic = new_ms1.filteredExtractedIonChromatogramSparse(mz_range, rt_range)
            self.assertGreater(old_xic.intensities.nnz, 0)
            self.assertEqual(old_xic.intensities.nnz, new_xic.intensities.nnz)
            self._chromatogram_assert_allclose(old_xic, new_xic)

        # check MS2
        old_ms2 = old_swath_run.loadSwathMap(ms2_window_of_interest)
        new_ms2 = new_swath_run.loadSwathMap(ms2_window_of_interest)
        for _, row in constructor.precursors.iterrows():
            mz = row.IsotopeMz
            rt = row.ForegroundRetentionTime
            mz_range = toffee.MassOverChargeRangeWithPixelHalfWidth(mz, constructor.px_half_width)
            rt_range = toffee.RetentionTimeRangeWithPixelHalfWidth(rt, constructor.px_half_width)
            old_xic = old_ms2.filteredExtractedIonChromatogramSparse(mz_range, rt_range)
            new_xic = new_ms2.filteredExtractedIonChromatogramSparse(mz_range, rt_range)
            self.assertGreater(old_xic.intensities.nnz, 0)
            self.assertEqual(old_xic.intensities.nnz, new_xic.intensities.nnz)
            self._chromatogram_assert_allclose(old_xic, new_xic)

        # clean up
        if os.path.isfile(new_tof_file):
            os.remove(new_tof_file)


if __name__ == '__main__':
    unittest.main()
