"""
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import os
import unittest

import plotly.graph_objs as go

from toffee.viz import ToffeeFragmentsPlotter


class TestToffeeFragmentsPlotter(unittest.TestCase):

    def test_create_plotly_figure_include_ms1(self):
        base_dir = os.environ.get('DIA_TEST_DATA_REPO', None)
        self.assertIsNotNone(base_dir)
        tof_fname = f'{base_dir}/ProCan90/tof/ProCan90-M03-01.iRT.tof'

        px_half_width = 10
        px_width = 2 * px_half_width + 1
        plotter = ToffeeFragmentsPlotter(
            tof_fname=tof_fname,
            use_ms1=True,
            extraction_width=px_half_width,
        )
        psm_name = 'test'
        figure = plotter.create_plotly_figure(
            precursor_mz_list=[611.280029296875],
            product_mz_list=[611.280029296875],
            psm_name=psm_name,
        )

        plot_layout = figure['layout']
        self.assertIn('xaxis', plot_layout)
        self.assertIn('yaxis', plot_layout)
        self.assertIn('yaxis2', plot_layout)
        self.assertIn('title', plot_layout)
        self.assertEqual(plot_layout['title'], psm_name)
        self.assertFalse(plot_layout['showlegend'])

        expected_rt = plotter.ms1_swath_map.retentionTime()
        plot_data = figure['data']
        self.assertEqual(len(plot_data), 5)

        # look at the heatmap
        heatmap = plot_data[0]
        self.assertIsInstance(heatmap, go.Heatmap)
        self.assertEqual(len(heatmap.x), expected_rt.size)
        self.assertEqual(len(heatmap.y), 2 * px_width)  # MS1 + MS2
        self.assertEqual(heatmap.xaxis, 'x')
        self.assertEqual(heatmap.yaxis, 'y2')

        # look at the horizontal & XIC annotations
        for line in plot_data[1:-1]:
            self.assertIsInstance(line, go.Scatter)
            self.assertEqual(len(line.x), 2)
            self.assertEqual(line.mode, 'lines')
            self.assertEqual(line.xaxis, 'x')
            self.assertEqual(line.yaxis, 'y2')

        # look at the chromatograms
        line = plot_data[-1]
        self.assertIsInstance(line, go.Scatter)
        self.assertEqual(len(line.x), expected_rt.size)
        self.assertEqual(line.mode, 'lines')
        self.assertEqual(line.xaxis, 'x')
        self.assertEqual(line.yaxis, 'y')

    def test_create_plotly_figure_ms2_only(self):
        base_dir = os.environ.get('DIA_TEST_DATA_REPO', None)
        self.assertIsNotNone(base_dir)
        tof_fname = f'{base_dir}/ProCan90/tof/ProCan90-M03-01.iRT.tof'

        px_half_width = 10
        px_width = 2 * px_half_width + 1
        plotter = ToffeeFragmentsPlotter(
            tof_fname=tof_fname,
            use_ms1=False,
            extraction_width=px_half_width,
        )
        psm_name = 'test'
        precursor_mz = 611.280029296875
        figure = plotter.create_plotly_figure(
            precursor_mz_list=[precursor_mz],
            product_mz_list=[635.29296875, 821.39343261718],
            psm_name=psm_name,
        )
        self.assertIsNone(plotter.ms1_swath_map)
        ms2_swath_map = plotter.load_ms2_swath_map(precursor_mz)
        self.assertIsNotNone(ms2_swath_map)

        plot_layout = figure['layout']
        self.assertIn('xaxis', plot_layout)
        self.assertIn('yaxis', plot_layout)
        self.assertIn('yaxis2', plot_layout)
        self.assertIn('title', plot_layout)
        self.assertEqual(plot_layout['title'], psm_name)
        self.assertFalse(plot_layout['showlegend'])

        expected_rt = ms2_swath_map.retentionTime()
        plot_data = figure['data']
        self.assertEqual(len(plot_data), 8)

        # look at the heatmap
        heatmap = plot_data[0]
        self.assertIsInstance(heatmap, go.Heatmap)
        self.assertEqual(len(heatmap.x), expected_rt.size)
        self.assertEqual(len(heatmap.y), 2 * px_width)
        self.assertEqual(heatmap.xaxis, 'x')
        self.assertEqual(heatmap.yaxis, 'y2')

        # look at the horizontal & XIC annotations
        indices = [1, 2, 3, 5, 6]
        for i in indices:
            line = plot_data[i]
            self.assertIsInstance(line, go.Scatter)
            self.assertEqual(len(line.x), 2)
            self.assertEqual(line.mode, 'lines')
            self.assertEqual(line.xaxis, 'x')
            self.assertEqual(line.yaxis, 'y2')

        # look at the chromatograms
        indices = [4, 7]
        for i in indices:
            line = plot_data[i]
            self.assertIsInstance(line, go.Scatter)
            self.assertEqual(len(line.x), expected_rt.size)
            self.assertEqual(line.mode, 'lines')
            self.assertEqual(line.xaxis, 'x')
            self.assertEqual(line.yaxis, 'y')


if __name__ == '__main__':
    unittest.main()
