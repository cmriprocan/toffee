#!/bin/bash
set -e

# activate the base conda env
if [ -d ${HOME}/anaconda ]; then
    . ${HOME}/anaconda/etc/profile.d/conda.sh
elif [ -d ${HOME}/conda ]; then
    . ${HOME}/conda/etc/profile.d/conda.sh
elif [ -d ${HOME}/anaconda3 ]; then
    . ${HOME}/anaconda3/etc/profile.d/conda.sh
elif [ -d ${HOME}/opt/anaconda3 ]; then
    . ${HOME}/opt/anaconda3/etc/profile.d/conda.sh
elif [ -d /conda ]; then
    . /conda/etc/profile.d/conda.sh
elif [ -d /anaconda3 ]; then
    . /anaconda3/etc/profile.d/conda.sh
elif [ -d ${HOME}/miniconda ]; then
    . ${HOME}/miniconda/etc/profile.d/conda.sh
elif [ -d ${HOME}/miniconda3 ]; then
    . ${HOME}/miniconda3/etc/profile.d/conda.sh
else
    echo "warning: anaconda directory not found"
    exit 1
fi
conda activate
echo `which python`

# set the environment name based on the name of the parent folder
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
SPLIT_PATH=`echo ${SCRIPT_DIR} | rev | cut -d '/' -f 2 | rev`
PY_ENV=dev-${SPLIT_PATH}

# ensure that the conda environment is good to go
echo "Installing conda-build"
conda config --set channel_priority flexible
conda install --yes conda-build

echo "Creating dev env"
python create_dev_conda_environment_helper.py --add_dev_channel --include_build_deps --env ${PY_ENV} --debug

# some debugging
echo "============================== $PY_ENV ====="
conda env list
echo "=============================="

# activate the new conda env
conda activate $PY_ENV
echo `which python`

# install pip packages that don't exist in conda
python -m pip install -r requirements.txt
python -m pip install -r ../.conda/requirements.txt

# setup the local
python -m pip install --no-deps --ignore-installed --no-cache-dir -e ..

conda deactivate
