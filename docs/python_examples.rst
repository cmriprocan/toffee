.. toffee documentation master file, created by
   sphinx-quickstart on Thu Jun 21 02:38:31 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Python Examples
===============

.. toctree::
    :titlesonly:
    :maxdepth: 2
    :caption: Contents

    jupyter/why-we-created-toffee.ipynb
    jupyter/spatial-searching-using-toffee.ipynb
    jupyter/fragment-visualisation.ipynb
    jupyter/filtering-just-for-iRT-peptides.ipynb
    jupyter/requant.ipynb

