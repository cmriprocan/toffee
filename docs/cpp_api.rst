.. toffee documentation master file, created by
   sphinx-quickstart on Thu Jun 21 02:38:31 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


==============
Toffee C++ API
==============

.. contents::
   :depth: 3


Creating a Toffee File
======================

The following API documentation gives a flavour for how you can create a
``toffee`` file. However, it is best explained by way of example, such as the
following python code in the unit testing framework. As mentioned elsewhere,
the python and C++ APIs are laregly equivalent.

.. literalinclude:: ../pysrc/tests/helper.py
   :pyobject: DummyToffeeFile

ScanDataPointCloud
------------------

.. doxygenclass:: toffee::ScanDataPointCloud

RawSwathScanData
----------------

.. doxygenclass:: toffee::RawSwathScanData

ToffeeWriter
------------

.. doxygenclass:: toffee::ToffeeWriter


Accessing Data in a Toffee File
===============================

SwathRun
--------

.. doxygenclass:: toffee::SwathRun

.. doxygenclass:: toffee::MS2WindowDescriptor

SwathMap
--------

.. doxygenclass:: toffee::SwathMap

.. doxygenclass:: toffee::MassOverChargeTransformer

.. doxygenclass:: toffee::MassOverChargeTransformerTOF

Spectra & Chromatograms
-----------------------

.. doxygenclass:: toffee::Spectrum1D

.. doxygenclass:: toffee::Chromatogram1D

.. doxygenclass:: toffee::Chromatogram2DSparse

.. doxygenclass:: toffee::Chromatogram2DDense

MassOverChargeRange
-------------------

.. doxygenclass:: toffee::IMassOverChargeRange

.. doxygenclass:: toffee::MassOverChargeRangeWithPixelWidth

.. doxygenclass:: toffee::MassOverChargeRangeWithPPMWidth

.. doxygenclass:: toffee::MassOverChargeRange

.. doxygenclass:: toffee::MassOverChargeRangeIMSCoords

RetentionTimeRange
------------------

.. doxygenclass:: toffee::IRetentionTimeRange

.. doxygenclass:: toffee::RetentionTimeRangeWithPixelWidth

.. doxygenclass:: toffee::RetentionTimeRange


Versioning
==========

.. doxygenclass:: toffee::Version
