.. toffee documentation master file, created by
   sphinx-quickstart on Thu Jun 21 02:38:31 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

toffee: A library for fast access to Time of Flight SWATH-MS data
=================================================================

.. toctree::
  :maxdepth: 2
  :caption: Contents

  python_examples.rst
  cpp_api.rst

.. include:: ../README.rst
  :start-after: inclusion-marker-do-not-remove


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

.. commented out until we have python modules * :ref:`modindex`