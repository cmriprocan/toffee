#!/bin/bash

# Modified from: https://github.com/bioconda/bioconda-utils/blob/master/.circleci/setup.sh

set -eu

WORKSPACE=$(pwd)

# Set path
echo "export PATH=$WORKSPACE/miniconda/bin:$PATH" >> $BASH_ENV
echo "export MINICONDA_VER=py39_4.10.3" >> $BASH_ENV
source $BASH_ENV
echo "MINICONDA_VER = $MINICONDA_VER"

# Download and install SDK
if [[ ! -d $WORKSPACE/MacOSX13.3.sdk ]]; then
    if [[ $OSTYPE == darwin* ]]; then
        # tar cf - MacOSX13.3.sdk/ | xz -z - > MacOSX13.3.sdk.tar.xz
        tar -xzf .circleci/MacOSX13.3.sdk.tar.xz -C $WORKSPACE
        export CPATH=$WORKSPACE/MacOSX13.3.sdk/usr/include
        export LIBRARY_PATH=$WORKSPACE/MacOSX13.3.sdk/usr/lib
    elif [[ $OSTYPE == linux* ]]; then
        echo "Linux"
    else
        echo "Unsupported OS: $OSTYPE"
        exit 1
    fi
fi

if [[ ! -d $WORKSPACE/miniconda ]]; then
    # setup conda and dependencies if not loaded from cache
    mkdir -p $WORKSPACE

    # step 1: download and install miniconda
    if [[ $OSTYPE == darwin* ]]; then
        tag="MacOSX"
    elif [[ $OSTYPE == linux* ]]; then
        tag="Linux"
    else
        echo "Unsupported OS: $OSTYPE"
        exit 1
    fi

    curl -L -o miniconda.sh https://repo.continuum.io/miniconda/Miniconda3-$MINICONDA_VER-$tag-x86_64.sh
    bash miniconda.sh -b -p $WORKSPACE/miniconda
    conda config --set always_yes true

    # Fixes 'failed with initial frozen solve. Retrying with f solve.' issues for OSX
    conda config --set channel_priority flexible

    # step 2: setup channels -- the order of the channels is important, only change this if you know
    # what you are doing
    conda config --system --append channels plotly
    conda config --system --append channels conda-forge
    conda config --set quiet true

    conda install --yes python=3.10.* conda-build

    # step 3: install bioconda-utils and test requirements
    conda update requests  # to solve ImportError: cannot import name 'JSONDecodeError' from 'requests.exceptions'
    python .dev-environment/create_dev_conda_environment_helper.py --include_build_deps --env base --debug
    python -m pip install -r .conda/requirements.txt
    python -m pip install -r .dev-environment/requirements.txt

    # step 4: cleanup
    conda clean -y --all

    # Add local channel as highest priority
    conda index $WORKSPACE/miniconda/conda-bld
    conda config --system --add channels file://$WORKSPACE/miniconda/conda-bld
fi

echo "***** conda info **********************"
conda info

echo "***** conda config --get **********************"
conda config --get

echo "***** ls $WORKSPACE/miniconda/conda-bld **********************"
ls -lhF $WORKSPACE/miniconda/conda-bld

echo "***** ls $WORKSPACE/miniconda/conda-bld/noarch **********************"
ls -lhF $WORKSPACE/miniconda/conda-bld/noarch
