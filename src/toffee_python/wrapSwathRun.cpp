/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#include "wrapSwathRun.h"
#include <pybind11/stl_bind.h>
#include <toffee/SwathRun.h>

namespace py = pybind11;

PYBIND11_MAKE_OPAQUE(std::vector<toffee::MS2WindowDescriptor>);

namespace toffee {
namespace python {
void
wrapSwathRun(
    py::module &m
) {
    {
        py::class_<MS2WindowDescriptor> c(m, "MS2WindowDescriptor");
        c.def_readwrite("name", &MS2WindowDescriptor::name);
        c.def_readwrite("lower", &MS2WindowDescriptor::lower);
        c.def_readwrite("upper", &MS2WindowDescriptor::upper);
    }
    {
        py::bind_vector<std::vector<MS2WindowDescriptor>>(m, "VecMS2WindowDescriptor");
    }
    {
        py::class_<SwathRun> c(m, "SwathRun");
        c.def(py::init<const std::string &>());
        c.def("header", &SwathRun::header);
        c.def("formatMajorVersion", &SwathRun::formatMajorVersion);
        c.def("formatMinorVersion", &SwathRun::formatMinorVersion);
        c.def("imsType", &SwathRun::imsType);
        c.def("hasMS1Data", &SwathRun::hasMS1Data);
        c.def("ms2Windows", &SwathRun::ms2Windows);
        c.def("mapPrecursorsToMS2Names", &SwathRun::mapPrecursorsToMS2Names);
        c.def("loadSwathMap", &SwathRun::loadSwathMap, py::arg("mapName"), py::arg("lossyAdjustIMSCoords") = false);
        c.def("loadSwathMapPtr", &SwathRun::loadSwathMapPtr, py::arg("mapName"), py::arg("lossyAdjustIMSCoords") = false);
        c.def("loadSwathMapInMemorySpectrumAccess", &SwathRun::loadSwathMapInMemorySpectrumAccess);
        c.def("loadSwathMapInMemorySpectrumAccessPtr", &SwathRun::loadSwathMapInMemorySpectrumAccessPtr);
        c.def("loadSwathMapSpectrumAccess", &SwathRun::loadSwathMapSpectrumAccess);
        c.def("loadSwathMapSpectrumAccessPtr", &SwathRun::loadSwathMapSpectrumAccessPtr);
    }
}
}
}
