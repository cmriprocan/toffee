/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#include "wrapRequant.h"
#include <pybind11/stl_bind.h>
#include <toffee/requant/RequantPeakGroup.h>
#include <toffee/requant/RequantModel.h>
#include <toffee/requant/Requantifier.h>

namespace py = pybind11;

namespace toffee {
namespace requant {
namespace python {
void
wrapRequant(
    py::module &m
) {
    {
        py::class_<RequantPeakGroup> c(m, "RequantPeakGroup");
        c.def(py::init<std::string, int, int, double, double, const std::vector<double> &>());
        c.def("modifiedSequence", &RequantPeakGroup::modifiedSequence);
        c.def("charge", &RequantPeakGroup::charge);
        c.def("rank", &RequantPeakGroup::rank);
        c.def("retentionTime", &RequantPeakGroup::retentionTime);
        c.def("massOverCharge", &RequantPeakGroup::massOverCharge);
        c.def("fragmentMassOverCharges", &RequantPeakGroup::fragmentMassOverCharges);
        c.def("__str__", [](const RequantPeakGroup &self) {
            std::ostringstream os;
            os << self;
            return os.str();
        });
    }
    {
        py::bind_vector<std::vector<RequantPeakGroup>>(m, "VecRequantPeakGroup");
        py::bind_map<std::map<std::string, std::vector<RequantPeakGroup>>>(m, "MapStrVecRequantPeakGroup");
    }

    int defaultRetentionTimePixelHalfWidth = 15;
    int defaultMassOverChargePixelHalfWidth = 20;
    int defaultNumMS1Isotopes = 1;
    bool defaultModelNoise = true;

    {
        py::class_<RequantModel> c(m, "RequantModel");
        c.def(
            py::init<int, int, int, bool>(),
            py::arg("retentionTimePixelHalfWidth") = defaultRetentionTimePixelHalfWidth,
            py::arg("massOverChargePixelHalfWidth") = defaultMassOverChargePixelHalfWidth,
            py::arg("numMS1Isotopes") = defaultNumMS1Isotopes,
            py::arg("modelNoise") = defaultModelNoise
        );
        c.def("eval", &RequantModel::eval);
    }
    {
        py::class_<Requantifier> c(m, "Requantifier");
        c.def(
            py::init<int, int, int, bool>(),
            py::arg("retentionTimePixelHalfWidth") = defaultRetentionTimePixelHalfWidth,
            py::arg("massOverChargePixelHalfWidth") = defaultMassOverChargePixelHalfWidth,
            py::arg("numMS1Isotopes") = defaultNumMS1Isotopes,
            py::arg("modelNoise") = defaultModelNoise
        );
        c.def("eval", &Requantifier::eval);
    }
}
}
}
}
