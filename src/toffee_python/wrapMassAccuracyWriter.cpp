/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#include "wrapMassAccuracyWriter.h"
#include <toffee/MassAccuracyWriter.h>

namespace py = pybind11;

namespace toffee {
namespace python {
void
wrapMassAccuracyWriter(
    py::module &m
) {
    {
        py::class_<MassAccuracyWriter> c(m, "MassAccuracyWriter");
        c.def(py::init<const std::string &>());
        c.def_readonly_static("DATASET_ORIGINAL", &MassAccuracyWriter::DATASET_ORIGINAL);
        c.def_readonly_static("DATASET_CONVERTED_LOSSLESS", &MassAccuracyWriter::DATASET_CONVERTED_LOSSLESS);
        c.def_readonly_static("DATASET_CONVERTED_LOSSY", &MassAccuracyWriter::DATASET_CONVERTED_LOSSY);
        c.def_readonly_static("DATASET_RETENTION_TIME", &MassAccuracyWriter::DATASET_RETENTION_TIME);
        c.def("addWindowData", &MassAccuracyWriter::addWindowData);
    }
}
}
}
