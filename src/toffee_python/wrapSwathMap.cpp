/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#include "wrapSwathMap.h"
#include <pybind11/eigen.h>
#include <toffee/DataSlices.h>
#include <toffee/SwathMap.h>
#include <toffee/SwathMapSpectrumAccess.h>
#include <Eigen/Core>

namespace py = pybind11;

namespace toffee {
namespace python {
void
wrapSwathMap(
    py::module &m
) {
    {
        py::class_<Chromatogram2DSparse> c(m, "Chromatogram2DSparse");
        c.def_readonly("retentionTime", &Chromatogram2DSparse::retentionTime);
        c.def_readonly("massOverCharge", &Chromatogram2DSparse::massOverCharge);
        c.def_readonly("intensities", &Chromatogram2DSparse::intensities);
    }
    {
        py::class_<Chromatogram2DDense> c(m, "Chromatogram2DDense");
        c.def_readonly("retentionTime", &Chromatogram2DDense::retentionTime);
        c.def_readonly("massOverCharge", &Chromatogram2DDense::massOverCharge);
        c.def_readonly("intensities", &Chromatogram2DDense::intensities);
    }
    {
        py::class_<Chromatogram1D> c(m, "Chromatogram1D");
        c.def_readonly("retentionTime", &Chromatogram1D::retentionTime);
        c.def_readonly("intensities", &Chromatogram1D::intensities);
    }
    {
        py::class_<Spectrum1D> c(m, "Spectrum1D");
        c.def_readonly("massOverCharge", &Spectrum1D::massOverCharge);
        c.def_readonly("intensities", &Spectrum1D::intensities);
    }

    {
        py::class_<SwathMap, std::shared_ptr<SwathMap> > c(m, "SwathMap");
        c.def("name", &SwathMap::name);
        c.def("isMS1", &SwathMap::isMS1);
        c.def("precursorLowerMz", &SwathMap::precursorLowerMz);
        c.def("precursorCenterMz", &SwathMap::precursorCenterMz);
        c.def("precursorUpperMz", &SwathMap::precursorUpperMz);
        c.def("scanCycleTime", &SwathMap::scanCycleTime);
        c.def("firstScanRetentionTimeOffset", &SwathMap::firstScanRetentionTimeOffset);
        c.def("retentionTime", &SwathMap::retentionTime, py::return_value_policy::reference_internal);
        c.def("numberOfSpectra", &SwathMap::numberOfSpectra);
        c.def("getMzTransformer", &SwathMap::getMzTransformer);
        c.def("massOverCharge", &SwathMap::massOverCharge, py::return_value_policy::reference_internal);
        c.def("extractedIonChromatogram", &SwathMap::extractedIonChromatogram);
        c.def("extractedIonChromatogramSparse", &SwathMap::extractedIonChromatogramSparse);
        c.def("filteredExtractedIonChromatogram", &SwathMap::filteredExtractedIonChromatogram);
        c.def("filteredExtractedIonChromatogramSparse", &SwathMap::filteredExtractedIonChromatogramSparse);
    }

    {
        py::class_<SwathMapSpectrumAccessBase, std::shared_ptr<SwathMapSpectrumAccessBase> > c(m, "SwathMapSpectrumAccessBase");
        c.def("name", &SwathMapSpectrumAccessBase::name);
        c.def("isMS1", &SwathMapSpectrumAccessBase::isMS1);
        c.def("precursorLowerMz", &SwathMapSpectrumAccessBase::precursorLowerMz);
        c.def("precursorCenterMz", &SwathMapSpectrumAccessBase::precursorCenterMz);
        c.def("precursorUpperMz", &SwathMapSpectrumAccessBase::precursorUpperMz);
        c.def("scanCycleTime", &SwathMapSpectrumAccessBase::scanCycleTime);
        c.def("firstScanRetentionTimeOffset", &SwathMapSpectrumAccessBase::firstScanRetentionTimeOffset);
        c.def("retentionTime", &SwathMapSpectrumAccessBase::retentionTime, py::return_value_policy::reference_internal);
        c.def("numberOfSpectra", &SwathMapSpectrumAccessBase::numberOfSpectra);
        c.def("getMzTransformer", &SwathMapSpectrumAccess::getMzTransformer);
        c.def("spectrumByIndex", &SwathMapSpectrumAccessBase::spectrumByIndex);
    }
    {
        py::class_<SwathMapSpectrumAccess, SwathMapSpectrumAccessBase, std::shared_ptr<SwathMapSpectrumAccess> > c(m, "SwathMapSpectrumAccess");
    }
    {
        py::class_<SwathMapInMemorySpectrumAccess, SwathMapSpectrumAccessBase, std::shared_ptr<SwathMapInMemorySpectrumAccess> > c(m, "SwathMapInMemorySpectrumAccess");
        c.def("totalIonChromatogram", &SwathMapInMemorySpectrumAccess::totalIonChromatogram);
        c.def("basePeakChromatogram", &SwathMapInMemorySpectrumAccess::basePeakChromatogram);
        c.def("intensityFrequencyCount", &SwathMapInMemorySpectrumAccess::intensityFrequencyCount);
        c.def("toRawSwathScanData", &SwathMapInMemorySpectrumAccess::toRawSwathScanData);
    }
}
}
}
