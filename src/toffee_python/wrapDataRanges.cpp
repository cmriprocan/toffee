/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#include "wrapDataRanges.h"
#include <toffee/DataRanges.h>

namespace py = pybind11;

namespace toffee {
namespace python {
void
wrapDataRanges(
    py::module &m
) {
    {
        py::class_<IMassOverChargeRange> c(m, "IMassOverChargeRange");
        c.def("lowerMzInIMSCoords", &IMassOverChargeRange::lowerMzInIMSCoords);
        c.def("upperMzInIMSCoords", &IMassOverChargeRange::upperMzInIMSCoords);
    }
    {
        py::class_<MassOverChargeRangeWithPixelHalfWidth, IMassOverChargeRange> c(m, "MassOverChargeRangeWithPixelHalfWidth");
        c.def(py::init<double>());
        c.def(py::init<double, int32_t>());
        c.def("massOverCharge", &MassOverChargeRangeWithPixelHalfWidth::massOverCharge);
        c.def("pixelHalfWidth", &MassOverChargeRangeWithPixelHalfWidth::pixelHalfWidth);
        c.def("pixelFullWidth", &MassOverChargeRangeWithPixelHalfWidth::pixelFullWidth);
    }
    {
        py::class_<MassOverChargeRangeWithPPMFullWidth, IMassOverChargeRange> c(m, "MassOverChargeRangeWithPPMFullWidth");
        c.def(py::init<double>());
        c.def(py::init<double, double>());
        c.def("massOverChargeLower", &MassOverChargeRangeWithPPMFullWidth::massOverChargeLower);
        c.def("massOverChargeUpper", &MassOverChargeRangeWithPPMFullWidth::massOverChargeUpper);
    }
    {
        py::class_<MassOverChargeRange, IMassOverChargeRange> c(m, "MassOverChargeRange");
        c.def(py::init<double, double>());
        c.def("massOverChargeLower", &MassOverChargeRange::massOverChargeLower);
        c.def("massOverChargeUpper", &MassOverChargeRange::massOverChargeUpper);
    }
    {
        py::class_<MassOverChargeRangeIMSCoords, IMassOverChargeRange> c(m, "MassOverChargeRangeIMSCoords");
        c.def(py::init<int32_t, int32_t>());
    }

    {
        py::class_<IRetentionTimeRange> c(m, "IRetentionTimeRange");
        c.def_static("toRTIdx", &IRetentionTimeRange::toRTIdx);
        c.def("lowerRTIdx", &IRetentionTimeRange::lowerRTIdx);
        c.def("upperRTIdx", &IRetentionTimeRange::upperRTIdx);
    }
    {
        py::class_<RetentionTimeRangeWithPixelHalfWidth, IRetentionTimeRange> c(m, "RetentionTimeRangeWithPixelHalfWidth");
        c.def(py::init<double>());
        c.def(py::init<double, int32_t>());
        c.def("retentionTime", &RetentionTimeRangeWithPixelHalfWidth::retentionTime);
        c.def("pixelHalfWidth", &RetentionTimeRangeWithPixelHalfWidth::pixelHalfWidth);
        c.def("pixelFullWidth", &RetentionTimeRangeWithPixelHalfWidth::pixelFullWidth);
    }
    {
        py::class_<RetentionTimeRange, IRetentionTimeRange> c(m, "RetentionTimeRange");
        c.def(py::init<double, double>());
        c.def("retentionTimeLower", &RetentionTimeRange::retentionTimeLower);
        c.def("retentionTimeUpper", &RetentionTimeRange::retentionTimeUpper);
    }
}
}
}
