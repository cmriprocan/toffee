/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#include "wrapScanData.h"
#include <pybind11/numpy.h>
#include <toffee/RawSwathScanData.h>
#include <toffee/RawSwathScanDataOps.h>
#include <toffee/ScanDataPointCloud.h>
#include <toffee/ToffeeWriter.h>

namespace py = pybind11;

namespace toffee {
namespace python {
void
wrapScanData(
    py::module &m
) {
    {
        py::class_<RawSwathScanData> c(m, "RawSwathScanData");
        c.def(py::init<const std::string &, IntrinsicMassSpacingType>());
        c.def_readonly("name", &RawSwathScanData::name);
        c.def_readonly("imsType", &RawSwathScanData::imsType);
        c.def_readonly("numberOfScans", &RawSwathScanData::numberOfScans);
        c.def_readwrite("windowLower", &RawSwathScanData::windowLower);
        c.def_readwrite("windowCenter", &RawSwathScanData::windowCenter);
        c.def_readwrite("windowUpper", &RawSwathScanData::windowUpper);
        c.def("addScan", &RawSwathScanData::addScan);
        c.def("toPointCloud", &RawSwathScanData::toPointCloud);
    }
    {
        py::class_<RawSwathScanDataOps> c(m, "RawSwathScanDataOps");
        c.def("calculateIMSProperties", &RawSwathScanDataOps::calculateIMSProperties);
        c.def("mzTransformer", &RawSwathScanDataOps::mzTransformer);
        c.def("applyIMSTransform", &RawSwathScanDataOps::applyIMSTransform);
    }
    {
        py::class_<RawSwathScanDataOpsTOF, RawSwathScanDataOps> c(m, "RawSwathScanDataOpsTOF");
        c.def(py::init<>());
    }
    {
        py::class_<RawSwathScanDataOpsOrbitrap, RawSwathScanDataOps> c(m, "RawSwathScanDataOpsOrbitrap");
        c.def(py::init<>());
    }
    {
        auto setCoordinateFromNumpy = [](
            IMSProperties &c,
            const py::array_t<uint32_t, py::array::c_style | py::array::forcecast> &array
        ) -> void {
            auto r = array.unchecked<1>();
            auto d = r.data(0);
            c.coordinate = std::move(std::vector<uint32_t>(d, d + r.size()));
        };

        py::class_<IMSProperties> c(m, "IMSProperties");
        c.def(py::init<>());
        c.def_readwrite("medianAlpha", &IMSProperties::medianAlpha);
        c.def_readwrite("alpha", &IMSProperties::alpha);
        c.def_readwrite("beta", &IMSProperties::beta);
        c.def_readwrite("medianBeta", &IMSProperties::medianBeta);
        c.def_readwrite("gamma", &IMSProperties::gamma);
        c.def_readwrite("sliceIndex", &IMSProperties::sliceIndex);
        c.def_readwrite("coordinate", &IMSProperties::coordinate);
        c.def("setCoordinateFromNumpy", setCoordinateFromNumpy);
    }
    {
        auto setIntensityFromNumpy = [](
            ScanDataPointCloud &c,
            const py::array_t<uint32_t, py::array::c_style | py::array::forcecast> &array
        ) -> void {
            auto r = array.unchecked<1>();
            auto d = r.data(0);
            c.intensity = std::move(std::vector<uint32_t>(d, d + r.size()));
        };

        py::class_<ScanDataPointCloud> c(m, "ScanDataPointCloud");
        c.def(py::init<>());
        c.def_readwrite("name", &ScanDataPointCloud::name);
        c.def_readwrite("windowLower", &ScanDataPointCloud::windowLower);
        c.def_readwrite("windowCenter", &ScanDataPointCloud::windowCenter);
        c.def_readwrite("windowUpper", &ScanDataPointCloud::windowUpper);
        c.def_readwrite("scanCycleTime", &ScanDataPointCloud::scanCycleTime);
        c.def_readwrite("firstScanRetentionTimeOffset", &ScanDataPointCloud::firstScanRetentionTimeOffset);
        c.def_readwrite("imsType", &ScanDataPointCloud::imsType);
        c.def_readwrite("imsProperties", &ScanDataPointCloud::imsProperties);
        c.def_readwrite("intensity", &ScanDataPointCloud::intensity);
        c.def("setIntensityFromNumpy", setIntensityFromNumpy);
    }
}
}
}
