/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#include <toffee_python/pybind11_wrapper.h>

#include <toffee_python/wrapVersion.h>
#include <toffee_python/wrapMathUtils.h>
#include <toffee_python/wrapDataRanges.h>
#include <toffee_python/wrapSwathMap.h>
#include <toffee_python/wrapSwathRun.h>
#include <toffee_python/wrapToffeeWriter.h>
#include <toffee_python/wrapScanData.h>
#include <toffee_python/wrapMassAccuracyWriter.h>
#include <toffee_python/wrapMassOverChargeTransformer.h>

#include <toffee/io.h>
#include <toffee/Version.h>

namespace py = pybind11;

PYBIND11_MODULE(toffee_python, m) {
    toffee::python::wrapVersion(m);
    toffee::python::wrapMathUtils(m);
    toffee::python::wrapDataRanges(m);
    toffee::python::wrapSwathMap(m);
    toffee::python::wrapSwathRun(m);
    toffee::python::wrapToffeeWriter(m);
    toffee::python::wrapScanData(m);
    toffee::python::wrapMassAccuracyWriter(m);
    toffee::python::wrapMassOverChargeTransformer(m);

    m.attr("__version__") = toffee::Version::LIBRARY_VERSION;
}

