/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#include "wrapToffeeWriter.h"
#include <toffee/IntrinsicMassSpacingType.h>
#include <toffee/ToffeeWriter.h>

namespace py = pybind11;

namespace toffee {
namespace python {
void
wrapToffeeWriter(
    py::module &m
) {
    {
        py::class_<ToffeeWriter> c(m, "ToffeeWriter");
        c.def(py::init<const std::string &, IntrinsicMassSpacingType>());
        c.def("addPointCloud", &ToffeeWriter::addPointCloud);
        c.def("addHeaderMetadata", &ToffeeWriter::addHeaderMetadata);
        c.def_readonly_static("ATTR_MAJOR_VERSION", &ToffeeWriter::ATTR_MAJOR_VERSION);
        c.def_readonly_static("ATTR_MINOR_VERSION", &ToffeeWriter::ATTR_MINOR_VERSION);
        c.def_readonly_static("ATTR_LIBRARY_VERSION", &ToffeeWriter::ATTR_LIBRARY_VERSION);
        c.def_readonly_static("ATTR_IMS_TYPE", &ToffeeWriter::ATTR_IMS_TYPE);
        c.def_readonly_static("ATTR_IMS_ALPHA", &ToffeeWriter::ATTR_IMS_ALPHA);
        c.def_readonly_static("ATTR_IMS_BETA", &ToffeeWriter::ATTR_IMS_BETA);
        c.def_readonly_static("ATTR_IMS_GAMMA", &ToffeeWriter::ATTR_IMS_GAMMA);
        c.def_readonly_static("ATTR_WINDOW_BOUNDS_LOWER", &ToffeeWriter::ATTR_WINDOW_BOUNDS_LOWER);
        c.def_readonly_static("ATTR_WINDOW_BOUNDS_UPPER", &ToffeeWriter::ATTR_WINDOW_BOUNDS_UPPER);
        c.def_readonly_static("ATTR_WINDOW_CENTER", &ToffeeWriter::ATTR_WINDOW_CENTER);
        c.def_readonly_static("DATASET_RETENTION_TIME_IDX", &ToffeeWriter::DATASET_RETENTION_TIME_IDX);
        c.def_readonly_static("DATASET_IMS_ALPHA_PER_SCAN", &ToffeeWriter::DATASET_IMS_ALPHA_PER_SCAN);
        c.def_readonly_static("DATASET_IMS_BETA_PER_SCAN", &ToffeeWriter::DATASET_IMS_BETA_PER_SCAN);
        c.def_readonly_static("DATASET_MZ_IMS_IDX", &ToffeeWriter::DATASET_MZ_IMS_IDX);
        c.def_readonly_static("DATASET_INTENSITY", &ToffeeWriter::DATASET_INTENSITY);
        c.def_readonly_static("MS1_NAME", &ToffeeWriter::MS1_NAME);
        c.def_readonly_static("MS2_PREFIX", &ToffeeWriter::MS2_PREFIX);
        c.def_static("ms2Name", &ToffeeWriter::ms2Name);
        c.def_static("imsTypeAsStr", &ToffeeWriter::imsTypeAsStr);
        c.def_static("imsTypeFromStr", &ToffeeWriter::imsTypeFromStr);
    }
    {
        py::enum_<IntrinsicMassSpacingType> e(m, "IntrinsicMassSpacingType");
        e.value("TOF", IntrinsicMassSpacingType::TOF);
        e.value("ORBITRAP", IntrinsicMassSpacingType::ORBITRAP);
    }
}
}
}
