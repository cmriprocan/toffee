/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#include "SwathRun.h"
#include <boost/algorithm/string/predicate.hpp>
#include <boost/filesystem.hpp>
#include <toffee/io.h>
#include <toffee/Version.h>
#include <toffee/ToffeeWriter.h>

namespace toffee {

namespace {
/// Extract MS2 window properties using a visitor pattern. The complexity in
/// this class comes from the need to cast this into a ``void *`` to pass
/// it into the HDF5 API. Otherwise, it uses RAII to control access to the
/// HDF5 file.
struct MS2WindowInfoFunctor {
    MS2WindowInfoFunctor(
        const std::string &fname,
        bool preVersion1
    ) :
        H5File(fname.c_str(), H5F_ACC_RDONLY),
        result(),
        mPreVersion1(preVersion1) {}

    ~MS2WindowInfoFunctor() {
        H5File.close();
    }

    static herr_t
    callback(
        hid_t locId,
        const char *name,
        const H5L_info_t *linfo,
        void *opdata
    ) {
        auto functor = reinterpret_cast<MS2WindowInfoFunctor *>(opdata);
        if (boost::starts_with(name, ToffeeWriter::MS2_PREFIX)) {
            MS2WindowDescriptor window;
            window.name = name;
            H5::Group group = functor->H5File.openGroup(window.name);
            auto datatype = H5::PredType::NATIVE_DOUBLE;
            auto readAttr = [&group, &datatype, &name](const std::string &attrName, double &val) -> void {
                try{
                    group.openAttribute(attrName).read(datatype, &val);
                } catch(H5::Exception& e) {
                    throw_assert(
                        false,
                        "Failed to read: " << attrName << " in " << name
                    );
                }
            };
            readAttr(
                functor->mPreVersion1 ? "lower" : ToffeeWriter::ATTR_WINDOW_BOUNDS_LOWER,
                window.lower
            );
            readAttr(
                functor->mPreVersion1 ? "upper" : ToffeeWriter::ATTR_WINDOW_BOUNDS_UPPER,
                window.upper
            );
            functor->result.push_back(window);
        }
        return 0;
    }

    H5::H5File H5File;
    std::vector<MS2WindowDescriptor> result;

private:
    bool mPreVersion1;
};
}

SwathRun::SwathRun(
    const std::string &fname
) :
    mFilePath(fname),
    mHeader(),
    mFormatMajorVersion(),
    mFormatMinorVersion(),
    mIMSType(IntrinsicMassSpacingType::TOF), // default to Sciex
    mMS2Windows(),
    mHasMS1Data(false)
{
    throw_assert(
        boost::filesystem::exists(fname),
        "File does not exist: " << fname
    );

    // ---
    // read HDF attributes
    H5::H5File h5File(fname.c_str(), H5F_ACC_RDONLY);

    // check if we can open the MS1 datasets
    try {
        H5::Exception::dontPrint();
        h5File.openGroup(ToffeeWriter::MS1_NAME);
        mHasMS1Data = true;
    } catch(H5::Exception& e) {
        mHasMS1Data = false;
    }

    // header -- it was once an attribute, but is now a dataset
    try {
        H5::Exception::dontPrint();
        auto ds = h5File.openDataSet(ToffeeWriter::ATTR_EXPERIMENT_METADATA);
        ds.read(mHeader, ds.getStrType(), ds.getSpace());
    } catch(H5::Exception& e) {
        auto attr = h5File.openAttribute(ToffeeWriter::ATTR_EXPERIMENT_METADATA);
        attr.read(attr.getDataType(), mHeader);
    }

    // versions
    {
        auto attr = h5File.openAttribute(ToffeeWriter::ATTR_MAJOR_VERSION);
        attr.read(attr.getDataType(), &mFormatMajorVersion);
        attr = h5File.openAttribute(ToffeeWriter::ATTR_MINOR_VERSION);
        attr.read(attr.getDataType(), &mFormatMinorVersion);
    }

    // IMS type
    std::string imsTypeStr;
    if (mFormatMajorVersion == 0) { // for backwards compatability, assume Sciex file
        imsTypeStr = "TOF";
    }
    else {
        auto attr = h5File.openAttribute(ToffeeWriter::ATTR_IMS_TYPE);
        attr.read(attr.getDataType(), imsTypeStr);
    }
    mIMSType = ToffeeWriter::imsTypeFromStr(imsTypeStr);

    h5File.close();

    // ---
    // check version
    auto totalVersion = Version::combineFileFormatVersions(mFormatMajorVersion, mFormatMinorVersion);
    auto totalVersionCompatability = Version::combineFileFormatVersions(
            Version::FILE_FORMAT_MAJOR_VERSION_COMPATIBILITY,
            Version::FILE_FORMAT_MINOR_VERSION_COMPATIBILITY
    );
    throw_assert(
        totalVersion >= totalVersionCompatability,
        "File format major version is less than the current compatability version! "
            << mFormatMajorVersion
            << "."
            << mFormatMinorVersion
            << " < "
            << Version::FILE_FORMAT_MAJOR_VERSION_COMPATIBILITY
            << "."
            << Version::FILE_FORMAT_MINOR_VERSION_COMPATIBILITY
    );

    // ---
    // load MS2 windows
    MS2WindowInfoFunctor functor(mFilePath, mFormatMajorVersion == 0);
    H5Literate(
        functor.H5File.getId(),
        H5_INDEX_NAME,
        H5_ITER_INC,
        nullptr,
        MS2WindowInfoFunctor::callback,
        &functor
    );
    mMS2Windows = functor.result;
}

std::map<double, std::string>
SwathRun::mapPrecursorsToMS2Names(
    const std::vector<double> &precursorMzValues,
    double lowerMzOverlap,
    double upperMzOverlap
) const
{
    std::map<double, std::string> result;
    for (const auto& precursorMz : precursorMzValues) {
        for (const auto &window : mMS2Windows) {
            if (precursorMz >= window.lower + lowerMzOverlap &&
                    precursorMz <= window.upper - upperMzOverlap) {
                result[precursorMz] = window.name;
                break;
            }
        }
    }
    return result;
}

namespace {
struct WindowAttrs {
    double imsAlpha;
    double imsBeta;
    int32_t imsGamma;
    double lower;
    double center;
    double upper;
    double scanCycleTime;
    double firstScanRetentionTimeOffset;
};

WindowAttrs
readAttrs(
    const H5::H5File &h5File,
    const std::string &mapName,
    int version
) {
    // prior to v1.0, we had different attribute names
    auto version1p0 = Version::combineFileFormatVersions(1, 0);

    WindowAttrs result;

    H5::Group group;
    try {
        group = h5File.openGroup(mapName.c_str());
    } catch(H5::Exception& e) {
        throw_assert(
            false,
            "Failed to read group: " << mapName
        );
    }

    auto readAttr = [&group, &mapName](const std::string &attrName, double &val) -> void {
        try {
            auto datatype = H5::PredType::NATIVE_DOUBLE;
            group.openAttribute(attrName).read(datatype, &val);
        } catch(H5::Exception& e) {
            throw_assert(
                false,
                "Failed to read: " << attrName << " in " << mapName
            );
        }
    };
    auto readAttrI32 = [&group, &mapName](const std::string &attrName, int32_t &val) -> void {
        try {
            auto datatype = H5::PredType::NATIVE_INT32;
            group.openAttribute(attrName).read(datatype, &val);
        } catch(H5::Exception& e) {
            throw_assert(
                false,
                "Failed to read: " << attrName << " in " << mapName
            );
        }
    };
    if (version < version1p0) {
        readAttr("rootMzIdxAlpha", result.imsAlpha);
        readAttr("rootMzIdxBeta", result.imsBeta);
        result.imsGamma = 0;
        readAttr("lower", result.lower);
        readAttr("center", result.center);
        readAttr("upper", result.upper);
    }
    else {
        readAttr(ToffeeWriter::ATTR_IMS_ALPHA, result.imsAlpha);
        readAttr(ToffeeWriter::ATTR_IMS_BETA, result.imsBeta);
        readAttrI32(ToffeeWriter::ATTR_IMS_GAMMA, result.imsGamma);
        readAttr(ToffeeWriter::ATTR_WINDOW_BOUNDS_LOWER, result.lower);
        readAttr(ToffeeWriter::ATTR_WINDOW_CENTER, result.center);
        readAttr(ToffeeWriter::ATTR_WINDOW_BOUNDS_UPPER, result.upper);
    }
    readAttr(ToffeeWriter::ATTR_SCAN_CYCLE_TIME, result.scanCycleTime);
    readAttr(ToffeeWriter::ATTR_WINDOW_RETENTION_TIME_OFFSET, result.firstScanRetentionTimeOffset);

    return result;
}

struct WindowDatasets {
    std::vector<uint32_t> scanSliceIndices;
    std::vector<double> scanAlpha;
    std::vector<double> scanBeta;
    std::vector<uint32_t> imsCoordinates;
    std::vector<uint32_t> intensities;
};

WindowDatasets
readDatasets(
    const H5::H5File &h5File,
    const std::string &mapName,
    int version,
    bool includeIntensities
) {
    // prior to v1.0, we had different dataset names
    auto version1p0 = Version::combineFileFormatVersions(1, 0);
    // prior to v1.1 we didn't store alpha and beta per scan
    auto version1p1 = Version::combineFileFormatVersions(1, 1);

    WindowDatasets result;
    
    H5::Group group;
    try {
        group = h5File.openGroup(mapName.c_str());
    } catch(H5::Exception& e) {
        throw_assert(
            false,
            "Failed to read group: " << mapName
        );
    }

    auto readDataset = [&group, &mapName](const std::string &datasetName) -> H5::DataSet {
        try{
            return group.openDataSet(datasetName);
        } catch(H5::Exception& e) {
            throw_assert(
                false,
                "Failed to read: " << datasetName << " in " << mapName
            );
        }
    };

    // ---
    // read in the retentionTimeIdx arrays
    static constexpr size_t RANK = 1;
    hsize_t dims[RANK];
    H5::DataSet rtIdxDataset = readDataset(
        version < version1p0 ? "rtIdx" : ToffeeWriter::DATASET_RETENTION_TIME_IDX
    );
    H5::DataSpace dataspace = rtIdxDataset.getSpace();
    int rank = dataspace.getSimpleExtentDims(dims);
    throw_assert(rank == RANK, "invalid rank for dataset: " << rank << " != " << RANK);
    H5::DataSpace memoryspace(RANK, dims);
    result.scanSliceIndices = std::vector<uint32_t>(dims[0]);
    auto datatype = H5::PredType::NATIVE_UINT32;
    rtIdxDataset.read(result.scanSliceIndices.data(), datatype, memoryspace, dataspace);

    // read in scan alpha and beta
    if (version >= version1p1) {
        result.scanAlpha = std::vector<double>(dims[0]);
        result.scanBeta = std::vector<double>(dims[0]);

        datatype = H5::PredType::NATIVE_DOUBLE;
        readDataset(ToffeeWriter::DATASET_IMS_ALPHA_PER_SCAN)
            .read(result.scanAlpha.data(), datatype, memoryspace, dataspace);
        readDataset(ToffeeWriter::DATASET_IMS_BETA_PER_SCAN)
            .read(result.scanBeta.data(), datatype, memoryspace, dataspace);
    }

    // ---
    // read in the large arrays
    H5::DataSet imsCoordinatesDataset = readDataset(
        version < version1p0 ? "rootMzIdx" : ToffeeWriter::DATASET_MZ_IMS_IDX
    );
    dataspace = imsCoordinatesDataset.getSpace();
    rank = dataspace.getSimpleExtentDims(dims);
    throw_assert(rank == RANK, "invalid rank for dataset: " << rank << " != " << RANK);
    memoryspace = H5::DataSpace(RANK, dims);
    datatype = H5::PredType::NATIVE_UINT32;

    result.imsCoordinates = std::vector<uint32_t>(dims[0]);
    imsCoordinatesDataset.read(result.imsCoordinates.data(), datatype, memoryspace, dataspace);

    if (includeIntensities) {
        H5::DataSet intensityDataset = readDataset(ToffeeWriter::DATASET_INTENSITY);
        result.intensities = std::vector<uint32_t>(dims[0]);
        intensityDataset.read(result.intensities.data(), datatype, memoryspace, dataspace);
    }

    return result;
}

std::shared_ptr<const MassOverChargeTransformer>
createMzTransformer(
    IntrinsicMassSpacingType imsType,
    double alpha,
    double beta,
    int32_t gamma,
    SwathMap::RTreeIndex lowerIMSCoord,
    SwathMap::RTreeIndex upperIMSCoord
) {
    std::shared_ptr<const MassOverChargeTransformer> mzTransformer{nullptr};
    switch (imsType) {
    case IntrinsicMassSpacingType::TOF:
        mzTransformer = std::make_shared<const MassOverChargeTransformerTOF>(
            alpha,
            beta,
            gamma,
            lowerIMSCoord,
            upperIMSCoord
        );
        break;
    case IntrinsicMassSpacingType::ORBITRAP:
        mzTransformer = std::make_shared<const MassOverChargeTransformerOrbitrap>(
            alpha,
            beta,
            gamma,
            lowerIMSCoord,
            upperIMSCoord
        );
        break;
    }
    return mzTransformer;
}

struct WindowMzTransformers {
    std::shared_ptr<const MassOverChargeTransformer> global;
    std::vector<std::shared_ptr<const MassOverChargeTransformer> > scanByScan;
};

WindowMzTransformers
createMzTransformers(
    IntrinsicMassSpacingType imsType,
    const WindowAttrs &attr,
    const WindowDatasets &ds,
    int version
) {
    WindowMzTransformers result;
    auto lowerIMSCoord = *std::min_element(ds.imsCoordinates.begin(), ds.imsCoordinates.end());
    auto upperIMSCoord = *std::max_element(ds.imsCoordinates.begin(), ds.imsCoordinates.end());

    // prior to v1.1 we didn't store alpha and beta per scan
    auto version1p1 = Version::combineFileFormatVersions(1, 1);
    // from v1.2 onwards, scan alpha and beta are not relative
    auto version1p2 = Version::combineFileFormatVersions(1, 2);

    result.global = createMzTransformer(
        imsType,
        attr.imsAlpha,
        attr.imsBeta,
        attr.imsGamma,
        lowerIMSCoord,
        upperIMSCoord
    );

    for (size_t i = 0; i < ds.scanAlpha.size(); ++i) {
        double alpha;
        double beta;
        if (version < version1p1) {
            alpha = attr.imsAlpha;
            beta = attr.imsBeta;
        }
        else if (version >= version1p2) {
            alpha = ds.scanAlpha[i];
            beta = ds.scanBeta[i];
        }
        else {
            alpha = ds.scanAlpha[i] + attr.imsAlpha;
            beta = ds.scanBeta[i] + attr.imsBeta;
        }
        result.scanByScan.push_back(createMzTransformer(
            imsType,
            alpha,
            beta,
            attr.imsGamma,
            lowerIMSCoord,
            upperIMSCoord
        ));
    }

    return result;
}
}

SwathMap
SwathRun::loadSwathMap(
    const std::string &mapName,
    bool lossyAdjustIMSCoords
) const
{
    return *loadSwathMapPtr(mapName, lossyAdjustIMSCoords);
}

std::shared_ptr<SwathMap>
SwathRun::loadSwathMapPtr(
    const std::string &mapName,
    bool lossyAdjustIMSCoords
) const 
{
    // read data from the HDF5
    H5::H5File h5File(mFilePath.c_str(), H5F_ACC_RDONLY);
    auto version = Version::combineFileFormatVersions(mFormatMajorVersion, mFormatMinorVersion);
    auto attr = readAttrs(h5File, mapName, version);
    auto ds = readDatasets(h5File, mapName, version, true);
    h5File.close();
    auto mzTransformers = createMzTransformers(mIMSType, attr, ds, version);

    // build point cloud
    auto numScans = ds.scanSliceIndices.size();
    auto numPoints = ds.imsCoordinates.size();

    std::vector<SwathMap::PCLPoint> points;
    points.reserve(numPoints);
    for (size_t retentionTimeIdx = 0; retentionTimeIdx < numScans; ++retentionTimeIdx) {
        size_t start = (retentionTimeIdx == 0) ? 0 : ds.scanSliceIndices[retentionTimeIdx - 1];
        size_t end = ds.scanSliceIndices[retentionTimeIdx];
        // allow an empty scan to be skipped
        if (start == end) {
            continue;
        }
        throw_assert(
            start < numPoints,
            "Out of bounds: " << start << ", " << numPoints
        );
        throw_assert(
            end <= numPoints,
            "Out of bounds" << end << ", " << numPoints
        );
        auto rtIdx = boost::numeric_cast<SwathMap::RTreeIndex>(retentionTimeIdx);

        // if we are adjusting the IMS coords to better match the window wide alpha and beta
        // parameters, we need to make sure that we don't create two points with the same
        // coordinate. We make this adjustment by "round-tripping" between the scan transformer
        // and the global transformer.
        //
        // Warning: using this option is lossy. You will not be able to undo thise summation
        // of overlapping coordinates
        if (lossyAdjustIMSCoords) {
            auto scanMzTransformer = mzTransformers.scanByScan[retentionTimeIdx];
            std::map<SwathMap::RTreeIndex, SwathMap::RTreeIntensity> pointMap;
            for (size_t i = start; i < end; ++i) {
                auto mz = scanMzTransformer->toWorldCoords(ds.imsCoordinates[i]);
                auto imsCoord = boost::numeric_cast<SwathMap::RTreeIndex>(
                    mzTransformers.global->toIMSCoords(mz)
                );
                pointMap[imsCoord] += ds.intensities[i];
            }
            for (const auto& point : pointMap) {
                points.emplace_back(
                    std::make_pair(
                        SwathMap::RtImsCoord(rtIdx, point.first),
                        point.second
                    )
                );
            }
        }
        else {
            for (size_t i = start; i < end; ++i) {
                auto imsCoord = boost::numeric_cast<SwathMap::RTreeIndex>(ds.imsCoordinates[i]);
                points.emplace_back(
                    std::make_pair(
                        SwathMap::RtImsCoord(rtIdx, imsCoord),
                        ds.intensities[i]
                    )
                );
            }
        }
    }

    bool isMS1 = mapName == ToffeeWriter::MS1_NAME;
    return std::make_shared<SwathMap>(
        mapName,
        attr.lower,
        attr.center,
        attr.upper,
        isMS1,
        attr.scanCycleTime,
        attr.firstScanRetentionTimeOffset,
        numScans,
        mzTransformers.global,
        points
    );
}

SwathMapInMemorySpectrumAccess
SwathRun::loadSwathMapInMemorySpectrumAccess(
    const std::string &mapName
) const 
{
    return *loadSwathMapInMemorySpectrumAccessPtr(mapName);
}

std::shared_ptr<SwathMapInMemorySpectrumAccess>
SwathRun::loadSwathMapInMemorySpectrumAccessPtr(
    const std::string &mapName
) const 
{
    // read data from the HDF5
    H5::H5File h5File(mFilePath.c_str(), H5F_ACC_RDONLY);
    auto version = Version::combineFileFormatVersions(mFormatMajorVersion, mFormatMinorVersion);
    auto attr = readAttrs(h5File, mapName, version);
    auto ds = readDatasets(h5File, mapName, version, true);
    h5File.close();
    throw_assert(
        ds.scanAlpha.size() > 0,
        "SwathMapInMemorySpectrumAccess was only introduced after file format v1.1."
            << " This file is only on v"
            << mFormatMajorVersion
            << "."
            << mFormatMinorVersion
    );

    // create a m/z transformer for each scan
    auto mzTransformers = createMzTransformers(mIMSType, attr, ds, version).scanByScan;

    bool isMS1 = mapName == ToffeeWriter::MS1_NAME;
    return std::make_shared<SwathMapInMemorySpectrumAccess>(
        mapName,
        attr.lower,
        attr.center,
        attr.upper,
        isMS1,
        attr.scanCycleTime,
        attr.firstScanRetentionTimeOffset,
        mzTransformers,
        ds.scanSliceIndices,
        ds.intensities,
        ds.imsCoordinates
    );
}


SwathMapSpectrumAccess
SwathRun::loadSwathMapSpectrumAccess(
    const std::string &mapName
) const 
{
    return *loadSwathMapSpectrumAccessPtr(mapName);
}

std::shared_ptr<SwathMapSpectrumAccess>
SwathRun::loadSwathMapSpectrumAccessPtr(
    const std::string &mapName
) const 
{
    // read data from the HDF5
    H5::H5File h5File(mFilePath.c_str(), H5F_ACC_RDONLY);
    auto version = Version::combineFileFormatVersions(mFormatMajorVersion, mFormatMinorVersion);
    auto attr = readAttrs(h5File, mapName, version);
    auto ds = readDatasets(h5File, mapName, version, false);
    h5File.close();
    throw_assert(
        ds.scanAlpha.size() > 0,
        "SwathMapSpectrumAccess was only introduced after file format v1.1."
            << " This file is only on v"
            << mFormatMajorVersion
            << "."
            << mFormatMinorVersion
    );

    // create a m/z transformer for each scan
    auto mzTransformers = createMzTransformers(mIMSType, attr, ds, version).scanByScan;

    bool isMS1 = mapName == ToffeeWriter::MS1_NAME;
    return std::make_shared<SwathMapSpectrumAccess>(
        mFilePath,
        mapName,
        attr.lower,
        attr.center,
        attr.upper,
        isMS1,
        attr.scanCycleTime,
        attr.firstScanRetentionTimeOffset,
        mzTransformers,
        ds.scanSliceIndices
    );
}
}
