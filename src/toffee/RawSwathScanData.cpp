/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#include "RawSwathScanData.h"
#include <fstream>
#include <iostream>
#include <algorithm>
#include <numeric>
#include <toffee/MathUtils.h>
#include <toffee/SwathRun.h>
#include <toffee/io.h>
#include <toffee/ToffeeWriter.h>

namespace toffee {
RawSwathScanData::RawSwathScanData(
    const std::string &name,
    IntrinsicMassSpacingType imsType
) :
    name(name),
    imsType(imsType),
    mMassAccuracyWriter(nullptr),
    mRawSwathScanDataOps(nullptr) 
{
    switch (this->imsType) {
    case IntrinsicMassSpacingType::TOF:
        mRawSwathScanDataOps = std::make_shared<const RawSwathScanDataOpsTOF>();
        break;
    case IntrinsicMassSpacingType::ORBITRAP:
        mRawSwathScanDataOps = std::make_shared<const RawSwathScanDataOpsOrbitrap>();
        break;
    }
}

ScanDataPointCloud
RawSwathScanData::toPointCloud() const {
    throw_assert(
        numberOfScans == retentionTime.size(),
        "Something has gone wrong reading the file. Number of scans doesn't match data"
    );
    throw_assert(
        numberOfScans == mzData.size(),
        "Something has gone wrong reading the file. Number of scans doesn't match data"
    );
    throw_assert(
        numberOfScans == intensityData.size(),
        "Something has gone wrong reading the file. Number of scans doesn't match data"
    );

    ScanDataPointCloud pcl;
    pcl.name = name;
    pcl.imsType = imsType;
    pcl.windowLower = windowLower;
    pcl.windowCenter = windowCenter;
    pcl.windowUpper = windowUpper;

    // ---
    // calculate the retention time properties
    calculateRetentionTimeProperties(retentionTime, pcl);
    pcl.imsProperties = mRawSwathScanDataOps->calculateIMSProperties(mzData);
    throw_assert(
        pcl.imsProperties.sliceIndex.size() == numberOfScans,
        "Number of scans different to expected: "
            << pcl.imsProperties.sliceIndex.size() << " != " << numberOfScans
    );

    // ---
    // set up intensity and retention time vectors
    auto sliceIndex = pcl.imsProperties.sliceIndex;
    auto alpha = pcl.imsProperties.alpha;
    auto beta = pcl.imsProperties.beta;
    pcl.imsProperties.sliceIndex.clear();
    pcl.imsProperties.alpha.clear();
    pcl.imsProperties.beta.clear();
    std::vector<int> imsPropToMZDataIndexMapping;

    auto toRtIdx = [&pcl](double rt) -> size_t {
        return boost::numeric_cast<size_t>(std::round(
            (rt - pcl.firstScanRetentionTimeOffset) / pcl.scanCycleTime
        ));
    };

    size_t expectedRtIdx = 0;
    for (size_t i = 0; i < numberOfScans; ++i) {
        // fill up the rtIdx vector for the cases where a retention time was missed
        auto rt = retentionTime[i];
        auto currentRtIdx = toRtIdx(rt);
        // expand out for missing retention times
        for (size_t j = expectedRtIdx; j <= currentRtIdx; ++j) {
            pcl.imsProperties.sliceIndex.push_back(sliceIndex[i]);
            pcl.imsProperties.alpha.push_back(alpha[i]);
            pcl.imsProperties.beta.push_back(beta[i]);
            // set this to -1 in the cases where we have a missed retention time
            imsPropToMZDataIndexMapping.push_back((j > expectedRtIdx) ? -1 : boost::numeric_cast<int>(i));
        }
        expectedRtIdx = currentRtIdx + 1;

        // only need to add to the point cloud if this scan has data
        const auto &intensity = intensityData[i];
        if (!intensity.empty()) {
            pcl.intensity.insert(pcl.intensity.end(), intensity.begin(), intensity.end());
        }
    }

    // ---
    auto numPoints = pcl.imsProperties.sliceIndex.back();
    throw_assert(
        pcl.intensity.size() == numPoints,
        "The last element of imsProperties.sliceIndex should match the size of intensity"
    );
    throw_assert(
        pcl.imsProperties.coordinate.size() == numPoints,
        "The last element of imsProperties.sliceIndex should match the size of imsProperties.coordinate"
    );
    auto expectedNumberOfScans = toRtIdx(retentionTime.back()) + 1;
    throw_assert(
        pcl.imsProperties.sliceIndex.size() == expectedNumberOfScans,
        "The size of imsProperties.sliceIndex should match the index of the last retention time: "
            << pcl.imsProperties.sliceIndex.size() << " != " << expectedNumberOfScans
    );
    throw_assert(
        pcl.imsProperties.alpha.size() == expectedNumberOfScans,
        "The size of imsProperties.alpha should match the index of the last retention time: "
            << pcl.imsProperties.alpha.size() << " != " << expectedNumberOfScans
    );
    throw_assert(
        pcl.imsProperties.beta.size() == expectedNumberOfScans,
        "The size of imsProperties.beta should match the index of the last retention time: "
            << pcl.imsProperties.beta.size() << " != " << expectedNumberOfScans
    );

    // ---
    // debugging for mass accuracy
    if (mMassAccuracyWriter) {
        addMassAccuracyData(pcl, imsPropToMZDataIndexMapping);
    }
    return pcl;
}

void
RawSwathScanData::addScan(
    double rt,
    const std::vector<double> &mz,
    const std::vector<uint32_t> &intensity
) {
    ++numberOfScans;
    retentionTime.emplace_back(rt);
    mzData.emplace_back(mz);
    intensityData.emplace_back(intensity);
}

void
RawSwathScanData::addMassAccuracyWriter(
    const std::shared_ptr<const MassAccuracyWriter> &massAccuracyWriter
) {
    mMassAccuracyWriter = massAccuracyWriter;
}

void
RawSwathScanData::calculateRetentionTimeProperties(
    const std::vector<double> &retentionTime,
    ScanDataPointCloud &pcl
) const {
    // calculate the differences to find the cycle time
    std::vector<double> deltaRT(retentionTime);
    std::adjacent_difference(
        deltaRT.begin(),
        deltaRT.end(),
        deltaRT.begin()
    );
    pcl.scanCycleTime = MathUtils::median(deltaRT);

    // calculate the offset to the first scan for this window
    pcl.firstScanRetentionTimeOffset = retentionTime[0];
    while (true) {
        if (pcl.firstScanRetentionTimeOffset - pcl.scanCycleTime < 0) {
            break;
        }
        pcl.firstScanRetentionTimeOffset -= pcl.scanCycleTime;
    }
}

void
RawSwathScanData::addMassAccuracyData(
    const ScanDataPointCloud &pcl,
    const std::vector<int> &imsPropToMZDataIndexMapping
) const {
    const auto &imsProps = pcl.imsProperties;
    auto numPoints = imsProps.coordinate.size();
    auto numScans = imsPropToMZDataIndexMapping.size();

    std::vector<double> originalMz(numPoints, 0);
    std::vector<double> convertedMzLossless(numPoints, 0);
    std::vector<double> convertedMzLossy(numPoints, 0);
    std::vector<double> rt(numPoints, 0);

    auto globalTransformer = mRawSwathScanDataOps->mzTransformer(
        imsProps.medianAlpha,
        imsProps.medianBeta,
        imsProps.gamma
    );
    for (size_t i = 0; i < numScans; ++i) {
        uint32_t start = (i == 0) ? 0 : imsProps.sliceIndex[i - 1];
        uint32_t end = imsProps.sliceIndex[i];

        // index into mzData
        auto mzIdx = imsPropToMZDataIndexMapping[i];
        if (mzIdx < 0) { // this is set when there is a missed scan
            throw_assert(
                start == end,
                "This condition should only be met for empty scans: "
                    << start << " != " << end
                    << ". i = " << i
            );
            continue;
        }
        const auto &mz = mzData[mzIdx];
        throw_assert(
            end - start == mz.size(),
            "Slicing index has gone awry: "
                << end - start
                << " != "
                << mz.size()
                << ". i = "
                << i
                << ", imsPropToMZDataIndexMapping[i] = "
                << imsPropToMZDataIndexMapping[i]
                << ", start = "
                << start
                << ", end = "
                << end
        );

        auto scanTransformer = mRawSwathScanDataOps->mzTransformer(
            imsProps.alpha[i] + imsProps.medianAlpha,
            imsProps.beta[i] + imsProps.medianBeta,
            imsProps.gamma
        );
        for (size_t j = start; j < end; ++j) {
            originalMz[j] = mz[j - start];
            convertedMzLossless[j] = scanTransformer->toWorldCoords(imsProps.coordinate[j]);
            convertedMzLossy[j] = globalTransformer->toWorldCoords(imsProps.coordinate[j]);
            rt[j] = retentionTime[i];
        }
    }
    mMassAccuracyWriter->addWindowData(
        pcl.name,
        originalMz,
        convertedMzLossless,
        convertedMzLossy,
        rt
    );
}
}
