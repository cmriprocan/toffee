/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#include "SwathMapSpectrumAccess.h"
#include <H5Cpp.h>
#include <boost/filesystem.hpp>
#include <toffee/io.h>
#include <toffee/ToffeeWriter.h>
#include <toffee/Version.h>

namespace bg = boost::geometry;
namespace bgi = boost::geometry::index;

namespace toffee {
namespace {
std::vector<uint32_t>
readDataset(
    const H5::Group &group,
    const std::string &groupName,
    const std::string &datasetName,
    uint32_t start,
    uint32_t numPoints
) {
    H5::DataSet dataset;
    try {
        dataset = group.openDataSet(datasetName);
    } catch(H5::Exception& e) {
        throw_assert(
            false,
            "Failed to read: " << datasetName << " in " << groupName
        );
    }

    static constexpr size_t RANK = 1;
    auto datatype = H5::PredType::NATIVE_UINT32;
    hsize_t dims[RANK] = {numPoints};

    H5::DataSpace memoryspace(RANK, dims);
    // Define the column (hyperslab) to read.
    hsize_t offset[RANK] = {start};
    hsize_t count[RANK] = {numPoints};
    std::vector<uint32_t> result(numPoints);

    H5::DataSpace filespace = dataset.getSpace();
    filespace.selectHyperslab(H5S_SELECT_SET, count, offset);
    dataset.read(result.data(), datatype, memoryspace, filespace);

    return result;
}
}

SwathMapSpectrumAccessBase::SwathMapSpectrumAccessBase() :
    mName(),
    mWindowPrecursorLowerMz(-1),
    mWindowPrecursorCenterMz(-1),
    mWindowPrecursorUpperMz(-1),
    mIsMS1(false),
    mScanCycleTime(-1),
    mFirstScanRetentionTimeOffset(-1),
    mRetentionTime(0),
    mMzTransformers(),
    mSliceIndicies() {
}

SwathMapSpectrumAccessBase::SwathMapSpectrumAccessBase(
    std::string name,
    double lower,
    double center,
    double upper,
    bool isMS1,
    double scanCycleTime,
    double firstScanRetentionTimeOffset,
    std::vector<std::shared_ptr<const MassOverChargeTransformer> > mzTransformers,
    std::vector<uint32_t> sliceIndicies
) :
    mName(name),
    mWindowPrecursorLowerMz(lower),
    mWindowPrecursorCenterMz(center),
    mWindowPrecursorUpperMz(upper),
    mIsMS1(isMS1),
    mScanCycleTime(scanCycleTime),
    mFirstScanRetentionTimeOffset(firstScanRetentionTimeOffset),
    mRetentionTime(sliceIndicies.size()),
    mMzTransformers(mzTransformers),
    mSliceIndicies(sliceIndicies)
{
    throw_assert(
        mSliceIndicies.size() == mMzTransformers.size(),
        "There must be exactly one transformer for each scan: "
            << mSliceIndicies.size()
            << " != "
            << mMzTransformers.size()
    );

    // check the transformers
    if (!mMzTransformers.empty()) { 
        auto imsType = mMzTransformers.front()->imsType();
        for (const auto &t : mMzTransformers) {
            throw_assert(t->imsType() == imsType, "All transformers should have the same type");
        }
    }

    // set up the retention time vector
    for (size_t i = 0; i < mzTransformers.size(); ++i) {
        mRetentionTime[i] = mFirstScanRetentionTimeOffset + i * mScanCycleTime;
    }
}

SwathMapSpectrumAccessBase &
SwathMapSpectrumAccessBase::operator=(
    const SwathMapSpectrumAccessBase &other
) {
    if (this != &other) {
        *this = other;
    }
    return *this;
}

SwathMapSpectrumAccessBase::SwathMapSpectrumAccessBase(
    SwathMapSpectrumAccessBase &&other
) noexcept :
    mName(std::move(other.mName)),
    mWindowPrecursorLowerMz(other.mWindowPrecursorLowerMz),
    mWindowPrecursorCenterMz(other.mWindowPrecursorCenterMz),
    mWindowPrecursorUpperMz(other.mWindowPrecursorUpperMz),
    mIsMS1(other.mIsMS1),
    mScanCycleTime(other.mScanCycleTime),
    mFirstScanRetentionTimeOffset(other.mFirstScanRetentionTimeOffset),
    mRetentionTime(std::move(other.mRetentionTime)),
    mMzTransformers(std::move(other.mMzTransformers)),
    mSliceIndicies(std::move(other.mSliceIndicies)) {}

SwathMapSpectrumAccessBase&
SwathMapSpectrumAccessBase::operator=(
    SwathMapSpectrumAccessBase &&other
) noexcept {
    assert(this != &other);
    mName = std::move(other.mName);
    mWindowPrecursorLowerMz = other.mWindowPrecursorLowerMz;
    mWindowPrecursorCenterMz = other.mWindowPrecursorCenterMz;
    mWindowPrecursorUpperMz = other.mWindowPrecursorUpperMz;
    mIsMS1 = other.mIsMS1;
    mScanCycleTime = other.mScanCycleTime;
    mFirstScanRetentionTimeOffset = other.mFirstScanRetentionTimeOffset;
    mRetentionTime = std::move(other.mRetentionTime);
    mMzTransformers = std::move(other.mMzTransformers);
    mSliceIndicies = std::move(other.mSliceIndicies);
    return *this;
}

SwathMapSpectrumAccess::SwathMapSpectrumAccess() :
    SwathMapSpectrumAccessBase(),
    mTofFilepathPtr(nullptr) {
}

SwathMapSpectrumAccess::SwathMapSpectrumAccess(
    std::string tofFilepath,
    std::string name,
    double lower,
    double center,
    double upper,
    bool isMS1,
    double scanCycleTime,
    double firstScanRetentionTimeOffset,
    std::vector<std::shared_ptr<const MassOverChargeTransformer> > mzTransformers,
    std::vector<uint32_t> sliceIndicies
) :
    SwathMapSpectrumAccessBase(
        name,
        lower,
        center,
        upper,
        isMS1,
        scanCycleTime,
        firstScanRetentionTimeOffset,
        mzTransformers,
        sliceIndicies
    ),
    mTofFilepathPtr(nullptr)
{
    // ---
    // uncompress the data from the original tof file as reading compressed data slices
    // is super slow!

    // read data
    uint32_t start{0};
    auto numPoints = mSliceIndicies.back();
    H5::H5File compressedH5File(tofFilepath.c_str(), H5F_ACC_RDONLY);
    H5::Group compressedGroup = compressedH5File.openGroup(mName);
    auto coordinates = readDataset(
        compressedGroup,
        mName,
        ToffeeWriter::DATASET_MZ_IMS_IDX,
        start,
        numPoints
    );
    auto intensities = readDataset(
        compressedGroup,
        mName,
        ToffeeWriter::DATASET_INTENSITY,
        start,
        numPoints
    );
    compressedH5File.close();

    // write data
    mTofFilepathPtr = std::make_shared<std::string>(tofFilepath + "." + mName + ".cache");
    H5::H5File cachedH5File(mTofFilepathPtr->c_str(), H5F_ACC_TRUNC);
    auto group = cachedH5File.createGroup(mName);
    static constexpr size_t RANK = 1;
    hsize_t dims[RANK] = {coordinates.size()};

    H5::DSetCreatPropList plist;
    H5::DataSpace dataspace(RANK, dims);

    auto datatype = H5::PredType::NATIVE_UINT32;
    group.createDataSet(ToffeeWriter::DATASET_MZ_IMS_IDX, datatype, dataspace, plist)
         .write(coordinates.data(), datatype);
    group.createDataSet(ToffeeWriter::DATASET_INTENSITY, datatype, dataspace, plist)
         .write(intensities.data(), datatype);
    cachedH5File.close();
}

SwathMapSpectrumAccess &
SwathMapSpectrumAccess::operator=(
    const SwathMapSpectrumAccess &other
) {
    if (this != &other) {
        *this = dynamic_cast<SwathMapSpectrumAccess&>(SwathMapSpectrumAccessBase::operator=(other));
        mTofFilepathPtr = other.mTofFilepathPtr; // dont move this, need ref counter
    }
    return *this;
}

SwathMapSpectrumAccess::SwathMapSpectrumAccess(
    SwathMapSpectrumAccess &&other
) noexcept :
    SwathMapSpectrumAccessBase(std::move(other)),
    mTofFilepathPtr(other.mTofFilepathPtr) {}  // dont move this, need ref counter

SwathMapSpectrumAccess&
SwathMapSpectrumAccess::operator=(
    SwathMapSpectrumAccess &&other
) noexcept {
    assert(this != &other);
    *this = dynamic_cast<SwathMapSpectrumAccess&>(SwathMapSpectrumAccessBase::operator=(other));
    mTofFilepathPtr = other.mTofFilepathPtr; // dont move this, need ref counter
    return *this;
}

SwathMapSpectrumAccess::~SwathMapSpectrumAccess() {
    bool allowedToDelete = mTofFilepathPtr != nullptr && mTofFilepathPtr.use_count() == 1;
    if (allowedToDelete && boost::filesystem::exists(*mTofFilepathPtr)) {
        std::remove(mTofFilepathPtr->c_str());
    }
}

Spectrum1D
SwathMapSpectrumAccess::spectrumByIndex(
    size_t idx
) const {
    throw_assert(idx >= 0 && idx < mRetentionTime.size(), "Out of bounds");
    throw_assert(
        mTofFilepathPtr != nullptr,
        "File has not been set!"
    );
    throw_assert(
        boost::filesystem::exists(*mTofFilepathPtr),
        "File does not exist: " << *mTofFilepathPtr
    );

    // get the slice indices for this scan
    uint32_t start = (idx == 0) ? 0 : mSliceIndicies[idx - 1];
    uint32_t end = mSliceIndicies[idx];

    std::vector<double> mz;
    std::vector<int> intensities;

    // ---
    // read data from the HDF5
    if (end > start) {
        auto numPoints = end - start;
        H5::H5File h5File(mTofFilepathPtr->c_str(), H5F_ACC_RDONLY);
        H5::Group group = h5File.openGroup(mName);

        // ---
        // construct the mass over charge
        auto sliceCoordinates = readDataset(
            group,
            mName,
            ToffeeWriter::DATASET_MZ_IMS_IDX,
            start,
            numPoints
        );
        mz = std::vector<double>(numPoints, 0.0);
        const auto &mzTransformer = *mMzTransformers[idx];
        for (size_t i = 0; i < numPoints; ++i) {
            mz[i] = mzTransformer.toWorldCoords(sliceCoordinates[i]);
        }

        // construct the intensities
        auto sliceIntensities = readDataset(
            group,
            mName,
            ToffeeWriter::DATASET_INTENSITY,
            start,
            numPoints
        );
        intensities = std::vector<int>(sliceIntensities.begin(), sliceIntensities.end());

        h5File.close();
    }

    return Spectrum1D(mz, intensities);
}

SwathMapInMemorySpectrumAccess::SwathMapInMemorySpectrumAccess() :
    SwathMapSpectrumAccessBase(),
    mIntensities() {
}

SwathMapInMemorySpectrumAccess::SwathMapInMemorySpectrumAccess(
    std::string name,
    double lower,
    double center,
    double upper,
    bool isMS1,
    double scanCycleTime,
    double firstScanRetentionTimeOffset,
    std::vector<std::shared_ptr<const MassOverChargeTransformer> > mzTransformers,
    std::vector<uint32_t> sliceIndicies,
    std::vector<uint32_t> intensities,
    std::vector<uint32_t> imsCoordinates
) :
    SwathMapSpectrumAccessBase(
        name,
        lower,
        center,
        upper,
        isMS1,
        scanCycleTime,
        firstScanRetentionTimeOffset,
        mzTransformers,
        sliceIndicies
    ),
    mIntensities(intensities),
    mIMSCoordinates(imsCoordinates) {}

SwathMapInMemorySpectrumAccess::SwathMapInMemorySpectrumAccess(
    const SwathMapInMemorySpectrumAccess &other
) :
    SwathMapSpectrumAccessBase(other),
    mIntensities(other.mIntensities),
    mIMSCoordinates(other.mIMSCoordinates) {}

SwathMapInMemorySpectrumAccess &
SwathMapInMemorySpectrumAccess::operator=(
    const SwathMapInMemorySpectrumAccess &other
) {
    if (this != &other) {
        *this = dynamic_cast<SwathMapInMemorySpectrumAccess&>(SwathMapSpectrumAccessBase::operator=(other));
        mIntensities = other.mIntensities;
        mIMSCoordinates = other.mIMSCoordinates;
    }
    return *this;
}

SwathMapInMemorySpectrumAccess::SwathMapInMemorySpectrumAccess(
    SwathMapInMemorySpectrumAccess &&other
) noexcept :
    SwathMapSpectrumAccessBase(std::move(other)),
    mIntensities(std::move(other.mIntensities)),
    mIMSCoordinates(std::move(other.mIMSCoordinates)) {}

SwathMapInMemorySpectrumAccess&
SwathMapInMemorySpectrumAccess::operator=(
    SwathMapInMemorySpectrumAccess &&other
) noexcept {
    assert(this != &other);
    *this = dynamic_cast<SwathMapInMemorySpectrumAccess&>(
        SwathMapSpectrumAccessBase::operator=(other)
    );
    mIntensities = std::move(other.mIntensities);
    mIMSCoordinates = std::move(other.mIMSCoordinates);
    return *this;
}

Spectrum1D
SwathMapInMemorySpectrumAccess::spectrumByIndex(
    size_t idx
) const {
    throw_assert(idx >= 0 && idx < mRetentionTime.size(), "Out of bounds");

    // get the slice indices for this scan
    uint32_t start = (idx == 0) ? 0 : mSliceIndicies[idx - 1];
    uint32_t end = mSliceIndicies[idx];

    std::vector<double> mz;
    std::vector<int> intensities;

    // ---
    // read data from the HDF5
    if (end > start) {
        // construct the mass over charge
        auto numPoints = end - start;
        mz = std::vector<double>(numPoints, 0.0);
        const auto &mzTransformer = *mMzTransformers[idx];
        for (size_t i = start; i < end; ++i) {
            mz[i - start] = mzTransformer.toWorldCoords(mIMSCoordinates[i]);
        }

        // construct the intensities
        intensities = std::vector<int>(mIntensities.begin() + start, mIntensities.begin() + end);
    }

    return Spectrum1D(mz, intensities);
}

Chromatogram1D
SwathMapInMemorySpectrumAccess::totalIonChromatogram() const {
    Chromatogram1D result(mRetentionTime);
    auto numScans = mRetentionTime.size();
    throw_assert(
        result.intensities.size() == numScans,
        "Chromatogram dimension mismatch: " << result.intensities.size() << " != " << numScans
    );
    for (size_t i = 0; i < numScans; ++i) {
        uint32_t start = (i == 0) ? 0 : mSliceIndicies[i - 1];
        uint32_t end = mSliceIndicies[i];
        for (uint32_t j = start; j < end; ++j) {
            result.intensities[i] += mIntensities[j];
        }
    }
    return result;
}

Chromatogram1D
SwathMapInMemorySpectrumAccess::basePeakChromatogram() const {
    Chromatogram1D result(mRetentionTime);
    auto numScans = mRetentionTime.size();
    throw_assert(
        result.intensities.size() == numScans,
        "Chromatogram dimension mismatch: " << result.intensities.size() << " != " << numScans
    );
    for (size_t i = 0; i < numScans; ++i) {
        uint32_t start = (i == 0) ? 0 : mSliceIndicies[i - 1];
        uint32_t end = mSliceIndicies[i];
        uint32_t bpc = 0;
        if (end > start) {
            bpc = *std::max_element(mIntensities.begin() + start, mIntensities.begin() + end);
        }
        result.intensities[i] = bpc;
    }
    return result;
}

std::map<uint32_t, int>
SwathMapInMemorySpectrumAccess::intensityFrequencyCount() const {
    std::map<uint32_t, int> result;
    int total = 0;
    std::for_each(
        mIntensities.begin(),
        mIntensities.end(),
        [&result, &total](const auto &i) {
            // assumes that value defaults to 0, I am pretty sure this is
            // guaranteed by the standard
            ++result[i];
            ++total;
        }
    );
    // add the zero intensity count -- the size of the dense matrix, minus the
    // number of entries in the sparse matrix
    auto imsCoordMin = *std::min_element(mIMSCoordinates.begin(), mIMSCoordinates.end());
    auto imsCoordMax = *std::max_element(mIMSCoordinates.begin(), mIMSCoordinates.end());
    auto deltaIMSCoord = imsCoordMax - imsCoordMin + 1;  // ranges are inclusive
    result[0] = boost::numeric_cast<int>(mRetentionTime.size() * deltaIMSCoord) - total;
    return result;
};

RawSwathScanData
SwathMapInMemorySpectrumAccess::toRawSwathScanData() const {
    // ---
    // create the scan data
    RawSwathScanData scanData(mName, mMzTransformers.front()->imsType());
    scanData.windowLower = mWindowPrecursorLowerMz;
    scanData.windowCenter = mWindowPrecursorCenterMz;
    scanData.windowUpper = mWindowPrecursorUpperMz;

    // construct the vectors
    scanData.numberOfScans = boost::numeric_cast<size_t>(mRetentionTime.size());
    for (size_t i = 0; i < scanData.numberOfScans; ++i) {
        scanData.retentionTime.push_back(mRetentionTime[i]);
        uint32_t start = (i == 0) ? 0 : mSliceIndicies[i - 1];
        uint32_t end = mSliceIndicies[i];

        // construct the mass over charge
        scanData.mzData.emplace_back(end - start, 0.0);
        auto& mz = scanData.mzData.back();
        const auto &mzTransformer = *mMzTransformers[i];
        for (auto j = start; j < end; ++j) {
            mz[j - start] = mzTransformer.toWorldCoords(mIMSCoordinates[j]);
        }

        // copy over the intensities
        scanData.intensityData.emplace_back(
            mIntensities.begin() + start,
            mIntensities.begin() + end
        );
    }

    return scanData;
}
}
