/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#include "RawSwathScanDataOps.h"
#include <fstream>
#include <iostream>
#include <algorithm>
#include <numeric>
#include <toffee/MathUtils.h>
#include <toffee/io.h>

#include <Eigen/Dense>
#include <unsupported/Eigen/NonLinearOptimization>

namespace toffee {

IMSProperties 
RawSwathScanDataOps::calculateIMSProperties(
    const std::vector<std::vector<double> > &mzData
) const {
    auto transforms = constructMassTransforms(mzData);
    auto imsProperties = calculateIMSPropertiesImpl(transforms);
    throw_assert(
        imsProperties.alpha.size() == mzData.size(),
        "There should be one alpha value for each scan: " 
            << imsProperties.alpha.size()
            << " != "
            << mzData.size()
    );
    throw_assert(
        imsProperties.beta.size() == mzData.size(),
        "There should be one beta value for each scan: " 
            << imsProperties.beta.size()
            << " != "
            << mzData.size()
    );
    size_t sizeOfMz = 0;
    for (const auto &m : mzData) {
        sizeOfMz += m.size();
    }
    throw_assert(
        imsProperties.coordinate.size() == sizeOfMz,
        "There should be one IMS coordinate value for each m/z value: " 
            << imsProperties.coordinate.size()
            << " != "
            << sizeOfMz
    );
    return imsProperties;
}

RawSwathScanDataOps::MassTransforms
RawSwathScanDataOps::constructMassTransforms(
    const std::vector<std::vector<double> > &mzData
) const {
    MassTransforms transforms;

    // initialise vectors
    size_t sizeOfMz = 0;
    for (const auto &m : mzData) {
        sizeOfMz += m.size();
        transforms.sliceIndex.push_back(boost::numeric_cast<uint32_t>(sizeOfMz));
    }
    transforms.mz.reserve(sizeOfMz);
    transforms.transformedMz.reserve(sizeOfMz);
    transforms.deltaTransformedMz.reserve(sizeOfMz);

    for (const auto &mz : mzData) {

        // only need to add to the point cloud if this scan has data
        const size_t dataSize = mz.size();
        if (dataSize == 0) {
            continue;
        }
        transforms.mz.insert(
            transforms.mz.end(),
            mz.begin(),
            mz.end()
        );

        // calculate the transformed m/z
        auto transformedMz = mz;
        std::for_each(
            transformedMz.begin(),
            transformedMz.end(),
            [this](double &val) -> void { val = this->applyIMSTransform(val); }
        );
        transforms.transformedMz.insert(
            transforms.transformedMz.end(),
            transformedMz.begin(),
            transformedMz.end()
        );

        // calculate the differences
        std::adjacent_difference(
            transformedMz.begin(),
            transformedMz.end(),
            transformedMz.begin()
        );
        transforms.deltaTransformedMz.insert(
            transforms.deltaTransformedMz.end(),
            transformedMz.begin(),
            transformedMz.end()
        );
    }

    throw_assert(
        transforms.mz.size() == sizeOfMz,
        "mz does not contain enough points: "
            << transforms.mz.size()
            << " != "
            << sizeOfMz
    );
    throw_assert(
        transforms.transformedMz.size() == sizeOfMz,
        "transformedMz does not contain enough points: "
            << transforms.transformedMz.size()
            << " != "
            << sizeOfMz
    );
    throw_assert(
        transforms.deltaTransformedMz.size() == sizeOfMz,
        "deltaTransformedMz does not contain enough points: "
            << transforms.deltaTransformedMz.size()
            << " != "
            << sizeOfMz
    );
    return transforms;
}

IMSProperties
RawSwathScanDataOps::calculateIMSPropertiesImpl(
    MassTransforms &transforms
) const {
    size_t numScans = transforms.sliceIndex.size();
    size_t numPoints = transforms.mz.size();

    // ---
    // calculate alpha
    const auto alphaAndDeltaCoord = calculateIMSAlphaAndDeltaCoord(
        transforms
    );
    throw_assert(
        alphaAndDeltaCoord.alpha.size() == numScans,
        "Alpha must be recorded on a per scan basis: "
            << alphaAndDeltaCoord.alpha.size()
            << " != "
            << numScans
    );
    throw_assert(
        alphaAndDeltaCoord.deltaCoord.size() == numPoints,
        "Delta IMS coord must be recorded for every m/z value: "
            << alphaAndDeltaCoord.deltaCoord.size()
            << " != "
            << numPoints
    );

    // ---
    // calculate beta
    const auto betaAndIMSCoord = calculateIMSBetaGammaAndCoord(
        transforms,
        alphaAndDeltaCoord
    );
    throw_assert(
        betaAndIMSCoord.beta.size() == numScans,
        "Beta must be recorded on a per scan basis: "
            << betaAndIMSCoord.beta.size()
            << " != "
            << numScans
    );
    throw_assert(
        betaAndIMSCoord.coord.size() == numPoints,
        "IMS coord must be recorded for every m/z value: "
            << betaAndIMSCoord.coord.size()
            << " != "
            << numPoints
    );

    IMSProperties result;
    result.sliceIndex = transforms.sliceIndex;
    result.alpha = alphaAndDeltaCoord.alpha;
    result.medianAlpha = alphaAndDeltaCoord.medianAlpha;
    result.beta = betaAndIMSCoord.beta;
    result.medianBeta = betaAndIMSCoord.medianBeta;
    result.gamma = betaAndIMSCoord.gamma;
    result.coordinate = betaAndIMSCoord.coord;
    return result;
}

RawSwathScanDataOps::IMSAlphaAndDeltaCoord
RawSwathScanDataOps::calculateIMSAlphaAndDeltaCoord(
    MassTransforms &transforms
) const {
    auto numScans = transforms.sliceIndex.size();
    auto numPoints = transforms.deltaTransformedMz.size();

    std::vector<int> deltaIMSCoord(numPoints, 0);
    std::vector<double> fullAlpha(numPoints, 0.0);
    std::vector<double> alpha(numScans, 0.0);

    // ---
    // adjust delta transformed m/z to ignore the first m/z value
    for (size_t i = 0; i < numScans; ++i) {
        auto start = transforms.startSliceIndex(i);
        transforms.deltaTransformedMz[start] = 0;
    }

    // helper functions
    auto updateDeltaIMSCoord = [&transforms, &deltaIMSCoord](uint32_t start, uint32_t end, double alpha) -> void {
        for (auto i = start; i < end; ++i) {
            if (transforms.deltaTransformedMz[i] == 0) { deltaIMSCoord[i] = 0; }
            else { deltaIMSCoord[i] = boost::numeric_cast<int>(std::round(transforms.deltaTransformedMz[i] / alpha)); }
        }
    };

    // ---
    // iterate to find the best set of IMS delta coords and a matching alpha
    double alphaGuess = guessIMSAlpha(transforms.deltaTransformedMz);
    updateDeltaIMSCoord(0, numPoints, alphaGuess);
    for (size_t scan = 0; scan < numScans; ++scan) {
        auto start = transforms.startSliceIndex(scan);
        auto end = transforms.endSliceIndex(scan);
        
        auto numPointsInScan = end - start;
        if (numPointsInScan < 2) {
            alpha[scan] = std::numeric_limits<double>::quiet_NaN();
            continue;
        }

        double scanAlpha = alphaGuess;
        double scanAlphaLast1 = scanAlpha;
        double scanAlphaLast2 = scanAlpha;

        std::stringstream ss;
        ss << "Scan = " << scan << endl;
        ss << "\tStart slice index = " << start << endl;
        ss << "\t  End slice index = " << end << endl;
        ss << "\t      alpha guess = " << boost::lexical_cast<std::string>(alphaGuess) << endl;

        size_t cnt = 0;
        while (true) {
            for (auto i = start; i < end; ++i) {
                if (transforms.deltaTransformedMz[i] == 0) {
                    fullAlpha[i] = std::numeric_limits<double>::quiet_NaN();
                }
                else {
                    fullAlpha[i] = transforms.deltaTransformedMz[i] / deltaIMSCoord[i];
                }
            }

            scanAlphaLast2 = scanAlphaLast1;
            scanAlphaLast1 = scanAlpha;
#if 0
            // Keeping this around for the next few releases in case this bug comes back.

            // occasionally, alpha will "flip-flop" between iterations -- i.e. every 2 iterations, the value will
            // be exactly the same. By using this momentum parameter, we slightly slow the change of alpha and
            // this should work to prevent the flip-flopping
            double blend = 1e-4;
            scanAlpha = blend * scanAlpha + (1 - blend) * MathUtils::nanmedian(fullAlpha);
#else
            scanAlpha = MathUtils::nanmedian(
                std::vector<double>(fullAlpha.cbegin() + start, fullAlpha.cbegin() + end)
            );
#endif

            auto deltaIMSCoordOld = deltaIMSCoord;
            updateDeltaIMSCoord(start, end, scanAlpha);

            if (MathUtils::almostEqual(scanAlpha, scanAlphaLast1)) {
                break;
            }

            int diffInDeltas = 0;
            for (auto i = start; i < end; ++i) {
                diffInDeltas += std::abs(deltaIMSCoord[i] - deltaIMSCoordOld[i]);
            }
            // stop if we have converged on the IMS coords
            if (diffInDeltas == 0) {
                break;
            }

            // add some debugging in case the asssertion fails
            ss << "\tStep = " << cnt << endl;
            ss << "\t\t             Scan alpha = " << boost::lexical_cast<std::string>(scanAlpha) << endl;
            ss << "\t\t    Diff scan Alpha (1) = " << boost::lexical_cast<std::string>(scanAlpha - scanAlphaLast1);
            ss << "\t\t    Diff scan Alpha (2) = " << boost::lexical_cast<std::string>(scanAlpha - scanAlphaLast2);
            ss << "\t\tDiff in delta IMS coord = " << diffInDeltas << endl;

            // check if alpha is flip-flopping
            if (scanAlpha == scanAlphaLast2) {
                // take a mid point and move to the next the iteration
                scanAlpha = 0.5 * (scanAlpha + scanAlphaLast1);
                ss << "\t\t         New scan alpha = " << boost::lexical_cast<std::string>(scanAlpha) << endl;
            }

            throw_assert(
                ++cnt < 15,
                "Too many iterations without convergence!" << endl << ss.str()
            );
        }

        throw_assert(
            scanAlpha != 0,
            "Scan alpha cannot be zero." << endl << ss.str()
        );
        alpha[scan] = scanAlpha;
    }

    double medianAlpha = MathUtils::nanmedian(fullAlpha);

    // ---
    // summarise alpha per scan and check validity of the delta IMS coords only the first
    // element should be zero
    size_t numDeltasBelowOne = 0;
    for (size_t scan = 0; scan < numScans; ++scan) {
        auto &scanAlpha = alpha[scan];
        if (std::isnan(scanAlpha)) {
            scanAlpha = medianAlpha;
        }
        auto start = transforms.startSliceIndex(scan);
        auto end = transforms.endSliceIndex(scan);
        updateDeltaIMSCoord(start, end, scanAlpha);

        // ignore the first one as it is always zero
        for (auto i = start + 1; i < end; ++i) {
            if (deltaIMSCoord[i] < 1) {
                ++numDeltasBelowOne;
            }
        }
    }
    throw_assert(
        numDeltasBelowOne == 0,
        "Delta Index cannot have values less than 1, it has: " << numDeltasBelowOne
    );

    IMSAlphaAndDeltaCoord result;
    result.alpha = alpha;
    result.medianAlpha = medianAlpha;
    result.deltaCoord = deltaIMSCoord;
    return result;
}

RawSwathScanDataOps::IMSBetaGammaAndCoord
RawSwathScanDataOps::calculateIMSBetaGammaAndCoord(
    MassTransforms &transforms,
    const IMSAlphaAndDeltaCoord &imsAlphaAndDeltaCoord
) const {
    auto numPoints = transforms.transformedMz.size();
    auto numScans = transforms.sliceIndex.size();

    std::vector<int> fullCoord(numPoints);
    std::vector<double> fullBeta(numPoints);
    std::vector<double> beta(numScans);

    for (size_t scan = 0; scan < numScans; ++scan) {
        auto start = transforms.startSliceIndex(scan);
        auto end = transforms.endSliceIndex(scan);

        // allow an empty scan to be skipped
        if (start >= end) {
            beta[scan] = std::numeric_limits<double>::quiet_NaN();
            continue;
        }

        // accumulate the IMS coords
        for (auto i = start; i < end; ++i) {
            fullCoord[i] = imsAlphaAndDeltaCoord.deltaCoord[i];
            if (i > start) {
                fullCoord[i] += fullCoord[i - 1];
            } 
        }

        // recall that alpha values are relative to the median
        double scanAlpha = imsAlphaAndDeltaCoord.alpha[scan];

        // calculate beta for this scan
        for (auto i = start; i < end; ++i) {
            fullBeta[i] = transforms.transformedMz[i] - scanAlpha * fullCoord[i];
        }
        double scanBeta = MathUtils::nanmedian(
            std::vector<double>(fullBeta.cbegin() + start, fullBeta.cbegin() + end)
        );

        // work out what the first IMS coord should be
        int idx0 = boost::numeric_cast<int>(std::round(scanBeta / scanAlpha));
        scanBeta -= idx0 * scanAlpha;

        // adjust so that the beta is always negative
        if (scanBeta > 0) {
            auto sgn = MathUtils::sign(scanAlpha);
            idx0 += sgn;
            scanBeta -= sgn * scanAlpha;
        }

        // adjust the full vectors
        for (auto i = start; i < end; ++i) {
            fullBeta[i] -= idx0 * scanAlpha;
            fullCoord[i] += idx0;
        }
        beta[scan] = scanBeta;
    }

    double medianBeta = MathUtils::nanmedian(fullBeta);

    // Finalise the IMS coords using these parameters for alpha and beta
    for (size_t scan = 0; scan < numScans; ++scan) {
        auto &scanAlpha = imsAlphaAndDeltaCoord.alpha[scan];
        auto &scanBeta = beta[scan];
        if (std::isnan(scanBeta)) {
            scanBeta = medianBeta;
        }

        auto start = transforms.startSliceIndex(scan);
        auto end = transforms.endSliceIndex(scan);
        for (auto i = start; i < end; ++i) {
            fullCoord[i] = boost::numeric_cast<int>(
                std::round((transforms.transformedMz[i] - scanBeta) / scanAlpha)
            );
        }
    }

    // calculate gamma and adjust all of the coordinates relative to that
    auto gamma = boost::numeric_cast<int32_t>(
        *std::min_element(fullCoord.begin(), fullCoord.end())
    );
    gamma -= 10;  // give some padding to allow for truncation error
    std::vector<uint32_t> finalIMSCoord(numPoints);
    for (size_t i = 0; i < numPoints; ++i) {
        finalIMSCoord[i] = boost::numeric_cast<uint32_t>(fullCoord[i] - gamma);
    }

    IMSBetaGammaAndCoord result;
    result.beta = beta;
    result.medianBeta = medianBeta;
    result.gamma = gamma;
    result.coord = finalIMSCoord;
    return result;
}

double
RawSwathScanDataOpsTOF::guessIMSAlpha(
    const std::vector<double> &deltaTransformedMz
) const {
    // we expect the deltas to be positive with TOFs -- however the first
    // element of each scan could be zero so it needs to be excluded    
    double minDelta = std::numeric_limits<double>::max();
    for (const auto& d : deltaTransformedMz) {
        if (d > 0) {
            minDelta = std::min(minDelta, d);
        }
    }
    throw_assert(
        minDelta > 0,
        "Minimum difference between delta m/z in TOF should always be greater than"
        " zero: minDelta = " << minDelta
    );
    return minDelta;
}

std::shared_ptr<const MassOverChargeTransformer> 
RawSwathScanDataOpsTOF::mzTransformer(
    double alpha,
    double beta,
    int32_t gamma
) const {
    return std::make_shared<const MassOverChargeTransformerTOF>(alpha, beta, gamma);
}

double
RawSwathScanDataOpsTOF::applyIMSTransform(
    double mz
) const {
    return std::sqrt(mz);
}

double
RawSwathScanDataOpsOrbitrap::guessIMSAlpha(
    const std::vector<double> &deltaTransformedMz
) const {
    // we expect the deltas to be negative with orbitraps -- however the first
    // element of each scan will be positive so it needs to be excluded    
    double maxDelta = -std::numeric_limits<double>::max();
    for (const auto& d : deltaTransformedMz) {
        if (d < 0) {
            maxDelta = std::max(maxDelta, d);
        }
    }
    throw_assert(
        -maxDelta > 0,
        "Minimum negative difference between delta m/z in Orbitrap should always be greater than"
        " zero: -maxDelta = " << -maxDelta
    );
    return maxDelta;
}

std::shared_ptr<const MassOverChargeTransformer> 
RawSwathScanDataOpsOrbitrap::mzTransformer(
    double alpha,
    double beta,
    int32_t gamma
) const {
    return std::make_shared<const MassOverChargeTransformerOrbitrap>(alpha, beta, gamma);    
}

double
RawSwathScanDataOpsOrbitrap::applyIMSTransform(
    double mz
) const {
    return 1 / std::sqrt(mz);
}
}
