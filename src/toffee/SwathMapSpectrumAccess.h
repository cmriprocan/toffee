/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#pragma once

#include <string>

#include <toffee/DataSlices.h>
#include <toffee/DataRanges.h>
#include <toffee/Windows.h>
#include <toffee/MassOverChargeTransformer.h>
#include <toffee/RawSwathScanData.h>

#include <Eigen/Core>
#include <Eigen/Sparse>

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/register/point.hpp>
#include <boost/geometry/index/rtree.hpp>

namespace toffee {

/// TODO
class SwathMapSpectrumAccessBase {
public:

    /// Primary constructor of a SwathMapSpectrumAccess. In general operation, it is unlikely that
    /// a user of the library will need to call this directly. Instead, prefer functions on
    /// `SwathRun` that load in windows.
    ///
    /// @param name name of the MS selection window that is being represented by this data.
    /// This must take the form of ToffeeWriter::MS1_NAME or ToffeeWriter::ms2Name
    /// @param lower lower (m/z) bound of the MS selection window
    /// @param center center (m/z) of the MS selection window
    /// @param upper upper (m/z) bound of the MS selection window
    /// @param isMS1 if true, this SwathMap represents data from the MS1 precursor ions,
    /// otherwise, the data is from MS2 fragment ions
    /// @param scanCycleTime the time (in seconds) between each scan for this window
    /// @param firstScanRetentionTimeOffset the time (in seconds) from the start of the experiment to
    /// the first scan for this window
    /// @param numberOfScans the number of entries in the retention time vector
    /// @param mzTransformer transforms to and from the mass over charge world and IMS index space
    SwathMapSpectrumAccessBase(
        std::string name,
        double lower,
        double center,
        double upper,
        bool isMS1,
        double scanCycleTime,
        double firstScanRetentionTimeOffset,
        std::vector<std::shared_ptr<const MassOverChargeTransformer> > mzTransformers,
        std::vector<uint32_t> sliceIndicies
    );

    SwathMapSpectrumAccessBase();

    SwathMapSpectrumAccessBase(
        const SwathMapSpectrumAccessBase &other
    ) = default;

    SwathMapSpectrumAccessBase &
    operator=(
        const SwathMapSpectrumAccessBase &other
    );

    SwathMapSpectrumAccessBase(
        SwathMapSpectrumAccessBase &&other
    ) noexcept;

    SwathMapSpectrumAccessBase &
    operator=(
        SwathMapSpectrumAccessBase &&other
    ) noexcept;

    virtual ~SwathMapSpectrumAccessBase() = default;

    /// Return the name of the SwathMap as it was recorded in the original
    /// toffee file
    std::string name() const { return mName; }

    /// Return true if this SwathMap represents data from the MS1 precursor ions,
    /// otherwise false if the data is from MS2 fragment ions
    bool isMS1() const { return mIsMS1; }

    /// Lower m/z bound of the MS selection window
    double precursorLowerMz() const { return mWindowPrecursorLowerMz; }

    /// Center m/z of the MS selection window
    double precursorCenterMz() const { return mWindowPrecursorCenterMz; }

    /// Upper m/z bound of the MS selection window
    double precursorUpperMz() const { return mWindowPrecursorUpperMz; }

    /// The time (in seconds) between each scan for this window. In a typical DIA workflow,
    /// we anticipate that this is constant for all windows in a SwathRun
    double scanCycleTime() const { return mScanCycleTime; }

    /// The time (in seconds) from the start of the experiment to the first scan for this window.
    double firstScanRetentionTimeOffset() const { return mFirstScanRetentionTimeOffset; }

    /// A vector (of size ``N``) retention times where ``N`` is
    /// the number of scans performed, or spectra, for this window
    const Eigen::VectorXd &retentionTime() const { return mRetentionTime; }

    /// The number of scans performed, or spectra, for this window
    ptrdiff_t numberOfSpectra() const { return mRetentionTime.size(); }

    /// Get a transformer that can report the bounds of the mass over charge
    /// for this SwathMap, as well as perform conversions between m/z and
    /// IMS coordinate
    ///
    /// @param idx The index of the retention time vector that corresponds to
    /// the time of the scan we wish to extract
    inline std::shared_ptr<const MassOverChargeTransformer>
    getMzTransformer(
        size_t idx
    ) const {
        return mMzTransformers.at(idx);
    }

    /// Extract a single spectrum, or scan, from the window, based on the index
    /// of the retention time vector.
    ///
    /// @param idx The index of the retention time vector that corresponds to
    /// the time of the scan we wish to extract
    virtual Spectrum1D
    spectrumByIndex(
        size_t idx
    ) const = 0;

protected:
    std::string mName;
    double mWindowPrecursorLowerMz;
    double mWindowPrecursorCenterMz;
    double mWindowPrecursorUpperMz;
    bool mIsMS1;

    double mScanCycleTime;
    double mFirstScanRetentionTimeOffset;
    Eigen::VectorXd mRetentionTime;

    std::vector<std::shared_ptr<const MassOverChargeTransformer> > mMzTransformers;
    std::vector<uint32_t> mSliceIndicies;
};

/// TODO
class SwathMapSpectrumAccess :
    public SwathMapSpectrumAccessBase {
public:

    /// Primary constructor of a SwathMapSpectrumAccess. In general operation, it is unlikely that
    /// a user of the library will need to call this directly. Instead, prefer functions on 
    /// `SwathRun` that load in windows.
    ///
    /// @param tofFilepath path to the toffee file that we wish to read. An exception will be
    /// thrown if this does not exist
    /// @param name name of the MS selection window that is being represented by this data.
    /// This must take the form of ToffeeWriter::MS1_NAME or ToffeeWriter::ms2Name
    /// @param lower lower (m/z) bound of the MS selection window
    /// @param center center (m/z) of the MS selection window
    /// @param upper upper (m/z) bound of the MS selection window
    /// @param isMS1 if true, this SwathMap represents data from the MS1 precursor ions,
    /// otherwise, the data is from MS2 fragment ions
    /// @param scanCycleTime the time (in seconds) between each scan for this window
    /// @param firstScanRetentionTimeOffset the time (in seconds) from the start of the experiment to
    /// the first scan for this window
    /// @param numberOfScans the number of entries in the retention time vector
    /// @param mzTransformer transforms to and from the mass over charge world and IMS index space
    SwathMapSpectrumAccess(
        std::string tofFilepath,
        std::string name,
        double lower,
        double center,
        double upper,
        bool isMS1,
        double scanCycleTime,
        double firstScanRetentionTimeOffset,
        std::vector<std::shared_ptr<const MassOverChargeTransformer> > mzTransformers,
        std::vector<uint32_t> sliceIndicies
    );

    SwathMapSpectrumAccess();

    SwathMapSpectrumAccess(
        const SwathMapSpectrumAccess &other
    ) = default;

    SwathMapSpectrumAccess &
    operator=(
        const SwathMapSpectrumAccess &other
    );

    SwathMapSpectrumAccess(
        SwathMapSpectrumAccess &&other
    ) noexcept;

    SwathMapSpectrumAccess &
    operator=(
        SwathMapSpectrumAccess &&other
    ) noexcept;

    ~SwathMapSpectrumAccess() final;

    Spectrum1D
    spectrumByIndex(
        size_t idx
    ) const final;

private:
    // use a shared pointer to tell us if we can clear out the cache file
    std::shared_ptr<std::string> mTofFilepathPtr;
};

/// TODO
class SwathMapInMemorySpectrumAccess :
    public SwathMapSpectrumAccessBase {
public:

    /// Primary constructor of a SwathMapSpectrumAccess. In general operation, it is unlikely that
    /// a user of the library will need to call this directly. Instead, prefer functions on
    /// `SwathRun` that load in windows.
    ///
    /// @param tofFilepath path to the toffee file that we wish to read. An exception will be
    /// thrown if this does not exist
    /// @param name name of the MS selection window that is being represented by this data.
    /// This must take the form of ToffeeWriter::MS1_NAME or ToffeeWriter::ms2Name
    /// @param lower lower (m/z) bound of the MS selection window
    /// @param center center (m/z) of the MS selection window
    /// @param upper upper (m/z) bound of the MS selection window
    /// @param isMS1 if true, this SwathMap represents data from the MS1 precursor ions,
    /// otherwise, the data is from MS2 fragment ions
    /// @param scanCycleTime the time (in seconds) between each scan for this window
    /// @param firstScanRetentionTimeOffset the time (in seconds) from the start of the experiment to
    /// the first scan for this window
    /// @param numberOfScans the number of entries in the retention time vector
    /// @param mzTransformer transforms to and from the mass over charge world and IMS index space
    SwathMapInMemorySpectrumAccess(
        std::string name,
        double lower,
        double center,
        double upper,
        bool isMS1,
        double scanCycleTime,
        double firstScanRetentionTimeOffset,
        std::vector<std::shared_ptr<const MassOverChargeTransformer> > mzTransformers,
        std::vector<uint32_t> sliceIndicies,
        std::vector<uint32_t> intensities,
        std::vector<uint32_t> imsCoordinates
    );

    SwathMapInMemorySpectrumAccess();

    SwathMapInMemorySpectrumAccess(
        const SwathMapInMemorySpectrumAccess &other
    );

    SwathMapInMemorySpectrumAccess &
    operator=(
        const SwathMapInMemorySpectrumAccess &other
    );

    SwathMapInMemorySpectrumAccess(
        SwathMapInMemorySpectrumAccess &&other
    ) noexcept;

    SwathMapInMemorySpectrumAccess &
    operator=(
        SwathMapInMemorySpectrumAccess &&other
    ) noexcept;

    Spectrum1D
    spectrumByIndex(
        size_t idx
    ) const final;

    /// Extract the total ion chromatogram for the swath map. In essence, this
    /// function sums all intensities at each retention time into a single value.
    Chromatogram1D
    totalIonChromatogram() const;

    /// Extract the base peak chromatogram for the swath map. In essence, this
    /// function records the maximum intensity for each retention time.
    Chromatogram1D
    basePeakChromatogram() const;

    /// This function counts the number of times a particular intensity value
    /// is recorded in the SwathMap. It's primary use is in understanding
    /// properties of particular mass spectrometry instruments such that
    /// in-silico data can be produced for testing purposes.
    std::map<uint32_t, int>
    intensityFrequencyCount() const;

    /// Convert the data stored in the RTree back into its raw format. This is useful
    /// for being able to save the raw data back to an HDF5 file scan-by-scan, for instance
    /// so that we can write a toffee file back to mzML
    RawSwathScanData
    toRawSwathScanData() const;

private:
    std::vector<uint32_t> mIntensities;
    std::vector<uint32_t> mIMSCoordinates;
};
}