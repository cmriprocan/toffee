/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#pragma once
#include <string>
#include <toffee/ScanDataPointCloud.h>
#include <toffee/IntrinsicMassSpacingType.h>
#include <toffee/Windows.h>

namespace toffee {
/// If we consider the raw data collected within a given window across a full
/// gradient during a SWATH-MS run, each data point represents:
///
/// - **Mass over charge** The abscissa refers to the mass over charge ratio
/// of the peptide ion
/// - **Retention time**: The ordinate refers to the time at which the peptide
/// ion is elutes from the Liquid Chromatography column
/// - **Intensity**: The applicate is a count of the number of times a peptide
/// ion is detected on the sensor
///
/// Through the Intrinsic Mass Spacing, mass over charge can be represented by
/// an unsigned integer, as can intensity. Furthermore, the retention time for
/// each data point can be considered as an index into an associated (but much
/// smaller) vector of doubles. In this manner, we can save the raw data into a
/// file format as 3 vectors of unsigned 32-bit integers with some associated
/// metadata.
///
/// ``toffee`` takes this exact approach, saving each SWATH-MS window as a group
/// in an HDF5 file; where each group has:
///
/// - `double` attributes `lower`, `center`, and `upper` to describe the properties
/// of the window;
/// - `double` attributes `scanCycleTime` ($t_c$) and
/// `firstScanRetentionTimeOffset` ($t_0$) that describe the retention time vector
/// for each window such that $t(i) = t_c i + t_0$ in seconds;
/// - a `uint32` dataset of `retentionTimeIdx`, of equal length as the retention time vector,
/// that represents indices into the `imsCoord` and `intensity` between which the
/// corresponding value of $t(i)$ is applied;
/// - `double` attributes `imsAlpha` ($\alpha_{IMS}$) and `imsBeta`
/// ($\beta_{trunc}$) for the Intrinsic Mass Spacing m/z conversion such that
/// $\frac{m}{z} = \left( \alpha_{IMS} i_{\sqrt{m/z}} + \beta_{trunc} \right)^2$;
/// and
/// - `uint32` datasets of `imsCoord` and `intensity` for the point cloud, with
/// one entry for each point in the cloud.
///
/// Thus, through this ToffeeWriter, we can generate a file with lossless
/// compression that can represent a SWATH-MS ``mzML`` file with a ten-fold
/// decrease in the file size - making it on par with the file size of the
/// original vendor format.
class ToffeeWriter {
public:
    /// @param h5FilePath the output path of the toffee file. If a file already
    /// exists at that path, it will be over-written
    explicit ToffeeWriter(
        const std::string& h5FilePath,
        IntrinsicMassSpacingType imsType
    );

    /// Add a point cloud for a given window to the toffee file. If a window
    /// with the same name already exists in the file, it will throw an
    /// exception
    ///
    /// @param pcl the point cloud to be added
    void addPointCloud(
        const ScanDataPointCloud& pcl
    ) const;

    /// Add a header string to the HDF5 file. In normal circumstances, this will be a
    /// PSI-formatted XML string matching the header for mzML files
    void addHeaderMetadata(
        const std::string &header
    ) const;

    /// Name of the file major version HDF5 attribute
    static TOF_WIN_DLL_DECLSPEC const std::string ATTR_MAJOR_VERSION;

    /// Name of the file minor version HDF5 attribute
    static TOF_WIN_DLL_DECLSPEC const std::string ATTR_MINOR_VERSION;

    /// Name of the softare version used to create the file HDF5 attribute
    static TOF_WIN_DLL_DECLSPEC const std::string ATTR_LIBRARY_VERSION;

    /// Name of the run's header HDF5 attribute
    static TOF_WIN_DLL_DECLSPEC const std::string ATTR_EXPERIMENT_METADATA;

    /// Name of a window's Intrinsic Mass Spacing ``type`` HDF5 attribute
    /// Valid values are: TOF, Orbitrap
    static TOF_WIN_DLL_DECLSPEC const std::string ATTR_IMS_TYPE;

    /// Name of a window's Intrinsic Mass Spacing ``alpha`` HDF5 attribute
    static TOF_WIN_DLL_DECLSPEC const std::string ATTR_IMS_ALPHA;

    /// Name of a window's Intrinsic Mass Spacing ``beta`` HDF5 attribute
    static TOF_WIN_DLL_DECLSPEC const std::string ATTR_IMS_BETA;

    /// Name of a window's Intrinsic Mass Spacing ``gamma`` HDF5 attribute
    static TOF_WIN_DLL_DECLSPEC const std::string ATTR_IMS_GAMMA;

    /// Name of a window's mass over charge lower bounds HDF5 attribute
    static TOF_WIN_DLL_DECLSPEC const std::string ATTR_WINDOW_BOUNDS_LOWER;

    /// Name of a window's center mass over charge HDF5 attribute
    static TOF_WIN_DLL_DECLSPEC const std::string ATTR_WINDOW_BOUNDS_UPPER;

    /// Name of a window's mass over charge upper bounds HDF5 attribute
    static TOF_WIN_DLL_DECLSPEC const std::string ATTR_WINDOW_CENTER;

    /// Name of a window's scan cycle time HDF5 attribute
    static TOF_WIN_DLL_DECLSPEC const std::string ATTR_SCAN_CYCLE_TIME;

    /// Name of a window's first retention time offset HDF5 attribute
    static TOF_WIN_DLL_DECLSPEC const std::string ATTR_WINDOW_RETENTION_TIME_OFFSET;

    /// Name of a window's retention time index HDF5 dataset
    static TOF_WIN_DLL_DECLSPEC const std::string DATASET_RETENTION_TIME_IDX;

    /// Name of a window's Intrinsic Mass Spacing ``alpha per scan`` HDF5 dataset
    static TOF_WIN_DLL_DECLSPEC const std::string DATASET_IMS_ALPHA_PER_SCAN;

    /// Name of a window's Intrinsic Mass Spacing ``beta per scan`` HDF5 dataset
    static TOF_WIN_DLL_DECLSPEC const std::string DATASET_IMS_BETA_PER_SCAN;

    /// Name of a window's √(m/z) index HDF5 dataset
    static TOF_WIN_DLL_DECLSPEC const std::string DATASET_MZ_IMS_IDX;

    /// Name of a window's intensity HDF5 dataset
    static TOF_WIN_DLL_DECLSPEC const std::string DATASET_INTENSITY;

    /// Name of the MS1 HDF5 group
    static TOF_WIN_DLL_DECLSPEC const std::string MS1_NAME;

    /// Prefix for the MS2 HDF5 groups
    static TOF_WIN_DLL_DECLSPEC const std::string MS2_PREFIX;

    /// Name of the MS2 HDF5 group corresponding to the specified index
    static std::string
    ms2Name(
        const size_t& idx
    );

    /// Name of the IMS Type in string format
    static std::string
    imsTypeAsStr(
        const IntrinsicMassSpacingType& imsType
    );

    /// Convert the name of the IMS Type in string format to the enum
    static IntrinsicMassSpacingType
    imsTypeFromStr(
        const std::string& imsTypeAsStr
    );

private:
    std::string mH5FilePath;
    IntrinsicMassSpacingType mIMSType;
};
}
