/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#include "SwathMap.h"
#include <H5Cpp.h>
#include <toffee/io.h>
#include <toffee/ToffeeWriter.h>
#include <toffee/Version.h>

namespace bg = boost::geometry;
namespace bgi = boost::geometry::index;

namespace toffee {
SwathMap::SwathMap() :
    mName(),
    mWindowPrecursorLowerMz(-1),
    mWindowPrecursorCenterMz(-1),
    mWindowPrecursorUpperMz(-1),
    mIsMS1(false),
    mScanCycleTime(-1),
    mFirstScanRetentionTimeOffset(-1),
    mRetentionTime(0),
    mMzTransformer(),
    mRTree(),
    mMassOverCharge() {
}

SwathMap::SwathMap(
    std::string name,
    double lower,
    double center,
    double upper,
    bool isMS1,
    double scanCycleTime,
    double firstScanRetentionTimeOffset,
    size_t numberOfScans,
    std::shared_ptr<const MassOverChargeTransformer> mzTransformer,
    const std::vector<PCLPoint> &points
) :
    mName(name),
    mWindowPrecursorLowerMz(lower),
    mWindowPrecursorCenterMz(center),
    mWindowPrecursorUpperMz(upper),
    mIsMS1(isMS1),
    mScanCycleTime(scanCycleTime),
    mFirstScanRetentionTimeOffset(firstScanRetentionTimeOffset),
    mRetentionTime(numberOfScans),
    mMzTransformer(mzTransformer),
    mRTree(points.begin(), points.end()),
    mMassOverCharge()
{
    // set up the retention time vector
    for (size_t i = 0; i < numberOfScans; ++i) {
        mRetentionTime[i] = mFirstScanRetentionTimeOffset + i * mScanCycleTime;
    }

    // generate the mass over charge vector
    auto numIMSCoord = mMzTransformer->upperMzInIMSCoords() - mMzTransformer->lowerMzInIMSCoords() + 1;
    mMassOverCharge.resize(numIMSCoord);
    for (RTreeIndex i = 0; i < numIMSCoord; ++i) {
        RTreeIndex idx = i + mMzTransformer->lowerMzInIMSCoords();
        mMassOverCharge[i] = mMzTransformer->toWorldCoords(idx);
    }
}

SwathMap &
SwathMap::operator=(
    const SwathMap &other
) {
    if (this != &other) {
        *this = other;
    }
    return *this;
}

SwathMap::SwathMap(
    SwathMap &&other
) noexcept :
    mName(std::move(other.mName)),
    mWindowPrecursorLowerMz(other.mWindowPrecursorLowerMz),
    mWindowPrecursorCenterMz(other.mWindowPrecursorCenterMz),
    mWindowPrecursorUpperMz(other.mWindowPrecursorUpperMz),
    mIsMS1(other.mIsMS1),
    mScanCycleTime(other.mScanCycleTime),
    mFirstScanRetentionTimeOffset(other.mFirstScanRetentionTimeOffset),
    mRetentionTime(std::move(other.mRetentionTime)),
    mMzTransformer(std::move(other.mMzTransformer)),
    mRTree(std::move(other.mRTree)),
    mMassOverCharge(std::move(other.mMassOverCharge)) {}

SwathMap&
SwathMap::operator=(
    SwathMap &&other
) noexcept {
    assert(this != &other);
    mName = std::move(other.mName);
    mWindowPrecursorLowerMz = other.mWindowPrecursorLowerMz;
    mWindowPrecursorCenterMz = other.mWindowPrecursorCenterMz;
    mWindowPrecursorUpperMz = other.mWindowPrecursorUpperMz;
    mIsMS1 = other.mIsMS1;
    mScanCycleTime = other.mScanCycleTime;
    mFirstScanRetentionTimeOffset = other.mFirstScanRetentionTimeOffset;
    mRetentionTime = std::move(other.mRetentionTime);
    mMzTransformer = std::move(other.mMzTransformer),
    mRTree = std::move(other.mRTree);
    mMassOverCharge = std::move(other.mMassOverCharge);
    return *this;
}

Chromatogram2DDense
SwathMap::extractedIonChromatogram(
    const IMassOverChargeRange &mzRange
) const {
    return Chromatogram2DDense(extractedIonChromatogramSparse(mzRange));
}

Chromatogram2DSparse
SwathMap::extractedIonChromatogramSparse(
    const IMassOverChargeRange &mzRange
) const {
    RtImsCoord lowerLeft(
        0,
        mzRange.lowerMzInIMSCoords(mMzTransformer)
    );
    // ranges are inclusive
    RtImsCoord upperRight(
        boost::numeric_cast<RTreeIndex>(mRetentionTime.size() - 1),
        mzRange.upperMzInIMSCoords(mMzTransformer)
    );
    return runQuery(Box(lowerLeft, upperRight));
}

Chromatogram2DDense
SwathMap::filteredExtractedIonChromatogram(
    const IMassOverChargeRange &mzRange,
    const IRetentionTimeRange &rtRange
) const {
    return Chromatogram2DDense(filteredExtractedIonChromatogramSparse(mzRange, rtRange));
}

Chromatogram2DSparse
SwathMap::filteredExtractedIonChromatogramSparse(
    const IMassOverChargeRange &mzRange,
    const IRetentionTimeRange &rtRange
) const {
    RtImsCoord lowerLeft(
        rtRange.lowerRTIdx(mRetentionTime),
        mzRange.lowerMzInIMSCoords(mMzTransformer)
    );
    // ranges are inclusive
    RtImsCoord upperRight(
        rtRange.upperRTIdx(mRetentionTime),
        mzRange.upperMzInIMSCoords(mMzTransformer)
    );
    return runQuery(Box(lowerLeft, upperRight));
}

Chromatogram2DSparse
SwathMap::runQuery(
    Box queryBox
) const {
    // get the RTree boundaries
    RTreeIndex rtLowerBound{0};
    auto rtUpperBound = boost::numeric_cast<RTreeIndex>(mRetentionTime.size() - 1);
    auto imsLowerBound = mMzTransformer->lowerMzInIMSCoords();
    auto imsUpperBound = mMzTransformer->upperMzInIMSCoords();

    // fix up the filter box to be contained within the boundaries of the RTree
    RTreeIndex rtIdxLowerLeft = std::min(rtUpperBound, std::max(rtLowerBound, extractRTIdx(queryBox.min_corner())));
    RTreeIndex rtIdxUpperRight = std::max(rtLowerBound, std::min(rtUpperBound, extractRTIdx(queryBox.max_corner())));
    throw_assert(
        rtIdxLowerLeft <= rtIdxUpperRight,
        "Lower bound cannot be bigger than the upper bound: " << rtIdxLowerLeft << ", " << rtIdxUpperRight
    );

    RTreeIndex imsIdxLowerLeft = std::min(imsUpperBound, std::max(imsLowerBound, extractIMSCoord(queryBox.min_corner())));
    RTreeIndex imsIdxUpperRight = std::max(imsLowerBound, std::min(imsUpperBound, extractIMSCoord(queryBox.max_corner())));
    throw_assert(
        imsIdxLowerLeft <= imsIdxUpperRight,
        "Lower bound cannot be bigger than the upper bound: " << imsIdxLowerLeft << ", " << imsIdxUpperRight
    );

    queryBox = Box(
        RtImsCoord(rtIdxLowerLeft, imsIdxLowerLeft),
        RtImsCoord(rtIdxUpperRight, imsIdxUpperRight)
    );

    // find values intersecting some area defined by the box
    std::vector<PCLPoint> queryResult;
    mRTree.query(bgi::intersects(queryBox), std::back_inserter(queryResult));
    std::vector<Chromatogram2DSparse::Triplet> triplets;
    triplets.reserve(queryResult.size());
    for (const auto &pt : queryResult) {
        triplets.emplace_back(
            extractRTIdx(pt) - rtIdxLowerLeft,
            extractIMSCoord(pt) - imsIdxLowerLeft,
            extractIntensity(pt)
        );
    }

    // construct the result class
    RTreeIndex rtIdxDelta = rtIdxUpperRight - rtIdxLowerLeft + 1;
    RTreeIndex imsIdxDelta = imsIdxUpperRight - imsIdxLowerLeft + 1;
    Chromatogram2DSparse result(rtIdxDelta, imsIdxDelta, triplets);

    // slice the retention time into the result vector
    for (ptrdiff_t i = 0; i < rtIdxDelta; ++i) {
        auto idx = i + rtIdxLowerLeft;
        double retentionTime = mRetentionTime[std::min(idx, mRetentionTime.size() - 1)];
        result.retentionTime[i] = retentionTime;
    }
    throw_assert(
        result.retentionTime.size() == rtIdxDelta,
        "Chromatogram dimension mismatch"
    );

    // slice the mass over charge into the result vector
    for (ptrdiff_t i = 0; i < imsIdxDelta; ++i) {
        auto idx = i + imsIdxLowerLeft - mMzTransformer->lowerMzInIMSCoords();
        double mz = mMassOverCharge[std::min(idx, mMassOverCharge.size() - 1)];
        result.massOverCharge[i] = mz;
    }
    throw_assert(
        result.massOverCharge.size() == imsIdxDelta,
        "Chromatogram dimension mismatch"
    );

    return result;
}
}
