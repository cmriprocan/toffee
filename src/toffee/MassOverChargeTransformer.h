/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#pragma once

#include <string>

#include <toffee/Windows.h>
#include <toffee/IntrinsicMassSpacingType.h>

namespace toffee {
/// A simple class for converting between (m/z) space and √(m/z) index space.
/// Furthermore, it is typically used to define the full range of allowable
/// mass over charge of a SwathMap. Importantly, this class and its concrete
/// children allow a user to convert between mass over charge space and
/// √(m/z) index space using the Intrinsic Mass Spacing parameters ``alpha``
/// and ``beta`` where:
///
/// @f[
/// \frac{m}{z} = \left( \alpha_{IMS} i_{\sqrt{m/z}} + \beta_{trunc} \right)^2
/// @f]
///
/// where @f$\alpha_{IMS}@f$ is the IMS value, @f$i_{\sqrt{m/z}}@f$ is the
/// unsigned integer index of the @f$m/z@f$ value in @f$\sqrt{m/z}@f$ space,
/// and @f$\beta_{trunc}@f$ is to control for any truncation error that may be
/// introduced from the conversion process.
///
/// The principle use of the ranges is to enable multiple different ways of defining
/// a range, and then converting between world and index space. In the instance
/// of converting from world to index, the range classes will 'snap' to the closest
/// index. The upper and lower bounds are *inclusive*, meaning that a range with
/// lowerIMSCoord, upperIMSCoord = 154, 154 would return data with a single row
/// (the data contained along index 154). Similarly, a range with
/// lowerIMSCoord, upperIMSCoord = 153, 155 would return data with three rows,
/// centered at imsCoord 154.
class MassOverChargeTransformer {
public:
    MassOverChargeTransformer() = default;
    virtual ~MassOverChargeTransformer() = default;

    /// The lower bound of mass over charge range in "normal" space
    virtual double lowerMzInWorldCoords() const = 0;

    /// The upper bound of mass over charge range in "normal" space
    virtual double upperMzInWorldCoords() const = 0;

    /// The lower bound of mass over charge range in √(m/z) index space
    virtual int32_t lowerMzInIMSCoords() const = 0;

    /// The upper bound of mass over charge range in √(m/z) index space
    virtual int32_t upperMzInIMSCoords() const = 0;

    /// The Intrinsic Mass Spacing type
    /// See toffee::IMassOverChargeRange for more details
    IntrinsicMassSpacingType imsType() const { return mIMSType; }

    /// The Intrinsic Mass Spacing ``alpha`` parameter.
    /// See toffee::IMassOverChargeRange for more details
    double imsAlpha() const { return mIMSAlpha; }

    /// The Intrinsic Mass Spacing ``beta`` parameter
    /// See toffee::IMassOverChargeRange for more details
    double imsBeta() const { return mIMSBeta; }

    /// The Intrinsic Mass Spacing ``gamma`` parameter
    /// See toffee::IMassOverChargeRange for more details
    int32_t imsGamma() const { return mIMSGamma; }

    /// Convert the specified mass over charge into Intrinsic Mass Spacing
    /// index space
    /// @param mz mass over charge value to be converted
    int32_t
    toIMSCoords(
        double mz
    ) const;

    /// Convert the specified mass over charge into Intrinsic Mass Spacing
    /// index space before applying IMS gamma. This function allows the coord
    /// to be negative, and should only every be used when first creating the
    /// the point cloud
    ///
    /// @param mz mass over charge value to be converted
    virtual int32_t
    toIMSCoordsPreGammaAdjustment(
        double mz
    ) const = 0;

    /// Convert the specified Intrinsic Mass Spacing index into mass over charge
    /// space
    /// @param imsCoord IMS index to be converted
    virtual double
    toWorldCoords(
        int32_t imsIdx
    ) const = 0;

protected:
    double mIMSAlpha;
    double mIMSBeta;
    int32_t mIMSGamma;
    IntrinsicMassSpacingType mIMSType;

    MassOverChargeTransformer(
        double imsAlpha,
        double imsBeta,
        int32_t imsGamma,
        IntrinsicMassSpacingType imsType
    );
};

/// A simple class for converting between (m/z) space and √(m/z) index space.
/// Furthermore, it is typically used to define the full range of allowable
/// mass over charge of a SwathMap.
///
/// See toffee::IMassOverChargeRange for how this conversion takes place
class MassOverChargeTransformerTOF : 
    public MassOverChargeTransformer {
public:
    explicit MassOverChargeTransformerTOF();

    /// @param imsAlpha the Intrinsic Mass Spacing ``alpha`` parameter
    /// @param imsBeta the Intrinsic Mass Spacing ``beta`` parameter
    explicit MassOverChargeTransformerTOF(
        double imsAlpha,
        double imsBeta,
        int32_t imsGamma
    );

    /// @param imsAlpha the Intrinsic Mass Spacing ``alpha`` parameter
    /// @param imsBeta the Intrinsic Mass Spacing ``beta`` parameter
    /// @param lowerMzInIMSCoords the lower bound of mass over charge range in 
    /// IMS index space
    /// @param upperMzInIMSCoords the upper bound of mass over charge range in
    /// IMS index space
    explicit MassOverChargeTransformerTOF(
        double imsAlpha,
        double imsBeta,
        int32_t imsGamma,
        int32_t lowerMzInIMSCoords,
        int32_t upperMzInIMSCoords
    );

    double lowerMzInWorldCoords() const final { return mLowerMzInWorldCoords; };

    double upperMzInWorldCoords() const final { return mUpperMzInWorldCoords; };

    int32_t lowerMzInIMSCoords() const final { return mLowerMzInIMSCoords; }

    int32_t upperMzInIMSCoords() const final { return mUpperMzInIMSCoords; }

    int32_t
    toIMSCoordsPreGammaAdjustment(
        double mz
    ) const final;

    double
    toWorldCoords(
        int32_t imsCoord
    ) const final;

private:
    int32_t mLowerMzInIMSCoords;
    int32_t mUpperMzInIMSCoords;
    double mLowerMzInWorldCoords;
    double mUpperMzInWorldCoords;
};

/// A simple class for converting between (m/z) space and √(m/z) index space.
/// Furthermore, it is typically used to define the full range of allowable
/// mass over charge of a SwathMap.
///
/// See toffee::IMassOverChargeRange for how this conversion takes place
class MassOverChargeTransformerOrbitrap : 
    public MassOverChargeTransformer {
public:
    explicit MassOverChargeTransformerOrbitrap();

    /// @param imsAlpha the Intrinsic Mass Spacing ``alpha`` parameter
    /// @param imsBeta the Intrinsic Mass Spacing ``beta`` parameter
    /// @param imsBeta the Intrinsic Mass Spacing ``gamma`` parameter
    explicit MassOverChargeTransformerOrbitrap(
        double imsAlpha,
        double imsBeta,
        int32_t imsGamma
    );

    /// @param imsAlpha the Intrinsic Mass Spacing ``alpha`` parameter
    /// @param imsBeta the Intrinsic Mass Spacing ``beta`` parameter
    /// @param lowerMzInIMSCoords the lower bound of mass over charge range in 
    /// IMS index space
    /// @param upperMzInIMSCoords the upper bound of mass over charge range in
    /// IMS index space
    explicit MassOverChargeTransformerOrbitrap(
        double imsAlpha,
        double imsBeta,
        int32_t imsGamma,
        int32_t lowerMzInIMSCoords,
        int32_t upperMzInIMSCoords
    );

    double lowerMzInWorldCoords() const final { return mLowerMzInWorldCoords; };

    double upperMzInWorldCoords() const final { return mUpperMzInWorldCoords; };

    int32_t lowerMzInIMSCoords() const final { return mLowerMzInIMSCoords; }

    int32_t upperMzInIMSCoords() const final { return mUpperMzInIMSCoords; }

    int32_t
    toIMSCoordsPreGammaAdjustment(
        double mz
    ) const final;

    double
    toWorldCoords(
        int32_t imsCoord
    ) const final;

private:
    int32_t mLowerMzInIMSCoords;
    int32_t mUpperMzInIMSCoords;
    double mLowerMzInWorldCoords;
    double mUpperMzInWorldCoords;
};
}
