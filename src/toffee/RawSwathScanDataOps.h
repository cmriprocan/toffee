/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#pragma once
#include <memory>
#include <string>
#include <vector>
#include <toffee/ScanDataPointCloud.h>
#include <toffee/IntrinsicMassSpacingType.h>
#include <toffee/MassAccuracyWriter.h>
#include <toffee/MassOverChargeTransformer.h>

namespace toffee {

/// The raw data from a single MS1 or MS2 window collected as a vector of ``N``
/// individual scans at discrete retention times
class RawSwathScanDataOps {
public:
    virtual ~RawSwathScanDataOps() = default;

    /// Calculate the IMS properties
    IMSProperties
    calculateIMSProperties(
        const std::vector<std::vector<double> > &mzData
    ) const;

    virtual double applyIMSTransform(double mz) const = 0;

    /// Create the relevant IMS transformer transform
    virtual std::shared_ptr<const MassOverChargeTransformer> 
    mzTransformer(
        double alpha,
        double beta,
        int32_t gamma
    ) const = 0;

private:

    /// Construct the m/z, transformed m/z, and delta transformed m/z vectors
    /// as well as adding RT and intensity to the point cloud
    struct MassTransforms {
        std::vector<double> mz;
        std::vector<double> transformedMz;
        std::vector<double> deltaTransformedMz;
        std::vector<uint32_t> sliceIndex;

        uint32_t startSliceIndex(size_t i) const { return (i == 0) ? 0 : sliceIndex[i - 1]; }
        uint32_t endSliceIndex(size_t i) const { return sliceIndex[i]; }
    };
    MassTransforms
    constructMassTransforms(
        const std::vector<std::vector<double> > &mzData
    ) const;

    /// TODO
    struct IMSAlphaAndDeltaCoord {
        std::vector<double> alpha;
        double medianAlpha;
        std::vector<int> deltaCoord;
    };
    IMSAlphaAndDeltaCoord
    calculateIMSAlphaAndDeltaCoord(
        MassTransforms &transforms
    ) const;

    /// TODO
    struct IMSBetaGammaAndCoord {
        std::vector<double> beta;
        double medianBeta;
        int32_t gamma;
        std::vector<uint32_t> coord;
    };
    IMSBetaGammaAndCoord
    calculateIMSBetaGammaAndCoord(
        MassTransforms &transforms,
        const IMSAlphaAndDeltaCoord &imsAlphaAndDeltaCoord
    ) const;

    /// TODO
    IMSProperties
    calculateIMSPropertiesImpl(
        MassTransforms &transforms
    ) const;

    /// Calculate IMS alpha assuming the relevant transform
    virtual double guessIMSAlpha(
        const std::vector<double> &deltaTransformedMz
    ) const = 0;
};

class RawSwathScanDataOpsTOF : 
    public RawSwathScanDataOps {
public:
    std::shared_ptr<const MassOverChargeTransformer> 
    mzTransformer(
        double alpha,
        double beta,
        int32_t gamma
    ) const final;

    double applyIMSTransform(double mz) const final;

private:
    double guessIMSAlpha(
        const std::vector<double> &deltaTransformedMz
    ) const final;
};

class RawSwathScanDataOpsOrbitrap : 
    public RawSwathScanDataOps {
public:
    std::shared_ptr<const MassOverChargeTransformer> 
    mzTransformer(
        double alpha,
        double beta,
        int32_t gamma
    ) const final;

    double applyIMSTransform(double mz) const final;

private:
    double guessIMSAlpha(
        const std::vector<double> &deltaTransformedMz
    ) const final;
};
}
