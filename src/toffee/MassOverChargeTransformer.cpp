/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#include <cmath>

#include "MassOverChargeTransformer.h"
#include <toffee/io.h>
#include <toffee/ToffeeWriter.h>
#include <toffee/Version.h>

namespace toffee {

// ========================================================================
// MassOverChargeTransformer
// ========================================================================

MassOverChargeTransformer::
MassOverChargeTransformer(
    double imsAlpha,
    double imsBeta,
    int32_t imsGamma,
    IntrinsicMassSpacingType imsType
) :
    mIMSAlpha(imsAlpha),
    mIMSBeta(imsBeta),
    mIMSGamma(imsGamma),
    mIMSType(imsType) {}

int32_t
MassOverChargeTransformer::
toIMSCoords(
    double mz
) const {
    return toIMSCoordsPreGammaAdjustment(mz) - mIMSGamma;
}

// ========================================================================
// MassOverChargeTransformerTOF
// ========================================================================

MassOverChargeTransformerTOF::
MassOverChargeTransformerTOF():
    MassOverChargeTransformerTOF(0, 0, 0) {}

MassOverChargeTransformerTOF::
MassOverChargeTransformerTOF(
    double imsAlpha,
    double imsBeta,
    int32_t imsGamma
) :
    MassOverChargeTransformerTOF(
        imsAlpha,
        imsBeta,
        imsGamma,
        std::numeric_limits<int32_t>::min(),
        std::numeric_limits<int32_t>::max()
    ) {}

MassOverChargeTransformerTOF::
MassOverChargeTransformerTOF(
    double imsAlpha,
    double imsBeta,
    int32_t imsGamma,
    int32_t lowerMzInIMSCoords,
    int32_t upperMzInIMSCoords
) :
    MassOverChargeTransformer(
        imsAlpha,
        imsBeta,
        imsGamma,
        IntrinsicMassSpacingType::TOF
    ),
    mLowerMzInIMSCoords(lowerMzInIMSCoords),
    mUpperMzInIMSCoords(upperMzInIMSCoords),
    mLowerMzInWorldCoords(toWorldCoords(mLowerMzInIMSCoords)),
    mUpperMzInWorldCoords(toWorldCoords(mUpperMzInIMSCoords)) {}

int32_t
MassOverChargeTransformerTOF::
toIMSCoordsPreGammaAdjustment(
    double mz
) const {
    double imsIdx = (std::sqrt(mz) - mIMSBeta) / mIMSAlpha;
    return boost::numeric_cast<int32_t>(std::round(imsIdx));
}

double
MassOverChargeTransformerTOF::
toWorldCoords(
    int32_t imsIdx
) const {
    double mz = (imsIdx + mIMSGamma) * mIMSAlpha + mIMSBeta;
    mz = std::pow<double, int>(mz, 2);
    return mz;
}

// ========================================================================
// MassOverChargeTransformerOrbitrap
// ========================================================================

MassOverChargeTransformerOrbitrap::
MassOverChargeTransformerOrbitrap():
    MassOverChargeTransformerOrbitrap(0, 0, 0) {}

MassOverChargeTransformerOrbitrap::
MassOverChargeTransformerOrbitrap(
    double imsAlpha,
    double imsBeta,
    int32_t imsGamma
) :
    MassOverChargeTransformerOrbitrap(
        imsAlpha,
        imsBeta,
        imsGamma,
        std::numeric_limits<int32_t>::min(),
        std::numeric_limits<int32_t>::max()
    ) {}

MassOverChargeTransformerOrbitrap::
MassOverChargeTransformerOrbitrap(
    double imsAlpha,
    double imsBeta,
    int32_t imsGamma,
    int32_t lowerMzInIMSCoords,
    int32_t upperMzInIMSCoords
) :
    MassOverChargeTransformer(
        imsAlpha,
        imsBeta,
        imsGamma,
        IntrinsicMassSpacingType::ORBITRAP
    ),
    mLowerMzInIMSCoords(lowerMzInIMSCoords),
    mUpperMzInIMSCoords(upperMzInIMSCoords),
    mLowerMzInWorldCoords(toWorldCoords(mLowerMzInIMSCoords)),
    mUpperMzInWorldCoords(toWorldCoords(mUpperMzInIMSCoords)) {}

int32_t
MassOverChargeTransformerOrbitrap::
toIMSCoordsPreGammaAdjustment(
    double mz
) const {
    double imsIdx = (1.0 / std::sqrt(mz) - mIMSBeta) / mIMSAlpha;
    return boost::numeric_cast<int32_t>(std::round(imsIdx));
}

double
MassOverChargeTransformerOrbitrap::
toWorldCoords(
    int32_t imsIdx
) const {
    // need to watch for negative numbers being added to unsigned ints,
    // so convert to double first
    // double mz = (imsIdx + mIMSGamma) * mIMSAlpha + mIMSBeta;
    double mz = imsIdx;
    mz += mIMSGamma;
    mz *= mIMSAlpha;
    mz += mIMSBeta;
    mz = std::pow<double, int>(mz, -2);
    return mz;
}
}
