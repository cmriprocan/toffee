/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#pragma once
#include <H5Cpp.h>
#include <toffee/SwathMap.h>
#include <toffee/SwathMapSpectrumAccess.h>
#include <toffee/IntrinsicMassSpacingType.h>

namespace toffee {

/// A simple data class for describing an MS2 window
class MS2WindowDescriptor {
public:
    /// The name of the window in the toffee file
    std::string name;

    /// The lower bound of the range of precursor mass over charge values that
    /// are selected to be fragmented in this MS2 window
    double lower;

    /// The upper bound of the range of precursor mass over charge values that
    /// are selected to be fragmented in this MS2 window
    double upper;

    /// Windows are considered equal if and only if they have the same name,
    /// and upper and lower mass over charge ranges
    bool operator==(
        const MS2WindowDescriptor &other
    ) const {
        return name == other.name && lower == other.lower && upper == other.upper;
    }
};

/// A class that enables access to properties of a full toffee file
class SwathRun {
public:

    /// @param fname path to the toffee file of interest
    explicit SwathRun(
        const std::string &fname
    );

    /// Extract the metadata header from the specified file. In usual circumstances
    /// one could expect this to look like a PSI-formatter XML description of the run
    std::string header() const { return mHeader; }

    /// The file format major version
    int formatMajorVersion() const { return mFormatMajorVersion; }

    /// The file format minor version
    int formatMinorVersion() const { return mFormatMinorVersion; }

    /// The type of mass analyzer Intrinsic Mass Spacing
    IntrinsicMassSpacingType imsType() const { return mIMSType; }

    /// Return true if this toffee file contains data for the MS1 window
    bool hasMS1Data() const { return mHasMS1Data; }

    /// Return a vector of descriptions of each the MS2 windows contained in the
    /// toffee file
    std::vector<MS2WindowDescriptor> ms2Windows() const { return mMS2Windows; }

    /// It is often useful to know which precursors map to specific MS2
    /// windows in the toffee file. For instance, this can be useful for
    /// performance reasons when iterating through a spectral library
    /// @param precursorMzValues A vector of mass over charge values that
    /// correspond to the precursors that we wish to map to MS2 windows
    /// @param lowerMzOverlap It is usual to have MS2 windows overlap in an
    /// experiment, this allows us to exclude precursors with this offest from
    /// the lower boundary of an MS2 window
    /// @param upperMzOverlap It is usual to have MS2 windows overlap in an
    /// experiment, this allows us to exclude precursors with this offest from
    /// the upper boundary of an MS2 window
    std::map<double, std::string>
    mapPrecursorsToMS2Names(
        const std::vector<double> &precursorMzValues,
        double lowerMzOverlap,
        double upperMzOverlap
    ) const;

    /// Load a SwathMap for a specific window from the toffee file. As this is a
    /// read-only operation, it is thread-safe and multiple threads can read
    /// from the same `SwathRun` concurrently.
    ///
    /// This function is primarily for the python wrapping, when calling from
    /// C++, we recommend using `SwathRun::loadSwathMapPtr`
    ///
    /// @param mapName name of the window of interest (must match the naming 
    /// convention to `ToffeeWriter`)
    /// @param lossyAdjustIMSCoords if true, the IMS coords are translated from the scan
    /// specific alpha and beta to the window alpha and beta in a way that minimizes
    /// the ppm error. Note, this will minimize the mass error of the transform, but
    /// this is a lossy operation that cannot be undone!
    SwathMap
    loadSwathMap(
        const std::string &mapName,
        bool lossyAdjustIMSCoords = false
    ) const;


    /// Load a SwathMap for a specific window from the toffee file. As this is a
    /// read-only operation, it is thread-safe and multiple threads can read
    /// from the same `SwathRun` concurrently.
    ///
    /// @param mapName name of the window of interest (must match the naming 
    /// convention to `ToffeeWriter`)
    /// @param lossyAdjustIMSCoords if true, the IMS coords are translated from the scan
    /// specific alpha and beta to the window alpha and beta in a way that minimizes
    /// the ppm error. Note, this will minimize the mass error of the transform, but
    /// this is a lossy operation that cannot be undone!
    std::shared_ptr<SwathMap>
    loadSwathMapPtr(
        const std::string &mapName,
        bool lossyAdjustIMSCoords = false
    ) const;

    /// Load a SwathMapInMemorySpectrumAccess for a specific window from the toffee file. As this is a
    /// read-only operation, it is thread-safe and multiple threads can read
    /// from the same `SwathRun` concurrently.
    ///
    /// This function is primarily for the python wrapping, when calling from
    /// C++, we recommend using `SwathRun::loadSwathMapPtr`
    ///
    /// @param mapName name of the window of interest (must match the naming 
    /// convetion to `ToffeeWriter`)
    SwathMapInMemorySpectrumAccess
    loadSwathMapInMemorySpectrumAccess(
        const std::string &mapName
    ) const;


    /// Load a SwathMapInMemorySpectrumAccess for a specific window from the toffee file. As this is a
    /// read-only operation, it is thread-safe and multiple threads can read
    /// from the same `SwathRun` concurrently.
    ///
    /// @param mapName name of the window of interest (must match the naming 
    /// convetion to `ToffeeWriter`)
    std::shared_ptr<SwathMapInMemorySpectrumAccess>
    loadSwathMapInMemorySpectrumAccessPtr(
        const std::string &mapName
    ) const;

    /// Load a SwathMapSpectrumAccess for a specific window from the toffee file. As this is a
    /// read-only operation, it is thread-safe and multiple threads can read
    /// from the same `SwathRun` concurrently.
    ///
    /// This function is primarily for the python wrapping, when calling from
    /// C++, we recommend using `SwathRun::loadSwathMapPtr`
    ///
    /// @param mapName name of the window of interest (must match the naming 
    /// convetion to `ToffeeWriter`)
    SwathMapSpectrumAccess
    loadSwathMapSpectrumAccess(
        const std::string &mapName
    ) const;


    /// Load a SwathMapSpectrumAccess for a specific window from the toffee file. As this is a
    /// read-only operation, it is thread-safe and multiple threads can read
    /// from the same `SwathRun` concurrently.
    ///
    /// @param mapName name of the window of interest (must match the naming 
    /// convetion to `ToffeeWriter`)
    std::shared_ptr<SwathMapSpectrumAccess>
    loadSwathMapSpectrumAccessPtr(
        const std::string &mapName
    ) const;

private:
    std::string mFilePath;
    std::string mHeader;
    int mFormatMajorVersion;
    int mFormatMinorVersion;
    IntrinsicMassSpacingType mIMSType;
    std::vector<MS2WindowDescriptor> mMS2Windows;
    bool mHasMS1Data;
};

}
