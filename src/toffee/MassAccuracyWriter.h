/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#pragma once
#include <string>
#include <vector>
#include <toffee/Windows.h>

namespace toffee {
/// When added to a RawSwathScanData object, this class can be used to capture the
/// difference between the original mass over charge values and the values that
/// are converted using the Intrinsic Mass Spacing
class MassAccuracyWriter {
public:
    /// @param h5FilePath the path to the output file. If it already exists, it will
    /// overwritten
    explicit MassAccuracyWriter(
        const std::string &h5FilePath
    );

    /// Add data to the HDF5 file. All three vectors should be of the same length.
    ///
    /// @param windowName the group name for this data
    /// @param orginal a vector of original mass over charge values
    /// @param convertedLossless a vector of mass over charge values following a round-trip
    /// through the lossless IMS conversion
    /// @param convertedLossy a vector of mass over charge values following a round-trip
    /// through the lossy IMS conversion
    /// @param retentionTime a vector of retention times that corresponds to each of
    /// the mass over charge values
    void addWindowData(
        const std::string &windowName,
        const std::vector<double> &original,
        const std::vector<double> &convertedLossless,
        const std::vector<double> &convertedLossy,
        const std::vector<double> &retentionTime
    ) const;

    /// Name of the file original mass over change HDF5 dataset
    static TOF_WIN_DLL_DECLSPEC const std::string DATASET_ORIGINAL;

    /// Name of the file converted mass over change using lossless conversion HDF5 dataset
    static TOF_WIN_DLL_DECLSPEC const std::string DATASET_CONVERTED_LOSSLESS;

    /// Name of the file converted mass over change using lossy conversion HDF5 dataset
    static TOF_WIN_DLL_DECLSPEC const std::string DATASET_CONVERTED_LOSSY;

    /// Name of the file converted retention time HDF5 dataset
    static TOF_WIN_DLL_DECLSPEC const std::string DATASET_RETENTION_TIME;

private:
    std::string mH5FilePath;
};
}
