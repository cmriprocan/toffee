/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#pragma once

#include <string>

#include <toffee/DataSlices.h>
#include <toffee/DataRanges.h>
#include <toffee/Windows.h>
#include <toffee/MassOverChargeTransformer.h>
#include <toffee/RawSwathScanData.h>

#include <Eigen/Core>
#include <Eigen/Sparse>

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/register/point.hpp>
#include <boost/geometry/index/rtree.hpp>

namespace toffee {

/// The SwathMap class is one of the key elements of the the ``toffee`` library.
/// It allows access raw Time of Flight mass spectrometry data in near
/// constant time, with relatively a small memory footprint.
class SwathMap {
public:
    /// Defines the type used to index the mass over charge and retention time
    /// values in the RTree. For search performance and memory optimisation, we
    /// use ``int32_t`` as our index type -- if the resolution of the mass
    /// spectrometer means greater than 4e9 mass over charge values can be
    /// recorded using the intrinsic mass spacing method of toffee, then this
    /// will need to change
    using RTreeIndex = int32_t;

    /// A coordinate in the Point Cloud with retention time as the 'x' value and
    /// √(m/z) index as the 'y' value.
    using RtImsCoord = boost::geometry::model::point<
        RTreeIndex,
        2,
        boost::geometry::cs::cartesian
    >;

    /// Defines the type used to record intensity values in the RTree.
    /// For memory optimisation, intensity values remain as ``uint32_t`` -- if
    /// the sensitivity of the mass spectrometer means signals greater than 4e9
    /// can be recorded, then this will need to change
    using RTreeIntensity = uint32_t;

    /// A point in the point cloud, with the signal intesity as the 'value'.
    using PCLPoint = std::pair<RtImsCoord, RTreeIntensity>;

    /// The integer value passed here defines the number of points per leaf node
    /// of the RTree. It is selected based on a balance between search
    /// performance and memory. Using 4 is analogous to a quad-tree.
    using RStar = boost::geometry::index::rstar<32>;

    /// A spatial search tree from <a href="https://www.boost.org">boost</a>
    /// that can be used for high performance spatial searching of the data
    /// contained within a SwathMap
    using RTree = boost::geometry::index::rtree<PCLPoint, RStar>;

    /// A type used for defining axis aligned box queries of the RTree
    using Box = boost::geometry::model::box<RtImsCoord>;

    /// A type used for defining segment queries of the RTree
    using Segment = boost::geometry::model::segment<RtImsCoord>;

    /// Primary constructor of a SwathMap. In general operation, it is unlikely that
    /// a user of the library will need to call this directly. Instead, prefer
    /// functions on `SwathRun` that load in windows.
    ///
    /// @param name name of the MS selection window that is being represented by this data.
    /// This must take the form of ToffeeWriter::MS1_NAME or ToffeeWriter::ms2Name
    /// @param lower lower (m/z) bound of the MS selection window
    /// @param center center (m/z) of the MS selection window
    /// @param upper upper (m/z) bound of the MS selection window
    /// @param isMS1 if true, this SwathMap represents data from the MS1 precursor ions,
    /// otherwise, the data is from MS2 fragment ions
    /// @param scanCycleTime the time (in seconds) between each scan for this window
    /// @param firstScanRetentionTimeOffset the time (in seconds) from the start of the experiment to
    /// the first scan for this window
    /// @param numberOfScans the number of entries in the retention time vector
    /// @param mzTransformer transforms to and from the mass over charge world and IMS index space
    /// @param points A vector of toffee::SwathMap::PCLPoint representing the signal
    /// intensities detected by the mass spectrometer
    SwathMap(
        std::string name,
        double lower,
        double center,
        double upper,
        bool isMS1,
        double scanCycleTime,
        double firstScanRetentionTimeOffset,
        size_t numberOfScans,
        std::shared_ptr<const MassOverChargeTransformer> mzTransformer,
        const std::vector<PCLPoint> &points
    );

    SwathMap();

    SwathMap(
        const SwathMap &other
    ) = default;

    SwathMap &
    operator=(
        const SwathMap &other
    );

    SwathMap(
        SwathMap &&other
    ) noexcept;

    SwathMap &
    operator=(
        SwathMap &&other
    ) noexcept;

    /// Return the name of the SwathMap as it was recorded in the original
    /// toffee file
    std::string name() const { return mName; }

    /// Return true if this SwathMap represents data from the MS1 precursor ions,
    /// otherwise false if the data is from MS2 fragment ions
    bool isMS1() const { return mIsMS1; }

    /// Lower √(m/z) bound of the MS selection window
    double precursorLowerMz() const { return mWindowPrecursorLowerMz; }

    /// Center √(m/z) of the MS selection window
    double precursorCenterMz() const { return mWindowPrecursorCenterMz; }

    /// Upper √(m/z) bound of the MS selection window
    double precursorUpperMz() const { return mWindowPrecursorUpperMz; }

    /// The time (in seconds) between each scan for this window. In a typical DIA workflow,
    /// we anticipate that this is constant for all windows in a SwathRun
    double scanCycleTime() const { return mScanCycleTime; }

    /// The time (in seconds) from the start of the experiment to the first scan for this window.
    double firstScanRetentionTimeOffset() const { return mFirstScanRetentionTimeOffset; }

    /// A vector (of size ``N``) retention times where ``N`` is
    /// the number of scans performed, or spectra, for this window
    const Eigen::VectorXd &retentionTime() const { return mRetentionTime; }

    /// The number of scans performed, or spectra, for this window
    ptrdiff_t numberOfSpectra() const { return mRetentionTime.size(); }

    /// A vector (of size ``M``) mass over charge (m/z) values, where ``M``
    /// represents all possible (m/z) values that **could** be detected by
    /// the mass spectrometer in its current configuration
    const Eigen::VectorXd &massOverCharge() const { return mMassOverCharge; }

    /// Get a transformer that can report the bounds of the mass over charge
    /// for this SwathMap, as well as perform conversions between (m/z) and
    /// √(m/z) index
    inline std::shared_ptr<const MassOverChargeTransformer> getMzTransformer() const { return mMzTransformer; }

    /// Extract a dense two-dimensional chromatogram from the SwathMap based on
    /// the specified mass over change range. In this instance, the returned
    /// chromatogram will span the full retention time range -- if you only
    /// require a subset of retention times, see
    /// toffee::SwathMap::filteredExtractedIonChromatogram
    ///
    /// @warning If the mass range is large, you can expect a large memory
    /// requirement for this function. In this case, you may wish to use
    /// toffee::SwathMap::extractedIonChromatogramSparse
    ///
    /// @param mzRange specifies the upper and lower bounds of mass over charge
    /// for querying the SwathMap
    Chromatogram2DDense
    extractedIonChromatogram(
        const IMassOverChargeRange &mzRange
    ) const;

    /// Extract a sparse two-dimensional chromatogram from the SwathMap based on
    /// the specified mass over change range. In this instance, the returned
    /// chromatogram will span the full retention time range -- if you only
    /// require a subset of retention times, see
    /// toffee::SwathMap::filteredExtractedIonChromatogramSparse
    ///
    /// @param mzRange specifies the upper and lower bounds of mass over charge
    /// for querying the SwathMap
    Chromatogram2DSparse
    extractedIonChromatogramSparse(
        const IMassOverChargeRange &mzRange
    ) const;

    /// Extract a dense two-dimensional chromatogram from the SwathMap based on
    /// the specified mass over change and retention time ranges.
    ///
    /// @warning If the ranges are large, you can expect a large memory
    /// requirement for this function. In this case, you may wish to use
    /// toffee::SwathMap::filteredExtractedIonChromatogramSparse
    ///
    /// @param mzRange specifies the upper and lower bounds of mass over charge
    /// for querying the SwathMap
    /// @param rtRange specifies the upper and lower bounds of retention time
    /// for querying the SwathMap
    Chromatogram2DDense
    filteredExtractedIonChromatogram(
        const IMassOverChargeRange &mzRange,
        const IRetentionTimeRange &rtRange
    ) const;

    /// Extract a sparse two-dimensional chromatogram from the SwathMap based on
    /// the specified mass over change and retention time ranges.
    ///
    /// @param mzRange specifies the upper and lower bounds of mass over charge
    /// for querying the SwathMap
    /// @param rtRange specifies the upper and lower bounds of retention time
    /// for querying the SwathMap
    Chromatogram2DSparse
    filteredExtractedIonChromatogramSparse(
        const IMassOverChargeRange &mzRange,
        const IRetentionTimeRange &rtRange
    ) const;

private:

    std::string mName;
    double mWindowPrecursorLowerMz;
    double mWindowPrecursorCenterMz;
    double mWindowPrecursorUpperMz;
    bool mIsMS1;

    double mScanCycleTime;
    double mFirstScanRetentionTimeOffset;
    Eigen::VectorXd mRetentionTime;

    std::shared_ptr<const MassOverChargeTransformer> mMzTransformer;
    Eigen::VectorXd mMassOverCharge;

    RTree mRTree;

    Chromatogram2DSparse
    runQuery(
        Box queryBox
    ) const;

    /// Helper function to extract retention time index from the coordinates
    inline static const RTreeIndex &
    extractRTIdx(
        const RtImsCoord &coord
    ) {
        return coord.get<0>();
    }

    /// Helper function to extract retention time index from the point cloud point
    inline static const RTreeIndex &
    extractRTIdx(
        const PCLPoint &pt
    ) {
        return extractRTIdx(pt.first);
    }

    /// Helper function to extract √(m/z) index from the coordinates
    inline static const RTreeIndex &
    extractIMSCoord(
        const RtImsCoord &coord
    ) {
        return coord.get<1>();
    }

    /// Helper function to extract √(m/z) index from the point cloud point
    inline static const RTreeIndex &
    extractIMSCoord(
        const PCLPoint &pt
    ) {
        return extractIMSCoord(pt.first);
    }

    /// Helper function to extract intensity from the point cloud point
    inline static const RTreeIntensity &
    extractIntensity(
        const PCLPoint &pt
    ) {
        return pt.second;
    }
};
}
