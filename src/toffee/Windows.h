/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#pragma once
#include <exception>

/// This is here to ensure that the static variables are correctly
/// exported in windows -- see https://stackoverflow.com/a/3492124/758811
#ifndef TOF_WIN_DLL_DECLSPEC

#ifdef _WIN32
#ifdef BUILDING_TOFFEE_LIB
#define TOF_WIN_DLL_DECLSPEC __declspec(dllexport)
#else
#define TOF_WIN_DLL_DECLSPEC __declspec(dllimport)
#endif

#else
#define TOF_WIN_DLL_DECLSPEC

#endif
#endif

#ifdef BOOST_NO_EXCEPTIONS
namespace boost
{
    template<class E>
    inline void
    throw_exception(
        const E& e
    ) {
        throw e;
    }
}
#endif
