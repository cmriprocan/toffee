/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#pragma once
#include <string>
#include <toffee/Windows.h>

namespace toffee {

class Version {
public:

    /// The version of the library
    static TOF_WIN_DLL_DECLSPEC const std::string LIBRARY_VERSION;

    /// The latest major version of the `toffee` file format
    static TOF_WIN_DLL_DECLSPEC const int FILE_FORMAT_MAJOR_VERSION_CURRENT;

    /// The latest minor version of the `toffee` file format
    static TOF_WIN_DLL_DECLSPEC const int FILE_FORMAT_MINOR_VERSION_CURRENT;

    /// The version of the major `toffee` file that we are backwards
    /// compatible with
    static TOF_WIN_DLL_DECLSPEC const int FILE_FORMAT_MAJOR_VERSION_COMPATIBILITY;

    /// The version of the minor `toffee` file that we are backwards
    /// compatible with
    static TOF_WIN_DLL_DECLSPEC const int FILE_FORMAT_MINOR_VERSION_COMPATIBILITY;

    /// We do not support minor versions greater than or equal to this number
    static TOF_WIN_DLL_DECLSPEC const int FILE_FORMAT_NUM_ALLOWED_MINOR_VERSIONS;

    /// A helper function to combine major and minor version numbers into a
    /// form that can be easily compared
    static inline int combineFileFormatVersions(
        int majorVersion,
        int minorVersion
    ) {
        return FILE_FORMAT_NUM_ALLOWED_MINOR_VERSIONS * majorVersion + minorVersion;
    }
};
}
