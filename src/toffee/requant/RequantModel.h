/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#pragma once
#include <string>
#include <vector>
#include <Eigen/Dense>
#include <Eigen/Core>

#include <toffee/SwathMap.h>
#include <toffee/requant/RequantPeakGroup.h>

namespace toffee {
namespace requant {

/// 
class RequantModel {
public:
    using ResultType = std::tuple<
        std::string, // peak group name
        int, // peak group charge
        int, // peak group rank
        double, // MS1 intensity
        double, // MS2 intensity
        double, // Sigma RT
        double, // Sigma Mz
        double, // RT_0
        double, // Mz_0,MS1
        double, // Mz_0,MS2
        std::string // amplitude of each fragment
    >;

    RequantModel(
        int retentionTimePixelHalfWidth,
        int massOverChargePixelHalfWidth,
        int numMS1Isotopes,
        bool modelNoise
    );

    ResultType
    eval(
        const std::shared_ptr<SwathMap> &ms1SwathMap,
        const std::shared_ptr<SwathMap> &ms2SwathMap,
        const RequantPeakGroup &peakGroup
    ) const;

    struct RawData {
        std::vector<Eigen::MatrixXd> intensities;
        std::vector<double> normalisers;
        std::vector<double> deltaRetentionTime;
        std::vector<double> deltaMassOverCharge;
        size_t nMS1Frag;

        size_t nRT() const { return intensities.front().rows(); }
        size_t nMz() const { return intensities.front().cols(); }
        size_t nSlice() const { return nRT() * nMz(); }
        size_t nFrag() const { return intensities.size(); }
        size_t nFlatData() const { return nSlice() * nFrag(); }
    };

private:
    int mRetentionTimePixelHalfWidth;
    int mMassOverChargePixelHalfWidth;
    int mNumMS1Isotopes;
    bool mModelNoise;

    RawData
    extractData(
        const std::shared_ptr<SwathMap> &ms1SwathMap,
        const std::shared_ptr<SwathMap> &ms2SwathMap,
        const RequantPeakGroup &peakGroup
    ) const;

    ResultType
    fitModel(
        const RequantPeakGroup &peakGroup,
        const RawData &rawData
    ) const;
};
}
}
