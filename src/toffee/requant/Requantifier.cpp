/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#include "Requantifier.h"
#include <numeric>
#include <cmath>
#include <toffee/io.h>
#include <toffee/ToffeeWriter.h>

#include <boost/progress.hpp>

#include <tbb/parallel_reduce.h>
#include <tbb/blocked_range.h>


namespace toffee {
namespace requant {

Requantifier::
Requantifier(
    int retentionTimePixelHalfWidth,
    int massOverChargePixelHalfWidth,
    int numMS1Isotopes,
    bool modelNoise
) :
    mModel(retentionTimePixelHalfWidth, massOverChargePixelHalfWidth, numMS1Isotopes, modelNoise) {}


namespace {
class RequantifierImpl {
public:
    using InputVecType = std::vector<RequantPeakGroup>;
    using RangeType = tbb::blocked_range<InputVecType::const_iterator>;
    using ResultType = std::vector<RequantModel::ResultType>;

    explicit RequantifierImpl(
        const RequantModel &model,
        const std::shared_ptr<SwathMap> &ms1SwathMap,
        const std::shared_ptr<SwathMap> &ms2SwathMap
    ) :
        mResult(),
        mModel(model),
        mMS1SwathMap(ms1SwathMap),
        mMS2SwathMap(ms2SwathMap) {}

    RequantifierImpl(
        RequantifierImpl& other,
        tbb::split
    ) :
        mResult(),
        mModel(other.mModel),
        mMS1SwathMap(other.mMS1SwathMap),
        mMS2SwathMap(other.mMS2SwathMap) {}

    void
    join(
        const RequantifierImpl& other
    ) {
        mResult.insert(mResult.end(), other.mResult.begin(), other.mResult.end());
    }

    ResultType
    sortedResult() const
    {
        auto result = mResult;
        std::sort(
            result.begin(),
            result.end(),
            [](const auto& a, const auto& b) -> bool { return (std::get<0>(a) < std::get<0>(b)); }
        );
        return result;
    }

    void
    operator()(
        const RangeType& peakGroups
    )
    {
        for (const auto &peakGroup: peakGroups) {
            mResult.emplace_back(mModel.eval(mMS1SwathMap, mMS2SwathMap, peakGroup));
        }
    }

private:
    ResultType mResult;
    RequantModel mModel;
    std::shared_ptr<SwathMap> mMS1SwathMap;
    std::shared_ptr<SwathMap> mMS2SwathMap;
};
}

std::vector<RequantModel::ResultType>
Requantifier::
eval(
    const toffee::SwathRun &swathRun,
    const std::map<std::string, std::vector<RequantPeakGroup> > &peakGroups
) const 
{
    RequantifierImpl::ResultType result;

    boost::progress_display progressBar(peakGroups.size());
    boost::progress_timer progressTimer;
    int numQuantified = 0;

    auto ms1SwathMap = swathRun.loadSwathMapPtr(toffee::ToffeeWriter::MS1_NAME);
    for (const auto &pair : peakGroups) {
        auto ms2SwathMap = swathRun.loadSwathMapPtr(pair.first);

        // run with threads
        RequantifierImpl functor(mModel, ms1SwathMap, ms2SwathMap);
        RequantifierImpl::RangeType range(pair.second.cbegin(), pair.second.cend());
        tbb::parallel_reduce(range, functor);

        // save result
        auto ms2Result = functor.sortedResult();
        result.insert(result.end(), ms2Result.begin(), ms2Result.end());

        ++progressBar;
        numQuantified += pair.second.size();
    }
    std::cout << "Re-Quantified " << numQuantified << " peak groups in ";
    return result;
}

}
}
