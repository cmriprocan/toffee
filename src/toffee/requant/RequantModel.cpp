/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#include "RequantModel.h"
#include <numeric>
#include <cmath>
#include <toffee/io.h>
#include <toffee/DataRanges.h>

#include <Eigen/Dense>
#include <unsupported/Eigen/MatrixFunctions>
#include <unsupported/Eigen/NonLinearOptimization>

namespace toffee {
namespace requant {

RequantModel::
RequantModel(
    int retentionTimePixelHalfWidth,
    int massOverChargePixelHalfWidth,
    int numMS1Isotopes,
    bool modelNoise
) :
    mRetentionTimePixelHalfWidth(retentionTimePixelHalfWidth),
    mMassOverChargePixelHalfWidth(massOverChargePixelHalfWidth),
    mNumMS1Isotopes(numMS1Isotopes),
    mModelNoise(modelNoise) {}

RequantModel::ResultType
RequantModel::
eval(
    const std::shared_ptr<SwathMap> &ms1SwathMap,
    const std::shared_ptr<SwathMap> &ms2SwathMap,
    const RequantPeakGroup &peakGroup
) const 
{
    auto rawData = extractData(ms1SwathMap, ms2SwathMap, peakGroup);
    return fitModel(peakGroup, rawData);
}

RequantModel::RawData
RequantModel::
extractData(
    const std::shared_ptr<SwathMap> &ms1SwathMap,
    const std::shared_ptr<SwathMap> &ms2SwathMap,
    const RequantPeakGroup &peakGroup
) const
{
    RawData result;
    result.nMS1Frag = mNumMS1Isotopes + 1;

    auto nRT = 2 * mRetentionTimePixelHalfWidth + 1;
    auto nMz = 2 * mMassOverChargePixelHalfWidth + 1;

    RetentionTimeRangeWithPixelHalfWidth rtRange(
        peakGroup.retentionTime(),
        mRetentionTimePixelHalfWidth
    );

    auto extract = [this, &rtRange, &nRT, &nMz, &result](
        const std::shared_ptr<SwathMap> &swathMap,
        double massOverCharge
    ) -> void {
        MassOverChargeRangeWithPixelHalfWidth mzRange(massOverCharge, mMassOverChargePixelHalfWidth);
        auto chrom = swathMap->filteredExtractedIonChromatogram(mzRange, rtRange);
        if (chrom.intensities.rows() != nRT || chrom.intensities.cols() != nMz) {
            return;
        }
        auto intensities = chrom.intensities.cast<double>();
        auto normaliser = std::max(1.0, intensities.maxCoeff());
        result.intensities.push_back(intensities / normaliser);
        result.normalisers.push_back(normaliser);
        result.deltaRetentionTime.push_back(
            chrom.retentionTime[1] - chrom.retentionTime[0]
        );
        double meanDeltaMz = 0.0;
        for (size_t i = 1; i < nMz; ++i) {
            meanDeltaMz += chrom.massOverCharge[i] - chrom.massOverCharge[i - 1];
        }
        result.deltaMassOverCharge.push_back(meanDeltaMz / (nMz - 1));
    };

    // MS1 data
    for (size_t i = 0; i < mNumMS1Isotopes + 1; ++i) {
        static constexpr double c12c13IsotopeMassOffset = 1.0033548;
        double offset = c12c13IsotopeMassOffset / peakGroup.charge();
        extract(ms1SwathMap, peakGroup.massOverCharge() + i * offset);
    }

    // MS2 data
    for (const auto &mz : peakGroup.fragmentMassOverCharges()) {
        extract(ms2SwathMap, mz);
    }

    return result;
}

namespace {
template<typename _Scalar, int NX = Eigen::Dynamic, int NY = Eigen::Dynamic>
struct GenericEigenLMFunctor
{
    using Scalar = _Scalar;
    enum {
        InputsAtCompileTime = NX,
        ValuesAtCompileTime = NY
    };
    using InputType = Eigen::Matrix<Scalar, InputsAtCompileTime, 1>;
    using ValueType = Eigen::Matrix<Scalar, ValuesAtCompileTime, 1>;
    using JacobianType = Eigen::Matrix<Scalar, ValuesAtCompileTime, InputsAtCompileTime>;

    GenericEigenLMFunctor() : 
        mInputs(InputsAtCompileTime),
        mValues(ValuesAtCompileTime) {}

    GenericEigenLMFunctor(int inputs, int values) : 
        mInputs(inputs),
        mValues(values) {}

    int inputs() const { return mInputs; }
    int values() const { return mValues; }

protected:
    int mInputs;
    int mValues;
};

struct IntermediateData {
    size_t nRT;
    size_t nMz;
    size_t nSlice;
    size_t nMS1Frag;
    size_t nFrag;
    size_t nFlatData;

    Eigen::ArrayXd rawDataFullVec;
    Eigen::ArrayXd rtVec;
    Eigen::ArrayXd mzVec;
    Eigen::ArrayXd rtLogVec;

    explicit IntermediateData(
        const RequantModel::RawData &rawData
    ) :
        nRT(rawData.nRT()),
        nMz(rawData.nMz()),
        nSlice(rawData.nRT() * rawData.nMz()),
        nMS1Frag(rawData.nMS1Frag),
        nFrag(rawData.nFrag()),
        nFlatData(rawData.nFlatData()),
        rawDataFullVec(nFlatData),
        rtVec(nSlice),
        mzVec(nSlice),
        rtLogVec(nSlice)
    {
        for (size_t iFrag = 0; iFrag < nFrag; ++iFrag) {
            const auto &raw = rawData.intensities[iFrag];
            for (size_t iRT = 0; iRT < nRT; ++iRT) {
                for (size_t iMz = 0; iMz < nMz; ++iMz) {
                    size_t idx = iFrag * nSlice + iRT * nMz + iMz;
                    rawDataFullVec(idx) = raw(iRT, iMz);
                    if (iFrag == 0) {
                        rtVec(idx) = iRT + 0.5;
                        mzVec(idx) = iMz + 0.5;
                    }
                }
            }
        }
        rtLogVec = rtVec.log();
    }
};

struct IntensityFunctor : public GenericEigenLMFunctor<double>
{
    IntensityFunctor(
        size_t nParams,
        bool modelNoise,
        const RequantModel::RawData &rawData
    ) : 
        GenericEigenLMFunctor(nParams, rawData.nFlatData()),
        modelNoise(modelNoise),
        data(rawData)
    {
        throw_assert(
            data.nFlatData == mValues,
            "Initialisation failed: " << data.nFlatData << " != " << mValues
        );
    }

    int 
    operator()(
        const Eigen::VectorXd &x,
        Eigen::VectorXd &fvec
    ) const {
        throw_assert(
            x.size() == mInputs,
            "x is incorrect size: " << x.size() << " != " << mInputs
        );
        throw_assert(
            fvec.size() == mValues,
            "fvec is incorrect size: " << fvec.size() << " != " << mValues
        );

        auto fitDataFullVec = guassian2d(x);
        fvec.segment(0, data.nFlatData) = fitDataFullVec - data.rawDataFullVec;

        return 0;
    }

    // --- functions for clipping ranges
    double sigmoid(double n, double x) const { return 0.5 * n * (1 + std::tanh(x)); }
    double makePositive(double x) const { return x * x; }
    double rtSigmaPrime(double x) const { return makePositive(x); }
    double mzSigmaPrime(double x) const { return makePositive(x); }
    double rt0Prime(double x) const { return sigmoid(data.nRT, x); }
    double mz0Prime(double x) const { return sigmoid(data.nMz, x); }
    double amplitudePrime(double x) const { return makePositive(x); }
    double chemicalNoiseAmplitudePrime(double x) const { return makePositive(x); }

    /// gaussian function for fitting
    Eigen::ArrayXd
    guassian2d (
        const Eigen::ArrayXd &params
    ) const
    {
        throw_assert(
            params.size() == 5 + data.nFrag + data.nMS1Frag,
            "Invalid size of parameter vector"
        );
        Eigen::ArrayXd fitDataFullVec(data.nFlatData);

        double rtSigma = rtSigmaPrime(params(0));
        double mzSigma = mzSigmaPrime(params(1));
        double rt0Log = std::log(rt0Prime(params(2)));
        double mz0MS1 = mz0Prime(params(3));
        double mz0MS2 = mz0Prime(params(4));
        for (size_t iFrag = 0; iFrag < data.nFrag; ++iFrag) {
            bool isMS1 = iFrag < data.nMS1Frag;
            double mz0 = isMS1 ? mz0MS1 : mz0MS2;
            double amplitude = amplitudePrime(params(5 + iFrag));

            // calculate the fit model
            constexpr double pi = 3.14159265358979323846;
            auto denomRT = std::sqrt(2 * pi) * rtSigma * data.rtVec;
            auto rtExponent = -0.5 * ((data.rtLogVec - rt0Log) / rtSigma).pow(2);
            auto mzExponent = -0.5 * ((data.mzVec - mz0) / mzSigma).pow(2);
            auto fitData = amplitude * (rtExponent + mzExponent).exp() / denomRT;

            auto blockStart = iFrag * data.nSlice;
            if (isMS1) {
                double noiseAmplitude = chemicalNoiseAmplitudePrime(params(5 + data.nFrag + iFrag));
                auto noise = noiseAmplitude * mzExponent.exp();
                fitDataFullVec.segment(blockStart, data.nSlice) = fitData + noise;
            } else {
                fitDataFullVec.segment(blockStart, data.nSlice) = fitData;
            }
        }
        return fitDataFullVec;
    }

    bool modelNoise;
    IntermediateData data;
};

using IntensityDiffFunctor = Eigen::NumericalDiff<IntensityFunctor>;
}

RequantModel::ResultType
RequantModel::
fitModel(
    const RequantPeakGroup &peakGroup,
    const RawData &rawData
) const
{
    // --- set up initial guess
    size_t nParams = 5 + rawData.nFrag() + rawData.nMS1Frag;
    Eigen::VectorXd params(nParams);
    params(0) = 0.5; // rtSigma
    params(1) = 2.0; // mzSigma
    params(2) = 0.0; // rt0
    params(3) = 0.0; // mz0MS1
    params(4) = 0.0; // mz0MS2
    for (size_t iFrag = 0; iFrag < rawData.nFrag(); ++iFrag) {
        params(5 + iFrag) = 1.0; // amplitude
    }
    for (size_t iFrag = 0; iFrag < rawData.nMS1Frag; ++iFrag) {
        params(5 + rawData.nFrag() + iFrag) = 0.1; // noise amplitude
    }
    auto initParams = params;  // store a copy

    // --- set up the data vectors
    IntensityFunctor functor(nParams, mModelNoise, rawData);
    IntensityDiffFunctor diffFunctor(functor);
    Eigen::LevenbergMarquardt<IntensityDiffFunctor> lm(diffFunctor);
    lm.parameters.maxfev = 10000;

    // --- run the fit
    auto status = lm.minimize(params);

    // if we failed to converge, return NaN's
    if (status == Eigen::LevenbergMarquardtSpace::Status::TooManyFunctionEvaluation) {
#if 0
        LOG(
            "Minimize failed to converge: "
                << "\n" << peakGroup
                << "\n" << lm.iter
                << ", " << lm.nfev
                << ", " << lm.njev
                << ", " << lm.fnorm
                << ", " << lm.gnorm
                << "\n" << params
        );
#endif
        return std::make_tuple(
            peakGroup.modifiedSequence(),
            peakGroup.charge(),
            peakGroup.rank(),
            std::numeric_limits<double>::quiet_NaN(),
            std::numeric_limits<double>::quiet_NaN(),
            std::numeric_limits<double>::quiet_NaN(),
            std::numeric_limits<double>::quiet_NaN(),
            std::numeric_limits<double>::quiet_NaN(),
            std::numeric_limits<double>::quiet_NaN(),
            std::numeric_limits<double>::quiet_NaN(),
            std::string("")
        );
    }
    throw_assert(
        status != Eigen::LevenbergMarquardtSpace::Status::ImproperInputParameters,
        "Input parameters were incorrect"
    );
    throw_assert(
        status != Eigen::LevenbergMarquardtSpace::Status::UserAsked,
        "User is debugging, this shouldn't be in production"
    );

    // --- return the final fit model but ignoiring noise
    for (size_t iFrag = 0; iFrag < rawData.nMS1Frag; ++iFrag) {
        params(5 + rawData.nFrag() + iFrag) = 0.0; // noise amplitude
    }
    auto fitDataVec = functor.guassian2d(params);

    // integrate the area under the surface for each fragment
    std::vector<double> areaUnderCurve;
    for (size_t iFrag = 0; iFrag < rawData.nFrag(); ++iFrag) {
        double a = fitDataVec.segment(iFrag * rawData.nSlice(), rawData.nSlice()).sum();
        a *= rawData.normalisers[iFrag];
        a *= rawData.deltaRetentionTime[iFrag];
        a *= rawData.deltaMassOverCharge[iFrag];
        areaUnderCurve.push_back(a);
    }
    auto start = areaUnderCurve.cbegin();
    auto endMS1 = start + rawData.nMS1Frag;
    double ms1Intensity = std::accumulate(start, endMS1, 0.0);
    double ms2Intensity = std::accumulate(endMS1, areaUnderCurve.cend(), 0.0);

    // combine the parameters into a string for reporting
    std::stringstream ss;
    for (size_t iFrag = 0; iFrag < rawData.nFrag(); ++iFrag) {
        double a = rawData.normalisers[iFrag] * functor.amplitudePrime(params(5 + iFrag));
        if (iFrag > 0) {
            ss << (iFrag == rawData.nMS1Frag ? ";" : ",") << a;
        }
        ss << a;
    }
    return std::make_tuple(
        peakGroup.modifiedSequence(),
        peakGroup.charge(),
        peakGroup.rank(),
        ms1Intensity,
        ms2Intensity,
        functor.rtSigmaPrime(params(0)),
        functor.mzSigmaPrime(params(1)),
        functor.rt0Prime(params(2)),
        functor.mz0Prime(params(3)),
        functor.mz0Prime(params(4)),
        ss.str()
    );
}

}
}
