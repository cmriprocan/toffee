/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#pragma once
#include <string>
#include <vector>
#include <ostream>

namespace toffee {
namespace requant {

/// 
class RequantPeakGroup {
public:
    RequantPeakGroup(
        const std::string modifiedSequence,
        int charge,
        int rank,
        double retentionTime,
        double massOverCharge,
        const std::vector<double> &fragmentMassOverCharges
    );

    friend std::ostream& 
    operator<<(
        std::ostream& os,
        const RequantPeakGroup &p
    ) {
        std::string sep{"|"};
        return os << p.mModifiedSequence 
            << sep << p.mCharge
            << sep << p.mRank 
            << sep << p.mRetentionTime 
            << sep << p.mMassOverCharge;
    }

    std::string modifiedSequence() const { return mModifiedSequence; }
    int charge() const { return mCharge; }
    int rank() const { return mRank; }
    double retentionTime() const { return mRetentionTime; }
    double massOverCharge() const { return mMassOverCharge; }
    std::vector<double> fragmentMassOverCharges() const { return mFragmentMassOverCharges; }

private:
    std::string mModifiedSequence;
    int mCharge;
    int mRank;
    double mRetentionTime;
    double mMassOverCharge;
    std::vector<double> mFragmentMassOverCharges;
};
}
}
