/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#pragma once
#include <memory>
#include <string>
#include <vector>
#include <toffee/ScanDataPointCloud.h>
#include <toffee/IntrinsicMassSpacingType.h>
#include <toffee/MassAccuracyWriter.h>
#include <toffee/MassOverChargeTransformer.h>
#include <toffee/RawSwathScanDataOps.h>

namespace toffee {

/// The raw data from a single MS1 or MS2 window collected as a vector of ``N``
/// individual scans at discrete retention times
class RawSwathScanData {
public:

    /// @param name the name of the window. This should match one of the formats
    /// specified by toffee::ToffeeWriter
    explicit RawSwathScanData(
        const std::string& name,
        IntrinsicMassSpacingType imsType
    );

    /// The name of the window. This should match one of the formats specified
    /// by toffee::ToffeeWriter
    const std::string name;

    /// The lower (m/z) bound of the MS selection window - should be `-1` for
    /// MS1 scans
    double windowLower;

    /// The center (m/z) of the MS selection window - should be `-1` for
    /// MS1 scans
    double windowCenter;

    /// The upper (m/z) bound of the MS selection window - should be `-1` for
    /// MS1 scans
    double windowUpper;

    /// The type of mass analyzer in terms of the Intrinsic Mass Spacing
    IntrinsicMassSpacingType imsType;

    /// The number (``N``) of scans in this window
    size_t numberOfScans = 0;

    /// A vector (of size ``N``) retention times
    std::vector<double> retentionTime;

    /// A vector (of size ``N``) of vectors where the nested vector contains the
    /// mass over charge data collected during the ``n-th`` scan
    std::vector<std::vector<double> > mzData;

    /// A vector (of size ``N``) of vectors where the nested vector contains the
    /// intensity data collected during the ``n-th`` scan
    std::vector<std::vector<uint32_t> > intensityData;

    /// Add a scan to the raw data
    /// @param rt the retention time of this scan
    /// @param mz the mass over charge data collected during this scan
    /// @param intensity the intensity data collected during this scan
    void
    addScan(
        double rt,
        const std::vector<double>& mz,
        const std::vector<uint32_t>& intensity
    );

    /// The mass accuracy can be used as a debugging tool to keep track of
    /// the actual and converted mass over charge values during conversion
    /// to point cloud (index space) using the Intrinsic Mass Spacing.
    /// @param if this is not null, then a debugging step will be taken
    /// during the conversion to a point cloud
    void
    addMassAccuracyWriter(
        const std::shared_ptr<const MassAccuracyWriter> &massAccuracyWriter
    );

    /// Convert the raw data into the point cloud format. Included in this is
    /// the conversion of mass over charge to √(m/z) index space using the
    /// Intrinsic Mass Spacing properties (c.f. toffee::IMassOverChargeRange)
    ScanDataPointCloud toPointCloud() const;

private:
    std::shared_ptr<const MassAccuracyWriter> mMassAccuracyWriter;
    std::shared_ptr<const RawSwathScanDataOps> mRawSwathScanDataOps;

    /// Convert the retention time vector to an offset and a delta (y = mx + b) where
    /// x is an integer. This assumes that the retention time in the experiment is such
    /// that the cycle time is constant across the full gradient
    void
    calculateRetentionTimeProperties(
        const std::vector<double> &retentionTime,
        ScanDataPointCloud &pcl
    ) const;

    /// for each m/z value in the raw data, use the mass over charge transformer to
    /// round trip between IMS coords and back. Both this value and the original can
    /// then be saved to file using the mass accuracy writer.
    void
    addMassAccuracyData(
        const ScanDataPointCloud &pcl,
        const std::vector<int> &imsPropToMZDataIndexMapping
    ) const;
};
}
