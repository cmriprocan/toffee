/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#pragma once

#include <string>

#include <Eigen/Core>
#include <Eigen/Sparse>

namespace toffee {

/// A simple data holding class primarily used to return results from
/// querying a SwathMap.
class Spectrum1D {
public:
    /// Initialise the Spectrum1D with a given set of mass over charge values
    /// and setting a correspondingly sized intensity vector to zeroes
    ///
    /// @param mz a vector (of size ``M``) mass over charge values captured by
    /// this spectrum
    Spectrum1D(
        const Eigen::VectorXd &mz
    ) :
        massOverCharge(mz),
        intensities(Eigen::VectorXi::Zero(mz.size())) {}

    /// Initialise the Spectrum1D with a given set of mass over charge values
    /// and a correspondingly sized intensity vector
    ///
    /// @param mz a vector (of size ``M``) mass over charge values captured by
    /// this spectrum
    /// @param intens a vector (of size ``M``) intensity values captured by
    /// this spectrum
    Spectrum1D(
        const std::vector<double> &mz,
        const std::vector<int> &intens
    ) :
        massOverCharge(mz.size()),
        intensities(intens.size()) {
        for (size_t i = 0; i < mz.size(); ++i) {
            massOverCharge[i] = mz[i];
            intensities[i] = intens[i];
        }
    }

    /// An <a href="https://eigen.tuxfamily.org/dox/group__TutorialMatrixClass.html">Eigen::VectorXd</a>
    /// (of size ``M``) mass over charge values captured by this spectrum
    Eigen::VectorXd massOverCharge;

    /// An <a href="https://eigen.tuxfamily.org/dox/group__TutorialMatrixClass.html">Eigen::VectorXi</a>
    /// (of size ``M``) intensity values captured by this spectrum
    Eigen::VectorXi intensities;
};

/// A simple data holding class primarily used to return results from
/// querying a SwathMap.
class Chromatogram1D {
public:
    /// Initialise the Chromatogram1D with a given set of retention times
    /// and setting a correspondingly sized intensity vector to zeroes
    ///
    /// @param rt a vector (of size ``N``) retention times captured by
    /// this chromatogram
    Chromatogram1D(
        const Eigen::VectorXd &rt
    ) :
        retentionTime(rt),
        intensities(Eigen::VectorXi::Zero(rt.size())) {}

    /// An <a href="https://eigen.tuxfamily.org/dox/group__TutorialMatrixClass.html">Eigen::VectorXd</a>
    /// (of size ``N``) retention times captured by this chromatogram
    Eigen::VectorXd retentionTime;

    /// An <a href="https://eigen.tuxfamily.org/dox/group__TutorialMatrixClass.html">Eigen::VectorXi</a>
    /// (of size ``N``) intensity values captured by this chromatogram
    Eigen::VectorXi intensities;
};

/// A simple data holding class primarily used to return results from
/// querying a SwathMap. The sparse moiniker refers to the fact that zero
/// intensities are excluded from the intensity matrix to improve performance
class Chromatogram2DSparse {
public:
    /// The type used for representing the intensity values
    using Value = int;

    /// A (retention time index, √(m/z) index, intensity) triple that corresponds
    /// to a point in the point cloud. These are primarily used for loading the
    /// toffee::Chromatogram2DSparse::Martix
    using Triplet = Eigen::Triplet<Value>;

    /// An <a href="https://eigen.tuxfamily.org/dox/classEigen_1_1SparseMatrix.html">Eigen::SparseMatrix</a>
    /// in column major layout that is used to represent the intensities
    using Matrix = Eigen::SparseMatrix<Value>;

    /// @param retentionTimeSize the number of retention times captured by this
    /// chromatogram
    /// @param massOverChargeSize the number of mass over charge values captured
    /// by this chromatogram
    /// @param triplets a vector of triplets that represent points in the point
    /// cloud that are used to initialise the Chromatogram2DSparse::intensities
    /// sparse matrix
    Chromatogram2DSparse(
        size_t retentionTimeSize,
        size_t massOverChargeSize,
        const std::vector<Triplet>& triplets
    ) :
        retentionTime(Eigen::VectorXd::Zero(retentionTimeSize)),
        massOverCharge(Eigen::VectorXd::Zero(massOverChargeSize)),
        intensities(retentionTimeSize, massOverChargeSize) {
        intensities.setFromTriplets(triplets.cbegin(), triplets.cend());
    }

    Chromatogram2DSparse(
        Chromatogram2DSparse&& other
    ) noexcept :
        retentionTime(std::move(other.retentionTime)),
        massOverCharge(std::move(other.massOverCharge)),
        intensities(std::move(other.intensities))
    {}

    Chromatogram2DSparse&
    operator=(
        Chromatogram2DSparse&& other
    ) noexcept {
        retentionTime = std::move(other.retentionTime);
        massOverCharge = std::move(other.massOverCharge);
        intensities = std::move(other.intensities);
        return *this;
    }

    /// An <a href="https://eigen.tuxfamily.org/dox/group__TutorialMatrixClass.html">Eigen::VectorXd</a>
    /// (of size ``N``) retention times captured by this chromatogram
    Eigen::VectorXd retentionTime;

    /// An <a href="https://eigen.tuxfamily.org/dox/group__TutorialMatrixClass.html">Eigen::VectorXd</a>
    /// (of size ``M``) mass over charge values captured by this chromatogram
    Eigen::VectorXd massOverCharge;

    /// An <a href="https://eigen.tuxfamily.org/dox/classEigen_1_1SparseMatrix.html">Eigen::SparseMatrix</a>
    /// (of shape ``N``x``M``) in column major layout, of intensity values
    /// captured by this chromatogram
    Matrix intensities;
};

/// A simple data holding class primarily used to return results from
/// querying a SwathMap. The dense moniker refers to the fact that zero
/// intensities are included in the intensity matrix and may result in a
/// large memory footprint.
///
/// @warning Careless useage of this class may result in very large memory
/// requirements of the calling function. In these cases, Chromatogram2DSparse
/// may be useful.
class Chromatogram2DDense {
public:
    /// Convert a two-dimensional chromatogram with sparse intensities into a
    /// two-dimensional chromatogram with intensities in a dense matrix format.
    ///
    /// @param chromatogram2DSparse the sparse representation of the chromatogram
    explicit Chromatogram2DDense(
        const Chromatogram2DSparse & chromatogram2DSparse
    ) :
        retentionTime(chromatogram2DSparse.retentionTime),
        massOverCharge(chromatogram2DSparse.massOverCharge),
        intensities(chromatogram2DSparse.intensities) {}

    /// An <a href="https://eigen.tuxfamily.org/dox/group__TutorialMatrixClass.html">Eigen::VectorXd</a>
    /// (of size ``N``) retention times captured by this chromatogram
    Eigen::VectorXd retentionTime;

    /// An <a href="https://eigen.tuxfamily.org/dox/group__TutorialMatrixClass.html">Eigen::VectorXd</a>
    /// (of size ``M``) mass over charge values captured by this chromatogram
    Eigen::VectorXd massOverCharge;

    /// An <a href="https://eigen.tuxfamily.org/dox/group__TutorialMatrixClass.html">Eigen::MatrixXi</a>
    /// (of shape ``N``x``M``) in column major layout, of intensity values
    /// captured by this chromatogram
    Eigen::MatrixXi intensities;
};
}
