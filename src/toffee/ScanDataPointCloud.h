/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#pragma once
#include <vector>
#include <string>
#include <toffee/IntrinsicMassSpacingType.h>

namespace toffee {

struct IMSProperties {

    double medianAlpha;
    std::vector<double> alpha;

    double medianBeta;
    std::vector<double> beta;

    int32_t gamma;

    /// A vector (of size ``N``) of indices into the imsCoord & intensity vectors
    /// that represent the boundaries of data corresponding to the element in the
    /// retentionTime vector. I.e. pseudo-code looks like:
    ///
    ///     for (size_t i = 0; i < retentionTimeIdx.size(); ++i) {
    ///         auto start = (i == 0) ? 0 : retentionTimeIdx[i - 1];
    ///         auto end = retentionTimeIdx[i];
    ///         auto currentIMSCoord = imsCoord[start:end];
    ///         auto currentIntensity = intensity[start:end];
    ///         double currentRt = firstScanRetentionTimeOffset + i * scanCycleTime;
    ///     }
    ///
    std::vector<uint32_t> sliceIndex;

    /// The index for each of the ``X`` points in the point cloud, as
    /// calculated using the Intrinsic Mass Spacing parameters
    std::vector<uint32_t> coordinate;
};

/// The data from a single MS1 or MS2 window collected as a point cloud of ``X``
/// points discovered from ``N`` scans
class ScanDataPointCloud {
public:
    /// The name of the window. This should match one of the formats specified
    /// by toffee::ToffeeWriter
    std::string name;

    /// The lower (m/z) bound of the MS selection window - should be `-1` for
    /// MS1 scans
    double windowLower;

    /// The center (m/z) of the MS selection window - should be `-1` for
    /// MS1 scans
    double windowCenter;

    /// The upper (m/z) bound of the MS selection window - should be `-1` for
    /// MS1 scans
    double windowUpper;

    /// The time (in seconds) between each scan for this window. In a typical DIA
    /// workflow, we anticipate that this is constant for all windows in a SwathRun
    double scanCycleTime;

    /// The time (in seconds) from the start of the experiment to the first scan for
    /// this window.
    double firstScanRetentionTimeOffset;

    /// The Intrinsic Mass Spacing ``type`` (i.e. 'TOF', 'Orbitrap')
    IntrinsicMassSpacingType imsType;

    /// The Intrinsic Mass Spacing properties
    IMSProperties imsProperties;

    /// The intensity value for each of the ``X`` points in the point cloud
    std::vector<uint32_t> intensity;
};
}
