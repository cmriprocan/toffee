/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#pragma once

#include <cmath>
#include <limits>
#include <type_traits>
#include <algorithm>
#include <toffee/io.h>
#include <boost/math/special_functions/relative_difference.hpp>
#include <boost/lexical_cast.hpp>

namespace toffee {

class MathUtils {
public:
    /// if x is negative, return -1, otherwise return 1
    template<class InputT, class ReturnT = double>
    static ReturnT
    sign(InputT x) {
        return std::signbit(x) ? ReturnT(-1.0) : ReturnT(1.0);
    }

    /// Determine if two real numbers are almost equal. This is borrowed from:
    /// https://en.cppreference.com/w/cpp/types/numeric_limits/epsilon and
    /// https://bitbashing.io/comparing-floats.html
    template<class T>
    static typename std::enable_if<!std::numeric_limits<T>::is_integer, bool>::type
    almostEqual(T x, T y) {
        T diffXY{std::abs(x - y)};
        T maxXY{std::max(std::abs(x), std::abs(y))};
        // allow results to differ by 5 ULPs -- this catches difference
        // between big numbers
        T relTol{5 * std::numeric_limits<T>::epsilon() * maxXY};
        // alllow results to be near zero -- this catches difference
        // between small numbers
        static constexpr T absTol{10 * std::numeric_limits<T>::epsilon()};
        return diffXY <= relTol || diffXY <= absTol;
    }

    /// Determine if one real number is less than, or almost equal, to another
    template<class T>
    static typename std::enable_if<!std::numeric_limits<T>::is_integer, bool>::type
    lessThanOrAlmostEqual(T x, T y) {
        return x < y || MathUtils::almostEqual(x, y);
    }

    /// Determine if one real number is less than, or almost equal, to another
    template<class T>
    static typename std::enable_if<!std::numeric_limits<T>::is_integer, bool>::type
    greaterThanOrAlmostEqual(T x, T y) {
        return x > y || MathUtils::almostEqual(x, y);
    }

    /// A helper function for calculating the median of a vector. Public only for
    /// unit testing purposes -- could equally belong in a utility class if we
    /// had one
    ///
    /// @param vec pass a copy of the vector to find the median of -- we need a copy
    /// as the vector needs to be sorted
    template<class T>
    static T median(
        std::vector<T> vec
    ) {
        throw_assert(
            !vec.empty(),
            "You can't find the median of an empty vector!"
        );

        const auto startIter = vec.begin();
        const auto endIter = vec.end();
        const auto numPoints = vec.size();

        if (numPoints % 2 == 0) {
            // even needs special case
            const auto leftIter = startIter + numPoints / 2 - 1;
            const auto rightIter = startIter + numPoints / 2;
            std::nth_element(startIter, leftIter, endIter);
            std::nth_element(startIter, rightIter, endIter);
            return T{0.5} * (*leftIter + *rightIter);
        }
        else {
            const auto medIter = startIter + numPoints / 2;
            std::nth_element(startIter, medIter, endIter);
            return *medIter;
        }
    }
    template<class T>
    static T nanmedian(
        std::vector<T> vec
    ) {
        std::vector<T> noNansVec;
        noNansVec.reserve(vec.size());
        for (const auto& v : vec) {
            if (!std::isnan(v)) {
                noNansVec.push_back(v);
            }
        }
        return MathUtils::median(noNansVec);
    }

    /// A helper function for calculating the mean of a vector. Public only for
    /// unit testing purposes -- could equally belong in a utility class if we
    /// had one
    ///
    /// @param vec pass a copy of the vector to find the median of -- we need a copy
    /// as the vector needs to be sorted
    template<class T>
    static T mean(
        std::vector<T> vec
    ) {
        throw_assert(
            !vec.empty(),
            "You can't find the mean of an empty vector!"
        );
        return std::accumulate(vec.cbegin(), vec.cend(), T{0}) / vec.size();
    }
    template<class T>
    static T nanmean(
        std::vector<T> vec
    ) {
        std::vector<T> noNansVec;
        noNansVec.reserve(vec.size());
        for (const auto& v : vec) {
            if (!std::isnan(v)) {
                noNansVec.push_back(v);
            }
        }
        return MathUtils::mean(noNansVec);
    }
};
}
