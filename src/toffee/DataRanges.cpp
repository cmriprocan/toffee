/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#include "DataRanges.h"
#include <stdexcept>
#include <algorithm>
#include <toffee/io.h>

namespace toffee {

// ========================================================================
// IMassOverChargeRange
// ========================================================================

// ------------------------------------------------------------------------
// MassOverChargeRangeWithPixelHalfWidth

MassOverChargeRangeWithPixelHalfWidth::
MassOverChargeRangeWithPixelHalfWidth(
    double mz,
    int32_t halfWidth
) :
    mMassOverCharge(mz),
    mPixelHalfWidth(halfWidth),
    mPixelFullWidth(2 * mPixelHalfWidth) {}

int32_t
MassOverChargeRangeWithPixelHalfWidth::
lowerMzInIMSCoords(
    const std::shared_ptr<const MassOverChargeTransformer> &mzTransformer
) const {
    return mzTransformer->toIMSCoords(mMassOverCharge) - mPixelHalfWidth;
}

int32_t
MassOverChargeRangeWithPixelHalfWidth::
upperMzInIMSCoords(
    const std::shared_ptr<const MassOverChargeTransformer> &mzTransformer
) const {
    return lowerMzInIMSCoords(mzTransformer) + mPixelFullWidth;
}

// ------------------------------------------------------------------------
// MassOverChargeRangeWithPPMFullWidth

MassOverChargeRangeWithPPMFullWidth::
MassOverChargeRangeWithPPMFullWidth(
    double mz,
    double ppm
) :
    mMassOverChargeLower(mz * (1 - 0.5e-6 * ppm)),
    mMassOverChargeUpper(mz * (1 + 0.5e-6 * ppm)) {}

int32_t
MassOverChargeRangeWithPPMFullWidth::
lowerMzInIMSCoords(
    const std::shared_ptr<const MassOverChargeTransformer> &mzTransformer
) const {
    return mzTransformer->toIMSCoords(mMassOverChargeLower);
}

int32_t
MassOverChargeRangeWithPPMFullWidth::
upperMzInIMSCoords(
    const std::shared_ptr<const MassOverChargeTransformer> &mzTransformer
) const {
    return mzTransformer->toIMSCoords(mMassOverChargeUpper);
}

// ------------------------------------------------------------------------
// MassOverChargeRange

MassOverChargeRange::
MassOverChargeRange(
    double lowerMz,
    double upperMz
) :
    mMassOverChargeLower(lowerMz),
    mMassOverChargeUpper(upperMz) {}

int32_t
MassOverChargeRange::
lowerMzInIMSCoords(
    const std::shared_ptr<const MassOverChargeTransformer> &mzTransformer
) const {
    return mzTransformer->toIMSCoords(mMassOverChargeLower);
}

int32_t
MassOverChargeRange::
upperMzInIMSCoords(
    const std::shared_ptr<const MassOverChargeTransformer> &mzTransformer
) const {
    return mzTransformer->toIMSCoords(mMassOverChargeUpper);
}

// ------------------------------------------------------------------------
// MassOverChargeRangeIMSCoords

MassOverChargeRangeIMSCoords::
MassOverChargeRangeIMSCoords(
    int32_t lowerIMSCoord,
    int32_t upperIMSCoord
) :
    mLowerIMSCoord(lowerIMSCoord),
    mUpperIMSCoord(upperIMSCoord) {}

int32_t
MassOverChargeRangeIMSCoords::
lowerMzInIMSCoords(
    const std::shared_ptr<const MassOverChargeTransformer> &mzTransformer
) const {
    return mLowerIMSCoord;
}

int32_t
MassOverChargeRangeIMSCoords::
upperMzInIMSCoords(
    const std::shared_ptr<const MassOverChargeTransformer> &mzTransformer
) const {
    return mUpperIMSCoord;
}

// ========================================================================
// IRetentionTimeRange
// ========================================================================

int32_t
IRetentionTimeRange::
toRTIdx(
    double retentionTime,
    const Eigen::VectorXd &retentionTimes
) {
    throw_assert(retentionTimes.size() > 0, "you must have at least one scan");

    // value at rightIdx will be greater, value at leftIdx will be smaller
    // we want to find which one is closer

    // first entry that is *not less* than
    auto rightIdx = boost::numeric_cast<int32_t>(std::distance(
        retentionTimes.data(),
        std::lower_bound(
            retentionTimes.data(),
            retentionTimes.data() + retentionTimes.size(),
            retentionTime
        )
    ));

    // check we're not at the end of the vector
    auto maxValue = boost::numeric_cast<int32_t>(retentionTimes.size()) - 1;
    rightIdx = std::min(rightIdx, maxValue);

    // get the index to the left of the current one, being sure not to go
    // outside the range
    auto leftIdx = (rightIdx > 0) ? rightIdx - 1 : 0;

    // find which index is closer
    auto rightDelta = retentionTimes[rightIdx] - retentionTime;
    auto leftDelta = retentionTime - retentionTimes[leftIdx];
    return leftDelta < rightDelta ? leftIdx : rightIdx;
}

RetentionTimeRangeWithPixelHalfWidth::
RetentionTimeRangeWithPixelHalfWidth(
    double retentionTime,
    int32_t halfWidth
) :
    mRetentionTime(retentionTime),
    mPixelHalfWidth(halfWidth),
    mPixelFullWidth(2 * mPixelHalfWidth) {}

int32_t
RetentionTimeRangeWithPixelHalfWidth::
lowerRTIdx(
    const Eigen::VectorXd &retentionTimes
) const {
    auto maxIdx = boost::numeric_cast<int32_t>(retentionTimes.size() - 1);
    throw_assert(maxIdx > mPixelFullWidth, "Not enough data to use that pixel width");

    // find the closest index
    auto idx = IRetentionTimeRange::toRTIdx(
        mRetentionTime,
        retentionTimes
    );

    // make sure the `max` is performed here to avoid underflow errors
    // the may occur due to unsigned ints being used
    idx = std::max(idx, mPixelHalfWidth);

    // make sure to take care of the upper boundary too
    return std::min(idx - mPixelHalfWidth, maxIdx - mPixelFullWidth);
}

int32_t
RetentionTimeRangeWithPixelHalfWidth::
upperRTIdx(
    const Eigen::VectorXd &retentionTimes
) const {
    return lowerRTIdx(retentionTimes) + mPixelFullWidth;
}

RetentionTimeRange::
RetentionTimeRange(
    double lowerRetentionTime,
    double upperRetentionTime
) :
    mRetentionTimeLower(lowerRetentionTime),
    mRetentionTimeUpper(upperRetentionTime) {}

int32_t
RetentionTimeRange::
lowerRTIdx(
    const Eigen::VectorXd &retentionTimes
) const {
    return IRetentionTimeRange::toRTIdx(mRetentionTimeLower, retentionTimes);
}

int32_t
RetentionTimeRange::
upperRTIdx(
    const Eigen::VectorXd &retentionTimes
) const {
    return IRetentionTimeRange::toRTIdx(mRetentionTimeUpper, retentionTimes);
}
}
