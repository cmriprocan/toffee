/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#include "MassAccuracyWriter.h"
#include <H5Cpp.h>
#include <toffee/io.h>
#include <toffee/Version.h>
#include <boost/filesystem.hpp>

namespace toffee {
const std::string MassAccuracyWriter::DATASET_ORIGINAL = "original";
const std::string MassAccuracyWriter::DATASET_CONVERTED_LOSSLESS = "convertedLossless";
const std::string MassAccuracyWriter::DATASET_CONVERTED_LOSSY = "convertedLossy";
const std::string MassAccuracyWriter::DATASET_RETENTION_TIME = "retentionTime";

MassAccuracyWriter::MassAccuracyWriter(
    const std::string &h5FilePath
) :
        mH5FilePath(h5FilePath) {
    // if the file exists, overwrite it -- otherwise, create a new file
    H5::H5File h5File(mH5FilePath, H5F_ACC_TRUNC);
    h5File.close();
}

void 
MassAccuracyWriter::addWindowData(
    const std::string &windowName,
    const std::vector<double> &original,
    const std::vector<double> &convertedLossless,
    const std::vector<double> &convertedLossy,
    const std::vector<double> &retentionTime
) const {
    throw_assert(
        original.size() == convertedLossless.size(),
        "Vectors must be of the same length"
    );
    throw_assert(
        original.size() == convertedLossy.size(),
        "Vectors must be of the same length"
    );
    throw_assert(
        original.size() == retentionTime.size(),
        "Vectors must be of the same length"
    );

    H5::H5File h5File(mH5FilePath, H5F_ACC_RDWR);
    H5::Group group = h5File.createGroup(windowName);

    {
        // write the data
        static constexpr size_t RANK = 1;
        auto datatype = H5::PredType::NATIVE_DOUBLE;

        H5::DSetCreatPropList plist;
        plist.setDeflate(6);

        hsize_t dims[RANK] = {original.size()};
        plist.setChunk(RANK, dims);
        H5::DataSpace dataspace(RANK, dims);
        
        group.createDataSet(
            DATASET_ORIGINAL, 
            datatype, 
            dataspace, 
            plist
        ).write(original.data(), datatype);
        
        group.createDataSet(
            DATASET_CONVERTED_LOSSLESS, 
            datatype, 
            dataspace, 
            plist
        ).write(convertedLossless.data(), datatype);
        
        group.createDataSet(
            DATASET_CONVERTED_LOSSY, 
            datatype, 
            dataspace, 
            plist
        ).write(convertedLossy.data(), datatype);
        
        group.createDataSet(
            DATASET_RETENTION_TIME, 
            datatype, 
            dataspace, 
            plist
        ).write(retentionTime.data(), datatype);
    }

    h5File.close();
}
}
