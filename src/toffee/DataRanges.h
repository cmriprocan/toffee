/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#pragma once

#include <memory>
#include <cstdint>
#include <vector>
#include <cmath>
#include <Eigen/Core>
#include <toffee/MassOverChargeTransformer.h>

namespace toffee {

class IMassOverChargeRange {
public:
    virtual ~IMassOverChargeRange() = default;

    /// Using the Intrinsic Mass Spacing parameters ``alpha`` and ``beta``,
    /// convert the lower bound of the concrete child class into its
    /// corresponding √(m/z) index
    virtual int32_t
    lowerMzInIMSCoords(
        const std::shared_ptr<const MassOverChargeTransformer> &mzTransformer
    ) const = 0;

    /// Using the Intrinsic Mass Spacing parameters ``alpha`` and ``beta``,
    /// convert the upper bound of the concrete child class into its
    /// corresponding √(m/z) index
    virtual int32_t
    upperMzInIMSCoords(
        const std::shared_ptr<const MassOverChargeTransformer> &mzTransformer
    ) const = 0;
};

/// An implementation of IMassOverChargeRange that allows the user to define
/// the center of the range, and how many pixels (or entries in the mass over
/// charge vector) that span the range.
class MassOverChargeRangeWithPixelHalfWidth :
    public IMassOverChargeRange {
public:
    /// @param mz the mass over charge at which to center this range
    /// @param halfWidth the number of pixels (or indices into the mass over
    /// charge vector) on each side of the center pixel to include in the range
    explicit MassOverChargeRangeWithPixelHalfWidth(
        double mz,
        int32_t halfWidth = 25
    );

    /// The mass over charge at which to center this range
    double massOverCharge() const { return mMassOverCharge; };

    /// The number of pixels (or indices into the mass over charge vector) on
    /// each side of the center pixel to include in the range
    int32_t pixelHalfWidth() const { return mPixelHalfWidth; };

    /// The number of pixels (or indices into the mass over charge vector) in the
    /// full range: `upperIMSCoord - lowerIMSCoord == pixelFullWidth`
    int32_t pixelFullWidth() const { return mPixelFullWidth; };

    /// See IMassOverChargeRange::lowerMzInIMSCoords
    int32_t lowerMzInIMSCoords(
        const std::shared_ptr<const MassOverChargeTransformer> &mzTransformer
    ) const final;

    /// See IMassOverChargeRange::upperMzInIMSCoords
    int32_t upperMzInIMSCoords(
        const std::shared_ptr<const MassOverChargeTransformer> &mzTransformer
    ) const final;

private:
    double mMassOverCharge;
    int32_t mPixelHalfWidth;
    int32_t mPixelFullWidth;
};

/// An implementation of IMassOverChargeRange that allows the user to define
/// the center of the range, and how many parts per million to add on either
/// side.
class MassOverChargeRangeWithPPMFullWidth :
    public IMassOverChargeRange {
public:
    /// @param mz the mass over charge at which to center this range
    /// @param ppm the parts per million factor for which to scale the center
    /// value to the upper and lower bounds of the range. Often, this is
    /// referred to as the mass accuracy factor.
    explicit MassOverChargeRangeWithPPMFullWidth(
        double mz,
        double ppm = 50
    );

    /// The lower bound of mass over charge for this range
    double massOverChargeLower() const { return mMassOverChargeLower; };

    /// The upper bound of mass over charge for this range
    double massOverChargeUpper() const { return mMassOverChargeUpper; };

    /// See IMassOverChargeRange::lowerMzInIMSCoords
    int32_t lowerMzInIMSCoords(
        const std::shared_ptr<const MassOverChargeTransformer> &mzTransformer
    ) const final;

    /// See IMassOverChargeRange::upperMzInIMSCoords
    int32_t upperMzInIMSCoords(
        const std::shared_ptr<const MassOverChargeTransformer> &mzTransformer
    ) const final;

private:
    double mMassOverChargeLower;
    double mMassOverChargeUpper;
};

/// An implementation of IMassOverChargeRange that allows the user to explicitly
/// set the upper and lower bounds of the mass over charge range
class MassOverChargeRange :
    public IMassOverChargeRange {
public:
    /// @param lowerMz the lower bound of mass over charge for this range
    /// @param upperMz the upper bound of mass over charge for this range
    explicit MassOverChargeRange(
        double lowerMz,
        double upperMz
    );

    /// The lower bound of mass over charge for this range
    double massOverChargeLower() const { return mMassOverChargeLower; };

    /// The upper bound of mass over charge for this range
    double massOverChargeUpper() const { return mMassOverChargeUpper; };

    /// See IMassOverChargeRange::lowerMzInIMSCoords
    int32_t lowerMzInIMSCoords(
        const std::shared_ptr<const MassOverChargeTransformer> &mzTransformer
    ) const final;

    /// See IMassOverChargeRange::upperMzInIMSCoords
    int32_t upperMzInIMSCoords(
        const std::shared_ptr<const MassOverChargeTransformer> &mzTransformer
    ) const final;

private:
    double mMassOverChargeLower;
    double mMassOverChargeUpper;
};

/// An implementation of IMassOverChargeRange that allows the user to explicitly
/// set the upper and lower bounds of the mass over charge range in terms of the
/// √(m/z) index space
class MassOverChargeRangeIMSCoords :
    public IMassOverChargeRange {
public:
    /// @param lowerIMSCoord the lower bound of mass over charge for this range
    /// in √(m/z) index space
    /// @param upperIMSCoord the upper bound of mass over charge for this range
    /// in √(m/z) index space
    explicit MassOverChargeRangeIMSCoords(
        int32_t lowerIMSCoord,
        int32_t upperIMSCoord
    );

    /// See IMassOverChargeRange::lowerMzInIMSCoords
    int32_t lowerMzInIMSCoords(
        const std::shared_ptr<const MassOverChargeTransformer> &mzTransformer
    ) const final;

    /// See IMassOverChargeRange::upperMzInIMSCoords
    int32_t upperMzInIMSCoords(
        const std::shared_ptr<const MassOverChargeTransformer> &mzTransformer
    ) const final;

private:
    int32_t mLowerIMSCoord;
    int32_t mUpperIMSCoord;
};

/// An interface class that allows users to define retention time ranges
/// using various methods. These ranges are used by toffee::SwathMap when
/// querying the intensity data.
///
/// The principle use of the ranges is to enable multiple different ways of defining
/// a range, and then converting between world and index space. In the instance
/// of converting from world to index, the range classes will 'snap' to the closest
/// index. The upper and lower bounds are *inclusive*, meaning that a range with
/// lowerRTIdx, upperRTIdx = 154, 154 would return data with a single row (the data
/// contained along index 154). Similarly, a range with lowerRTIdx, upperRTIdx = 153, 155
/// would return data with three rows, centered at retentionTimeIdx 154.
class IRetentionTimeRange {
public:
    virtual ~IRetentionTimeRange() = default;

    /// Find the index in the specified retention time vector that contains
    /// the value that is *closest* to the specified retention time we are
    /// searching for. If there are two values equidistant, arbitrarily
    /// preference the higher index.
    ///
    /// @warning This function assumes, but does not check, that the retention
    /// time vector is sorted in ascending order
    ///
    /// @param retentionTime the retention time value that we wish to search for
    /// @param retentionTimes a vector of retention times in ascending order
    static int32_t toRTIdx(
        double retentionTime,
        const Eigen::VectorXd &retentionTimes
    );

    /// Find the index in the specified retention time vector that contains
    /// the value that is *closest* to the lower bound specified in the
    /// construction of a concrete child class
    ///
    /// @warning This function assumes, but does not check, that the retention
    /// time vector is sorted in ascending order
    ///
    /// @param retentionTimes a vector of retention times in ascending order
    virtual int32_t
    lowerRTIdx(
        const Eigen::VectorXd &retentionTimes
    ) const = 0;

    /// Find the index in the specified retention time vector that contains
    /// the value that is *closest* to the upper bound specified in the
    /// construction of a concrete child class
    ///
    /// @warning This function assumes, but does not check, that the retention
    /// time vector is sorted in ascending order
    ///
    /// @param retentionTimes a vector of retention times in ascending order
    virtual int32_t
    upperRTIdx(
        const Eigen::VectorXd &retentionTimes
    ) const = 0;
};

/// An implementation of IRetentionTimeRange that allows the user to define
/// the center of the range, and how many pixels (or entries in the retention
/// time vector) that span the range.
class RetentionTimeRangeWithPixelHalfWidth :
    public IRetentionTimeRange {
public:
    /// @param retentionTime the retention time at which to center this range
    /// @param halfWidth the number of pixels (or indices into the retention
    /// time vector) on each side of the center pixel to include in the range
    explicit RetentionTimeRangeWithPixelHalfWidth(
        double retentionTime,
        int32_t halfWidth = 50
    );

    /// The retention time at which to center this range
    double retentionTime() const { return mRetentionTime; };

    /// The number of pixels (or indices into the retention time vector) on
    /// each side of the center pixel to include in the range
    int32_t pixelHalfWidth() const { return mPixelHalfWidth; };

    /// The number of pixels (or indices into the retention time vector) in the
    /// full range: `upperRTIdx - lowerRTIdx == pixelFullWidth`
    int32_t pixelFullWidth() const { return mPixelFullWidth; };

    /// See IRetentionTimeRange::lowerRTIdx
    int32_t
    lowerRTIdx(
        const Eigen::VectorXd &retentionTimes
    ) const final;

    /// See IRetentionTimeRange::upperRTIdx
    int32_t
    upperRTIdx(
        const Eigen::VectorXd &retentionTimes
    ) const final;

private:
    double mRetentionTime;
    int32_t mPixelHalfWidth;
    int32_t mPixelFullWidth;
};

/// An implementation of IRetentionTimeRange that allows the user to explicitly
/// set the upper and lower bounds of the retention time range
class RetentionTimeRange :
    public IRetentionTimeRange {
public:
    /// @param lowerRetentionTime the lower bound of retention time for this
    /// range
    /// @param upperRetentionTime the upper bound of retention time for this
    /// range
    RetentionTimeRange(
        double lowerRetentionTime,
        double upperRetentionTime
    );

    /// The lower bound of retention time for this range
    double retentionTimeLower() const { return mRetentionTimeLower; };

    /// The upper bound of retention time for this range
    double retentionTimeUpper() const { return mRetentionTimeUpper; };

    /// See IRetentionTimeRange::lowerRTIdx
    int32_t lowerRTIdx(
        const Eigen::VectorXd &retentionTimes
    ) const final;

    /// See IRetentionTimeRange::upperRTIdx
    int32_t upperRTIdx(
        const Eigen::VectorXd &retentionTimes
    ) const final;

private:
    double mRetentionTimeLower;
    double mRetentionTimeUpper;
};
}
