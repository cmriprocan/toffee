/**
MIT License

Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#include "ToffeeWriter.h"
#include <H5Cpp.h>
#include <toffee/io.h>
#include <toffee/Version.h>
#include <boost/filesystem.hpp>

namespace toffee {
const std::string ToffeeWriter::ATTR_MAJOR_VERSION = "FILE_FORMAT_MAJOR_VERSION";
const std::string ToffeeWriter::ATTR_MINOR_VERSION = "FILE_FORMAT_MINOR_VERSION";
const std::string ToffeeWriter::ATTR_LIBRARY_VERSION = "CREATED_BY_LIBRARY_VERSION";
const std::string ToffeeWriter::ATTR_EXPERIMENT_METADATA = "metadataXML";

const std::string ToffeeWriter::ATTR_IMS_TYPE = "IMSType";
const std::string ToffeeWriter::ATTR_IMS_ALPHA = "IMSAlpha";
const std::string ToffeeWriter::ATTR_IMS_BETA = "IMSBeta";
const std::string ToffeeWriter::ATTR_IMS_GAMMA = "IMSGamma";
const std::string ToffeeWriter::ATTR_WINDOW_BOUNDS_LOWER = "precursorLower";
const std::string ToffeeWriter::ATTR_WINDOW_BOUNDS_UPPER = "precursorUpper";
const std::string ToffeeWriter::ATTR_WINDOW_CENTER = "precursorCenter";
const std::string ToffeeWriter::ATTR_SCAN_CYCLE_TIME = "scanCycleTime";
const std::string ToffeeWriter::ATTR_WINDOW_RETENTION_TIME_OFFSET = "firstScanRetentionTimeOffset";

const std::string ToffeeWriter::DATASET_RETENTION_TIME_IDX = "retentionTimeIdx";
const std::string ToffeeWriter::DATASET_IMS_ALPHA_PER_SCAN = "IMSAlphaPerScan";
const std::string ToffeeWriter::DATASET_IMS_BETA_PER_SCAN = "IMSBetaPerScan";
const std::string ToffeeWriter::DATASET_MZ_IMS_IDX = "imsCoord";
const std::string ToffeeWriter::DATASET_INTENSITY = "intensity";

const std::string ToffeeWriter::MS1_NAME = std::string{"ms1"};
const std::string ToffeeWriter::MS2_PREFIX = std::string{"ms2-"};

std::string
ToffeeWriter::ms2Name(
        const size_t& idx
) {
    std::ostringstream ss;
    ss << MS2_PREFIX << std::setw(3) << std::setfill('0') << idx;
    return ss.str();
};

namespace {
static const std::string TOF_TEXT{"TOF"};
static const std::string ORBITRAP_TEXT{"Orbitrap"};
}

std::string
ToffeeWriter::imsTypeAsStr(
    const IntrinsicMassSpacingType& imsType
) {
    switch (imsType) {
        case IntrinsicMassSpacingType::TOF: return TOF_TEXT;
        case IntrinsicMassSpacingType::ORBITRAP: return ORBITRAP_TEXT;
    }
    RuntimeError("Invalid IMS type: " << static_cast<int>(imsType));
}

IntrinsicMassSpacingType
ToffeeWriter::imsTypeFromStr(
    const std::string& imsTypeStr
) {
    if (imsTypeStr == TOF_TEXT) {
        return IntrinsicMassSpacingType::TOF;
    }
    if (imsTypeStr == ORBITRAP_TEXT) {
        return IntrinsicMassSpacingType::ORBITRAP;
    }
    RuntimeError("Invalid IMS string: " << imsTypeStr);
}

ToffeeWriter::ToffeeWriter(
        const std::string& h5FilePath,
        IntrinsicMassSpacingType imsType
) :
        mH5FilePath(h5FilePath),
        mIMSType(imsType) {
    // if the file exists, overwrite it -- otherwise, create a new file
    H5::H5File h5File(mH5FilePath, H5F_ACC_TRUNC);

    // create the attributes
    {
        auto datatype = H5::PredType::NATIVE_INT;
        H5::DataSpace dataspace;
        h5File.createAttribute(ATTR_MAJOR_VERSION, datatype, dataspace)
                .write(datatype, &Version::FILE_FORMAT_MAJOR_VERSION_CURRENT);
        h5File.createAttribute(ATTR_MINOR_VERSION, datatype, dataspace)
                .write(datatype, &Version::FILE_FORMAT_MINOR_VERSION_CURRENT);
    }
    {
        auto imsTypeStr = ToffeeWriter::imsTypeAsStr(mIMSType);
        H5::DataSpace dataspace(H5S_SCALAR);
        H5::StrType datatype(H5::PredType::C_S1, imsTypeStr.size());
        h5File.createAttribute(ATTR_IMS_TYPE, datatype, dataspace).write(datatype, imsTypeStr);
        h5File.createAttribute(ATTR_LIBRARY_VERSION, datatype, dataspace)
                .write(datatype, Version::LIBRARY_VERSION);
    }
    h5File.close();
}

void
ToffeeWriter::addHeaderMetadata(
    const std::string &header
) const {
    H5::H5File h5File(mH5FilePath, H5F_ACC_RDWR);
    {
        H5::DataSpace dataspace(H5S_SCALAR);
        H5::StrType datatype(H5::PredType::C_S1, H5T_VARIABLE);
        h5File.createDataSet(ATTR_EXPERIMENT_METADATA, datatype, dataspace)
            .write(header, datatype);
    }
    h5File.close();
}

void
ToffeeWriter::addPointCloud(
    const ScanDataPointCloud &pcl
) const {
    throw_assert(
        pcl.imsProperties.sliceIndex.back() == pcl.intensity.size(),
        "The last element of imsProperties.sliceIndex should match the size of intensity"
    );
    throw_assert(
        pcl.imsProperties.coordinate.size() == pcl.intensity.size(),
        "The size of imsProperties.coordinate should match the size of intensity"
    );
    throw_assert(
        pcl.imsType == mIMSType,
        "Tried to write a point cloud with invalid IMS Type: "
        << ToffeeWriter::imsTypeAsStr(pcl.imsType)
        << " != "
        << ToffeeWriter::imsTypeAsStr(mIMSType)
    );

    H5::H5File h5File(mH5FilePath, H5F_ACC_RDWR);
    H5::Group group = h5File.createGroup(pcl.name);

    {
        // create the double attributes
        auto datatype = H5::PredType::NATIVE_DOUBLE;
        H5::DataSpace dataspace;
        group.createAttribute(ATTR_IMS_ALPHA, datatype, dataspace)
                .write(datatype, &pcl.imsProperties.medianAlpha);
        group.createAttribute(ATTR_IMS_BETA, datatype, dataspace)
                .write(datatype, &pcl.imsProperties.medianBeta);
        group.createAttribute(ATTR_WINDOW_BOUNDS_LOWER, datatype, dataspace)
                .write(datatype, &pcl.windowLower);
        group.createAttribute(ATTR_WINDOW_CENTER, datatype, dataspace)
                .write(datatype, &pcl.windowCenter);
        group.createAttribute(ATTR_WINDOW_BOUNDS_UPPER, datatype, dataspace)
             .write(datatype, &pcl.windowUpper);
        group.createAttribute(ATTR_SCAN_CYCLE_TIME, datatype, dataspace)
             .write(datatype, &pcl.scanCycleTime);
        group.createAttribute(ATTR_WINDOW_RETENTION_TIME_OFFSET, datatype, dataspace)
             .write(datatype, &pcl.firstScanRetentionTimeOffset);

        // create the int32 attributes
        datatype = H5::PredType::NATIVE_INT32;
        group.createAttribute(ATTR_IMS_GAMMA, datatype, dataspace)
            .write(datatype, &pcl.imsProperties.gamma);
    }

    {
        // write the per-scan properties
        static constexpr size_t RANK = 1;
        hsize_t dims[RANK] = {pcl.imsProperties.sliceIndex.size()};

        H5::DSetCreatPropList plist;
        plist.setDeflate(6);
        plist.setChunk(RANK, dims);
        H5::DataSpace dataspace(RANK, dims);

        auto datatype = H5::PredType::NATIVE_UINT32;
        group.createDataSet(DATASET_RETENTION_TIME_IDX, datatype, dataspace, plist)
            .write(pcl.imsProperties.sliceIndex.data(), datatype);

        datatype = H5::PredType::NATIVE_DOUBLE;
        group.createDataSet(DATASET_IMS_ALPHA_PER_SCAN, datatype, dataspace, plist)
            .write(pcl.imsProperties.alpha.data(), datatype);
        group.createDataSet(DATASET_IMS_BETA_PER_SCAN, datatype, dataspace, plist)
            .write(pcl.imsProperties.beta.data(), datatype);
    }

    {
        // write the indices
        static constexpr size_t RANK = 1;
        hsize_t dims[RANK] = {pcl.imsProperties.coordinate.size()};
        static constexpr size_t oneMBOfUint32{1024 * 1024 / 4};
        hsize_t chunkSize[RANK] = {std::min(oneMBOfUint32, pcl.imsProperties.coordinate.size())};

        H5::DSetCreatPropList plist;
        plist.setDeflate(6);
        plist.setChunk(RANK, dims);
        H5::DataSpace dataspace(RANK, dims);

        auto datatype = H5::PredType::NATIVE_UINT32;
        group.createDataSet(DATASET_MZ_IMS_IDX, datatype, dataspace, plist)
            .write(pcl.imsProperties.coordinate.data(), datatype);
        group.createDataSet(DATASET_INTENSITY, datatype, dataspace, plist)
            .write(pcl.intensity.data(), datatype);
    }

    h5File.close();
}
}
