toffee: A library for fast access to Time of Flight SWATH-MS data
=================================================================

For a full description of ``toffee`` please see our **documentation**, or within the development environment, run the following

.. code:: bash
    :linenos:

        python setup.py build_sphinx

.. inclusion-marker-do-not-remove


|Toffee Logo|


|Conda Version| |Conda Platforms| |License| |ReadTheDocs| 

Master: |Master CircleCI Status| || Dev: |Dev CircleCI Status|


``Toffee`` is a library and file format for Time of Flight SWATH-MS data. The file format provides lossless compression that results in files of a similar size to those from the proprietary and closed vendor format. In addition, the high-performance C++ library implements spatial data structures to allow a user to extract spectrographic (slice along mass over charge axis) and chromatographic (slice along retention time) data in constant time.

Toffee was born out of a need to store and access SWATH-MS data in a state-of-the-art high-throughput proteomics facility, `ProCan <https://www.cmri.org.au/procan>`_, capable of generating thousands of files per month. Using the ``mzML`` file format in this environment would quickly outstrip the storage hardware available and we believe that such a limitation limits the potential of this technology. The challenges around ``mzML`` can be summarised into three categories:

#. **File size**: Biobank-scale proteomics facilities may run upwards of 100,000 SWATH-MS runs; operating in a manner typical to ProCan results in Sciex ``wiff`` files 1-2 GB per each that unpack to 10-20 GB when converted to ``mzML`` leading to petabytes of data that needs to be stored and archived. Furthermore, this increase in file size adds significant time to processing, making analytics software largely IO-bound. On the ProCan90 dataset, toffee files are 95-100% the size of the original vendor files.
#. **Random access**: Indexed ``mzML`` substantially improves randomly accessing single scan data (at constant retention time), yet algorithms often require slices along the mass over charge axis and this requires iterating over the full ``mzML`` file. Toffee facilitates a different access model allowing near constant time slicing in both retention time and mass over charge axes.
#. **Testability**: A key challenge to improving downstream software is the slow iterative cycle imposed by storing experimental data in ``mzML``. Building reliable and robust algorithms requires a strong testing framework of both unit and regression tests and a test harness that encourages developers to use it. The IO-bound nature of ``mzML`` files risks artificial barriers to test adoption. However, by solving points 1 and 2 above, extremely small (small enough to be committed to the repository) toffee files can be generated with exemplar data for integration into a unit and regression testing frameworks.

Toffee files are based on the open `HDF5 <https://support.hdfgroup.org/HDF5/>`_ format and can thus be read by many different programming languages. Within the toffee documentation, the ToffeeWriter class outlines the structure of the HDF5 file, and should be considered the canonical description.

In addition to the file format, toffee is also a high-performance C++ library for accessing the data in toffee files. By and large, the python classes are direct wrappings of the C++ code and API documentation can be considered largely equivalent. We use `pybind11 <http://pybind11.readthedocs.io/>`_ for wrapping, and this will automatically take care of conversions of ``numpy`` and ``scipy`` matrices to corresponding ``Eigen`` matrices, albeit by creating copies.

For Users
---------

Toffee is made available through the `conda <https://conda.io/docs/>`_ python packaging system. It can be installed using:

.. code:: python
    :linenos:

    conda install --yes -c cmriprocan toffee

It is also included in a simple ``cmriprocan/toffee`` Docker image with conda and toffee only, along with ``cmriprocan/openms-toffee`` that is a Docker image for those operating a containerised workflow.

For Developers
--------------

We are basing our development workflow around `Microsoft Visual Studio Code <https://code.visualstudio.com/>`_ and conda. The following should help you set up a development environment. In general, we aim to use conda 'env' to manage dependencies.

1. If you haven't already, install ``git`` using your favourite method and clone this repository
2. If you haven't already, install ``conda``
3. If you haven't already, install ``anaconda-client`` using ``conda install --yes anaconda-client``
4. If you haven't already (and you're on a mac), install the MacOS SDK

.. code:: bash
    :linenos:

    curl -L -o MacOSX11.3.sdk.tar.xz https://github.com/phracker/MacOSX-SDKs/releases/download/11.3/MacOSX11.3.sdk.tar.xz
    tar -xzf MacOSX11.3.sdk.tar.xz
    sudo mv MacOSX11.3.sdk /opt/MacOSX11.3.sdk

5. Log in to anaconda client as ``ProCanSoftEngRobot`` -- you may need to ask the team for credentials
6. If you haven't already, download ``VSCode`` and install
7. If you haven't already, open VSCode and install the following extensions (look for the icon of the left side that looks like a square)
  - Microsoft "python" extension
  - Microsoft "C++" extension
  - vector-of-bool "CMake Tools" extension
  - Microsoft "Visual Studio Code Tools for AI" extension (this gets you jupyter notebooks working, among other things)
8. From within VSCode, open this repository's root directory; you don't need to worry about workspaces
9. Open up a terminal in VSCode (ctrl + backtick works on MacOS)
  - Change into the ``.dev-environment`` folder and run ``bash create_dev_conda_environment.sh`` -- this will set up all of the dependecies in a conda environment called ``dev-toffee`
10. Open the `Command Palette <https://code.visualstudio.com/docs/getstarted/userinterface>`_ (cmd + shift + p on MacOS) and search for "python: select interpreter" and chose any value. This will create a ``settings.json`` fie in ``.vscode`` in the root of the repository.
11. Copy the following into ``<repository-root>/.vscode/settings.json``, being sure to replace ``<your-anaconda-root>`` with the correct path

.. code:: json
    :linenos:

    {
        "python.pythonPath": "<your-anaconda-root>/envs/dev-toffee/bin/python",
        "cmake.cmakePath": "<your-anaconda-root>/envs/dev-toffee/bin/cmake",
        "cmake.generator": "Ninja",
        "cmake.configureSettings": {
            "CMAKE_MAKE_PROGRAM": "<your-anaconda-root>/envs/dev-toffee/bin/ninja",
            "CMAKE_C_COMPILER": "<your-anaconda-root>/envs/dev-toffee/bin/clang",
            "CMAKE_CXX_COMPILER": "<your-anaconda-root>/envs/dev-toffee/bin/clangxx"
        },
        "cmake.configureOnOpen": true,
        "files.associations": {
            "array": "cpp",
            "*.tcc": "cpp",
            "cctype": "cpp",
            "clocale": "cpp",
            "cmath": "cpp",
            "complex": "cpp",
            "cstdarg": "cpp",
            "cstddef": "cpp",
            "cstdint": "cpp",
            "cstdio": "cpp",
            "cstdlib": "cpp",
            "cstring": "cpp",
            "ctime": "cpp",
            "cwchar": "cpp",
            "cwctype": "cpp",
            "deque": "cpp",
            "forward_list": "cpp",
            "list": "cpp",
            "unordered_map": "cpp",
            "unordered_set": "cpp",
            "vector": "cpp",
            "exception": "cpp",
            "optional": "cpp",
            "fstream": "cpp",
            "functional": "cpp",
            "initializer_list": "cpp",
            "iomanip": "cpp",
            "iosfwd": "cpp",
            "iostream": "cpp",
            "istream": "cpp",
            "limits": "cpp",
            "memory": "cpp",
            "new": "cpp",
            "numeric": "cpp",
            "ostream": "cpp",
            "sstream": "cpp",
            "stdexcept": "cpp",
            "streambuf": "cpp",
            "string_view": "cpp",
            "system_error": "cpp",
            "cinttypes": "cpp",
            "type_traits": "cpp",
            "tuple": "cpp",
            "typeindex": "cpp",
            "typeinfo": "cpp",
            "utility": "cpp",
            "valarray": "cpp",
            "variant": "cpp",
            "atomic": "cpp"
        },
        "python.linting.pylintEnabled": false,
        "git.autofetch": true,
        "python.linting.flake8Enabled": true,
        "python.linting.flake8Args": [
            "--max-line-length=120"
        ],
        "editor.rulers": [120]
        "python.unitTest.unittestEnabled": false,
        "python.unitTest.nosetestsEnabled": false,
        "python.unitTest.pyTestEnabled": true,
        "editor.minimap.enabled": false,
        "C_Cpp.intelliSenseEngineFallback": "Disabled",
    }

We follow the `OpenVDB style guide <http://www.openvdb.org/documentation/doxygen/codingStyle.html>`_ for the C++ and PEP-8 for our python code, so please aim to stay consistent with the rest of the code base. Contributions will be pass through peer review and style will be one element that is reviewed.

Changes
-------

.. mdinclude:: CHANGES.md

.. |Conda Version| image:: https://anaconda.org/cmriprocan/toffee/badges/version.svg
    :target: https://anaconda.org/cmriprocan/toffee
    :scale: 100%
    :alt: Conda Version

.. |Conda Platforms| image:: https://anaconda.org/cmriprocan/toffee/badges/platforms.svg
    :target: https://anaconda.org/cmriprocan/toffee
    :scale: 100%
    :alt: Conda Platforms

.. |Master CircleCI Status| image:: https://circleci.com/bb/cmriprocan/toffee/tree/master.svg?style=svg&circle-token=a5aafef4b5ce6ea26e04201a18db8e80e11c1c00
    :target: https://circleci.com/bb/cmriprocan/toffee/tree/master
    :scale: 100%
    :alt: Master Build Status

.. |Dev CircleCI Status| image:: https://circleci.com/bb/cmriprocan/toffee/tree/dev.svg?style=svg&circle-token=a5aafef4b5ce6ea26e04201a18db8e80e11c1c00
    :target: https://circleci.com/bb/cmriprocan/toffee/tree/dev
    :scale: 100%
    :alt: Dev Build Status

.. |License| image:: https://img.shields.io/badge/License-MIT-blue.svg
    :target: https://opensource.org/licenses/MIT
    :scale: 100%
    :alt: MIT license

.. |ReadTheDocs| image:: https://readthedocs.org/projects/toffee/badge/?version=latest
    :target: https://toffee.readthedocs.io/en/latest/?badge=latest
    :scale: 100%
    :alt: Documentation Status

.. |Toffee Logo| image:: ../Toffee.svg
    :target: https://toffee.readthedocs.io
    :scale: 20%
    :alt: Toffee

License
-------

`MIT`_ Copyright (c) 2017-2019 Children's Medical Research Institute (CMRI)

.. _MIT: ../LICENSE