#!/bin/bash
flake8 --max-line-length=120 pysrc
${PYTHON} -m pip install . --no-deps --ignore-installed --no-cache-dir -vv
