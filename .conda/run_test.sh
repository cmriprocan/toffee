#!/bin/bash
echo "**********************"
SRC_DIR=${WORKSPACE}
ls -lhF ${SRC_DIR}

EXE_DIR=${PREFIX}/bin
PY_EXE=${EXE_DIR}/python
echo "Python: $PY_EXE"

# install dependencies
if [[ $OSTYPE == darwin* ]]; then
    export CPATH=$WORKSPACE/MacOSX13.3.sdk/usr/include
    export LIBRARY_PATH=$WORKSPACE/MacOSX13.3.sdk/usr/lib
fi

${PY_EXE} -m pip install -r ${SRC_DIR}/.conda/requirements.txt

TOFFEE_PATH=`${PY_EXE} -c "import toffee; print(list(toffee.__path__)[0])"`
echo "Path: $TOFFEE_PATH"
ls -lhF $TOFFEE_PATH

VERSION=`${PY_EXE} -c "import toffee; print(toffee.__version__)"`
echo "Version: $VERSION"

echo "**********************"
cd $TOFFEE_PATH
${PY_EXE} -m pytest \
    --junitxml=/tmp/pytest-junit.xml \
    --rootdir=${TOFFEE_PATH} \
    --import-mode=append \
    ${SRC_DIR}/pysrc/tests  # requires `conda build --no-remove-work-dir`