# we need to pin these explicitly otherwise the install will fail
{% set python_ver="3.10.*" %}
# Numpy can't be too recent because certain methods have been deprecated
{% set numpy_ver="1.21.*" %}
# Once we have dev releases in anaconda, I'll add in the solved for values and use them
{% set boost_ver="1.73.*" %}
{% set eigen_ver="3.3.*" %}
{% set hdf5_ver="1.12.*" %}
{% set tbb_ver="2021.8.*" %}

package:
  name: toffee
  version: {{ environ.get('NEW_GIT_TAG', 'dev') }}
  ignore_prefix_files: True # [win]

source:
  # path: ../
  git_url: ../

build:
  number: {{ environ.get('GIT_DESCRIBE_NUMBER', 0) }}
  entry_points:
    # ---
    # these are a duplicate of what is in setup.py
    # ---
    - mzml_to_toffee = toffee.conversion:mzml_to_toffee
    - toffee_to_mzml = toffee.conversion:toffee_to_mzml
    - raw_sciex_data_to_toffee = toffee.conversion:raw_sciex_data_to_toffee
    - requantify_pyprophet_tsv = toffee.requant:requantify_pyprophet_tsv
    - requantify_pyprophet_sqlite = toffee.requant:requantify_pyprophet_sqlite
  run_exports:
    # ---
    # add dependencies to downstream consumers to ensure compatibility
    # ---
    - {{ pin_compatible('boost', max_pin='x.x.x') }}
    - {{ pin_compatible('libboost', max_pin='x.x.x') }}
    - {{ pin_compatible('hdf5', max_pin='x.x.x') }}
    - {{ pin_compatible('eigen', max_pin='x.x.x') }}
    - {{ pin_compatible('numpy', max_pin='x.x.x') }}
    - {{ pin_compatible('tbb', max_pin='x.x.x') }}


requirements:
  build:
    # ---
    # tools required to actually run the build
    # ---
    - python {{ python_ver }}
    - {{ compiler('cxx') }}
    - pip
    - setuptools
    - cmake
    - make
    - ninja
    - cmake
    - pkg-config  # [not win]
    # code quality checking
    - flake8
    - pep8-naming
    - flake8-import-order
    - flake8-print
    - flake8-quotes
    # setup complains about needing these during build, not sure why
    - pytest
    - pytest-runner
    # pyteomics
    - cython
    - sqlalchemy
    - libopenblas
    - lxml

  host:
    # ---
    # External dependencies that the package needs during compilation
    # ---
    - python
    - boost {{ boost_ver }}
    - libboost {{ boost_ver }}
    - hdf5 {{ hdf5_ver }}
    - eigen {{ eigen_ver }}
    - numpy {{ numpy_ver }}
    - tbb {{ tbb_ver }}
    - tbb-devel {{ tbb_ver }}
    # pyteomics
    - cython
    - sqlalchemy
    - libopenblas
    - lxml

  run:
    # ---
    # External dependencies required during runtime, if these are linked, they should
    # also go in the run_exports above
    # ---
    - python {{ python_ver }}
    - {{ pin_compatible('boost', max_pin='x.x.x') }}
    - {{ pin_compatible('libboost', max_pin='x.x.x') }}
    - {{ pin_compatible('hdf5', max_pin='x.x.x') }}
    - {{ pin_compatible('eigen', max_pin='x.x.x') }}
    - {{ pin_compatible('numpy', max_pin='x.x.x') }}
    - {{ pin_compatible('tbb', max_pin='x.x.x') }}
    - scipy
    - pandas
    - xlrd  # so pandas can open excel docs
    - h5py
    - tqdm
    # viz
    - matplotlib
    - {{ pin_compatible('plotly', min_pin='x.x.x') }}
    - plotly-orca
    - pyqt
    - pillow
    # pyteomics
    - sqlalchemy
    - libopenblas
    - lxml

test:
  requires:
    # ---
    # External dependencies required to run the tests
    # ---
    - python {{ python_ver }}
    # ---
    - pytest
    - pytest-runner
  source_files:
    - pysrc/toffee
  imports:
    - toffee

about:
  summary: Toffee is a library and file format for Time of Flight SWATH-MS data.
  license_family: MIT
